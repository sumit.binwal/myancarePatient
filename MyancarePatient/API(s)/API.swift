//
//  API.swift
//  OOTTBusinessApp
//
//  Created by Sumit on 03/01/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import Foundation

//MARK:- DEVELOPMENT BASE URL
//let baseURLString = "http://202.157.76.19:15004/"
//let AWSBucket = "myancare-dev"

//MARK:- STAGING BASE URL
//let baseURLString = "https://constantlab.com:15001/"

//MARK:- CLIENT BASE URL
let baseURLString = "https://api.myancare.org:15001/"
let baseURLStringArticleShare = "https://myancare.org/"
let AWSBucket = "myancare-prod"

//MARK:- Protocol Endpoint path
protocol ApiEndpoint {
    var path :URL {get}
}

//MARK:- Enum Endpoint Final Path
enum EndPoints : ApiEndpoint {
    
    case articles
    case login
    case register
    case socialLogin
    case socialRegister
    case logout(String)
    case sendOTP
    case checkUser
    case changePassword(String)
    case addEmployee(String)
    case listEmployee(String)
    case getUserRole
    case deleteEmployee(String,String)
    case addBusinessImage(String)
    case deleteBusinessImage(String,String)
    case createProfile(String)
    case addMenuImage(String)
    case deleteMenuImage(String,String)
    case editEmployee(String,String)
    case getScene(String)
    case getCurrentStatus(String)
    case updateStatus(String)
    case viewBusiness(String,String,String,String)
    case eventListing(String)
    case deleteEvent(String,String)
    case createEvent(String)
    case editEvent(String,String)
    case myBusinessGroupDetail(String)
    case getUpcomingEvents(String)
    case getGroupsHome(String)
    case makeMoveToGroup(String,String)
    case favouriteGroup(String,String)
    case unFavouriteGroup(String,String)
    case getGroupFriendsList(String,String)
    case getGroupLikesList(String,String)
    
    case getFriendsList(String,String)
    case getSuggestedFriendsList(String,String)
    case getUserProfileApi(String,String)
    case followUser(String,String)
    case unfollowUser(String,String)
    
    case myActivityList(String)
    case followingsActivityList(String)
    case deleteAccount(String)
    case getUserCheckinComments(String,String)
    case addCommentOnCheckIn(String,String)
    case likeCheckin(String,String)
    case UnlikeCheckin(String,String)
    case editProfile(String)
    
    case getNearest(String,String)
    case districtTown
    case checkUserExist
    case resendOTP
    case changeMobileNumber
    case verifyOTP
    case forgotPassword
    case resetPassword
    case verifyForgotOtp
    case categoryList
    case articleListByCat(String)
    
    case articleLike
    case articleView
    case get_comment(String)
    case comment
    case search(String, String, Int)
    
    case articleBookMark
    case doctor(String, String, String, String, String, String, String, String, String, String, Int)
    
    case operationHours(String)
    
    case myLikes
    case myBookmark
    
    case bookAppointment
    
    case addToWallet
    
    case doctorFavourite
    
    case appointmentList(String, String, Int)
    case appointmentListProfileDoctor(String, String, String, Int)
    
    case cancelAppointment
    case rescheduleAppointment
    
    case userReview
    
    case doctorReviewGet(String, Int)
    
    case profile
    
    case medicalRecords(Int)
    case medicalRecordsAll(Int)
    
    case createMedicalRecords
    case medicalRecordShareWith
    
    case notification(Int)
    
    case update_profile
    case change_password
    
    case getRecordFiles(String)
    case uploadRecordFile
    
    case myTransactions(Int)
    
    case getMedicationReminder(Int)
    case getMedicationReminderToday(Int)
    
    case medicationReminder
    
    case getDoctorWorkingAddress(String, Int)
    
    case logoutUser
    case updateDeviceToken
    
    case termsandconditions
    case whysignup
    case patientrights
    case complaintpolicy
    case cancellationpolicy
    case privacypolicy
    case faq
    case contactUs
    
    case chat_service(String)
    case chat_confirm
    
    case get_status(String)
    
    case patient_confirmation
    
    case get_profile(String)//, String)
    
    case paymentUrl(String)
    
    case myWallet
    
    case specialization
    
    var path: URL
    {
        switch self {
            
        case .specialization:
            return URL(string: String(baseURLString+"specialization?status=1"))!
            
        case .paymentUrl(let user_id):
            return URL(string: String(baseURLString+"payment/recharge?p="+user_id+"&from=ios&return_url=https://www.google.com"))!
            
        case .patient_confirmation:
            return URL(string: String(baseURLString+"appointments/patient_confirmation"))!
            
        case .get_profile(let user_id)://, let username):
            return URL(string: String(baseURLString+"user/get_profile?user_id="+user_id))!//+"&username="+username))!
            
        case .chat_service(let doctor_id):
            return URL(string: String(baseURLString+"appointments/chat_service?doctor="+"\(doctor_id)"))!
            
        case .get_status(let user_id):
            return URL(string: String(baseURLString+"user/get_status?user_id="+"\(user_id)"))!
            
        case .chat_confirm:
            return URL(string: String(baseURLString+"appointments/chat_confirm"))!
            
        case .termsandconditions:
            return URL(string: String(baseURLString+"pages/terms-and-conditions"))!
        
        case .whysignup:
            return URL(string: String(baseURLString+"pages/why-sign-up"))!
            
        case .patientrights:
            return URL(string: String(baseURLString+"pages/patient-rights"))!
            
        case .complaintpolicy:
            return URL(string: String(baseURLString+"pages/complaint-policy"))!
            
        case .cancellationpolicy:
            return URL(string: String(baseURLString+"pages/cancellation-policy"))!
            
        case .contactUs:
            return URL(string: String(baseURLString+"pages/contact-us"))!
            
        case .privacypolicy:
            return URL(string: String(baseURLString+"pages/privacy-policy"))!
            
        case .faq:
            return URL(string: String(baseURLString+"pages/faq"))!
            
        case .updateDeviceToken:
            return URL(string: String(baseURLString+"user/updateFcmToken"))!
            
        case .logoutUser:
            return URL(string: String(baseURLString+"auth/logout"))!
            
        case .getDoctorWorkingAddress(let doctor_id, let skip):
            return URL(string: String(baseURLString+"doctorWorkingAddress?doctor_id="+doctor_id+"&limit=10&skip="+String(skip)))!
            
        case .getMedicationReminder(let skip):
            return URL(string: String(baseURLString+"medicationReminder?limit=10&skip="+String(skip)))!
            
        case .getMedicationReminderToday(let skip):
            return URL(string: String(baseURLString+"medicationReminder?getT=1&limit=10&skip="+String(skip)))!
            
        case .medicationReminder:
            return URL(string: String(baseURLString+"medicationReminder"))!
            
        case .medicalRecordShareWith:
            return URL(string: String(baseURLString+"medicalRecords/shareWith"))!
            
        case .change_password:
            return URL(string: String(baseURLString+"user/change_password"))!
            
        case .update_profile:
            return URL(string: String(baseURLString+"user/update_profile"))!
            
        case .getRecordFiles(let medical_record_id):
            return URL(string: String(baseURLString+"recordFiles?medical_record="+medical_record_id+"&limit=1000000&skip=0"))!

        case .myTransactions(let skip):
            return URL(string: String(baseURLString+"user/myTransactions?limit=10&skip="+String(skip)))!
            
        case .myWallet :
            return URL(string: String(baseURLString+"myWallet"))!

        case .uploadRecordFile:
            return URL(string: String(baseURLString+"recordFiles"))!
            
        case .medicalRecords(let skip):
            return URL(string: String(baseURLString+"medicalRecords?limit=10&skip="+String(skip)))!
            
        case .medicalRecordsAll(let skip):
            return URL(string: String(baseURLString+"medicalRecords?getAll=1&limit=10&skip="+String(skip)))!
            
        case .notification(let skip):
            return URL(string: String(baseURLString+"notification?limit=10&skip="+String(skip)))!
            
        case .createMedicalRecords:
            return URL(string: String(baseURLString+"medicalRecords"))!
            
        case .profile:
            return URL(string: String(baseURLString+"user/profile"))!
            
        case .userReview:
            return URL(string: String(baseURLString+"userReview"))!
            
        case .get_comment(let article_id):
            return URL(string: String(baseURLString+"article/get_comment?article_id="+article_id))!
            
        case .search(let searchString, let categoriesID, let skip):
            return URL(string: String(baseURLString+"article/search?categories="+categoriesID+"&q="+searchString+"&limit=10&skip="+String(skip)))!
            
        case .doctor(let longitude, let latitude, let maxDistance, let sr_pcode, let d_pcode, let town_pcode, let name, let specializations, let getFavorite, let getRecent, let skip):
            return URL(string: String(baseURLString+"doctor?longitude="+longitude+"&latitude="+latitude+"&maxDistance="+maxDistance+"&sr_pcode="+sr_pcode+"&d_pcode="+d_pcode+"&town_pcode="+town_pcode+"&name="+name+"&specializations="+specializations+"&getFavorite="+getFavorite+"&getRecent="+getRecent+"&limit=10&skip="+String(skip)))!
            
        case .comment:
            return URL(string: String(baseURLString+"article/comment"))!
            
        case .categoryList:
            return URL(string: String(baseURLString+"category?status=1"))!
            
        case .articleListByCat(let catID):
            return URL(string: String(baseURLString+"category/"+catID))!
            
        case .articleBookMark:
            return URL(string: String(baseURLString+"article/bookmark"))!
            
        case .articles:
            return URL(string: String(baseURLString+"article"))!
            
        case .login:
            return URL(string: String(baseURLString+"auth/login"))!
            
        case .register:
            return URL(string: String(baseURLString+"patient/register"))!
            
        case .socialLogin:
            return URL(string: String(baseURLString+"socialLogin"))!
            
        case .socialRegister:
            return URL(string: String(baseURLString+"socialRegister"))!
            
        case .logout(let sid):
            return URL(string: String(baseURLString+"logout/"+sid))!
            
        case .sendOTP:
            return URL(string: String(baseURLString+"sendOTP"))!
            
        case .checkUser:
            return URL(string: String(baseURLString+"checkUser"))!
            
        case .changePassword(let sid):
            return URL(string: String(baseURLString+"changePassword/"+sid))!
            
        case .addEmployee(let sid):
            return URL(string: String(baseURLString+"addEmployee/"+sid))!
            
        case .getUserRole:
            return URL(string: String(baseURLString+"userRoles"))!
            
        case .listEmployee(let sid):
            return URL(string: String(baseURLString+"listEmployee/"+sid))!
            
        case .deleteEmployee(let sid, let userID):
            return URL(string: String(baseURLString+"deleteEmployee/"+sid+"/"+userID))!
            
        case .addBusinessImage(let sid):
            return URL(string: String(baseURLString+"addBusinessImage/"+sid))!
            
        case .deleteBusinessImage(let sid, let picID):
            return URL(string: String(baseURLString+"removeBusinessImage/"+sid+"/"+picID))!
            
        case .createProfile(let sid):
            return URL(string: String(baseURLString+"createProfile/"+sid))!
            
        case .addMenuImage(let sid):
            return URL(string: String(baseURLString+"addMenuImage/"+sid))!
            
        case .deleteMenuImage(let sid, let picID):
            return URL(string: String(baseURLString+"removeMenuImage/"+sid+"/"+picID))!
            
        case .editEmployee(let sid, let employeeID):
            return URL(string: String(baseURLString+"editEmployee/"+sid+"/"+employeeID))!
            
        case .getScene(let sceneTag):
            return URL(string: String(baseURLString+"getScene/"+sceneTag))!

        case .getCurrentStatus(let sid):
            return URL(string: String(baseURLString+"currentStatus/"+sid))!
            
        case .updateStatus(let sid):
            return URL(string: String(baseURLString+"updateStatus/"+sid))!
            
        case .viewBusiness(let sid, let businessID, let latitude, let longitude):
            return URL(string: String(baseURLString+"viewBusiness/"+sid+"/"+businessID+"?latitude="+latitude+"&longitude="+longitude))!
            
        case .eventListing(let sid):
            return URL(string: String(baseURLString+"listEvent/"+sid))!
            
        case .myBusinessGroupDetail(let sid):
            return URL(string: String(baseURLString+"myBusinessGroup/"+sid))!
            
        case .deleteEvent(let sid, let userID):
            return URL(string: String(baseURLString+"deleteEvent/"+sid+"/"+userID))!
            
        case .createEvent(let sid):
            return URL(string: String(baseURLString+"addEvent/"+sid))!
            
        case .editEvent(let sid, let userID):
            return URL(string: String(baseURLString+"editEvent/"+sid+"/"+userID))!
            
        case .getUpcomingEvents(let sid):
            return URL(string: String(baseURLString+"upcommingEvent/"+sid))!
            
        case .getGroupsHome(let sid):
            return URL(string: String(baseURLString+"getBusiness/"+sid))!
            
        case .makeMoveToGroup(let sid, let businessID):
            return URL(string: String(baseURLString+"move/"+sid+"/"+businessID))!
            
        case .favouriteGroup(let sid, let businessID):
            return URL(string: String(baseURLString+"like/"+sid+"/"+businessID))!
            
        case .unFavouriteGroup(let sid, let businessID):
            return URL(string: String(baseURLString+"unlike/"+sid+"/"+businessID))!
            
        case .getGroupFriendsList(let sid, let businessID):
            return URL(string: String(baseURLString+"friendsOnBusiness/"+sid+"/"+businessID))!
            
        case .getGroupLikesList(let sid, let businessID):
            return URL(string: String(baseURLString+"businessLikes/"+sid+"/"+businessID))!
            
        case .getFriendsList(let sid, let page):
            return URL(string: String(baseURLString+"followingsList/"+sid+"?page="+page))!
            
        case .getSuggestedFriendsList(let sid, let page):
            return URL(string: String(baseURLString+"suggestiveFriends/"+sid+"?page="+page))!
            
        case .getUserProfileApi(let sid, let userID):
            return URL(string: String(baseURLString+"viewUser/"+sid+"/"+userID))!
            
        case .followUser(let sid, let userID):
            return URL(string: String(baseURLString+"follow/"+sid+"/"+userID))!
            
        case .unfollowUser(let sid, let userID):
            return URL(string: String(baseURLString+"unfollow/"+sid+"/"+userID))!
            
        case .myActivityList(let sid):
            return URL(string: String(baseURLString+"myActivityList/"+sid))!
            
        case .followingsActivityList(let sid):
            return URL(string: String(baseURLString+"followingsActivityList/"+sid))!
            
        case .deleteAccount(let sid):
            return URL(string: String(baseURLString+"deleteAccout/"+sid))!
            
        case .getUserCheckinComments(let sid, let checkInID):
            return URL(string: String(baseURLString+"checkinCommentList/"+sid+"/"+checkInID))!
            
        case .addCommentOnCheckIn(let sid, let checkInID):
            return URL(string: String(baseURLString+"commentOnCheckin/"+sid+"/"+checkInID))!
            
        case .likeCheckin(let sid, let checkInID):
            return URL(string: String(baseURLString+"checkinLike/"+sid+"/"+checkInID))!
            
        case .UnlikeCheckin(let sid, let checkInID):
            return URL(string: String(baseURLString+"checkinUnlike/"+sid+"/"+checkInID))!
            
        case .editProfile(let sid):
            return URL(string: String(baseURLString+"editProfile/"+sid))!
            
        case .getNearest(let lattitude, let longitude):
            return URL(string: String(baseURLString+"districttown/getNearest?location="+lattitude+"&location="+longitude+""))!
            
        case .districtTown:
            return URL(string: String(baseURLString+"districttown?limit=600"))!
            
        case .checkUserExist:
            return URL(string: String(baseURLString+"auth/checkUserExist"))!
            
        case .resendOTP:
            return URL(string: String(baseURLString+"auth/resendOtp"))!
            
        case .changeMobileNumber:
            return URL(string: String(baseURLString+"auth/changeMobileNumber"))!
            
        case .verifyOTP:
            return URL(string: String(baseURLString+"auth/verifyOtp"))!
            
        case .forgotPassword:
            return URL(string: String(baseURLString+"auth/forgotPassword"))!
            
        case .resetPassword:
            return URL(string: String(baseURLString+"auth/resetPassword"))!
            
        case .verifyForgotOtp:
            return URL(string: String(baseURLString+"auth/verifyForgotOtp"))!
            
        case .articleLike:
            return URL(string: String(baseURLString+"article/like"))!
            
        case .articleView:
            return URL(string: String(baseURLString+"article/view"))!
            
        case .myLikes:
            return URL(string: String(baseURLString+"article/myLikes"))!
            
        case .myBookmark:
            return URL(string: String(baseURLString+"article/myBookmark"))!
            
        case .operationHours(let doctorID):
            return URL(string: String(baseURLString+"operatinghours/\(doctorID)?getNew=1"))!
            
        case .bookAppointment:
            return URL(string: String(baseURLString+"appointments/new"))!
            
        case .addToWallet:
            return URL(string: String(baseURLString+"add-to-Wallet"))!
            
        case .doctorFavourite:
            return URL(string: String(baseURLString+"user/favorite"))!
            
        case .appointmentList(let historyStr, let upcomingStr, let skip):
            return URL(string: String(baseURLString+"appointments?getHistory="+historyStr+"&getUpcoming="+upcomingStr+"&limit=10&skip="+String(skip)))!
            
        case .appointmentListProfileDoctor(let historyStr, let upcomingStr, let doctor_id, let skip):
            return URL(string: String(baseURLString+"appointments?getHistory="+historyStr+"&getUpcoming="+upcomingStr+"&doctor_id="+doctor_id+"&limit=1000000&skip="+String(skip)))!
            
        case .doctorReviewGet(let user_id, let skip) :
            return URL(string: String(baseURLString+"userReview?user_id="+user_id+"&limit=10&skip="+String(skip)))!
            
        case .cancelAppointment:
            return URL(string: String(baseURLString+"appointments/cancel"))!
            
        case .rescheduleAppointment:
            return URL(string: String(baseURLString+"appointments/reschedule"))!
        }
    }
}

//MARK:- Enum Endpoint Final Path
enum LinksEnum : String
{
    //// staging
    //case privacyPolicy = "http://202.157.76.19:15060/privacy-policy"
    //case termsAndCond = "http://202.157.76.19:15060/terms-and-conditions"
    //case whySignUp = "http://202.157.76.19:15060/why-sign-up"
    //
    //case visitOurWebsite = "http://202.157.76.19:15060/"
    //case contactUs = "http://202.157.76.19:15060/contact-us"
    //case patientRights = "http://202.157.76.19:15060/patient-rights"
    //case doctorRights = "http://202.157.76.19:15060/doctor-rights"
    //case complaintPolicy = "http://202.157.76.19:15060/complaint-policy"
    //case cancellationPolicy = "http://202.157.76.19:15060/cancellation-policy"
    //case FAQs = "http://202.157.76.19:15060/faq"
    
    //// live
    case privacyPolicy = "https://api.myancare.org:15001/pages/privacy-policy"
    case termsAndCond = "https://api.myancare.org:15001/pages/terms-and-conditions"
    case whySignUp = "https://api.myancare.org:15001/pages/why-sign-up"
    
    case visitOurWebsite = "https://myancare.org/"
    case contactUs = "https://api.myancare.org:15001/pages/contact-us"
    case patientRights = "https://api.myancare.org:15001/pages/patient-rights"
    case doctorRights = "https://api.myancare.org:15001/pages/doctor-rights"
    case complaintPolicy = "https://api.myancare.org:15001/pages/complaint-policy"
    case cancellationPolicy = "https://api.myancare.org:15001/pages/cancellation-policy"
    case FAQs = "https://api.myancare.org:15001/faq"
    
    case cromeUrl = "https://www.google.com/"
    case facebookUrl = "https://www.facebook.com/Myancareapps/"
    case twitterUrl = "https://twitter.com/CareMyan"
    case youtubeUrl = " https://www.youtube.com/channel/UCtMdKsGqVZQ2iFuE-eJBhtQ"
    case instagramUrl = "https://www.instagram.com/myancare8/"
}
