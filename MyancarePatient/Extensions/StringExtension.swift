//
//  StringExtension.swift
//  MyancarePatient
//
//  Created by Jyoti on 04/01/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import Foundation
import UIKit

extension String
{
    //func localized() -> String {
    //
    //   return NSLocalizedString(self, comment: "")
    //}
    
    func isEmptyString() -> Bool
    {
        let newString = self.trimmingCharacters(in: .whitespacesAndNewlines)
        if newString.isEmpty
        {
            return true
        }
        return false
    }
    
    func getTrimmedText() -> String
    {
        let newString = self.trimmingCharacters(in: .whitespacesAndNewlines)
        return newString
    }
    
    func isValidEmailAddress() -> Bool {
        
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = self as NSString
            let results = regex.matches(in: self, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0
            {
                returnValue = false
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        
        return  returnValue
    }
    
    func base64Encoded() -> String {
        
        var encodedString = ""
        let trimmedText = getTrimmedText()
        
        if let data = trimmedText.data(using: .utf8) {
            encodedString = data.base64EncodedString()
        }
        
        return encodedString
    }
    
    func base64Decoded() -> String {
        
        var decodedString = ""
        if let data = Data (base64Encoded: self) {
            decodedString = String (data: data, encoding: .utf8) ?? ""
        }
        
        return decodedString
    }
}
