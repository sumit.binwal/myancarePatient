//
//  UIFontExtension.swift
//  MyancarePatient
//
//  Created by Jyoti on 04/01/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import Foundation
import UIKit
import Localize_Swift

extension UIFont
{
    //["Poppins-Bold", "Poppins-Light", "Poppins-Medium", "Poppins-SemiBold", "Poppins-Regular",Zawgyi-One]
    
    static func createPoppinsMediumFont(withSize size: CGFloat) -> UIFont {
        var font:UIFont
        if Localize.currentLanguage() == "en"
        {
            font = UIFont (name: "Poppins-Medium", size: size * scaleFactorX)!
        }
        else
        {
            font = UIFont (name: "Zawgyi-One", size: size * scaleFactorX)!
        }
        
        return font
    }
    static func createPoppinsBoldFont(withSize size: CGFloat) -> UIFont {
        var font:UIFont
        if Localize.currentLanguage() == "en"
        {
        font = UIFont (name: "Poppins-Bold", size: size * scaleFactorX)!
        }
        else
        {
            font = UIFont (name: "Zawgyi-One", size: size * scaleFactorX)!
        }
        
        return font
    }
    static func createPoppinsLightFont(withSize size: CGFloat) -> UIFont {
        var font:UIFont
        if Localize.currentLanguage() == "en"
        {
            font = UIFont (name: "Poppins-Light", size: size * scaleFactorX)!
        }
        else
        {
            font = UIFont (name: "Zawgyi-One", size: size * scaleFactorX)!
        }
    
        return font
    }
    static func createPoppinsSemiBoldFont(withSize size: CGFloat) -> UIFont {
        
        var font:UIFont
        if Localize.currentLanguage() == "en"
        {
            font = UIFont (name: "Poppins-SemiBold", size: size * scaleFactorX)!
        }
        else
        {
            font = UIFont (name: "Zawgyi-One", size: size * scaleFactorX)!
        }

        return font
    }
    static func createPoppinsRegularFont(withSize size: CGFloat) -> UIFont {
        
        var font:UIFont
        if Localize.currentLanguage() == "en"
        {
            font = UIFont (name: "Poppins-Regular", size: size * scaleFactorX)!
        }
        else
        {
            font = UIFont (name: "Zawgyi-One", size: size * scaleFactorX)!
        }
        return font
    }
    
}
