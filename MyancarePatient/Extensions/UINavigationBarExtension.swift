//
//  UINavigationBarExtension.swift
//  MyancarePatient
//
//  Created by Jyoti on 12/01/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import Foundation
import UIKit
extension UINavigationBar {
    
    func customiseNavBar()  {
        self.barTintColor = UIColor.MyanCarePatient.appDefaultGreenColor
        self.tintColor = UIColor.white
        //let backButton = UIButton()
       // backButton.frame = CGRect(x:0, y:0, width:50, height:50)
        self.backIndicatorImage = UIImage.init(named: "backbutton")
        self.backItem?.title = ""
        
    }
    
}
