//
//  UITableViewCellExtension.swift
//  MyancarePatient
//
//  Created by Jyoti on 05/01/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import Foundation
import UIKit

extension UITableViewCell
{
    func disableSelection()
    {
        self.selectionStyle = .none
    }
}
