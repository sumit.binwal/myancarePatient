//
//  AboutUsViewController.swift
//  MyancarePatient
//
//  Created by Jyoti on 11/01/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit
///AboutUSView Controller Class
class AboutUsViewController: UIViewController
{
    ///UIView Refrence Variable - About Table Footer View
    @IBOutlet var aboutTableFooterView: UIView!
    // MARK: - Variables
    ///Array LIst Items Variable
    var arrayListItems = NSArray()
    
    //MARK: - Properties
    ///UITableView Refrence Variable - TableView AboutUs List
    @IBOutlet weak var tableViewAboutUsList: UITableView!

    //MARK: - viewDidLoad
    ///UIViewController Life Cycle Method
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setUpTableView()
    }
    
    //MARK: - viewWillAppear
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        // Do any additional setup after appearing the view.
        
        self.tabBarController?.tabBar.isHidden = true
        
        if let path = Bundle.main.path(forResource: "AboutUsList", ofType: "plist")
        {
            arrayListItems = NSArray(contentsOfFile: path)!
        
            if arrayListItems.count > 0
            {
                tableViewAboutUsList.reloadData()
                tableViewAboutUsList.tableFooterView = aboutTableFooterView
            }
        }
        
        setUpNavigationBar()
    }
    
    //MARK: - viewWillDisappear
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        
        self.tabBarController?.tabBar.isHidden = false
    }

    //MARK: - Deinit
    deinit
    {
        print("AboutUsViewController deinit")
    }
    
    // MARK: - setUpNavigationBar
    ///setUpNavigationBar
    func setUpNavigationBar()
    {
        self.title = "About Us".localized()
        
        self.navigationController?.isNavigationBarHidden = false
        
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSAttributedStringKey.font: UIFont.createPoppinsBoldFont(withSize: 18.0)
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        // set navigation bar color
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 30/255.0, green: 197/255.0, blue: 155/255.0, alpha: 1.0)
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "back_white"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
    }
    
    // MARK: - Navigation Back Button Action
    ///Navigation Back Button Action
    @objc func backButtonPressed()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - setUpTableView
    ///setUpTableView
    func setUpTableView()
    {
        aboutTableFooterView.frame = CGRect(x: aboutTableFooterView.frame.origin.x, y: aboutTableFooterView.frame.origin.y, width: aboutTableFooterView.frame.size.width, height: aboutTableFooterView.frame.size.height*scaleFactorX)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK: - on Google Button Action
    ///on Google Button Action
    @IBAction func onGoogleButtonAction(_ sender: Any)
    {
        UtilityClass.openSafariBrowser(usingLink: .visitOurWebsite)
    }
    
    //MARK: - on Twitter Button Action
    ///on Twitter Button Action
    @IBAction func onTwitterButtonAction(_ sender: Any)
    {
        UtilityClass.openSafariBrowser(usingLink: .twitterUrl)
    }
    
    //MARK: - on Facebook Button Action
    ///on Facebook Button Action
    @IBAction func onFacebookButtonAction(_ sender: Any)
    {
        UtilityClass.openSafariBrowser(usingLink: .facebookUrl)
    }
}

//Mark:- Extension -> UITableView -> UITableViewDelegate, UITableViewDataSource
///Extension -> UITableView -> UITableViewDelegate, UITableViewDataSource
extension AboutUsViewController : UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if arrayListItems.count > 0
        {
            return arrayListItems.count
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        //CellForListItems
        let cellObj : AboutUsTableCustomCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier_AboutUsCell) as! AboutUsTableCustomCell
        
        //CellForSetting
        let cellObjSecond : AboutUsTableCustomCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier_AboutUsSecondCell) as! AboutUsTableCustomCell
        
        let dict = arrayListItems.object(at: indexPath.row) as! NSDictionary
        if indexPath.row == 0 || indexPath.row == 2
        {
            if indexPath.row == 0
            {
                cellObjSecond.labelAboutUsOrVist.text = "About Us".localized()
            }
            else
            {
                cellObjSecond.labelAboutUsOrVist.text = "Legal".localized()
            }
            
            cellObjSecond.buttonVistOrAboutIcon.setImage(UIImage.init(named: dict["Image"] as! String), for: UIControlState.normal)
            cellObjSecond.buttonVisitOrAboutUsListName.setTitle((dict["Name"] as? String)?.localized(), for: UIControlState.normal)
            
            return cellObjSecond
        }
        else
        {
            cellObj.buttonListIcons.setImage(UIImage.init(named: dict["Image"] as! String), for: UIControlState.normal)
            cellObj.buttonListNames.setTitle((dict["Name"] as? String)?.localized(), for: UIControlState.normal)
            
            return cellObj
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.row == 0 || indexPath.row == 2
        {
            return 96 * scaleFactorX
        }
        else
        {
            return 53 * scaleFactorX
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if indexPath.row == 0
        {
            // visit our website
            UtilityClass.openSafariBrowser(usingLink: .visitOurWebsite)
        }
        else if indexPath.row == 1
        {
            // contact us
            
            let staticPagesVC = UIStoryboard.getAboutUsStoryBoard().instantiateViewController(withIdentifier: "StaticPagesVC") as! StaticPagesViewController
            staticPagesVC.selectedIndex = 1
            self.navigationController?.pushViewController(staticPagesVC, animated: true)
        }
        else if indexPath.row == 2
        {
            // privacy policy
            
            let staticPagesVC = UIStoryboard.getAboutUsStoryBoard().instantiateViewController(withIdentifier: "StaticPagesVC") as! StaticPagesViewController
            staticPagesVC.selectedIndex = 2
            self.navigationController?.pushViewController(staticPagesVC, animated: true)
        }
        else if indexPath.row == 3
        {
            // terms of use
            
            let staticPagesVC = UIStoryboard.getAboutUsStoryBoard().instantiateViewController(withIdentifier: "StaticPagesVC") as! StaticPagesViewController
            staticPagesVC.selectedIndex = 3
            self.navigationController?.pushViewController(staticPagesVC, animated: true)
        }
        else if indexPath.row == 4
        {
            // patient rights
            
            let staticPagesVC = UIStoryboard.getAboutUsStoryBoard().instantiateViewController(withIdentifier: "StaticPagesVC") as! StaticPagesViewController
            staticPagesVC.selectedIndex = 4
            self.navigationController?.pushViewController(staticPagesVC, animated: true)
        }
        else if indexPath.row == 5
        {
            // complaint policy
            
            let staticPagesVC = UIStoryboard.getAboutUsStoryBoard().instantiateViewController(withIdentifier: "StaticPagesVC") as! StaticPagesViewController
            staticPagesVC.selectedIndex = 5
            self.navigationController?.pushViewController(staticPagesVC, animated: true)
        }
        else if indexPath.row == 6
        {
            // cancellation policy
           
            let staticPagesVC = UIStoryboard.getAboutUsStoryBoard().instantiateViewController(withIdentifier: "StaticPagesVC") as! StaticPagesViewController
            staticPagesVC.selectedIndex = 6
            self.navigationController?.pushViewController(staticPagesVC, animated: true)
        }
        else
        {
            // faqs
            
            let staticPagesVC = UIStoryboard.getAboutUsStoryBoard().instantiateViewController(withIdentifier: "StaticPagesVC") as! StaticPagesViewController
            staticPagesVC.selectedIndex = 7
            self.navigationController?.pushViewController(staticPagesVC, animated: true)
        }
    }
}
