//
//  StaticPagesViewController.swift
//  MyancarePatient
//
//  Created by Ratina on 3/27/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit
import PKHUD
///StaticPages ViewController Class
class StaticPagesViewController: UIViewController
{
    ///Int Variable Refrence - Selected Index
    var selectedIndex = 0

    ///UIWebView Refrence Variable - WebView
    @IBOutlet weak var webView: UIWebView!
    
    ///UIVIewController Life Cycle Method
    //MARK: - UIVIewController Life Cycle Method
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.webView.delegate = self
    }

    //MARK: - viewWillAppear
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        // Do any additional setup after appearing the view.
        
        self.tabBarController?.tabBar.isHidden = true
        
        setUpNavigationBar()
        
        getStaticPagesDataApi()
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.isNavigationBarHidden = true
    }
    
    //MARK: - Deinit
    deinit
    {
        print("StaticPagesViewController deinit")
    }
    
    // MARK: - setUpNavigationBar
    ///setUpNavigationBar
    func setUpNavigationBar()
    {
        self.navigationController?.isNavigationBarHidden = false
        
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSAttributedStringKey.font: UIFont.createPoppinsBoldFont(withSize: 18.0)
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        // set navigation bar color
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 30/255.0, green: 197/255.0, blue: 155/255.0, alpha: 1.0)
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "back_white"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
    }
    
    // MARK: - Navigation Back Button Action
    ///Navigation Back Button Action
    @objc func backButtonPressed()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - get static pages data from API
    ///get static pages data from API
    @objc func getStaticPagesDataApi()
    {
        var urlToHit = EndPoints.whysignup.path
        
        if selectedIndex == 2
        {
            urlToHit = EndPoints.privacypolicy.path
        }
        else if selectedIndex == 3
        {
            urlToHit = EndPoints.termsandconditions.path
        }
        else if selectedIndex == 4
        {
            urlToHit = EndPoints.patientrights.path
        }
        else if selectedIndex == 5
        {
            urlToHit = EndPoints.complaintpolicy.path
        }
        else if selectedIndex == 6
        {
            urlToHit = EndPoints.cancellationpolicy.path
        }
        else if selectedIndex == 7
        {
            urlToHit = EndPoints.faq.path
        }
        else if selectedIndex == 1
        {
            urlToHit = EndPoints.contactUs.path
        }
        
        print("static pages url = \(urlToHit)")
        
        HUD.show(.systemActivity, onView: self.view.window)
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod:.get, parameters: nil) {[weak self] (data, dictionary, statusCode, error) in
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                HUD.hide()
                
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            
            if dictionary == nil
            {
                HUD.hide()
                
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    let staticData = responseDictionary["data"] as? [String : Any]
                    
                    guard staticData != nil else
                    {
                        HUD.hide()
                        return
                    }
                    
                    self.updateStaticData(usingDict : staticData!)
                }
                else
                {
                    HUD.hide()
                    
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: {(buttonIndex) in
                        
                    })
                }
            }
            else
            {
                HUD.hide()
                
                let responseDictionary = dictionary!
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    UtilityClass.showAlertWithTitle(title: "", message:replyMsg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }
    
    ///UpdateStatic Data Method
    func updateStaticData(usingDict : [String : Any])
    {
        self.title = usingDict["title"] as? String
        self.webView.loadHTMLString((usingDict["description"] as? String)!, baseURL: nil)
    
        print("self.webView = ", self.webView.frame.origin.x, self.webView.frame.origin.y, self.webView.frame.size.width, self.webView.frame.size.height)
    }
}

extension StaticPagesViewController : UIWebViewDelegate
{
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
        HUD.hide()
    }
}
