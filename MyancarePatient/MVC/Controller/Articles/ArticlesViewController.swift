//
//  ArticlesViewController.swift
//  MyancarePatient
//
//  Created by Jyoti on 09/01/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit
import PKHUD

///Articles ViewController Class
class ArticlesViewController: UIViewController {

    //Mark :- Properties
    ///String Refrence Variable - Category ID
    var cateGoryID:String = ""
    ///Array Article Model Array
    var arrayArticle = [ArticleModel]()
    ///Array Category Article Model Data
    var arrayCategoryData = [ArticleModel]()
    
    ///UILabel Refrence - Label Cate Name
    @IBOutlet var labelCateName: UILabel!
    
    ///UICollectionView Refrence Variable - CollectionView Article Image
    @IBOutlet var collectionViewArticlesImages: UICollectionView!
    ///UITableView Refrence Variable - TableView Article List
    @IBOutlet var tableViewArticlesList: UITableView!
    ///UIPageControl Refrence Variable - pageControl Article
    @IBOutlet var pageControlArticles: UIPageControl!
    var numberOfRows = Int()
    
    ///UIViewController LIfe Cycle Methods
    //MARK: - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    //MARK: - viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        self.tabBarController?.tabBar.isHidden = true
        
        setUpNavigationBar()
        getCategoryListFromAPI()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.tabBarController?.tabBar.isHidden = false
    }

    //MARK: - Deinit
    deinit {
        print("ArticlesViewController deinit")
    }
    
    //MARK: - setUpNavigationBar
    ///setUpNavigationBar
    func setUpNavigationBar() {
        
        self.title = "Articles".localized()
        
        self.navigationController?.isNavigationBarHidden = false
        
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSAttributedStringKey.font: UIFont.createPoppinsBoldFont(withSize: 18.0)
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        // set navigation bar color
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 30/255.0, green: 197/255.0, blue: 155/255.0, alpha: 1.0)
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "back_white"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "search"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(searchButtonPressed))
    }
    
    //MARK: - Navigation Back Button Action
    ///Navigation Back Button Action
    @objc func backButtonPressed() {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - Navigation Search Button Action
    ///Navigation Search Button Action
    @objc func searchButtonPressed () {
        
        let articleSearchVC = UIStoryboard.getArticlesStoryBoard().instantiateViewController(withIdentifier: "ArticleSearchVC") as! ArticleSearchViewController
        self.navigationController?.pushViewController(articleSearchVC, animated: true)
    }
    
    //MARK: - Get Category APi
    ///Get Category APi
    
    func getCategoryListFromAPI()
    {
        let urlToHit = EndPoints.categoryList.path
        
        //HUD.show(.systemActivity, onView: self.view.window)
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod:.get, parameters: nil) {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    let dataDict = responseDictionary["data"] as? [[String : Any]]
                    
                    guard dataDict != nil else
                    {
                        return
                    }
                    
                    self.updateCateModelArray(usingArray: dataDict!)
                    
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: {(buttonIndex) in
                        
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    UtilityClass.showAlertWithTitle(title: "", message: replyMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                        
                    })
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }

    //Mark :- get Article List API Calling
    ///get Article List API Calling
    func getArticleListFromAPI(cateID : String)
    {
        let urlToHit = EndPoints.articleListByCat(cateID).path
        
        //HUD.show(.systemActivity, onView: self.view.window)
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod:.get, parameters: nil) {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    let dataDict = responseDictionary["data"] as? [String : Any]
                    
                    guard dataDict != nil else
                    {
                        return
                    }
                    
                    
                    let articleData = dataDict!["articles"] as? [[String : Any]]
                    
                    guard articleData != nil else
                    {
                        return
                    }
                    
                    self.updateModelArray(usingArray: articleData!)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: {(buttonIndex) in
                        
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    UtilityClass.showAlertWithTitle(title: "", message: replyMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                        
                    })
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }
    
    //MARK: - UPDATE CATEGORY MODEL
    ///UPDATE CATEGORY MODEL
    func updateCateModelArray(usingArray array : [[String:Any]]) -> Void
    {
        let dataArray = array
        arrayCategoryData.removeAll()
        
        for dict in dataArray // Iterating dictionaries
        {
            let model = ArticleModel () // Model creation
            model.updateCateModel(usingDictionary: dict) // Updating model
            arrayCategoryData.append(model) // Adding model to array
        }
        
        pageControlArticles.numberOfPages = arrayCategoryData.count
        collectionViewArticlesImages.reloadData()
    }
    
    //MARK:- Updating Model Array
    ///Updating Model Array
    func updateModelArray(usingArray array : [[String:Any]]) -> Void
    {
        let dataArray = array
        arrayArticle.removeAll()
        
        for dict in dataArray // Iterating dictionaries
        {
            let model = ArticleModel () // Model creation
            model.updateModel(usingDictionary: dict) // Updating model
            arrayArticle.append(model) // Adding model to array
        }
        
        tableViewArticlesList.reloadData()
    }
}

//MARK: - Extension -> UITableView -> UITableViewDelegate, UITableViewDataSource
///Extension -> UITableView -> UITableViewDelegate, UITableViewDataSource
extension ArticlesViewController : UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrayArticle.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: ArticlesTableCustomCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier_ArticlesTableCell) as! ArticlesTableCustomCell
        
        if((userDefaults.value(forKey: "customFontSize")) != nil)
        {
            let customFontSize:Float = (userDefaults.value(forKey: "customFontSize") as! Float)
            
            cell.labelArticleDate.font = UIFont(name: cell.labelArticleDate.font.fontName, size: (12.0 + CGFloat(customFontSize)))
            
            cell.labelArticleTitle.attributedText = UILabel.createAttributedStringForTextNormal(strObj: arrayArticle[indexPath.row].title!, location: 0, length: arrayArticle[indexPath.row].title!.count, color: UIColor.MyanCarePatient.darkGreyColorForText, font: UIFont.createPoppinsMediumFont(withSize: (15.0 + CGFloat(customFontSize))))
            
            cell.labelArticleDescription.attributedText = UILabel.createAttributedStringForTextNormal(strObj: arrayArticle[indexPath.row].description_short!, location: 0, length: arrayArticle[indexPath.row].description_short!.count, color: UIColor.MyanCarePatient.LightGreyColorForText, font: UIFont.createPoppinsRegularFont(withSize: (14.0 + CGFloat(customFontSize))))
            
            cell.labelArticleTitle.attributedText = UILabel.setLineSpacing(labelText:cell.labelArticleTitle.attributedText, lineHeightMultiple: 0.8)
            
            cell.labelArticleDescription.attributedText = UILabel.setLineSpacing(labelText:cell.labelArticleDescription.attributedText, lineHeightMultiple: 0.8)
        }
        else
        {
            cell.labelArticleTitle.attributedText = UILabel.createAttributedStringForTextNormal(strObj: arrayArticle[indexPath.row].title!, location: 0, length: arrayArticle[indexPath.row].title!.count, color: UIColor.MyanCarePatient.darkGreyColorForText, font: UIFont.createPoppinsMediumFont(withSize: 15))
            
            cell.labelArticleDescription.attributedText = UILabel.createAttributedStringForTextNormal(strObj: arrayArticle[indexPath.row].description_short!, location: 0, length: arrayArticle[indexPath.row].description_short!.count, color: UIColor.MyanCarePatient.LightGreyColorForText, font: UIFont.createPoppinsRegularFont(withSize: 14))
        }
        
        cell.imageViewArticleImage.setShowActivityIndicator(true)
        cell.imageViewArticleImage.setIndicatorStyle(.gray)
        
        cell.imageViewArticleImage.sd_setImage(with: URL.init(string: arrayArticle[indexPath.row].imageUrl!), placeholderImage: #imageLiteral(resourceName: "no_image_article"))
        
    cell.buttonTotalComments.setTitle(arrayArticle[indexPath.row].commentsCount!, for: .normal)
        
        cell.labelArticleDate.text = UtilityClass.getDateStringFromTimeStamp(timeStamp: arrayArticle[indexPath.row].postedTime!) as String
        
        return cell
    }
    
    //3 Simple Ways to Embrace Change Now
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 120*scaleFactorX
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let articleDetailVC = UIStoryboard.getArticlDetailStoryBoard().instantiateInitialViewController() as! ArticleDetailViewController
        
        articleDetailVC.articleModelData = arrayArticle[indexPath.row]
        //articleDetailVC.hidesBottomBarWhenPushed = true
        
        navigationController?.pushViewController(articleDetailVC, animated: true)
    }
}

//MARK: - Extension -> UICollectionView -> UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
///Extension -> UICollectionView -> UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
extension ArticlesViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return arrayCategoryData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        labelCateName.text = arrayCategoryData[indexPath.row].categoryName!
        
        getArticleListFromAPI(cateID: arrayCategoryData[indexPath.row].categoryId!)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: ArticlesCollectionCustomCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier_ArticlesCollectionCell, for: indexPath) as! ArticlesCollectionCustomCell
        
        if((userDefaults.value(forKey: "customFontSize")) != nil)
        {
            let customFontSize:Float = (userDefaults.value(forKey: "customFontSize") as! Float)
            
            cell.labelArticleTitle.font = UIFont(name: cell.labelArticleTitle.font.fontName, size: (18.0 + CGFloat(customFontSize)))
            labelCateName.font = UIFont(name: labelCateName.font.fontName, size: (17.0 + CGFloat(customFontSize)))
        }

        cell.labelArticleTitle.text = arrayCategoryData[indexPath.row].categoryFullName!
        cell.labelArticleTitle.numberOfLines = 2
        
        cell.imageViewArticleImage.setShowActivityIndicator(true)
        cell.imageViewArticleImage.setIndicatorStyle(.gray)
        
        cell.imageViewArticleImage.sd_setImage(with: URL.init(string: arrayCategoryData[indexPath.row].categoryImage!), placeholderImage: #imageLiteral(resourceName: "no_image_article_detail"))
        
        cell.imageViewArticleImage.layer.cornerRadius = 0
        cell.imageViewArticleImage.layer.borderWidth = 0
        cell.imageViewArticleImage.layer.borderColor = UIColor.clear.cgColor
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        // your code here
        
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {

        let indexPath = collectionViewArticlesImages.indexPathsForVisibleItems.first
        pageControlArticles.currentPage = (indexPath?.row)!
        tableViewArticlesList.reloadData()
    }
}
