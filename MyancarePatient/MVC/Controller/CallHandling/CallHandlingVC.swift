//
//  CallHandlingVC.swift
//  MyancarePatient
//
//  Created by Jitendra Singh on 02/02/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit

//enum EButtonsBar{
//    case kButtonsAnswerDecline
//    case kButtonsHangup
//}

class CallHandlingVC: SINUIViewController, SINCallDelegate
{
    
    /// UILabel Refrence - Show Remote UserName Text
    @IBOutlet weak var remoteUsername: UILabel!
    /// UILabel Refrence - Show Call State Text
    @IBOutlet weak var callStateLabel: UILabel!
    /// UIButton Refrence - Answer Call Button
    @IBOutlet weak var answerButton: UIButton!
    /// UIButton Refrence - Decline Call Button
    @IBOutlet weak var declineButton: UIButton!
    /// UIButton Refrence - End Call Button
    @IBOutlet weak var endCallButton: UIButton!
    /// UIButton Refrence - Mute Call Button
    @IBOutlet weak var muteButton: UIButton!
    /// UIButton Refrence - Speaker On/Off Button
    @IBOutlet weak var speakerButton: UIButton!
    /// UIImageView Refrence - User Image
    @IBOutlet weak var userImgView: UIImageView!
    /// UIImageView Refrence - Logo Image
    @IBOutlet weak var logoImgView: UIImageView!
    /// UILabel Refrence - When call is connection
    @IBOutlet weak var connectingLabel: UILabel!

    
    /// Bool Variable - Identify Mute/Unmute State
    var isMute : Bool = false
    /// Bool Variable - Identify Speaker On/Off State
    var isSpeaker : Bool = false
    
    /// String Variable - Store Appointment ID
    var appointmentID : String = ""
    /// String Variable - Store Doctor ID
    var doctorID : String = ""
    
    /// Timer Variable - Store Call Duration Timing
    var durationTimer: Timer?
    
    /// SINCall Refrence Varible - Handle All Calling Event
    weak var call: SINCall?

    
    /// Sinch Audio Controller
    ///
    /// - Returns: SinAudio Contoller
    func audioController() -> SINAudioController
    {
        return (((UIApplication.shared.delegate as? AppDelegate)?.client)?.audioController())!
    }
 
    
    /// Sinch SetCall Method
    ///
    /// - Parameter call: SInCall Refrence
    func setCall(_ call: SINCall)
    {
        self.call = call
        self.call?.delegate = self
    }
    
    /// MARK: - UIViewController Cycle Method
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isHidden = true
        
        if (((UIApplication.shared.delegate as? AppDelegate)?.callKitProvider)?.callExists(self.call?.callId))!
        {
            if self.call?.state == SINCallState.established
            {
                self.startCallDurationTimer(with: #selector(onDurationTimer(_:)))
                self.showButtons(EButtonsBar.kButtonsHangup)
            }
            else
            {
                self.setCallStatusText("")
                self.showButtons(EButtonsBar.kButtonsWakenByCallKit)
            }
        }
        else
        {
            if self.call?.direction == SINCallDirection.incoming
            {
                self.speakerButton.isHidden = true
                self.muteButton.isHidden = true
                self.setCallStatusText("incoming...")
                self.showButtons(EButtonsBar.kButtonsAnswerDecline)
                self.audioController().enableSpeaker()
                self.audioController().startPlayingSoundFile(path(forSound : "incoming"), loop: true)
            }
            else
            {
                self.speakerButton.isHidden = false
                self.muteButton.isHidden = false
                self.setCallStatusText("calling...")
                
                self.audioController().disableSpeaker()
                
                self.showButtons(EButtonsBar.kButtonsHangup)
            }
        }

        let currentRoute = AVAudioSession.sharedInstance().currentRoute
        if (currentRoute.outputs != nil)
        {
            for description in currentRoute.outputs
            {
                if (description.portType == AVAudioSessionPortHeadphones)
                {
                    self.audioController().disableSpeaker()
                    print("headphone plugged in")
                }
                else
                {
                    if self.call?.direction == SINCallDirection.incoming
                    {
                        self.audioController().enableSpeaker()
                    }
                    else
                    {
                        self.audioController().disableSpeaker()
                    }
                    
                    print("headphone pulled out")
                }
            }
        }
        else
        {
            print("requires connection to device")
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(audioRouteChangeListener1(notification:)), name: NSNotification.Name.AVAudioSessionRouteChange, object: nil)
    }
    
    
    /// Audio Route Change Listener to Change Speaker On/Off state
    ///
    /// - Parameter notification:
    @objc dynamic private func audioRouteChangeListener1(notification : NSNotification)
    {
        let audioRouteChangeReason = notification.userInfo![AVAudioSessionRouteChangeReasonKey] as! UInt
        
        switch audioRouteChangeReason {
            
        case AVAudioSessionRouteChangeReason.newDeviceAvailable.rawValue:
            
            if(isSpeaker)
            {
                self.audioController().enableSpeaker()
                //self.speakerButton.setTitle("Speaker Off", for: UIControlState.normal)
                self.speakerButton.setImage(#imageLiteral(resourceName: "callSpecOn"), for: UIControlState.normal)
            }
            else
            {
                self.audioController().disableSpeaker()
                //self.speakerButton.setTitle("Speaker On", for: UIControlState.normal)
                self.speakerButton.setImage(#imageLiteral(resourceName: "callSpecOff"), for: UIControlState.normal)
            }
            
            print("headphone plugged in")
            
        case AVAudioSessionRouteChangeReason.oldDeviceUnavailable.rawValue:
            
            if(isSpeaker)
            {
                self.audioController().enableSpeaker()
               // self.speakerButton.setTitle("Speaker Off", for: UIControlState.normal)
                self.speakerButton.setImage(#imageLiteral(resourceName: "callSpecOn"), for: UIControlState.normal)
            }
            else
            {
                self.audioController().disableSpeaker()
                //self.speakerButton.setTitle("Speaker On", for: UIControlState.normal)
                self.speakerButton.setImage(#imageLiteral(resourceName: "callSpecOff"), for: UIControlState.normal)
            }
            
            print("headphone pulled out")
        
        default:
            break
        }
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        
        self.tabBarController?.tabBar.isHidden = false
    }
    
    
    /// Call Dimissed Method
    func callDismissed()
    {
        self.call?.hangup()
        
        dismiss(animated: true)
        {
            self.audioController().stopPlayingSoundFile()
            
            self.stopCallDurationTimer()
            
            self.audioController().disableSpeaker()
            
            let MyAppointmentsReviewVC = UIStoryboard.getMyAppointmentsStoryBoard().instantiateViewController(withIdentifier: "MyAppointmentsReviewVC") as! MyAppointmentsReviewViewController
            
            MyAppointmentsReviewVC.isFromCall = true
            MyAppointmentsReviewVC.doctor_id = self.doctorID
            MyAppointmentsReviewVC.appointment_id = self.appointmentID
            
            MyAppointmentsReviewVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            
            guard let appD = appDelegate as? AppDelegate else {
                return
            }
            
            let topController = appD.window?.currentViewController()
            
            topController?.present(MyAppointmentsReviewVC, animated: true, completion: nil)
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        self.tabBarController?.tabBar.isHidden = true
        
        self.userImgView.layer.cornerRadius = 55
        self.userImgView.layer.borderWidth = 1
        self.userImgView.layer.borderColor = UIColor.white.cgColor
        self.userImgView.layer.masksToBounds = true
        
        self.audioController().unmute()
        
        if self.call?.direction == SINCallDirection.incoming
        {
            let dicData = self.call?.headers
            print(" calling user headers====== \(String(describing: dicData!))")
            
            if (dicData != nil && !(dicData?.isEmpty)!)
            {
                appointmentID = dicData!["APPOINTMENT_ID"] as! String
            }
            
            var pushData = NSDictionary()
            var pushDict = NSDictionary()
            
            if((userDefaults.object(forKey: "pushUserInfo")) != nil)
            {
                pushData = userDefaults.object(forKey: "pushUserInfo") as! NSDictionary
                pushDict = pushData["public_headers"] as! NSDictionary
                appointmentID = pushDict["APPOINTMENT_ID"] as! String
            }
            
            print("pushData = ", pushData)
            print("pushDict = ", pushDict)
            print("appointmentID = ", appointmentID)
            
            if (dicData != nil && !(dicData?.isEmpty)!)
            {
                self.remoteUsername.text = dicData!["CALLER_NAME"] as? String
                
                self.doctorID = (dicData!["CALL_ID"] as? String)!
            
                self.userImgView.setShowActivityIndicator(true)
                self.userImgView.setIndicatorStyle(.gray)
                
                self.userImgView.sd_setImage(with: URL.init(string:(dicData!["CALLER_IMAGE"] as? String)!), placeholderImage: #imageLiteral(resourceName: "welcome-doctor-logo"))
            }
            else if(pushDict != nil)
            {
                self.remoteUsername.text = pushDict["CALLER_NAME"] as? String
             
                self.doctorID = (pushDict["CALL_ID"] as? String)!
                
                self.userImgView.setShowActivityIndicator(true)
                self.userImgView.setIndicatorStyle(.gray)
                
                self.userImgView.sd_setImage(with: URL.init(string:(pushDict["CALLER_IMAGE"] as? String)!), placeholderImage: #imageLiteral(resourceName: "welcome-doctor-logo"))
            }
            else
            {
               self.remoteUsername.text = self.call?.remoteUserId
               self.userImgView.image = #imageLiteral(resourceName: "welcome-doctor-logo")
            }
        }
        else
        {
            let dicData = self.call?.headers
            print(" calling user headers====== \(dicData ?? [:])")
            
            if (dicData != nil && !(dicData?.isEmpty)!)
            {
                appointmentID = dicData!["APPOINTMENT_ID"] as! String
            }
            
            if (dicData != nil)
            {
                self.remoteUsername.text = dicData!["RECEIVER_NAME"] as? String
                
                self.userImgView.setShowActivityIndicator(true)
                self.userImgView.setIndicatorStyle(.gray)
                
                self.userImgView.sd_setImage(with: URL.init(string:(dicData!["RECEIVER_IMAGE"] as? String)!), placeholderImage: #imageLiteral(resourceName: "welcome-doctor-logo"))
            }
            else
            {
                self.remoteUsername.text = self.call?.remoteUserId
                self.userImgView.image = #imageLiteral(resourceName: "welcome-doctor-logo")
            }
        }
    }
    
    //MARK: - Sinch Call Fire Socket Event
    
    /// Sinch Socket Call Fire Event
    ///
    /// - Parameters:
    ///   - eventType: eventType - Video Call/ Voice Call
    ///   - appointmentID: Appointment ID
    func sinchSocketCallEvent(_ eventType:String, appointmentID: String)
    {
        if !SocketManagerHandler.sharedInstance().isSocketConnected()
        {
            return
        }
    
        SocketManagerHandler.sharedInstance().manageCallSocketEvent (callDuration, eventType: eventType, appointmentID: appointmentID) { (dataDict, statusCode) in
        
        }
    }
    
    // MARK: - Call Actions
    
    /// When Click To Accept Button Event
    ///
    /// - Parameter sender: Accept Button Refrence
    @IBAction func accept(_ sender: Any)
    {
        self.audioController().stopPlayingSoundFile()
        self.audioController().disableSpeaker()
        
        sinchSocketCallEvent (SocketManageCallEventKeyword.callEventPatientPicked.rawValue, appointmentID: appointmentID)
        
        self.call?.answer()
    }
    
    
    /// Decline Button Clicked Event
    ///
    /// - Parameter sender: Decline Button Refrence
    @IBAction func decline(_ sender: Any)
    {
        self.call?.hangup()
        
        self.audioController().disableSpeaker()
        
        sinchSocketCallEvent (SocketManageCallEventKeyword.callEventPatientReject.rawValue, appointmentID: appointmentID)

        self.dismiss()
    }
    
    
    /// Hangup Button Clicked Event
    ///
    /// - Parameter sender: HangUp Button Refrence
    @IBAction func hangup(_ sender: Any)
    {
        self.call?.hangup()
        
        dismiss(animated: true)
        {
            self.audioController().stopPlayingSoundFile()
            
            self.stopCallDurationTimer()
            
            self.audioController().disableSpeaker()
            
            let MyAppointmentsReviewVC = UIStoryboard.getMyAppointmentsStoryBoard().instantiateViewController(withIdentifier: "MyAppointmentsReviewVC") as! MyAppointmentsReviewViewController
            
            MyAppointmentsReviewVC.isFromCall = true
            MyAppointmentsReviewVC.doctor_id = self.doctorID
            MyAppointmentsReviewVC.appointment_id = self.appointmentID
            
            MyAppointmentsReviewVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            
            guard let appD = appDelegate as? AppDelegate else {
                return
            }
            
            let topController = appD.window?.currentViewController()
            
            topController?.present(MyAppointmentsReviewVC, animated: true, completion: nil)
        }
    }
    
    
    /// Mute/Unmute Button Click Event
    ///
    /// - Parameter sender: Mute/UnMute Button Refrence
    @IBAction func muteUnmute(_ sender: Any)
    {
        if(isMute)
        {
            isMute = false
            self.audioController().unmute()
            //self.muteButton.setTitle("Mute", for: UIControlState.normal)
            self.muteButton.setImage(#imageLiteral(resourceName: "callMute"), for: UIControlState.normal)
        }
        else
        {
            isMute = true
            self.audioController().mute()
            //self.muteButton.setTitle("Unmute", for: UIControlState.normal)
            self.muteButton.setImage(#imageLiteral(resourceName: "callUnmute"), for: UIControlState.normal)
        }
    }
    
    
    /// Speaker On/Off Clicked Event
    ///
    /// - Parameter sender: Speaker On/Off Button Refrence
    @IBAction func speaker(_ sender: Any)
    {
        if(!isSpeaker)
        {
            isSpeaker = true
            self.audioController().enableSpeaker()
            //self.speakerButton.setTitle("Speaker Off", for: UIControlState.normal)
            self.speakerButton.setImage(#imageLiteral(resourceName: "callSpecOn"), for: UIControlState.normal)
        }
        else
        {
            isSpeaker = false
            self.audioController().disableSpeaker()
            //self.speakerButton.setTitle("Speaker On", for: UIControlState.normal)
            self.speakerButton.setImage(#imageLiteral(resourceName: "callSpecOff"), for: UIControlState.normal)
        }
    }
    
    
    /// On Show Duration Timer When Call Is Initiated
    ///
    /// - Parameter unused: call timer
    @objc func onDurationTimer(_ unused: Timer)
    {
        let duration = Int(Date().timeIntervalSince ((self.call?.details.establishedTime)!))
        self.setDuration(duration)
    }
    
    // MARK: - SINCallDelegate
    
    /// Sinch Call Delegate Methods
    /// Call Did Progress Event
    /// - Parameter call: SINCall Refrence
    func callDidProgress(_ call: SINCall)
    {
        print("did Progress call detail ===== \(String(describing: self.call?.details))")
        
        self.setCallStatusText("ringing...")
        self.audioController().startPlayingSoundFile(path(forSound: "ringback"), loop: true)
    }
    
    
    /// Call Did Establised Method
    ///
    /// - Parameter call: SINCall Refrence
    func callDidEstablish(_ call: SINCall)
    {
        print("did establish call detail ===== \(String(describing: self.call?.details))")
    
        self.startCallDurationTimer(with: #selector(self.onDurationTimer(_:)))
        
        self.showButtons(EButtonsBar.kButtonsHangup)
        
        self.audioController().stopPlayingSoundFile()
        self.audioController().disableSpeaker()
       
        self.speakerButton.isHidden = false
        self.muteButton.isHidden = false
    }
    
    
    /// Call Did End Event
    ///
    /// - Parameter call: SINCall Refrence
    func callDidEnd(_ call: SINCall)
    {
        print("did end call detail ===== \(String(describing: self.call?.details))")
    
        //if isDisappearing {
        //    return
        //}
        //else
        if isAppearing
        {
            setShouldDeferredDismiss(true)
            return
        }
        
        dismiss(animated: true)
        {
            self.audioController().stopPlayingSoundFile()
            
            self.stopCallDurationTimer()
            
            self.audioController().disableSpeaker()
            
            let MyAppointmentsReviewVC = UIStoryboard.getMyAppointmentsStoryBoard().instantiateViewController(withIdentifier: "MyAppointmentsReviewVC") as! MyAppointmentsReviewViewController
            
            MyAppointmentsReviewVC.isFromCall = true
            MyAppointmentsReviewVC.doctor_id = self.doctorID
            MyAppointmentsReviewVC.appointment_id = self.appointmentID
            
            MyAppointmentsReviewVC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            
            guard let appD = appDelegate as? AppDelegate else {
                return
            }
            
            let topController = appD.window?.currentViewController()
                
            topController?.present(MyAppointmentsReviewVC, animated: true, completion: nil)
        }
    }
    
    // MARK: - Sounds
    
    func path(forSound soundName: String) -> String
    {
        let audioFileName = soundName
        
        let audioFilePath = Bundle.main.path(forResource: audioFileName, ofType: "wav")
        print(audioFilePath ?? "")
        
        return audioFilePath!
    }
}
