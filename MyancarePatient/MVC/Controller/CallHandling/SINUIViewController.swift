//
//  SINUIViewController.swift
//  MyancarePatient
//
//  Created by Jitendra Singh on 02/02/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit
import ObjectiveC

// used for associated object references to simulate property-like storage for
// this category

var sin_deferredDismissalKey = ""
///Sinch UIView Controller
class SINUIViewController: UIViewController
{
    
    /// Bool Variable To CHek Is Appearing
    private(set) var isAppearing = false
    /// Bool Variable To CHek Is Disapperaing
    private(set) var isDisappearing = false

    
    
    /// UIViewController Life Cycle Methods
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    
        if view.window == nil
        {
            isAppearing = false
            isDisappearing = false
        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
     
        isAppearing = true
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        
        isAppearing = false
        dismissIfNecessary()
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
     
        isDisappearing = true
        self.tabBarController?.tabBar.isHidden = false
    }
    
    override func viewDidDisappear(_ animated: Bool)
    {
        super.viewDidDisappear(animated)
    
        isAppearing = false
    }
    
    // MARK: - Dismissal
    func dismiss()
    {
        if isDisappearing
        {
            return
        }
        else if isAppearing
        {
            setShouldDeferredDismiss(true)
            return
        }
        
        dismiss(animated: true) {() -> Void in }
    }
    
    func dismissIfNecessary()
    {
        if shouldDeferrDismiss()
        {
            setShouldDeferredDismiss(false)
        
            DispatchQueue.main.async(execute: {() -> Void in
                self.dismiss()
            })
        }
    }
    
    func shouldDeferrDismiss() -> Bool
    {
        return sin_getAssociatedBOOL(forKey: sin_deferredDismissalKey)
    }
    
    func setShouldDeferredDismiss(_ v: Bool)
    {
        sin_setAssociatedBOOL(v, forKey: sin_deferredDismissalKey)
    }
    
    // MARK: -
    func sin_getAssociatedBOOL(forKey key: UnsafeRawPointer) -> Bool
    {
        let v = objc_getAssociatedObject(self, key) as? NSNumber
        return (v != nil) ? (v != 0) : false
    }
    
    func sin_setAssociatedBOOL(_ v: Bool, forKey key: UnsafeRawPointer)
    {
        objc_setAssociatedObject(self, key, (v ? 1 : 0), .OBJC_ASSOCIATION_COPY)
    }
}
