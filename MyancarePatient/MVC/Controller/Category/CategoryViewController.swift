//
//  CategoryViewController.swift
//  MyancarePatient
//
//  Created by Jyoti on 15/01/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit
import PKHUD

//MARK: -
///Category Array Delegate
protocol CategoryArrayDelegate: class
{
    
    /// Delegate Method to Select Category Value
    ///
    /// - Parameter dataValue: DataValues Contains In NSMutableArray
    func CategoryData(_selectedCategory dataValue: NSMutableArray)
}

///CategoryView Controller Class
class CategoryViewController: UIViewController {
    ///Array Varible - Contains Article Model Data
    var arrayCategory = [ArticleModel]()
///UIButton Refrence Variable - Apply Button
    @IBOutlet weak var applyButton: UIButton!
    ///Delegate Variable - CategoryArray Delegeate
    weak var delegate: CategoryArrayDelegate?
    
    ///Array Variable - Contains Selected Category ID Collection
    var selectedCategoryIdArr : NSMutableArray = []
    
    //MARK: - Properties
    ///UITableView Refrence - View Category Lists
    @IBOutlet weak var tableViewCategoryList: UITableView!

    //MARK: - viewDidLoad
    ///View Controller LIfe Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    //MARK: - viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        // Do any additional setup after appearing the view.
        
        self.tabBarController?.tabBar.isHidden = true
        
        self.setUpNavigationBar()
        
        getCategoryListFromAPI()
    }
    
    //MARK: - viewWillDisappear
    override func viewWillDisappear(_ animated: Bool)
    {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        self.tabBarController?.tabBar.isHidden = false
    }
    
    //MARK: - Deinit
    deinit {
        print("CategoryViewController deinit")
    }
    
    //MARK: - setUpNavigationBAr
    ///setUpNavigationBAr
    func setUpNavigationBar() {
        
        applyButton.setTitle("Apply".localized(), for: .normal)
        
        self.title = "Filter".localized()
        
        self.navigationController?.isNavigationBarHidden = false
        
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSAttributedStringKey.font: UIFont.createPoppinsBoldFont(withSize: 18.0)
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        // set navigation bar color
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 30/255.0, green: 197/255.0, blue: 155/255.0, alpha: 1.0)
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "back_white"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
    }
    
    //MARK: - Navigation bar Back Button Action
    ///Navigation bar Back Button Action
    @objc func backButtonPressed()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - on Apply Button Action
    ///on Apply Button Action
    @IBAction func btnApplyPressed(_ sender: Any) {
        
        print("selected arr = \(String(describing: selectedCategoryIdArr))")
        
        if selectedCategoryIdArr.count >  0 {
            guard let delegate1 = delegate else {
                return
            }
            
            delegate1.CategoryData(_selectedCategory: selectedCategoryIdArr)
            
            self.navigationController?.popViewController(animated: true)
        }
        else
        {
            UtilityClass.showAlertWithTitle(title: "", message: "choose atleast one option".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
        }
    }
    
    //Mark :- get category list API Calling
    /// get category list API Calling
    func getCategoryListFromAPI()
    {
        let urlToHit = EndPoints.categoryList.path
        
        HUD.show(.systemActivity, onView: self.view.window)
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod:.get, parameters: nil) {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    let dataDict = responseDictionary["data"] as? [[String : Any]]
                    
                    guard dataDict != nil else
                    {
                        return
                    }
                    
                    self.updateCateModelArray(usingArray: dataDict!)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: {(buttonIndex) in
                        
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    UtilityClass.showAlertWithTitle(title: "", message: replyMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                        
                    })
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }
    
    //MARK: - update Category Model Arr
    /// update Category Model Arr
    func updateCateModelArray(usingArray array : [[String:Any]]) -> Void
    {
        let dataArray = array
        arrayCategory.removeAll()
        
        for dict in dataArray // Iterating dictionaries
        {
            let model = ArticleModel () // Model creation
            model.updateCateModel(usingDictionary: dict) // Updating model
            arrayCategory.append(model) // Adding model to array
        }
        
        tableViewCategoryList.reloadData()
    }
}

//MARK:- Extention -> UITableView -> UITableViewDelegate, UITableViewDataSource
///Extention -> UITableView -> UITableViewDelegate, UITableViewDataSource
extension CategoryViewController :UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  arrayCategory.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CategoryTableCustomCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier_CategoryCell) as! CategoryTableCustomCell
    
        cell.buttonCategoryListName.setTitle(arrayCategory[indexPath.row].categoryName, for: UIControlState.normal)
        
        let strSelected = arrayCategory[indexPath.row].categoryId
        
        if (selectedCategoryIdArr.contains(strSelected!)) {
            cell.buttonSelection.isSelected = true
            cell.buttonSelection.setImage(UIImage.init(named: "checked"), for: .normal)
        } else {
            cell.buttonSelection.isSelected = false
            cell.buttonSelection.setImage(UIImage.init(named: "uncheck"), for: .normal)
        }
        
        cell.buttonSelection.tag = indexPath.row
        
        cell.buttonSelection.isUserInteractionEnabled = false
        cell.buttonCategoryListName.isUserInteractionEnabled = false
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (60 * scaleFactorX)
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell: CategoryTableCustomCell = tableView.cellForRow(at: indexPath) as! CategoryTableCustomCell
        
        if cell.buttonSelection.isSelected == true
        {
            cell.buttonSelection.isSelected = false
            cell.buttonSelection.setImage(UIImage.init(named: "uncheck"), for: .normal)
            
            let strSelected = arrayCategory[indexPath.row].categoryId
            //print("strSelected  = \(String(describing: strSelected))")
            
            if (selectedCategoryIdArr.contains(strSelected!))
            {
                selectedCategoryIdArr.remove(strSelected!)
            }
        }
        else
        {
            let strSelectedID = arrayCategory[indexPath.row].categoryId
            //print("strSelectedID = \(String(describing: strSelectedID))")
            
            selectedCategoryIdArr.add(strSelectedID!)
            
            cell.buttonSelection.isSelected = true
            cell.buttonSelection.setImage(UIImage.init(named: "checked"), for: .normal)
        }
    }
}
