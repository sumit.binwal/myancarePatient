//
//  ChatViewController.swift
//  MyanCareDoctor
//
//  Created by Sumit Sharma on 14/03/2018.
//  Copyright © 2018 sumit. All rights reserved.
//

import UIKit
import DZNEmptyDataSet
import UserNotifications
import IQKeyboardManagerSwift
import PKHUD
import AWSS3


/// ChatWindow Height
private let MaxChatWindowHeight = 160 * scaleFactorX

/// Chat View Controller
class ChatViewController: UIViewController
{
    /// TableView Chat List Refrence
    @IBOutlet weak var tableChatList: UITableView!

    /// UIView Container Text Box For InputChatBox
    @IBOutlet weak var containerTextBox: UIView!
    
    /// UIView Container For Show Expire Chat Status
    @IBOutlet weak var expireViewBox: UIView!
    /// ImageView To Show User Image
    @IBOutlet weak var userImageView: UIImageView!
    
    /// UiLabel Refrence Show User Expire Text
    @IBOutlet weak var userLabeExpire: UILabel!
    
    /// UiView Refrence To Show Input text Entry
    @IBOutlet weak var inputTextView: KMPlaceholderTextView!
    
    /// UIButton Refrence to Choose Media Selection
    @IBOutlet weak var buttonMediaSelect: UIButton!
    
    /// UIButton Refrence to Sent Typed Message
    @IBOutlet weak var buttonSend: UIButton!
    
    /// Constraint Refrence to Manage Media Selection Frames
    @IBOutlet weak var constraintMediaSelectWidth: NSLayoutConstraint!
    /// Constraint Refrence to Manage Media Selection Frames (Text Box Height)
    
    @IBOutlet weak var constraintContainerTextBoxHeight: NSLayoutConstraint!
    
    /// Constraint Refrence to Manage Media Selection Frames (Bottom Conatiner)
    @IBOutlet weak var constraintContainerBottom: NSLayoutConstraint!
    
    /// Constraint Refrence to Manage Media Selection Frames (Text View Height)
    @IBOutlet weak var constraintInputTextViewHeight: NSLayoutConstraint!
    
    /// Constraint Refrence to Manage Media Selection Frames (Send Button Height)
    @IBOutlet weak var constraintSendButtonHeight: NSLayoutConstraint!
    
    /// Constraint Refrence to Manage Media Selection Frames (Send Button Width)
    @IBOutlet weak var constraintSendButtonWidth: NSLayoutConstraint!
    
    /// Constraint Refrence to Manage Media Selection Frames (Send Button Width)
    var arrayChatMessages = [ModelChatMessage]()

    
    /// UILabel Refrence For Show Online Status
    @IBOutlet weak var chatUserOnlineLabel: UILabel!
    /// UIView Refrence For Show Connecting Status When Network is lost
    @IBOutlet weak var connectingView: UIView!

    @IBOutlet weak var groupChatMoreView: UIView!
    @IBOutlet weak var groupImageView: UIImageView!
    
    
    /// Custome Variable - Contains Orignal Text Box Height
    var originalContainerTextBoxHeight : CGFloat = 0
    /// Custome Variable - Contains Input Text View Height
    var originalInputTextViewHeight : CGFloat = 0
    /// Custome Variable - Contains UserName
    var chatUsername : String!
    /// Custome Variable - Contains Chat UserID
    var chatUserID : String?
    /// Custome Variable - Contains Chat ID
    var chatRoomID : String?
    /// Custome Variable - Contains Chat Image
    var chatPictureURL: String?
    /// Custome Variable - Contains Message Sender ID
    var senderID : String!

    /// Custome Variable - To Check Is User Or Not
    var isUser = true
    /// Custome Variable - To Check Is Account Delete Or Not
    var isAccountDeleted = false
    /// Custome Variable - Contains Folder Path
    var folderPath: String?
    
    var numberOfArrayElements = 0
    
    /// Variable - UIImage Picker Controller
    let imagePicker = UIImagePickerController()
    
    /// Variable - UIActivity Indicator
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    
    /// UILabel Refrnece to show Remaining time Text
    @IBOutlet weak var leftTimeLabel: UILabel!
    /// UIView Refrnece to show Remaining time Text Container
    @IBOutlet weak var leftTimeView: UIView!
    /// LayoutContraint Refrnece to show manage Left Time View Height Frame
    @IBOutlet weak var leftTimeViewHeightConstraint: NSLayoutConstraint!
    
    
    /// View Controller Life Cycle Methods
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        UIApplication.shared.isIdleTimerDisabled = true
        
        self.leftTimeView.isHidden = true
        leftTimeLabel.text = ""
        self.leftTimeViewHeightConstraint.constant = 0
        
        constraintContainerTextBoxHeight.constant = constraintContainerTextBoxHeight.constant * scaleFactorX
        originalContainerTextBoxHeight = constraintContainerTextBoxHeight.constant
        
        containerTextBox.viewWithTag(10)?.layer.cornerRadius = 5 * scaleFactorX
        
        inputTextView.delegate = self
        inputTextView.layoutManager.delegate = self
        
        inputTextView.inputAccessoryView = UIView ()
        
        setupView()
        setupTableView()
        
        IQKeyboardManager.sharedManager().enable = false
        
        // Keyobard frame change notification
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChangeFrame), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(textInputModeChangedNotification), name: NSNotification.Name.UITextInputCurrentInputModeDidChange, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(internetReachabilityChanged), name: NSNotification.Name(rawValue: NotificationInternetConnection), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(notificationSocketConnected), name: NSNotification.Name(rawValue: NOTIFICATION_SOCKET_CONNECTED), object: nil)
    }
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        setUpNavigationBar()
        
        self.tabBarController?.tabBar.isHidden = true
        
        inputTextView.placeholder = "Write a message...".localized()
        userLabeExpire.text = "Your chat is expired".localized()
        
        expireViewBox.isHidden = true
        
        userImageView.layer.cornerRadius = userImageView.frame.size.height/2 * scaleFactorX
        userImageView.layer.borderColor = UIColor.lightGray.cgColor
        userImageView.layer.borderWidth = 1.0
        userImageView.layer.masksToBounds = true
        
        userImageView.sd_setImage(with: URL.init(string: UtilityClass.getUserInfoData()["avatar_url"] as! String), placeholderImage: #imageLiteral(resourceName: "no_image_user"))
        
        if (chatRoomID == nil || chatRoomID?.isEmpty == true )
        {
            getConversationIDFromSocket()
        }
        else
        {
            getChatHistoryFromSocket()
        }
    }


    
    /// Show Connection Loader View When Network Gone
    func showIndicator()
    {
        self.navigationItem.titleView = self.activityIndicator
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
    }
    
    /// Hide Connection Loader View When Network Appear
    func hideIndicator()
    {
        self.navigationItem.titleView = nil
    }
    
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        
        self.tabBarController?.tabBar.isHidden = false
    }
    
    /// Setup Navigation Bar Method
    func setUpNavigationBar()
    {
        self.title = chatUsername.capitalized
        
        self.navigationController?.isNavigationBarHidden = false
        
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSAttributedStringKey.font: UIFont.createPoppinsBoldFont(withSize: 18.0)
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        // set navigation bar color
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 30/255.0, green: 197/255.0, blue: 155/255.0, alpha: 1.0)
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "back_white"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
    }
    
    //MARK:- navigation bar back nutton action
    
    /// Back Button Click Event
    @objc func backButtonPressed()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK:- Initial setup view
    /// Initial Setup View
    func setupView()
    {
        self.navigationItem.hidesBackButton = true
        
        if !SocketManagerHandler.sharedInstance().isSocketConnected()
        {
            showIndicator()
        }
    }
    
    //MARK : - Get Chat HIstory
    
    /// Get Message Histroy From Server (Socket)
    func getChatHistoryFromSocket()
    {
        if !SocketManagerHandler.sharedInstance().isSocketConnected()
        {
            hideIndicator()
            return
        }
        
        HUD.show(.systemActivity, onView: self.view.window)

        if  SocketManagerHandler.sharedInstance().isSocketConnected()
        {
            let strUsrID = UtilityClass.getUserIdData()
            
            SocketManagerHandler.sharedInstance().getChatHistory(chatRoomID!, doctorID: senderID!, patientID: strUsrID!, chatHistoryHandler: {[weak self] (arrChatHistory, statusCode, hasSlot, slotStartTime, slotEndTime) in
                
                HUD.hide()
                
                guard let `self` = self else {return}
                
                if hasSlot == "1"
                {
                    self.containerTextBox.isHidden = false
                    self.constraintContainerTextBoxHeight.constant = self.originalContainerTextBoxHeight
                    self.expireViewBox.isHidden = true
                    
                    let dateFormatter = DateFormatter()
                    
                    let date1 = Date(timeIntervalSince1970: Double(slotEndTime)!)
                    
                    dateFormatter.locale = NSLocale.current
                    dateFormatter.timeZone = NSTimeZone.system
                    
                    let timeString = UtilityClass.timeLeftFromNow(date1, currentDate: Date(), numericDates: true)
                    
                    self.leftTimeLabel.text = timeString
                    self.leftTimeView.isHidden = false
                    
                    self.leftTimeViewHeightConstraint.constant = 30
                }
                else
                {
                    self.constraintContainerTextBoxHeight.constant = self.originalContainerTextBoxHeight
                    self.containerTextBox.isHidden = true
                    self.expireViewBox.isHidden = false
                    
                    self.leftTimeLabel.text = ""
                    self.leftTimeView.isHidden = true
                    
                    self.leftTimeViewHeightConstraint.constant = 0
                }
                
                if statusCode == 200
                {
                    self.updateModel(usingArray: arrChatHistory)
                }
            })
        }
    }
    
    //MARK:- Update chat model array
    
    /// Update Chat History Model Data
    ///
    /// - Parameter array: Chat History Message Data Getting From Server
    func updateModel(usingArray array : [[String:Any]])
    {
        arrayChatMessages.removeAll()
        numberOfArrayElements  = 0
        
        let dataArray = array
        
        for dict in dataArray // Iterating dictionaries
        {
            let model = ModelChatMessage () // Model creation
            model.updateModel(usingDictionary: dict) // Updating model
            arrayChatMessages.append(model) // Adding model to array
        }
        
        arrayChatMessages = arrayChatMessages.reversed()
        
        tableChatList.reloadData()
        
        if numberOfArrayElements == 0
        {
            numberOfArrayElements = arrayChatMessages.count
            
            if numberOfArrayElements > 0
            {
                tableChatList.scrollToRow(at: IndexPath (row: arrayChatMessages.count - 1, section: 0), at: .top, animated: false)
            }
        }
    }
    
    
    /// Expire Button Clicked Event
    ///
    /// - Parameter sender: Button Refrence
    @IBAction func expireButtonClicked(_ sender: Any) {
        
        let doctorDetailVC = UIStoryboard.getDoctorDetailStoryBoard().instantiateInitialViewController() as! DoctorDetailViewController
        
        doctorDetailVC.doctor_id = senderID
        
        self.navigationController?.pushViewController (doctorDetailVC, animated: true)
    }
    
    //MARK : - Get Conversation ID
    
    /// Get/Create Conversation ID From Socket
    func getConversationIDFromSocket()
    {
        if  SocketManagerHandler.sharedInstance().isSocketConnected()
        {
            SocketManagerHandler.sharedInstance().createConversation(senderID) { (messageDict, statusCode) in
                
                print(messageDict)
                print(statusCode)
                
                if statusCode == 200
                {
                    self.chatRoomID = messageDict["id"]! as? String
                    self.getChatHistoryFromSocket()
                }
            }
        }
    }
    
    //MARK:- setupTableView
    
    /// SetupTable View Data
    func setupTableView()
    {
        tableChatList.delegate = self
        tableChatList.dataSource = self
        tableChatList.prefetchDataSource = self
        
        tableChatList.estimatedRowHeight = 100
        tableChatList.rowHeight = UITableViewAutomaticDimension
    }
    
    //MARK:- Media Button Action
    
    /// Choose Media Button Click Event
    ///
    /// - Parameter sender: Button Refrence
    @IBAction func onMediaButtonAction(_ sender: Any)
    {
        let optionMenu = UIAlertController(title: nil, message: "Choose Image".localized(), preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction(title: "Camera".localized(), style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            
            if UIImagePickerController.isSourceTypeAvailable (UIImagePickerControllerSourceType.camera)
            {
                self.imagePicker.sourceType     = .camera
                self.imagePicker.allowsEditing  = true
                self.imagePicker.delegate       = self
                
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        })
        
        let galleryAction = UIAlertAction(title: "Photo Gallery".localized(), style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            
            if UIImagePickerController.isSourceTypeAvailable (UIImagePickerControllerSourceType.photoLibrary)
            {
                self.imagePicker.sourceType = .photoLibrary
                self.imagePicker.allowsEditing =  true
                self.imagePicker.delegate = self
                
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        })
        
        let cancelAction = UIAlertAction(title: "Cancel".localized(), style: .cancel, handler:
        {
            (alert: UIAlertAction!) -> Void in
        })
        
        optionMenu.addAction(cameraAction)
        optionMenu.addAction(galleryAction)
        optionMenu.addAction(cancelAction)
        
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    //MARK:- Send Button Action
    
    /// Click Send Button Event
    ///
    /// - Parameter sender: Send Button Refrence
    @IBAction func onSendButtonAction(_ sender: UIButton)
    {
        if inputTextView.text.isEmptyString()
        {
            return
        }
        
        buttonSend.isUserInteractionEnabled = false
        
        //Call Socket Emit FOr Sent Message From Server
        if SocketManagerHandler.sharedInstance().isSocketConnected()
        {
            SocketManagerHandler.sharedInstance().sendMessage (inputTextView.text.base64Encoded(), senderID, filePath: "", conversationID: chatRoomID!) {[weak self] (messageDict, statusCode) in
                
                guard let `self` = self else {return}
                
                self.buttonSend.isUserInteractionEnabled = true
                
                print(messageDict)
                
                if statusCode == 200
                {
                    self.inputTextView.text = ""
                    self.textViewDidChange(self.inputTextView)
                    self.addNewModel(usingDictionary: messageDict)
                }
            }
        }
    }
    
    //MARK:- Add New chat model
    
    /// Add New Message Data In Existing Chat List
    ///
    /// - Parameter dictionary: Last Message Send to Server Data
    func addNewModel(usingDictionary dictionary : [String:Any])
    {
        let model = ModelChatMessage () // Model creation
        model.updateModel(usingDictionary: dictionary) // Updating model
        arrayChatMessages.append(model)
        
        tableChatList.reloadData()
        
        tableChatList.scrollToRow(at: IndexPath (row: arrayChatMessages.count-1, section: 0), at: .top, animated: true)
    }
    
    //MARK:- Notification Socket Connected
    
    /// Notification When Socket Is Connected To Network
    @objc func notificationSocketConnected()
    {
        buttonSend.isEnabled = true
        buttonSend.isUserInteractionEnabled = true
        
        hideIndicator()
        
        getChatHistoryFromSocket()
    }
    
    //MARK:- Reachability Internet Change
    /// Get Notfied If NetWork Status is Changed
    @objc func internetReachabilityChanged(notification: Notification)
    {
        if let isReachable = notification.object as? Bool
        {
            //print("chatinternet ",isReachable)
        
            if !isReachable
            {
                showIndicator()
                buttonSend.isEnabled = false
            }
        }
    }
    
    //MARK:- Notification Keyboard Frame Change
    
    /// Keyboard Will Change Their Frame
    @objc func keyboardWillChangeFrame(notification: Notification)
    {
        if let userInfo = notification.userInfo
        {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let duration:TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue
            let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
          
            if (endFrame?.origin.y)! >= UIScreen.main.bounds.size.height
            {
                self.constraintContainerBottom.constant = 0.0
            }
            else
            {
                self.constraintContainerBottom.constant = endFrame?.size.height ?? 0.0
            }
            
            weak var weakSelf = self
            
            UIView.animate(withDuration: duration, delay: TimeInterval(0), options: animationCurve, animations:
            {
                weakSelf?.view.layoutIfNeeded()
            }, completion: nil)
        }
    }
    
    //MARK: -
    
    /// Text inputMode Changed Notification Method
    @objc func textInputModeChangedNotification()
    {
        self.inputTextView.reloadInputViews()
    }
}


/// UITableView DataSource And Delegate Method Extension
extension ChatViewController : UITableViewDelegate, UITableViewDataSource, UITableViewDataSourcePrefetching
{
    //MARK:- UITableView Delegate and Datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrayChatMessages.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var cell : ChatViewCell
        
        var index = 1
        
        if arrayChatMessages[indexPath.row].isMsgSendByMe
        {
            if arrayChatMessages[indexPath.row].isMedia
            {
                index = 8
            }
            else
            {
                index = 1
            }
        }
        else
        {
            if arrayChatMessages[indexPath.row].isMedia
            {
                index = 9
            }
            else
            {
                index = 2
            }
        }
        
        let cellOther : ChatViewCell = (Bundle.main.loadNibNamed("ChatViewCell", owner: self, options: nil)?[index] as? ChatViewCell)!
        
        cell = cellOther
        
        cell.selectionStyle = .none
        
        cell.updateCell(usingModel: arrayChatMessages[indexPath.row], cellIndex: indexPath.row, folderPath: "")
        
        if index == 8
        {
            cell.delegate = self
            cell.buttonUserMedia.tag = indexPath.row
        }
        else if index == 9
        {
            cell.delegate = self
            cell.buttonSenderMedia.tag = indexPath.row
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath])
    {
        
    }
}

extension ChatViewController : ChatViewCellDelegate
{
    func chatCell(_ cell: ChatViewCell, didTapOnLikeUnLikeButton likeUnLikeButton: UIButton)
    {
        
    }
    
    func chatCellDidDownloadTheMedia(_ cell: ChatViewCell)
    {
        
    }
    
    func chatCell(_ cell: ChatViewCell, didTapOnMedia mediaButton: UIButton)
    {
        
    }
    
    func ChatViewCellUserImageButtonButtonAction(_ cell: ChatViewCell)
    {
        let tag = cell.buttonUserMedia.tag
        //print("user media button tag = ", tag)
        
        var arrayChatMessagesSendToMedaiView = [ModelChatMessage]()
        
        for message in arrayChatMessages
        {
            if message.isMedia
            {
                arrayChatMessagesSendToMedaiView.append(message)
            }
        }
        
        var selectedIndex = 0
        
        for (i, message) in arrayChatMessagesSendToMedaiView.enumerated()
        {
            if message.messageID == arrayChatMessages[tag].messageID
            {
                selectedIndex = i
            }
        }
        
        //print("selected index = ", selectedIndex)
        //print("media count = \(arrayChatMessagesSendToMedaiView.count)")
        
        let zoomVC = UIStoryboard.getMyRecordStoryBoard().instantiateViewController (withIdentifier: "ShowPhotosAndZoomVC") as! ShowPhotosAndZoomViewController
        
        zoomVC.arrayChatMessagesSendToMedaiView = arrayChatMessagesSendToMedaiView
        zoomVC.selectedIndex = selectedIndex
        zoomVC.isFromChatController = true
        
        self.navigationController?.pushViewController(zoomVC, animated: true)
    }
    
    func ChatViewCellSenderImageButtonButtonAction(_ cell: ChatViewCell)
    {
        let tag = cell.buttonSenderMedia.tag
        //print("sender media button tag = ", tag)
        
        var arrayChatMessagesSendToMedaiView = [ModelChatMessage]()
        
        for message in arrayChatMessages
        {
            if message.isMedia
            {
                arrayChatMessagesSendToMedaiView.append(message)
            }
        }
        
        var selectedIndex = 0
        
        for (i, message) in arrayChatMessagesSendToMedaiView.enumerated()
        {
            if message.messageID == arrayChatMessages[tag].messageID
            {
                selectedIndex = i
            }
        }
        
        //print("selected index = ", selectedIndex)
        //print("media count = \(arrayChatMessagesSendToMedaiView.count)")
        
        let zoomVC = UIStoryboard.getMyRecordStoryBoard().instantiateViewController (withIdentifier: "ShowPhotosAndZoomVC") as! ShowPhotosAndZoomViewController
        
        zoomVC.arrayChatMessagesSendToMedaiView = arrayChatMessagesSendToMedaiView
        zoomVC.selectedIndex = selectedIndex
        zoomVC.isFromChatController = true
        
        self.navigationController?.pushViewController(zoomVC, animated: true)
    }
}

///MARK:- Extension For TextView Delegate Methods
extension ChatViewController : UITextViewDelegate, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate, NSLayoutManagerDelegate
{
    func textViewDidChange(_ textView: UITextView)
    {
        self.inputTextView.isScrollEnabled = false
        
        let newHeight = textView.intrinsicContentSize.height + (constraintContainerTextBoxHeight.constant - textView.frame.size.height)
        
        constraintContainerTextBoxHeight.constant = max(originalContainerTextBoxHeight, min(newHeight, MaxChatWindowHeight))
        
        if constraintContainerTextBoxHeight.constant >= MaxChatWindowHeight
        {
            self.inputTextView.isScrollEnabled = true
            return
        }
        
        UIView.animate(withDuration: 0.25, delay: 0.0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: .curveLinear, animations:
            {
                self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    //MARK:- DZNEmptyDataSetSource -> Title
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString!
    {
        let text = "No conversation"
        
        let attributes = [NSAttributedStringKey.font:UIFont .boldSystemFont(ofSize: 16*scaleFactorX), NSAttributedStringKey.foregroundColor:UIColor.white]
        
        return NSAttributedString (string: text, attributes: attributes)
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool
    {
        return true
    }
    
    func layoutManager(_ layoutManager: NSLayoutManager, lineSpacingAfterGlyphAt glyphIndex: Int, withProposedLineFragmentRect rect: CGRect) -> CGFloat
    {
        return 3
    }
}



/// MARK: - Extension For UIImagePicker Delegate Methods
extension ChatViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        picker.dismiss(animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        if (info[UIImagePickerControllerEditedImage] as? UIImage) != nil
        {
            picker.dismiss(animated: false, completion: { () -> Void in
                
                _ = UtilityClass.addFileToFolder("saved", fileName: "save.jpg", fileData: UIImageJPEGRepresentation((info[UIImagePickerControllerEditedImage] as? UIImage)!, 0.6)!)
                
                if let fileURL = UtilityClass.getFileURLFromFolder("saved", fileName: "save.jpg")
                {
                    self.uploadImage(usingImage: fileURL as NSURL)
                }
            })
        }
    }
    
    func uploadImage(usingImage : NSURL)
    {
        HUD.show(.systemActivity, onView: self.view.window)
        
        let ext = "jpg"
        
        let keyName = UtilityClass.getCurrentTimeStamp() + ".jpg"
        
        // myancare-images = myancare-prod-images
        let uploadRequest = AWSS3TransferManagerUploadRequest()
        uploadRequest?.body = usingImage as URL
        uploadRequest?.key = keyName
        uploadRequest?.bucket = AWSBucket + "-images/" + "socketchat/" + chatRoomID!
        uploadRequest?.contentType = ext
        uploadRequest?.acl = .publicReadWrite
        
        let transferManager = AWSS3TransferManager.default()
        transferManager.upload(uploadRequest!).continueWith { (task) -> AnyObject! in
            
            if let error = task.error
            {
                print("Upload failed  (\(error))")
                
                DispatchQueue.main.async(execute: {
                    HUD.hide()
                    return
                })
            }
            
            if task.result != nil
            {
                print("Uploaded to:fksjflksdajf askldjf")
                
                DispatchQueue.main.async(execute: {
                    
                    HUD.hide()
                    
                    var fileUrl = "socketchat/" as NSString
                    fileUrl = fileUrl.appending(self.chatRoomID!) as NSString
                    fileUrl = fileUrl.appending("/\(keyName)") as NSString
                    
                    print("file url = ", fileUrl)
                    
                    if SocketManagerHandler.sharedInstance().isSocketConnected()
                    {
                        SocketManagerHandler.sharedInstance().sendMessage ("Media".base64Encoded(), self.senderID, filePath: fileUrl as String, conversationID: self.chatRoomID!) {[weak self] (messageDict, statusCode) in
                            
                            guard let `self` = self else {return}
                            
                            self.buttonSend.isUserInteractionEnabled = true
                            
                            print("media message dictionary ====> ", messageDict)
                            
                            if statusCode == 200
                            {
                                self.inputTextView.text = ""
                                self.textViewDidChange(self.inputTextView)
                                self.addNewModel(usingDictionary: messageDict)
                            }
                        }
                    }
                    
                    return
                })
            }
            else
            {
                print("Unexpected empty result.")
                DispatchQueue.main.async(execute: {
                    HUD.hide()
                    return
                })
            }
            
            return nil
        }
    }
}
