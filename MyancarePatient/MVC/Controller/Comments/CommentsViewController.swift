//
//  CommentsViewController.swift
//  MyancarePatient
//
//  Created by Sumit on 11/01/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit
import PKHUD

///Comment View Delegate
protocol CommentsViewDelegate {
    
    /// Update Comments Count
    ///
    /// - Parameter commentsCount: Count Value
    func updateCommentsCount(commentsCount : Int)
}

///Comments View Controller Class
class CommentsViewController: UIViewController{
    
    ///UITextView Refrence - Comment Text View
    @IBOutlet weak var commentTextView: UITextView!
    //Mark :- Properties
    ///UITableView Refrence - Table View Comments
    @IBOutlet var tableViewComments: UITableView!
    ///UIView Refrence Variable - View BOttom
    @IBOutlet var viewBottom: UIView!
    ///UIImageView Refrence Variable - text View Message
    @IBOutlet var textViewMessage: UIImageView!
    ///UITableViewCell Refrence Variable -
    var cellObj = CommentsTableCustomCell()
    ///CommentsViewDelegate Refrence Variable
    var commentsDelegate : CommentsViewDelegate?
    
    ///NSLayoutConstraint Refrence Variable - tableViewBottomConstraint
    @IBOutlet weak var tableViewBottomConstraint: NSLayoutConstraint!
    ///NSLayoutConstraint Refrence Variable - bottomViewConstraint
    @IBOutlet weak var bottomViewConstraint: NSLayoutConstraint!
    
    ///Array Comment - Contains Comment Model Data
    var arrayComment = [CommentModel]()
    ///String Variable - Contains Article ID
    var articleID = ""
    ///String Varible - Comment placeHolder
    let commentPlaceHolder = "How do you think?".localized()
    
    //MARK: - viewDidLoad
    ///UIViewController LIfe Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        commentTextView.text = commentPlaceHolder
        
        // Keyobard frame change notification
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChangeFrame), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(textInputModeChangedNotification), name: NSNotification.Name.UITextInputCurrentInputModeDidChange, object: nil)
    }
    
    //MARK: - viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tabBarController?.tabBar.isHidden = true
        
        UtilityClass.disableIQKeyboard()
        
        self.navigationController?.isNavigationBarHidden = false
        
        setUpNavigationBar()
    }
    
    //MARK: - viewWillDisappear
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        UtilityClass.enableIQKeyboard()
        
        self.tabBarController?.tabBar.isHidden = false
        
        self.navigationController?.isNavigationBarHidden = true
        
        guard let delegate1 = commentsDelegate else {
            return
        }
        
        delegate1.updateCommentsCount(commentsCount: arrayComment.count)
    }
    
    //MARK: - viewDidAppear
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        
        getCommentDataFromAPI()
    }
    
    //MARK: - Deinit
    deinit {
        print("CommentsViewController deinit")
    }
    
    //MARK: - setUpNavigationBar
    ///setUpNavigationBar
    func setUpNavigationBar() {
        
        self.title = "Comments".localized()
        
        self.navigationController?.isNavigationBarHidden = false
        
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSAttributedStringKey.font: UIFont.createPoppinsBoldFont(withSize: 18.0)
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        // set navigation bar color
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 30/255.0, green: 197/255.0, blue: 155/255.0, alpha: 1.0)
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "back_white"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
    }
    
    //MARK: - Navigation bar Back Button Action
    ///Navigation bar Back Button Action
    @objc func backButtonPressed() {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - textInputModeChangedNotification
    ///textInputModeChangedNotification
    @objc func textInputModeChangedNotification() {
        print(UITextInputMode .activeInputModes)
        self.commentTextView.reloadInputViews()
    }
    
    //MARK:- Notification Keyboard Frame Change
    ///Notification Keyboard Frame Change
    @objc func keyboardWillChangeFrame(notification: Notification)
    {
        if let userInfo = notification.userInfo {
            
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let duration:TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue
            let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            
            if (endFrame?.origin.y)! >= UIScreen.main.bounds.size.height {
                self.bottomViewConstraint.constant = 0.0
                self.tableViewBottomConstraint.constant = 61.0
            } else {
                self.bottomViewConstraint.constant = endFrame?.size.height ?? 0.0
                self.tableViewBottomConstraint.constant = ((endFrame?.size.height)!+61.0)
            }
            
            weak var weakSelf = self
            
            UIView.animate(withDuration: duration, delay: TimeInterval(0), options: animationCurve, animations:
                {
                    weakSelf?.view.layoutIfNeeded()
            },
            completion: nil)
        }
    }
    
    // Mark: - IBAction
    //MARK: - on Send Comment Button Action
    ///on Send Comment Button Action
    @IBAction func buttonSendTapped(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        if commentTextView.text == commentPlaceHolder {
            UtilityClass.showAlertWithTitle(title: App_Name, message: "Please enter comment".localized(), onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                
            })
        } else if commentTextView.text == "" {
            UtilityClass.showAlertWithTitle(title: App_Name, message: "Please enter comment".localized(), onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                
            })
        } else {
            callPostCommentAPi()
            commentTextView.text = commentPlaceHolder
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    //MARK: - get comments data from api
    ///get comments data from api
    func getCommentDataFromAPI()
    {
        let urlToHit = EndPoints.get_comment(articleID).path
        print(urlToHit)
        
        HUD.show(.systemActivity, onView: self.view.window)
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod:.post, parameters: nil) {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    let commentData = responseDictionary["data"] as? [[String : Any]]
                    
                    guard commentData != nil else
                    {
                        return
                    }
                    
                    self.updateCommentModelArray(usingArray: commentData!)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: {(buttonIndex) in
                        
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    UtilityClass.showAlertWithTitle(title: "", message: replyMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                        
                    })
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }
    
    //MARK: - call post comment api
    ///call post comment api
    func callPostCommentAPi()
    {
        HUD.show(.systemActivity, onView: self.view.window)
        
        let dictParamComment = ["article_id" : articleID, "comment" : commentTextView.text] as [String : Any]
        
        let urlToHit =  EndPoints.comment.path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .post, parameters: dictParamComment, shouldDeserialize: true)
        {[weak self] (data, dictionary, statusCode, error) in
            
            //HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    self.getCommentDataFromAPI()
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                        
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    UtilityClass.showAlertWithTitle(title: "", message: replyMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                        
                    })
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }
    
    //MARK:- Updating Comment Model Array
    ///Updating Comment Model Array
    func updateCommentModelArray(usingArray array : [[String:Any]]) -> Void
    {
        arrayComment.removeAll()
        
        let dataArray = array
        
        for dict in dataArray // Iterating dictionaries
        {
            let model = CommentModel () // Model creation
            model.updateCommentModel(usingDictionary: dict) // Updating model
            arrayComment.append(model) // Adding model to array
        }
        
        arrayComment = arrayComment.reversed()
        
        tableViewComments.reloadData()
        
        if arrayComment.count > 0 {
            self.tableViewComments.scrollToRow(at: IndexPath(row: arrayComment.count - 1, section: 0), at: .bottom, animated: true)
        }
    }
}

//MARK:- Extention -> UITableView -> UITableViewDelegate, UITableViewDataSource
///Extention -> UITableView -> UITableViewDelegate, UITableViewDataSource
extension CommentsViewController :UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayComment.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: CommentsTableCustomCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier_CommentsCell) as! CommentsTableCustomCell
        
        cell.labelComments.text = arrayComment[indexPath.row].commentDescription
        
        cell.labelName.text = arrayComment[indexPath.row].userProfile!["name"] as? String
        
        cell.userImageView.setShowActivityIndicator(true)
        cell.userImageView.setIndicatorStyle(.gray)
        
        cell.userImageView.sd_setImage(with: URL.init(string: (arrayComment[indexPath.row].userProfile!["avatar_url"] as? String)!), placeholderImage: #imageLiteral(resourceName: "no_image_user"))
        
        cell.labelDate.text = UtilityClass.getDateStringFromTimeStamp(timeStamp: arrayComment[indexPath.row].commentTime!) as String
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return (130 * scaleFactorX)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
}

//MARK: - Extension -> UITextView -> UITextViewDelegate
///Extension -> UITextView -> UITextViewDelegate
extension CommentsViewController : UITextViewDelegate
{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == commentPlaceHolder {
            textView.text = ""
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = commentPlaceHolder
        }
    }
}
