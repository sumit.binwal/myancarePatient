//
//  DoctorDetailViewController.swift
//  MyancarePatient
//
//  Created by iOS on 29/01/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit
import ScrollableSegmentedControl
import PKHUD

///Model Appointment Refrence Data
var modelAppointmentData = ModelAppointment()

///Doctor Detail View Controller
class DoctorDetailViewController: UIViewController {

    ///Scrollable Segmented Control Refrence Variable
    @IBOutlet fileprivate weak var segmentedControl: ScrollableSegmentedControl!
    
    //Mark:- Properties
    ///Doctor Model Data Refrence Variable
    var doctorModelData = DoctorModel()
    ///UIImageView Refrence - Segment Bacground Image
    @IBOutlet weak var segmentBgImage: UIImageView!
    ///UIBUtton Refrence - Back Button
    @IBOutlet weak var bookButton: UIButton!
    ///UIView Refrence - table Bottom View
    @IBOutlet weak var tableBottomView: UIView!
    ///UIImageView Refrence - Doctor Status Image View
    @IBOutlet weak var doctorStatusImageView: UIImageView!
    ///UIImageView Refrence - Doctor ImageView
    @IBOutlet weak var doctorImageView: UIImageView!
    ///UILabel Refrence - doctor Name Label Text
    @IBOutlet weak var doctorNameLabel: UILabel!
    ///UILabel Refrence - doctor Specialization Label Text
    @IBOutlet weak var doctorSpecializationLabel: UILabel!
    ///UILabel Refrence - Doctor Review label Text
    @IBOutlet weak var doctorReviewLabel: UILabel!
    
    ///UIView Refrence - Table Header View
    @IBOutlet var tableHeaderView: UIView!
    ///UIVIew Refrence - Container View
    @IBOutlet fileprivate weak var containerView: UIView!
    ///UIScrollView Refrence - scrollView
    @IBOutlet fileprivate weak var scrollView: UIScrollView!
    
    ///Variable String - Store doctor ID
    var doctor_id = ""
    
    ///ViewControllers Array
    fileprivate lazy var viewControllers: [UIViewController] = {
        return self.preparedViewControllers()
    }()
    
    //MARK: - viewDidLoad
    ///UIViewController Life Cycle Method
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        tableHeaderView.isHidden = true
        segmentBgImage.isHidden = true
        segmentedControl.isHidden = true
        containerView.isHidden = true
        tableBottomView.isHidden = true
        
        segmentedControl.layer.borderColor = UIColor.lightGray.cgColor
        segmentedControl.layer.borderWidth = 1.0
        segmentedControl.layer.masksToBounds = true
        
        doctorImageView.layer.cornerRadius = (doctorImageView.frame.size.width/2)*scaleFactorX
        doctorImageView.layer.masksToBounds = true
        doctorImageView.layer.borderColor = UIColor.lightGray.cgColor
        doctorImageView.layer.borderWidth = 1.0*scaleFactorX
        
        setupScrollView()
        
        segmentedControl.segmentStyle = .imageOnTop
        
        segmentedControl.selectedSegmentContentColor = UIColor.MyanCarePatient.colorForDoctorSegmentSelected
        
        segmentedControl.underlineSelected = true
        segmentedControl.selectedSegmentIndex = 0
        
        segmentedControl.insertSegment(withTitle: "Biography".localized(), image: #imageLiteral(resourceName: "recent"), at: 0)
        segmentedControl.insertSegment(withTitle: "Working Address".localized(), image: #imageLiteral(resourceName: "working_address_un_selected"), at: 1)
        segmentedControl.insertSegment(withTitle: "Reviewer".localized(), image: #imageLiteral(resourceName: "reviewer_un_selected"), at: 2)
        
        segmentedControl.addTarget(self, action: #selector(DoctorDetailViewController.segmentSelected(sender:)), for: .valueChanged)
    }
    
    ///Segement Selected Method
    @objc func segmentSelected(sender:ScrollableSegmentedControl) {
        
        let contentOffsetX = scrollView.frame.width * CGFloat(sender.selectedSegmentIndex)
        scrollView.setContentOffset(CGPoint(x: contentOffsetX, y: 0), animated: true)
        
        for controller in (sender.viewController()?.childViewControllers)!
        {
            if sender.selectedSegmentIndex == 0
            {
                if controller.isKind(of: DoctorDetailTableViewController.self)
                {
                    let controller1 = controller as! DoctorDetailTableViewController
                    
                    controller1.selectedSegmentIndex = 0
                    
                    controller1.getDoctorProfileDataFromAPI()
                    
                    return
                }
            }
            
            if sender.selectedSegmentIndex == 1
            {
                if controller.isKind(of: DoctorDetailTableViewController.self)
                {
                    let controller1 = controller as! DoctorDetailTableViewController
                    
                    controller1.workingAddressData.removeAll()
                    controller1.selectedSegmentIndex = 1
                    
                    controller1.getWorkingAddressListFromAPI()
                    
                    return
                }
            }
            
            if sender.selectedSegmentIndex == 2
            {
                if controller.isKind(of: DoctorDetailTableViewController.self)
                {
                    let controller1 = controller as! DoctorDetailTableViewController
                    
                    controller1.reviewModelData.removeAll()
                    controller1.selectedSegmentIndex = 2
                    
                    controller1.getDoctorReviewerListFromAPI()
                    
                    return
                }
            }
        }
    }
    
    // MARK: - viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        self.tabBarController?.tabBar.isHidden = true
        
        setUpNavigationBar()
        
        getDoctorProfileDataFromAPI()
    }
    
    // MARK: - viewWillDisappear
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.isNavigationBarHidden = true
    }
    
    // MARK: - viewDidAppear
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    //MARK: - Deinit
    deinit {
        print("DoctorDetailViewController deinit")
    }
    
    //Mark :- get doctor profile data from api
    ///get doctor profile data from api
    func getDoctorProfileDataFromAPI()
    {
        let urlToHit = EndPoints.get_profile(doctor_id).path
        
        HUD.show(.systemActivity, onView: self.view.window)
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod:.get, parameters: nil) {[weak self] (data, dictionary, statusCode, error) in
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                HUD.hide()
                
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            
            if dictionary == nil
            {
                HUD.hide()
                
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    let appointmentDtaArr1 = responseDictionary["data"] as? [String : Any]
                    
                    guard appointmentDtaArr1 != nil else
                    {
                        return
                    }
                    
                    self.updateDoctorModel(dict: appointmentDtaArr1!)
                }
                else
                {
                    HUD.hide()
                    
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: {(buttonIndex) in
                        
                    })
                }
            }
            else
            {
                HUD.hide()
                
                let responseDictionary = dictionary!
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    UtilityClass.showAlertWithTitle(title: "", message:replyMsg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }
    
    ///UPdate doctor Model
    func updateDoctorModel (dict : [String : Any])
    {
        let model = DoctorModel () // Model creation
        model.updateModel(usingDictionary: dict) // Updating model
       
        doctorModelData = model
        
        setUpView()
    }
    
    //MARK: - setUpView
    ///SetupView Method
    func setUpView()
    {
        tableHeaderView.isHidden = false
        segmentBgImage.isHidden = false
        segmentedControl.isHidden = false
        containerView.isHidden = false
        tableBottomView.isHidden = false
        
        bookButton.setTitle("Book Appointment".localized(), for: .normal)
        
        doctorNameLabel.text = doctorModelData.doctorName
        
        let arrSpec : NSArray = doctorModelData.specializations! as NSArray
        var specilaization = ""
        
        for i in 0 ..< arrSpec.count {
            let dict = arrSpec.object(at: i) as! NSDictionary
            specilaization = specilaization.appending(dict.object(forKey: "name") as! String)
            
            if i != arrSpec.count - 1 {
                specilaization = specilaization.appending(", ")
            }
        }
        
        doctorSpecializationLabel.text = specilaization
        
        doctorImageView.setShowActivityIndicator(true)
        doctorImageView.setIndicatorStyle(.gray)
        
        doctorImageView.sd_setImage(with: URL.init(string: doctorModelData.imageUrl!), placeholderImage: #imageLiteral(resourceName: "no_image_user"))
        
        if doctorModelData.online_status == "Online"
        {
            doctorStatusImageView.image = #imageLiteral(resourceName: "greenDot")
        }
        else if doctorModelData.online_status == "Busy"
        {
            doctorStatusImageView.image = #imageLiteral(resourceName: "redDot")
        }
        else
        {
            doctorStatusImageView.image = #imageLiteral(resourceName: "greyDot")
        }
        
        doctorReviewLabel.text = "(" + "\(String(doctorModelData.review_count)) " + "reviewer".localized() + ")"
        
        getDoctorStatusApi(doctor_id: doctorModelData.doctorID!)
        
        HUD.hide()
    }
    
    // MARK: - setUpNavigationBar
    ///setUpNavigationBar
    func setUpNavigationBar() {
        
        self.title = "Doctor Detail".localized()
        
        self.navigationController?.isNavigationBarHidden = false
        
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSAttributedStringKey.font: UIFont.createPoppinsBoldFont(withSize: 18.0)
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        // set navigation bar color
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 30/255.0, green: 197/255.0, blue: 155/255.0, alpha: 1.0)
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "back_white"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
        
        if doctorModelData.isLikeByMe! {
            let img = UIImage(named: "article-detail-like-icon")!.withRenderingMode(UIImageRenderingMode.alwaysOriginal);
            
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: img, style: UIBarButtonItemStyle.plain, target: self, action: #selector(likeButtonPressed))
        } else {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "article-detail-unlike"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(likeButtonPressed))
        }
    }
    
    // MARK: - Navigation bar Back Button Action
    ///Navigation bar Back Button Action
    @objc func backButtonPressed() {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Navigation bar Like Button Action
    /// Navigation bar Like Button Action
    @objc func likeButtonPressed () {
        doctorLikeUnLikeApiCall()
    }
    
    //MARK: - get doctor status from API
    ///get doctor status from API
    @objc func getDoctorStatusApi(doctor_id : String) {
        
        self.view.endEditing(true)
        
        let urlToHit = EndPoints.get_status(doctor_id).path
        
        print("doctor status url = \(urlToHit)")
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod:.get, parameters: nil) {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                return
            }
            
            if dictionary == nil
            {
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    let doctorData = responseDictionary["data"] as? [String : Any]
                    
                    let status = (doctorData!["current_status"] as! String).capitalized
                    
                    if status == "Online"
                    {
                        self.doctorStatusImageView.image = #imageLiteral(resourceName: "greenDot")
                    }
                    else if status == "Busy"
                    {
                        self.doctorStatusImageView.image = #imageLiteral(resourceName: "redDot")
                    }
                    else
                    {
                        self.doctorStatusImageView.image = #imageLiteral(resourceName: "greyDot")
                    }
                }
            }
        }
    }
    
    //MAR:- like/unlike api calling
    ///like/unlike api calling
    func doctorLikeUnLikeApiCall()
    {
        self.view.endEditing(true)
        
        let params = ["doctor" : doctorModelData.doctorID ?? ""] as [String : Any]
        
        print("parameters = ", params)
        
        let urlToHit =  EndPoints.doctorFavourite.path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .post, parameters: params, shouldDeserialize: true)
        {[weak self] (data, dictionary, statusCode, error) in
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    let dict = responseDictionary["data"] as! [String : Any]
                    
                    let favorite1 = dict["favorite"] as! Bool
                    
                    if favorite1
                    {
                        self.doctorModelData.isLikeByMe = true
                        
                        self.navigationItem.rightBarButtonItem = nil
                        
                        let img = UIImage(named: "article-detail-like-icon")!.withRenderingMode(UIImageRenderingMode.alwaysOriginal);
                        
                        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: img, style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.likeButtonPressed))
                    }
                    else
                    {
                        self.doctorModelData.isLikeByMe = false
                        
                        self.navigationItem.rightBarButtonItem = nil
                        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "article-detail-unlike"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.likeButtonPressed))
                    }
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: {(buttonIndex) in
                        
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    UtilityClass.showAlertWithTitle(title: "", message:replyMsg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }
    
    // MARK: - on Book Appointment Button Clicked
    ///on Book Appointment Button Clicked
    @IBAction func bookAppointmentClickedAction(_ sender: UIButton)
    {
        var arrVal = [] as [String]
        var arrValTax = [] as [String]
        var arrValTotal = [] as [String]
        var buttonVal = [] as [String]

        if let voicePrice = doctorModelData.rate!["voice"]
        {
            let voiceTaxPrice = doctorModelData.rate!["voice_service_tax"]
            let voiceTotalPrice = doctorModelData.rate!["voice_total_consultant_fees"]
            
            if voicePrice as! Int > 0
            {
                var voicePrice1 = "Voice".localized() as NSString
                
                buttonVal.append(voicePrice1 as String)
                
                voicePrice1 = voicePrice1.appending(" - ") as NSString
                voicePrice1 = voicePrice1.appending(String(voicePrice as! Int)) as NSString
                voicePrice1 = voicePrice1.appending(" ") as NSString
                voicePrice1 = voicePrice1.appending("Kyat".localized()) as NSString
        
                arrVal.append(voicePrice1 as String)
                
                let voiceTaxPrice1 = voiceTaxPrice as! Double
                let voiceTaxPrice2 = NSString(format : "%.2f", voiceTaxPrice1)
                
                arrValTax.append(String(voiceTaxPrice2))
                
                let voiceTotalPrice1 = voiceTotalPrice as! Double
                let voiceTotalPrice2 = NSString(format : "%.2f", voiceTotalPrice1)
                
                arrValTotal.append(String(voiceTotalPrice2))
            }
        }
        
        if let vedioPrice = doctorModelData.rate!["video"]
        {
            let voiceTaxPrice = doctorModelData.rate!["video_service_tax"]
            let voiceTotalPrice = doctorModelData.rate!["video_total_consultant_fees"]
            
            if vedioPrice as! Int > 0
            {
                var voicePrice1 = "Video".localized() as NSString
                
                buttonVal.append(voicePrice1 as String)
                
                voicePrice1 = voicePrice1.appending(" - ") as NSString
                voicePrice1 = voicePrice1.appending(String(vedioPrice as! Int)) as NSString
                voicePrice1 = voicePrice1.appending(" ") as NSString
                voicePrice1 = voicePrice1.appending("Kyat".localized()) as NSString
                
                arrVal.append(voicePrice1 as String)
                
                let voiceTaxPrice1 = voiceTaxPrice as! Double
                let voiceTaxPrice2 = NSString(format : "%.2f", voiceTaxPrice1)
                
                arrValTax.append(String(voiceTaxPrice2))
                
                let voiceTotalPrice1 = voiceTotalPrice as! Double
                let voiceTotalPrice2 = NSString(format : "%.2f", voiceTotalPrice1)
                
                arrValTotal.append(String(voiceTotalPrice2))
            }
        }

        if let chatPrice = doctorModelData.rate!["chat"]
        {
            let voiceTaxPrice = doctorModelData.rate!["chat_service_tax"]
            let voiceTotalPrice = doctorModelData.rate!["chat_total_consultant_fees"]
            
            if chatPrice as! Int > 0
            {
                var voicePrice1 = "Chat".localized() as NSString
                
                buttonVal.append(voicePrice1 as String)
                
                voicePrice1 = voicePrice1.appending(" - ") as NSString
                voicePrice1 = voicePrice1.appending(String(chatPrice as! Int)) as NSString
                voicePrice1 = voicePrice1.appending(" ") as NSString
                voicePrice1 = voicePrice1.appending("Kyat".localized()) as NSString
                
                arrVal.append(voicePrice1 as String)
                
                let voiceTaxPrice1 = voiceTaxPrice as! Double
                let voiceTaxPrice2 = NSString(format : "%.2f", voiceTaxPrice1)
                
                arrValTax.append(String(voiceTaxPrice2))
                
                let voiceTotalPrice1 = voiceTotalPrice as! Double
                let voiceTotalPrice2 = NSString(format : "%.2f", voiceTotalPrice1)
                
                arrValTotal.append(String(voiceTotalPrice2))
            }
        }
        
        UtilityClass.showActionSheetWithTitle(title: "", message: "Choose Services".localized(), onViewController: self, withButtonArray: buttonVal ) { (buttonIndex) in
            
                if arrVal[buttonIndex].contains("Chat".localized())
                {
                    var str1 = arrVal[buttonIndex]
                    
                    str1 = str1.replacingOccurrences(of: "Chat".localized(), with: "")
                    str1 = str1.replacingOccurrences(of: " - ", with: "")
                    str1 = str1.replacingOccurrences(of: "(", with: "")
                    str1 = str1.replacingOccurrences(of: ")", with: "")
                    
                    modelAppointmentData.serviceFees = str1
                    modelAppointmentData.appointmentType = "chat"
                    modelAppointmentData.service_tax = arrValTax[buttonIndex]
                    modelAppointmentData.total_consultant_fees = arrValTotal[buttonIndex]
                }
            
                if arrVal[buttonIndex].contains("Voice".localized())
                {
                    var str1 = arrVal[buttonIndex]
                    str1 = str1.replacingOccurrences(of: "Voice".localized(), with: "")
                    str1 = str1.replacingOccurrences(of: " - ", with: "")
                    str1 = str1.replacingOccurrences(of: "(", with: "")
                    str1 = str1.replacingOccurrences(of: ")", with: "")
                    
                    modelAppointmentData.serviceFees = str1
                    modelAppointmentData.appointmentType = "voice"
                    modelAppointmentData.service_tax = arrValTax[buttonIndex]
                    modelAppointmentData.total_consultant_fees = arrValTotal[buttonIndex]
                }
            
                if arrVal[buttonIndex].contains("Video".localized())
                {
                    var str1 = arrVal[buttonIndex]
                    str1 = str1.replacingOccurrences(of: "Video".localized(), with: "")
                    str1 = str1.replacingOccurrences(of: " - ", with: "")
                    str1 = str1.replacingOccurrences(of: "(", with: "")
                    str1 = str1.replacingOccurrences(of: ")", with: "")
                    
                    modelAppointmentData.serviceFees = str1
                    modelAppointmentData.appointmentType = "video"
                    modelAppointmentData.service_tax = arrValTax[buttonIndex]
                    modelAppointmentData.total_consultant_fees = arrValTotal[buttonIndex]
                }
            
                modelAppointmentData.doctorID = self.doctorModelData.doctorID!
            
                modelAppointmentData.doctorName = self.doctorModelData.doctorName!
            
                modelAppointmentData.issueDate = UtilityClass.getStringFromDateFormat(date: Date(), formate: "dd MMM yyyy | hh:mm a")
            
                modelAppointmentData.isReschedule = false
            
            if arrVal[buttonIndex].contains("Chat".localized())
            {
                self.chatServiceFromAPI()
            }
            else
            {
                let schdeuleVC = UIStoryboard.getScheduleAppointmentStoryBoard().instantiateInitialViewController()
                self.navigationController?.pushViewController(schdeuleVC!, animated: true)
            }
        }
    }
    
    //Mark :- chat service api
    ///chat service api
    func chatServiceFromAPI()
    {
        let urlToHit = EndPoints.chat_service(doctorModelData.doctorID!).path
        
        HUD.show(.systemActivity, onView: self.view.window)
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod:.get, parameters: nil) {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    let replyCode = responseDictionary["code"] as? String
                    
                    if replyCode == "CHAT_CONTINUE" {
                        
                        let data = responseDictionary["data"] as! [String : Any]
                        
                        UtilityClass.showAlertWithTitle(title: App_Name, message:data["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: {(buttonIndex) in
                            
                            if data["enable"] as! Int == 0
                            {
                                
                            }
                            else
                            {
                                guard let appDeleg = appDelegate as? AppDelegate else {
                                    return
                                }
                                
                                if !appDeleg.isInternetAvailable()
                                {
                                    UtilityClass.showAlertWithTitle(title: App_Name, message: "No Active Internet Connection Found".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                                    return
                                }
                                
                                let chatView = UIStoryboard.getChatViewStoryBoard().instantiateViewController(withIdentifier: "chatViewController") as! ChatViewController
                                
                                chatView.senderID = self.doctorModelData.doctorID!
                                chatView.chatUsername = self.doctorModelData.doctorName!
                                
                                self.navigationController?.pushViewController(chatView, animated: true)
                            }
                        })
                        
                    } else {
                        
                        modelAppointmentData.scheduleDate = modelAppointmentData.issueDate
                        
                        let serviceInvoiceVC = UIStoryboard.getScheduleAppointmentStoryBoard().instantiateViewController(withIdentifier: "InvioceVC") as! InvioceViewController
                        self.navigationController?.pushViewController(serviceInvoiceVC, animated: true)
                    }
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: {(buttonIndex) in
                        
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    UtilityClass.showAlertWithTitle(title: "", message:replyMsg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }
    
    // Example viewControllers
    ///Example viewControllers
    
    fileprivate func preparedViewControllers() -> [UIViewController] {
        
        let storyboard = self.storyboard
        
        let firstViewController = storyboard?
            .instantiateViewController(withIdentifier: "DoctorDetailTVC") as! DoctorDetailTableViewController
        firstViewController.selectedSegmentIndex = 0
        firstViewController.doctor_id = self.doctor_id
        
        let secondViewController = storyboard?
            .instantiateViewController(withIdentifier: "DoctorDetailTVC") as! DoctorDetailTableViewController
        secondViewController.selectedSegmentIndex = 1
        secondViewController.doctor_id = self.doctor_id
        
        let thirdViewController = storyboard?
            .instantiateViewController(withIdentifier: "DoctorDetailTVC") as! DoctorDetailTableViewController
        thirdViewController.selectedSegmentIndex = 2
        thirdViewController.doctor_id = self.doctor_id
        
        return [
            firstViewController,
            secondViewController,
            thirdViewController
        ]
    }
    
    // MARK: - Setup container view
    ///Setup container view
    fileprivate func setupScrollView() {
        
        scrollView.contentSize = CGSize(
            width: UIScreen.main.bounds.width * CGFloat(viewControllers.count),
            height: containerView.frame.height
        )
        
        for (index, viewController) in viewControllers.enumerated() {
            
            viewController.view.frame = CGRect(
                x: UIScreen.main.bounds.width * CGFloat(index),
                y: 0,
                width: scrollView.frame.width,
                height: scrollView.frame.height
            )
            
            addChildViewController(viewController)
            
            scrollView.addSubview(viewController.view)
            //scrollView.addSubview(viewController.view, options: .useAutoresize) // module's extension
            
            viewController.didMove(toParentViewController: self)
        }
    }
}

/// UIScrollView Delegate method Extension
extension DoctorDetailViewController: UIScrollViewDelegate {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let currentPage = floor(scrollView.contentOffset.x / scrollView.frame.width)
        segmentedControl.selectedSegmentIndex = Int(currentPage)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        scrollView.contentSize = CGSize(width: scrollView.contentSize.width, height: 0)
    }
}
