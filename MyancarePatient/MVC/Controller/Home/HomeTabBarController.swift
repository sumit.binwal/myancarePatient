//
//  HomeTabBarController.swift
//  MyancarePatient
//
//  Created by Jyoti on 06/01/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit
import InAppNotify

var currentUserID : String?

class SearchBarView: UIView
{
    override var intrinsicContentSize: CGSize
    {
        return UILayoutFittingExpandedSize
    }
}

class HomeTabBarController: UITabBarController, UITabBarControllerDelegate {
    
    //MARK: - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        _ = LocationManager.sharedInstance()
        
        self.tabBar.unselectedItemTintColor = UIColor.lightGray
        self.tabBar.selectionIndicatorImage = UIImage.init(named: "")
        self.tabBar.items![0].selectedImage = UIImage.init(named: "homeSelect")
        
        self.tabBar.items![0].title = "Home".localized()
        self.tabBar.items![1].title = "Doctors".localized()
        self.tabBar.items![2].title = "Notification".localized()
        self.tabBar.items![3].title = "More".localized()
        
        SocketManagerHandler.sharedInstance().connectSocket { [weak self] (data, ack) in
            
            guard let `self` = self else { return }
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: NOTIFICATION_SOCKET_CONNECTED), object: nil)
            
            self.applyReceiveMessageListener()
            self.applyNotificationCountListner()
            self.enableCallButtonEventListner()
            self.endAppointmentTimeEventListner()
            self.updateDoctorStatusEventListner()
            self.nextAppointmentInFiveMinutesEventListener()
            self.doctorDeclineCallEventListener()
            self.doctorAcceptedCallEventListener()
        }
    }
    
    //MARK: - Deinit
    deinit {
        print("HomeTabBarController deinit")
    }
    
    ////MARK: - viewWillLayoutSubviews
    //override func viewWillLayoutSubviews() {
    //    var tabFrame = self.tabBar.frame
    //    tabFrame.size.height = 64.5 * scaleFactorX
    //    tabFrame.size.width = self.view.frame.size.width
    //    tabFrame.origin.y = self.view.frame.size.height - 64.5 * scaleFactorX
    //    self.tabBar.frame = tabFrame
    //}
    
    //Mark:- UITabBarControllerMethods
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        if item.tag == 3
        {
            if (UtilityClass.getUserSidData()) == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: "Please Login First".localized(), onViewController: self, withButtonArray: ["Skip".localized()], dismissHandler: { (buttonIndex) in
                    
                    if buttonIndex != 0
                    {
                        let welcomeVC = UIStoryboard.getWelcomeStoryBoard().instantiateInitialViewController() as! WelcomeViewController
                        
                        let navigationController = UINavigationController.init(rootViewController: welcomeVC)
                        
                        UtilityClass.changeRootViewController(with: navigationController)
                    }
                })
            }
        }
    }
    
    //MARK: - getImageWithColorPosition
    func getImageWithColorPosition(color: UIColor, size: CGSize, lineSize: CGSize) -> UIImage {
        
        let rect = CGRect(x:0, y: 0, width: size.width, height: size.height)
        let rectLine = CGRect(x:0, y:2 ,width: lineSize.width,height: lineSize.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        UIColor.clear.setFill()
        UIRectFill(rect)
        color.setFill()
        UIRectFill(rectLine)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return image
    }
    
    // MARK:- Enable Call Button Event (Enable Button)
    func enableCallButtonEventListner()
    {
        SocketManagerHandler.sharedInstance().receiveEnableCallButtonEvent
            { /*[weak self]*/ (messageDict, statusCode) in
                
                if statusCode == 200
                {
                    if let appointmentVw = appDelegate?.window??.visibleViewController() as? MyAppointmentDetailViewController
                    {
                        appointmentVw.actionButton.alpha = 1
                        appointmentVw.actionButton.isUserInteractionEnabled = true
                    }
                }
        }
    }
    
    // MARK:- Enable Call Button Event (Enable Button)
    func endAppointmentTimeEventListner()
    {
        SocketManagerHandler.sharedInstance().endAppointmentTimeListner ({ (messageDict, statusCode) in
            
            if let chatVwCntroler = appDelegate?.window??.visibleViewController() as? ChatViewController
            {
                chatVwCntroler.containerTextBox.isHidden = true
                chatVwCntroler.expireViewBox.isHidden = false
                
                chatVwCntroler.getChatHistoryFromSocket()
            }
            else if let chatVwCntroler = appDelegate?.window??.visibleViewController() as? CallHandlingVC
            {
                chatVwCntroler.callDismissed()
            }
            else if let chatVwCntroler = appDelegate?.window??.visibleViewController() as? VideoCallHandlingVC
            {
                chatVwCntroler.callDismissed()
            }
            else if let chatVwCntroler = appDelegate?.window??.visibleViewController() as? MyAppointmentDetailViewController
            {
                chatVwCntroler.acceptButon.isUserInteractionEnabled = false
                chatVwCntroler.acceptButon.alpha = 0.5
            }
        })
    }
    
    // MARK:- Update doctor status Event
    func updateDoctorStatusEventListner()
    {
        SocketManagerHandler.sharedInstance().updateDoctorStatus ({ (messageDict, statusCode) in
            
            let doctorID = messageDict["doctor"] as! String
            let status = (messageDict["current_status"] as! String).capitalized
            
            if let doctorVwCntroler = appDelegate?.window??.visibleViewController() as? DoctorsViewController
            {
                for doctorData in doctorVwCntroler.doctorArr
                {
                    if doctorData.doctorID == doctorID
                    {
                        doctorData.online_status = status
                    }
                }
                
                doctorVwCntroler.tableViewDoctorList.reloadData()
            }
            else if let doctorVwCntroler = appDelegate?.window??.visibleViewController() as? RecentViewController
            {
                for doctorData in doctorVwCntroler.doctorArr
                {
                    if doctorData.doctorID == doctorID
                    {
                        doctorData.online_status = status
                    }
                }
                
                doctorVwCntroler.tableViewRecentDoctorList.reloadData()
            }
            else if let doctorVwCntroler = appDelegate?.window??.visibleViewController() as? FavouritesViewController
            {
                for doctorData in doctorVwCntroler.doctorArr
                {
                    if doctorData.doctorID == doctorID
                    {
                        doctorData.online_status = status
                    }
                }
                
                doctorVwCntroler.tableViewFavouriteDoctorList.reloadData()
            }
            else if let doctorVwCntroler = appDelegate?.window??.visibleViewController() as? DoctorDetailViewController
            {
                if doctorVwCntroler.doctorModelData.doctorID == doctorID
                {
                    doctorVwCntroler.doctorModelData.online_status = status
                }
                
                if doctorVwCntroler.doctorModelData.online_status == "Online"
                {
                    doctorVwCntroler.doctorStatusImageView.image = #imageLiteral(resourceName: "greenDot")
                }
                else if doctorVwCntroler.doctorModelData.online_status == "Offline"
                {
                    doctorVwCntroler.doctorStatusImageView.image = #imageLiteral(resourceName: "redDot")
                }
                else
                {
                    doctorVwCntroler.doctorStatusImageView.image = #imageLiteral(resourceName: "greyDot")
                }
            }
        })
    }

    // MARK:- Message Listener (New Message)
    func applyReceiveMessageListener() {
        
        SocketManagerHandler.sharedInstance().receiveIncomingMessages
            {[weak self] (messageDict, statusCode) in
                
                if statusCode == 200 {
                    guard let `self` = self else {return}
                    
                    self.manageTheIncomingMessage(usingDictionary: messageDict)
                }
        }
    }
    
    // MARK:- next Appointment In Five Minutes Event Listener
    func nextAppointmentInFiveMinutesEventListener()
    {
        SocketManagerHandler.sharedInstance().nextAppointmentFiveEvent
            { /*[weak self]*/ (messageDict, statusCode) in
                
                let data = messageDict["data"] as! [String : Any]
                let appointments_id = data["appointments_id"] as! String
                let time = data["time_str"] as! String
                
                var message = "Hi".localized()
                message = message + " \(UtilityClass.getPersonName()), " + "You have upcoming appointment. Are you sure to join at".localized() + " \(time)?"
                
                if statusCode == 200
                {
                    let alertController = UIAlertController.init(title: App_Name.localized(), message: message, preferredStyle: UIAlertControllerStyle.alert)
                    
                    let yesAction = UIAlertAction.init(title: "Yes Five Minutes".localized(), style: UIAlertActionStyle.default, handler: { (_) in
                        
                        self.callPatientConfirmationAPI(appointment_id: appointments_id, time_Str: time)
                    })
                    
                    let noAction = UIAlertAction.init(title: "No Five Minutes".localized(), style: UIAlertActionStyle.default, handler: { (_) in
                        
                    })
                    
                    alertController.addAction(yesAction)
                    alertController.addAction(noAction)
                    
                    appDelegate?.window??.visibleViewController()?.present(alertController, animated: true, completion: nil)
                }
        }
    }
    
    func callPatientConfirmationAPI(appointment_id : String, time_Str : String)
    {
        let params = [
            "appointment_id" : appointment_id,
            "time_str" : time_Str
            ] as [String : String]
        print("params = \(params)")
        
        let urlToHit =  EndPoints.patient_confirmation.path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .delete, parameters: params, shouldDeserialize: true)
        { [weak self] (data, dictionary, statusCode, error) in
            
            //HUD.hide()
            
            guard self != nil else {return}
            
            if error != nil
            {
                return
            }
            
            if dictionary == nil
            {
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    
                }
            }
        }
    }
    
    // MARK:- doctor decline call Event Listener
    func doctorDeclineCallEventListener()
    {
        SocketManagerHandler.sharedInstance().doctorDeclineCallEvent
            { /*[weak self]*/ (messageDict, statusCode) in
                
                let drName = messageDict["doctor_name"] as! String
                
                let message = "Hi".localized() + " \(UtilityClass.getPersonName()), " + "My appoligy for Dr.".localized() + " \(drName) " + "is not available for your appointment because of urgent matter.".localized()
                
                if statusCode == 200
                {
                    let alertController = UIAlertController.init(title: App_Name.localized(), message: message, preferredStyle: UIAlertControllerStyle.alert)
                    
                    let yesAction = UIAlertAction.init(title: "Ok".localized(), style: UIAlertActionStyle.default, handler: { (_) in
                        
                    })
                    
                    alertController.addAction(yesAction)
                    
                    appDelegate?.window??.visibleViewController()?.present(alertController, animated: true, completion: nil)
                }
        }
    }
    
    // MARK:- doctor accepted call Event Listener
    func doctorAcceptedCallEventListener()
    {
        SocketManagerHandler.sharedInstance().doctorAcceptedEvent
            { /*[weak self]*/ (messageDict, statusCode) in
                
                let time = messageDict["time_str"] as! String
                
                let message = "Hi".localized() + " \(UtilityClass.getPersonName()), " + "Doctor confirmed your call and will call you at".localized() + " \(time)"
                
                if statusCode == 200
                {
                    let alertController = UIAlertController.init(title: App_Name.localized(), message: message, preferredStyle: UIAlertControllerStyle.alert)
                    
                    let yesAction = UIAlertAction.init(title: "Ok".localized(), style: UIAlertActionStyle.default, handler: { (_) in
                        
                    })
                    
                    alertController.addAction(yesAction)
                    
                    appDelegate?.window??.visibleViewController()?.present(alertController, animated: true, completion: nil)
                }
        }
    }
    
    func applyNotificationCountListner() {
        
        SocketManagerHandler.sharedInstance().receiveNotificationCount { [weak self](messageDict, statusCode) in
            
            if statusCode == 200 {
                
                guard let `self` = self else {return}
                
                let strCount = messageDict["count"] as! Int
                
                if messageDict["chat_count"] != nil {
                    let chat_count = messageDict["chat_count"] as! Int
                    UtilityClass.saveChatUnReadCount(count: chat_count)
                }
                
                if strCount > 0 {
                    self.tabBar.items![2].badgeValue = String(strCount as Int)
                } else {
                    self.tabBar.items![2].badgeValue = nil
                }
            }
        }
    }
    
    func manageTheIncomingMessage(usingDictionary dictionary:[String:Any])
    {
        let model = self.getNewModel(usingDictionary: dictionary)
        
        // If chat view is open...
        if let groupChatVC = appDelegate?.window??.visibleViewController() as? ChatViewController
        {
            // If same group chat is open...
            if model.roomID == groupChatVC.chatRoomID
            {
                // Simply add new message model...
                groupChatVC.addNewModel(usingDictionary: dictionary)
            }
            else if model.senderID == currentUserID, model.roomID.isEmpty // Single chat is open
            {
                // Simply add new message model...
                groupChatVC.addNewModel(usingDictionary: dictionary)
            }
            else
            {
                self.createInAppNotification(usingModel: model)
            }
        }
        else if let groupChatVC = appDelegate?.window??.visibleViewController() as? ChatListViewController
        {
            groupChatVC.getConversationListFromSocket()
        }
        else
        {
            self.createInAppNotification(usingModel: model)
        }
    }
    
    // MARK:- Message Model Creation
    func getNewModel(usingDictionary dictionary : [String:Any]) -> ModelChatMessage
    {
        let model = ModelChatMessage () // Model creation
        model.updateModel(usingDictionary: dictionary) // Updating model
        return model
    }
    
    // MARK:- InApp Notification Fire
    func createInAppNotification(usingModel model:ModelChatMessage)
    {
        let title = model.roomID.isEmpty ? model.senderName : String (model.senderName+" @ ")
        
        let picture = model.senderPicture
        
        let message = model.message.base64Decoded()
        
        let announce = Announcement (title: title, subtitle: message, image: nil, urlImage: picture, duration: 5, interactionType: .none, userInfo: model)
        {[unowned self] (callBackType, str, announce) in
            
            if callBackType == CallbackType.tap
            {
                if let chatInfo = announce.userInfo as? ModelChatMessage
                {
                    _ = !chatInfo.roomID.isEmpty
                    
                    // open screen
                    let chatViewController = UIStoryboard.getChatViewStoryBoard().instantiateViewController(withIdentifier: "chatViewController") as! ChatViewController
                    
                    chatViewController.hidesBottomBarWhenPushed = true
                    
                    //chatViewController.chatUsername.text = isGroup ? chatInfo.groupName : chatInfo.senderName
                    
                    chatViewController.chatRoomID = chatInfo.roomID
                    chatViewController.chatUsername = chatInfo.senderName
                    chatViewController.senderID = chatInfo.senderID
                    
                    if let navVC = self.selectedViewController as? UINavigationController
                    {
                        navVC.pushViewController(chatViewController, animated: true)
                    }
                }
            }
        }
        
        InAppNotify .Show(announce, to: self.selectedViewController!)
    }
    
    /*
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}
