//
//  NotificationViewController.swift
//  MyancarePatient
//
//  Created by Jyoti on 06/01/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit
import PKHUD
import DZNEmptyDataSet
import BBBadgeBarButtonItem

class NotificationViewController: UIViewController {
    
    //Mark :- Properties
    @IBOutlet var tableViewNotification: UITableView!
    
    var notificationArr = [NotificationModel]()
    var refreshControl = UIRefreshControl()
    
    var isPaging = false
    
    //MARK: - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setTableViewDelegate()
    }
    
    //MARK: - viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setUpNavigationBar()
        
        if (UtilityClass.getUserSidData()) != nil
        {
            isPaging = true
            notificationArr.removeAll()
            getNotificationListFromAPI()
            
            // Add Refresh Control to Table View
            if #available(iOS 10.0, *) {
                tableViewNotification?.refreshControl = refreshControl
            } else {
                tableViewNotification?.addSubview(refreshControl)
            }
            
            // Configure Refresh Control
            refreshControl.addTarget(self, action: #selector(refreshNotificationData(_:)), for: .valueChanged)
        }
    }

    //MARK: - Deinit
    deinit {
        print("NotificationViewController deinit")
    }
    
    //MARK: - setTableViewDelegate
    func setTableViewDelegate()  {
        
        tableViewNotification.delegate = self
        tableViewNotification.dataSource = self
        
        tableViewNotification.emptyDataSetSource = self
        tableViewNotification.emptyDataSetDelegate = self
        
        tableViewNotification.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 12 * scaleFactorX, right: 0)
    }
    
    @objc func refreshNotificationData(_ sender: Any) {
        
        isPaging = true
        notificationArr.removeAll()
        
        AppWebHandler.sharedInstance().cancelTask(forEndpoint: "notification?")
        getNotificationListFromAPI()
    }
    
    //MARK: - setUpNavigationBar
    func setUpNavigationBar() {
        
        self.title = "Notification".localized()
        
        self.navigationController?.isNavigationBarHidden = false
        
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSAttributedStringKey.font: UIFont.createPoppinsBoldFont(withSize: 18.0)
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        // set navigation bar color
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 30/255.0, green: 197/255.0, blue: 155/255.0, alpha: 1.0)
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        if (UtilityClass.getUserSidData()) != nil
        {
            //self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "profile"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(profileButtonPressed))
            
            let profilePictureURL = URL.init(string: UtilityClass.getUserImageData()!)
            
            DispatchQueue.global().async
                {
                    let data = try? Data.init(contentsOf: profilePictureURL!)
                    
                    guard let appDeleg = appDelegate as? AppDelegate else {
                        return
                    }
                    
                    if appDeleg.isInternetAvailable()
                    {
                        DispatchQueue.main.async
                            {
                                let button = UIButton.init(type: .custom)
                                
                                button.setImage(UIImage(data: data!), for: UIControlState.normal)
                                
                                button.addTarget(self, action: #selector(self.profileButtonPressed), for: UIControlEvents.touchUpInside)
                                
                                button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
                                
                                button.clipsToBounds = true
                                
                                let barButton = UIBarButtonItem(customView: button)
                                
                                NSLayoutConstraint.activate ([(barButton.customView!.widthAnchor.constraint(equalToConstant: 30)), (barButton.customView!.heightAnchor.constraint(equalToConstant: 30))])
                                
                                barButton.customView?.layer.cornerRadius = 15
                                barButton.customView?.layer.borderColor = UIColor(red: 30/255.0, green: 197/255.0, blue: 155/255.0, alpha: 1.0).cgColor
                                barButton.customView?.layer.borderWidth = 1.0
                                
                                self.navigationItem.leftBarButtonItem = barButton
                        }
                    }
            }
        
            let customButton = UIButton(type: UIButtonType.custom)
            customButton.frame = CGRect(x: 0, y: 0, width: 35.0, height: 35.0)
            customButton.addTarget(self, action: #selector(self.messagesButtonPressed), for: .touchUpInside)
            customButton.setImage(#imageLiteral(resourceName: "messages"), for: .normal)
            
            let btnBarBadge = MJBadgeBarButton()
            btnBarBadge.setup(customButton: customButton)
            
            if (UtilityClass.getChatUnReadCount())! > 0 {
                btnBarBadge.badgeValue = "\(UtilityClass.getChatUnReadCount() ?? 0)"
            } else {
                btnBarBadge.badgeValue = "0"
            }
            
            btnBarBadge.badgeOriginX = 20.0
            btnBarBadge.badgeOriginY = -4
            
            self.navigationItem.rightBarButtonItem = btnBarBadge
        }
    }
    
    // MARK: - Navigation Profile Button Action
    @objc func profileButtonPressed() {
        
        let ProfileVC = UIStoryboard.getProfileStoryBoard().instantiateInitialViewController() as! ProfileViewController
        navigationController?.pushViewController(ProfileVC, animated: true)
    }
    
    // MARK: - Navigation Message Button Action
    @objc func messagesButtonPressed () {
        
        let chatListVC = UIStoryboard.getChatViewStoryBoard().instantiateViewController(withIdentifier: "chatListViewController") as! ChatListViewController
        self.navigationController?.pushViewController(chatListVC, animated: true)
    }
    
    //MARK: - Get Article APi Calling
    func getNotificationListFromAPI()
    {
        let urlToHit = EndPoints.notification(notificationArr.count).path
        print("notification url = ", urlToHit)
        
        if notificationArr.count == 0 {
            HUD.show(.systemActivity, onView: self.view.window)
        }
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod:.get, parameters: nil) {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            self.refreshControl.endRefreshing()
            
            if error != nil
            {
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    let dataDict = responseDictionary["data"] as? [[String : Any]]
                    
                    guard dataDict != nil else
                    {
                        return
                    }
                    
                    self.updateModelArray(usingArray: dataDict!)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: {(buttonIndex) in
                        
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    UtilityClass.showAlertWithTitle(title: "", message: replyMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                        
                    })
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }
    
    //MARK:- Updating Category Model Array
    func updateModelArray(usingArray array : [[String:Any]]) -> Void
    {
        let dataArray = array
        if dataArray.count == 0 {
            self.isPaging = false
        }
        
        for dict in dataArray // Iterating dictionaries
        {
            let model = NotificationModel () // Model creation
            model.updateModel(usingDictionary: dict) // Updating model
            notificationArr.append(model) // Adding model to array
        }
        
        tableViewNotification.reloadData()
    }
}

//Mark:- Extension -> UITableView -> UITableViewDelegate, UITableViewDataSource
extension NotificationViewController : UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notificationArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: NotificationCustomCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier_NotificationCell) as! NotificationCustomCell
        
        if notificationArr.count > 0
        {
            cell.labelNotificationText.text = notificationArr[indexPath.row].notification!["body"] as? String
            
            cell.labelNotificationTime.text = notificationArr[indexPath.row].dateString
            
            if isPaging && indexPath.row == notificationArr.count - 1
            {
                getNotificationListFromAPI()
            }
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75 * scaleFactorX
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
        if notificationArr[indexPath.row].notification_type == "CHAT" {
            
            let username = notificationArr[indexPath.row].from_user!["name"] as! String
            let userid = notificationArr[indexPath.row].from_user!["id"] as! String
            
            let chatVC = UIStoryboard.getChatViewStoryBoard().instantiateViewController(withIdentifier: "chatViewController") as! ChatViewController
            
            chatVC.chatRoomID = notificationArr[indexPath.row].conversation_id
            chatVC.senderID = userid
            chatVC.chatUsername = username
            
            navigationController?.pushViewController(chatVC, animated: true)
        }
    }
}

//MARK:- Extention -> EZEmptyDataSet Delegate Method -> UITableViewDelegate, UITableViewDataSource
extension NotificationViewController : DZNEmptyDataSetSource, DZNEmptyDataSetDelegate
{
    
    //MARK:- DZNEmptyDataSetSource -> Title
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        
        let text = "No Record Found".localized()
        
        let attributes = [NSAttributedStringKey.font:UIFont .boldSystemFont(ofSize: 16 * scaleFactorX), NSAttributedStringKey.foregroundColor:UIColor.black]
        
        return NSAttributedString (string: text, attributes: attributes)
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
}
