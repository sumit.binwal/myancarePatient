//
//  LanguageViewController.swift
//  MyanCareDoctor
//
//  Created by Sumit Sharma on 08/12/2017.
//  Copyright © 2017 sumit. All rights reserved.
//

import UIKit
import Localize_Swift
import IQKeyboardManagerSwift

//MARK:-
//MARK:-
class LanguageViewController: UIViewController {
    
    let availableLanguages = Localize.availableLanguages()
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(availableLanguages)
        
        Localize.setCurrentLanguage("en")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        NotificationCenter.default.addObserver(self, selector: #selector(redirect), name: NSNotification.Name( LCLLanguageChangeNotification), object: nil)
    }
//    // Remove the LCLLanguageChangeNotification on viewWillDisappear
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
//        NotificationCenter.default.removeObserver(self)
//    }
    //MARK: - Deinit
    deinit {
        print("LanguageViewController deinit")
    }
    
    //MARK:- Burmish Button Action
    @IBAction func btnBurnishTapped(_ sender: UIButton) {
        
        Localize.setCurrentLanguage("my")
        
        IQKeyboardManager.sharedManager().toolbarDoneBarButtonItemText = "Done".localized()
    }
    
    //MARK:- English Button Action
    @IBAction func btnEnglishTapped(_ sender: UIButton) {
        
        Localize.setCurrentLanguage("en")
        
        IQKeyboardManager.sharedManager().toolbarDoneBarButtonItemText = "Done".localized()
    }
    
    @objc func redirect(){
        
        let WelcomeVC = UIStoryboard.getWelcomeStoryBoard().instantiateInitialViewController() as! WelcomeViewController
        self.navigationController?.pushViewController(WelcomeVC, animated: true)
    }
}
