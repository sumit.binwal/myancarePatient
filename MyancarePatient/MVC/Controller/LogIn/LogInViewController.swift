//
//  LoginViewController.swift
//  MyanCareDoctor
//
//  Created by Sumit Sharma on 08/12/2017.
//  Copyright © 2017 sumit. All rights reserved.
//

import UIKit
import PKHUD

//MARK:-
class LoginViewController: UIViewController {
    
    @IBOutlet weak var buttonView: UIView!
    @IBOutlet var loginTableView: UITableView!
    
    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var screenHeaderLabel: UILabel!
    @IBOutlet weak var accountLabel: UILabel!
    @IBOutlet weak var signUpLabel: UILabel!
    
    @IBOutlet var signupButton: UIButton!
    @IBOutlet var forgotButton: UIButton!
    @IBOutlet weak var signInButton: UIButton!
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setupView()
        setupTableView()
    }
    
    //MARK:- viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = true
    }
    
    //MARK:- Deinit
    deinit
    {
        print("LoginViewController.swift Deinit")
    }
    
    //MARK:- Setup TableView
    func setupTableView()
    {
        loginTableView.delegate = self
        loginTableView.dataSource = self
        
        buttonView.frame = CGRect(x: buttonView.frame.origin.x, y: buttonView.frame.origin.y, width: buttonView.frame.size.width*scaleFactorX, height: buttonView.frame.size.height*scaleFactorX)
    }
    
    //MARK:- SetupView
    func setupView()
    {
        let headerString = "Please type mobile number \n and password to sign in".localized()
        
        let attributedString = NSMutableAttributedString(string: headerString, attributes: [
            .font: UIFont.createPoppinsRegularFont(withSize: 18.0),
            .foregroundColor: UIColor.MyanCarePatient.welcomeScreenIntroTextColor,
            .kern: 0.8
            ])
        screenHeaderLabel.attributedText = attributedString
        
        signInButton.setTitle("Sign In".localized(), for: .normal)
        forgotButton.setTitle("Forgot Password?".localized(), for: .normal)
        
        welcomeLabel.text = "Welcome Back!".localized()
        accountLabel.text = "Don’t have an account yet?".localized()
        signUpLabel.text = "Sign Up".localized()
        
        modelSignUpProcess.password = ""
        modelSignUpProcess.mobile = ""
    }
    
    //MARK : on Back Button Action
    @IBAction func onBackButtonAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    //MARK: - On Sign In Button Action
    @IBAction func onSignInButtonAction(_ sender: Any) {
        if validateTheInputField()
        {
            callLoginAPi()
        }
    }
    
    //MARK:- on Signup Button Action
    @IBAction func onSignupButtonAction(_ sender: UIButton)
    {
        let signupVC = UIStoryboard.getSignUpStoryBoard().instantiateInitialViewController() as! SignUpViewController
        navigationController?.pushViewController(signupVC, animated: true)
    }
    
    //MARK:- on Forgot Password Button Action
    @IBAction func onForgotPasswordButtonAction(_ sender: UIButton)
    {
        let forgotVC = UIStoryboard.getLoginStoryBoard().instantiateViewController(withIdentifier: "forgotPassVC") as! ForgotPassViewController
        navigationController?.pushViewController(forgotVC, animated: true)
    }
    
    //MARK: - validation method
    func validateTheInputField() -> Bool {
        
        if modelSignUpProcess.mobile == nil || modelSignUpProcess.mobile!.isEmptyString()
        {
            UtilityClass.showAlertWithTitle(title:App_Name, message: "Please enter mobile number.".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
            return false
        }
        else if modelSignUpProcess.password == nil || modelSignUpProcess.password!.isEmptyString()
        {
            UtilityClass.showAlertWithTitle(title:App_Name, message: "Please enter password.".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
            return false
        }
        else if (modelSignUpProcess.password?.count)! < 6
        {
            UtilityClass.showAlertWithTitle(title:App_Name, message: "Password should be in range of 6 to 20 characters.".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
            return false
        }
        
        return true
    }
    
    //MARK: - call Login API
    func callLoginAPi()
    {
        HUD.show(.systemActivity, onView: self.view.window)
        
        self.view.endEditing(true)
        
        let params = ModelLogin().getLoginDictionary()
        print("login = \(params)")
        
        let urlToHit =  EndPoints.login.path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .post, parameters: params, shouldDeserialize: true)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 404
            {
                let responseDictionary = dictionary!
                _ = responseDictionary["status"] as! String
                
                UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: {(buttonIndex) in
                    
                })
            }
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyType = responseDictionary["status"] as! String
                let replyCode = responseDictionary["code"] as! String
                
                let dataDict = responseDictionary["data"] as! [String : Any]
                
                guard dataDict != nil else
                {
                    return
                }
                
                if replyType == "success"
                {
                    if replyCode == "VERIFY_MOBILE"
                    {
                        let loginTempToken = dataDict["tmp_token"] as! String
                        modelSignUpProcess.tempToken = loginTempToken

                        UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: ["Verify".localized()], dismissHandler: {[unowned self] (buttonIndex) in
                            
                            if buttonIndex == 0
                            {
                                self.callReSentOtpAPI(loginTempToken)
                            }
                        })
                    }
                    else if replyCode == "LOGGED_IN"
                    {
                        let notificationStatus = dataDict["notification_setting"] as! Bool
                        UtilityClass.saveNotificationStatus(userDict: notificationStatus)
                        
                        UtilityClass.saveUserInfoData(userDict: dataDict)
                        
                        NotificationCenter.default.post(name: NSNotification.Name("UserDidLoginNotification"), object: nil, userInfo: ["userId": dataDict["id"]] as? [AnyHashable : Any])
                        
                        let homeTabVC = UIStoryboard.getHomeStoryBoard().instantiateInitialViewController() as! HomeTabBarController
                        UtilityClass.changeRootViewController(with: homeTabVC)
                    }
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                        
                    })
                }
            }
        }
    }
    
    //MARK: - Call resent OTP API
    func callReSentOtpAPI(_ tempToken:String)
    {
        HUD.show(.systemActivity, onView: self.view.window)
        
        self.view.endEditing(true)
        
        let params = ["tmp_token":tempToken]
        
        let urlToHit =  EndPoints.resendOTP.path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .post, parameters: params, shouldDeserialize: true)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
    
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 404
            {
                let responseDictionary = dictionary!
                _ = responseDictionary["status"] as! String
                
                UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: {(buttonIndex) in
                    
                })
            }
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyType = responseDictionary["status"] as! String
                
                if replyType == "success"
                {
                    UtilityClass.saveUserInfoData(userDict: responseDictionary["data"] as! [String : Any])
                    
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: ["OK".localized()], dismissHandler: {[unowned self] (buttonIndex) in
                        
                        if buttonIndex == 0
                        {
                            let otpVC = UIStoryboard.getPhoneInfoStoryBoard().instantiateViewController(withIdentifier: "otpVC")
                            self.navigationController?.pushViewController(otpVC, animated: true)
                            
                        }}
                    )
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: {(buttonIndex) in
                        
                    })
                }
            }
            else
                
            {
                UtilityClass.showAlertWithTitle(title: "", message:App_Global_Error_Msg  , onViewController: self, withButtonArray: nil, dismissHandler: {(buttonIndex) in
                    
                })
            }
        }
    }
}

//MARK:-
//MARK:- Extention -> UITableView -> UITableViewDelegate, UITableViewDataSource
extension LoginViewController:UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 54 * scaleFactorX
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell: LoginCustomeCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier_LoginCell) as! LoginCustomeCell
        
        var cellType = LoginCellType.none
        
        switch indexPath.row {
        case 0:
            cellType = .phoneNumber
            
        case 1:
            cellType = .password
        default:
            break
        }
        
        cell.tag = indexPath.row
        
        cell.cellType = cellType
        
        cell.delegate = self
        
        return cell
    }
}

//MARK: - Extention -> LoginCellDelegate
extension LoginViewController: LoginCellDelegate
{
    func loginDataCell(_ cell: LoginCustomeCell, updatedInputFieldText inputValue: String)
    {
        switch cell.cellType {
        case .phoneNumber:
            modelSignUpProcess.mobile = inputValue
        case .password:
            modelSignUpProcess.password = inputValue
        default:
            break
        }
    }
}

//MARK:-
//MARK:- Extention -> Random Utilities
extension LoginViewController
{
    func createAttributedStringForSignup() -> NSAttributedString {
        
        let attributedString = NSMutableAttributedString(string: "Don’t have an account yet? Sign Up")
        
        attributedString.addAttributes([
            NSAttributedStringKey.font: UIFont.createPoppinsSemiBoldFont(withSize: 16),
            NSAttributedStringKey.foregroundColor: UIColor.MyanCarePatient.signUpButtonHighlightColor
            ], range: NSRange(location: 27, length: 7)
        )
        
        attributedString.addAttributes([
            NSAttributedStringKey.font: UIFont.createPoppinsRegularFont(withSize: 16),
            NSAttributedStringKey.foregroundColor: UIColor.MyanCarePatient.signUpButtonColor
            ], range: NSRange(location: 0, length: 26)
        )
        
        return attributedString
    }
}
