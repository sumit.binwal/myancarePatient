//
//  MedicineReminderSegmentViewController.swift
//  MyancarePatient
//
//  Created by iOS on 09/02/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit
import ScrollableSegmentedControl

///MedicineReminderSegment View Controller - UIViewController Class
class MedicineReminderSegmentViewController: UIViewController
{
    ///ScrollableSegmentedControl Refrence - SegmentedControl
    @IBOutlet fileprivate weak var segmentedControl: ScrollableSegmentedControl!
    
    ///UIView Refrence - Container View
    @IBOutlet fileprivate weak var containerView: UIView!
    
    /// UIScroll View Refrence - scrollView
    @IBOutlet fileprivate weak var scrollView: UIScrollView!
    
    
    /// View Controller's Arry
    fileprivate lazy var viewControllers: [UIViewController] = {
        return self.preparedViewControllers()
    }()
    
    //MARK:- viewDidLoad
    ///ViewController LifeCycle Methods
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setupScrollView()
        
        segmentedControl.segmentStyle = .textOnly
        
        segmentedControl.selectedSegmentContentColor = UIColor.MyanCarePatient.colorForDoctorSegmentSelected
        
        segmentedControl.underlineSelected = true
        segmentedControl.selectedSegmentIndex = 0
        
        segmentedControl.insertSegment(withTitle: "All".localized(), image: #imageLiteral(resourceName: "appointment-upcoming-unfocus"), at: 0)
        segmentedControl.insertSegment(withTitle: "Today".localized(), image: #imageLiteral(resourceName: "appointment-history-unfocus"), at: 1)
        
        segmentedControl.addTarget(self, action: #selector(MedicineReminderSegmentViewController.segmentSelected(sender:)), for: .valueChanged)
    }
    //MARK:- viewWillAppear
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        setUpNavigationBar()
        
        self.tabBarController?.tabBar.isHidden = true
    }
    
    //MARK:- viewWillDisappear
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        
        self.tabBarController?.tabBar.isHidden = false
    }
    
    //MARK: - Deinit
    deinit
    {
        print("MedicineReminderSegmentViewController deinit")
    }
    
    /// Segement Selcted Method -
    ///
    /// - Parameter sender: <#sender description#>
    @objc func segmentSelected(sender:ScrollableSegmentedControl)
    {
        let contentOffsetX = scrollView.frame.width * CGFloat(sender.selectedSegmentIndex)
        scrollView.setContentOffset(CGPoint(x: contentOffsetX, y: 0), animated: true)
    }
    
    
    
    //MARK:- setUpNavigationBar
    
    /// SetupNavigation Bar Method
    func setUpNavigationBar()
    {
        self.title = "Medication Reminders".localized()
        
        self.navigationController?.isNavigationBarHidden = false
        
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSAttributedStringKey.font: UIFont.createPoppinsBoldFont(withSize: 18.0)
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        // set navigation bar color
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 30/255.0, green: 197/255.0, blue: 155/255.0, alpha: 1.0)
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "back_white"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "add-reminder"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(addButtonPressed))
    }
    
    //MARK:-
    /// Back Button Clicked Action Method
    @objc func backButtonPressed()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:-
    /// Add Button Clicked Action Method
    @objc func addButtonPressed()
    {
        let addMedicineRecordVC = UIStoryboard.getMedicineReminderStoryBoard().instantiateViewController(withIdentifier: "AddMedicineRecordVC") as! AddMedicineRecordViewController
        self.navigationController?.pushViewController(addMedicineRecordVC, animated: true)
    }
    
    
    // Example viewControllers
    
    /// Setup Segmented View Controller
    ///
    /// - Returns: Passed Array Of View Controller Which we want to add on Segemnted Controller
    fileprivate func preparedViewControllers() -> [UIViewController]
    {
        let storyboard = self.storyboard
        
        let firstViewController = storyboard?
            .instantiateViewController(withIdentifier: "MedicineReminderAllVC") as! MedicineReminderAllViewController
        
        let secondViewController = storyboard?
            .instantiateViewController(withIdentifier: "MedicineReminderTodayVC") as! MedicineReminderTodayViewController
        
        return [
            firstViewController,
            secondViewController
        ]
    }
    
    // MARK: - Setup container view
    ///Setup Container View method
    fileprivate func setupScrollView()
    {
        scrollView.contentSize = CGSize(
            width: UIScreen.main.bounds.width * CGFloat(viewControllers.count),
            height: containerView.frame.height
        )
        
        for (index, viewController) in viewControllers.enumerated()
        {
            viewController.view.frame = CGRect(
                x: UIScreen.main.bounds.width * CGFloat(index),
                y: 0,
                width: scrollView.frame.width,
                height: scrollView.frame.height
            )
            
            addChildViewController(viewController)
            
            scrollView.addSubview(viewController.view)
            
            viewController.didMove(toParentViewController: self)
        }
    }
}

///UIScrollView Delegate Method Extension
extension MedicineReminderSegmentViewController: UIScrollViewDelegate
{
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        let currentPage = floor(scrollView.contentOffset.x / scrollView.frame.width)
        segmentedControl.selectedSegmentIndex = Int(currentPage)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        scrollView.contentSize = CGSize(width: scrollView.contentSize.width, height: 0)
    }
}

