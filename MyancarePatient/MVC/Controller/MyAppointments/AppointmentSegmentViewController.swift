//
//  AppointmentSegmentViewController.swift
//  MyancarePatient
//
//  Created by iOS on 06/02/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit
import ScrollableSegmentedControl
///AppointmentSegementViewController ViewController Class
class AppointmentSegmentViewController: UIViewController {

    
    /// Scrollable Segmented Controller Refrence
    @IBOutlet fileprivate weak var segmentedControl: ScrollableSegmentedControl!
    
    ///UIView Refrence Variable
    @IBOutlet fileprivate weak var containerView: UIView!
    ///UIScrollView Refrence Variable
    @IBOutlet fileprivate weak var scrollView: UIScrollView!
    
    ///Bool Variable
    var isFromDoctorProfile = false
    ///String Variable
    var doctorID = ""
    
    
    /// View Controller Array methods
    fileprivate lazy var viewControllers: [UIViewController] = {
        return self.preparedViewControllers()
    }()
    
    //MARK:- viewDidLoad
    //UIViewController Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setupScrollView()
        
        segmentedControl.segmentStyle = .imageOnTop
        
        segmentedControl.selectedSegmentContentColor = UIColor.MyanCarePatient.colorForDoctorSegmentSelected
        
        segmentedControl.underlineSelected = true
        segmentedControl.selectedSegmentIndex = 0
        
        segmentedControl.insertSegment(withTitle: "Upcoming".localized(), image: #imageLiteral(resourceName: "appointment-upcoming-unfocus"), at: 0)
        segmentedControl.insertSegment(withTitle: "History".localized(), image: #imageLiteral(resourceName: "appointment-history-unfocus"), at: 1)
        
        segmentedControl.addTarget(self, action: #selector(AppointmentSegmentViewController.segmentSelected(sender:)), for: .valueChanged)
    }
    
    //MARK:- viewDidAppear
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setUpNavigationBar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.tabBarController?.tabBar.isHidden = false
    }
    
    //MARK: - Deinit
    deinit {
        print("AppointmentSegmentViewController deinit")
    }

    
    /// Segement Selected
    ///
    /// - Parameter sender: Scrollable Segemented Controller Refrence
    @objc func segmentSelected(sender:ScrollableSegmentedControl) {
        
        let contentOffsetX = scrollView.frame.width * CGFloat(sender.selectedSegmentIndex)
        scrollView.setContentOffset(CGPoint(x: contentOffsetX, y: 0), animated: true)
    }
    
   
    //MARK:- setUpNavigationBar
    
    /// Setup Navigation method
    func setUpNavigationBar() {
        
        self.title = "My Appointments".localized()
        
        self.navigationController?.isNavigationBarHidden = false
        
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSAttributedStringKey.font: UIFont.createPoppinsBoldFont(withSize: 18.0)
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        // set navigation bar color
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 30/255.0, green: 197/255.0, blue: 155/255.0, alpha: 1.0)
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "back_white"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
    }
    
    //MARK:- navigation bar bac button action
    /// Back Button Clicked Event Method
    @objc func backButtonPressed() {
        self.navigationController?.popViewController(animated: true)
    }
    
    // Example viewControllers
    
    /// Prepare or initialize a segemented View Controller
    ///
    /// - Returns: Array Of ViewController's
    fileprivate func preparedViewControllers() -> [UIViewController] {
        
        let storyboard = self.storyboard
        
        let firstViewController = storyboard?
            .instantiateViewController(withIdentifier: "MyAppointmentListVC") as! MyAppointmentListViewController
        firstViewController.isFromDoctorProfile = isFromDoctorProfile
        firstViewController.doctorID = doctorID
        
        let secondViewController = storyboard?
            .instantiateViewController(withIdentifier: "MyAppointmentHistoryVC") as! MyAppointmentHistoryViewController
        secondViewController.doctorID = doctorID
        secondViewController.isFromDoctorProfile = isFromDoctorProfile
        
        return [
            firstViewController,
            secondViewController
        ]
    }
    
    // MARK: - Setup container view
    
    /// Setup ScrollView - Container View
    fileprivate func setupScrollView() {
        scrollView.contentSize = CGSize(
            width: UIScreen.main.bounds.width * CGFloat(viewControllers.count),
            height: containerView.frame.height
        )
        
        for (index, viewController) in viewControllers.enumerated() {
            viewController.view.frame = CGRect(
                x: UIScreen.main.bounds.width * CGFloat(index),
                y: 0,
                width: scrollView.frame.width,
                height: scrollView.frame.height
            )
            addChildViewController(viewController)
            
            scrollView.addSubview(viewController.view)
            //scrollView.addSubview(viewController.view, options: .useAutoresize) // module's extension
            
            viewController.didMove(toParentViewController: self)
        }
    }
}

///UIScrollView Delegate Methods - Extension
extension AppointmentSegmentViewController: UIScrollViewDelegate {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let currentPage = floor(scrollView.contentOffset.x / scrollView.frame.width)
        segmentedControl.selectedSegmentIndex = Int(currentPage)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        scrollView.contentSize = CGSize(width: scrollView.contentSize.width, height: 0)
    }
}
