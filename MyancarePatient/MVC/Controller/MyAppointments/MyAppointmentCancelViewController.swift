//
//  MyAppointmentCancelViewController.swift
//  MyancarePatient
//
//  Created by iOS on 06/02/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit
import PKHUD
///MyAppointmentCancel View Controller
class MyAppointmentCancelViewController: UIViewController {

    ///UILabel Refrence - Reason Text
    @IBOutlet weak var reasonLabel: UILabel!
    
    ///UIButton Refrence - Cancel Button
    @IBOutlet weak var cancelButton: UIButton!
    
    ///UIButton Refrence - Confirm Button
    @IBOutlet weak var confirmButton: UIButton!
    
    ///UITextView Refrence - Reason TextView Text
    @IBOutlet weak var resonTextView: UITextView!
    
    ///UIView Refrence - OuterView
    @IBOutlet weak var outerView: UIView!
    
    ///UIImageView Refrence - Background Image
    @IBOutlet weak var backgroundImage: UIImageView!
    
    ///Model Variable - Appointment Model Data
    var appointmentModelData = AppointmentModel()
    
    ///UIImageView Refrence - Background Image Screen
    var backgroungImageScreen : UIImage?
    
    ///Variable - reason place holder Text
    let reasonPlaaceHolder = "Type reason here".localized()
    
    
    //MARK: - viewDidLoad
    ///UIViewController Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setUpView()
    }
    
    //MARK: - viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tabBarController?.tabBar.isHidden = true
        
        self.backgroundImage.image = backgroungImageScreen
        
        self.navigationController?.isNavigationBarHidden = true
    }
    
    //MARK: - viewWillDisappear
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationController?.isNavigationBarHidden = false
    }
    
    //MARK: - Deinit
    deinit {
        print("MyAppointmentCancelViewController deinit")
    }
    
    //MARK: - setUpView
    ///SetupView Method
    func setUpView() {
        
        resonTextView.delegate = self
        
        resonTextView.textColor = UIColor.lightGray
        resonTextView.text = reasonPlaaceHolder
        
        cancelButton.layer.cornerRadius = 5.0
        cancelButton.layer.borderColor = UIColor.MyanCarePatient.colorForAcceptedView.cgColor
        cancelButton.layer.borderWidth = 1.0
        cancelButton.layer.masksToBounds = true
        
        confirmButton.layer.cornerRadius = 5.0
        confirmButton.layer.borderColor = UIColor.MyanCarePatient.colorForAcceptedView.cgColor
        confirmButton.layer.borderWidth = 1.0
        confirmButton.layer.masksToBounds = true
        
        outerView.layer.cornerRadius = 5.0
        outerView.layer.borderColor = UIColor.MyanCarePatient.colorForAcceptedView.cgColor
        outerView.layer.borderWidth = 1.0
        outerView.layer.masksToBounds = true
        
        resonTextView.layer.cornerRadius = 5.0
        resonTextView.layer.borderColor = UIColor.lightGray.cgColor
        resonTextView.layer.borderWidth = 1.0
        resonTextView.layer.masksToBounds = true
        
        reasonLabel.text = "Reason for cancel:".localized()
        cancelButton.setTitle("Cancel".localized(), for: .normal)
        confirmButton.setTitle("Confirm".localized(), for: .normal)
    }
    
    //MARK: - on confirm Button Action
    ///Confirm Button Clicked Event Method
    @IBAction func confirmButtonAction(_ sender: Any) {
        
        if resonTextView.text == reasonPlaaceHolder {
            UtilityClass.showAlertWithTitle(title: "", message: "Please enter reason for Appointment Cancellation.".localized(), onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                
            })
        } else if resonTextView.text == "" {
            UtilityClass.showAlertWithTitle(title: "", message: "Please enter reason for Appointment Cancellation.".localized(), onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                
            })
        } else {
            appointmentModelData.rejectReason = resonTextView.text
            callCancelAppointmentAPi()
        }
    }
    
    //MARK: - on cancel Button Action
    ///On Cancel Button Clicked Action Method
    @IBAction func cancelButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    //MARK: - Call Cancel Appointment API
    ///Call Cancel Appointment API
    func callCancelAppointmentAPi()
    {
        self.view.endEditing(true)
        
        let params = appointmentModelData.getRejectAppointmentParametersdata()
        
        print("parameters = ", params)
        
        HUD.show(.systemActivity, onView: self.view.window)
        
        let urlToHit =  EndPoints.cancelAppointment.path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .post, parameters: params, shouldDeserialize: true)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    let rejectionMsg = responseDictionary["msg"] as? String
                    
                    UtilityClass.showAlertWithTitle(title: "", message: rejectionMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                        
                        let controllers = self.navigationController?.viewControllers
                        for controller in controllers!
                        {
                            if controller .isKind(of: AppointmentSegmentViewController.self)
                            {
                                self.navigationController?.popToViewController(controller, animated: true)
                            }
                        }
                    })
                }
                    
                else
                {
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: {(buttonIndex) in
                        
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    UtilityClass.showAlertWithTitle(title: "", message: replyMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                        
                    })
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }
}

//MARK: - extension -> UITextView -> UITextViewDelegate
///extension -> UITextView -> UITextViewDelegate
extension MyAppointmentCancelViewController : UITextViewDelegate
{
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        if textView.text == reasonPlaaceHolder {
            textView.text = ""
            textView.textColor = UIColor.darkGray
        }
        
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == reasonPlaaceHolder {
            textView.textColor = UIColor.lightGray
        } else {
            textView.textColor = UIColor.darkGray
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if textView.text.count > 240 {
            if text == "" {
                return true
            } else {
                return false
            }
        }
        
        return true
    }
}

