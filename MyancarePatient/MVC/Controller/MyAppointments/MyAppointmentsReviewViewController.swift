//
//  MyAppointmentsReviewViewController.swift
//  MyancarePatient
//
//  Created by iOS on 07/02/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit
import PKHUD

///MyAppointmentsReview View Controller
class MyAppointmentsReviewViewController: UIViewController {

    ///Variable - Appointment Model Values
    var appointmentModelData = AppointmentModel()
    
    ///UIButton Refrence - VeryBad Icon
    @IBOutlet weak var veryBadButon: UIButton!
    ///UIButton Refrence - Bad Icon
    @IBOutlet weak var badButon: UIButton!
    ///UIButton Refrence - Okay Icon
    @IBOutlet weak var okayButon: UIButton!
    ///UIButton Refrence - Good Icon
    @IBOutlet weak var goodButon: UIButton!
    ///UIButton Refrence - Great Icon
    @IBOutlet weak var greatButon: UIButton!
    
    ///UIButton Refrence - Submit Icon
    @IBOutlet weak var submitButon: UIButton!
    ///UIButton Refrence - Skip Icon
    @IBOutlet weak var skipButton: UIButton!
    ///UILabel Refrence - Rated Doctor Label
    @IBOutlet weak var ratedoctorlabel: UILabel!
    ///UILabel Refrence - Feedback Label
    @IBOutlet weak var feedbacklabel: UILabel!
    ///UILabel Refrence - Very Bad Label
    @IBOutlet weak var veryBadUILabel: UILabel!
    ///UILabel Refrence - Bad Label
    @IBOutlet weak var badUILabel: UILabel!
    ///UILabel Refrence - Okay Label
    @IBOutlet weak var okayUILabel: UILabel!
    ///UILabel Refrence - Good Label
    @IBOutlet weak var goodUILabel: UILabel!
    ///UILabel Refrence - Great Label
    @IBOutlet weak var greatUILabel: UILabel!
    
    ///UITextView Refrence - Comments TextView
    @IBOutlet weak var commentsTextView: UITextView!
    
    ///Variable Int - Given Rating
    var givenRating = 0
    
    ///Book Variable - isFromCall
    var isFromCall = false
    
    ///String Variable - AppointmentID
    var appointment_id = ""
    
    ///String Variable - Doctor ID
    var doctor_id = ""
    
    ///String Variable - Comment PlaceHolder
    let commentPlaaceHolder = "Comments...".localized()
    
    
    /// UIViewController LIfe Cycle Methods
    //MARK:- viewDidLoad

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setUpView()
    }
    
    //MARK: - viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tabBarController?.tabBar.isHidden = true
        
        setUpNavigationBar()
        
        if isFromCall {
            skipButton.isHidden = false
        } else {
            skipButton.isHidden = true
        }
    }
    
    //MARK: - viewWillDisappear
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    //MARK: - Deinit
    deinit {
        print("MyAppointmentsReviewViewController deinit")
    }
    
    //MARK:- setUpNavigationBar
    ///SetupNavigation Bar
    func setUpNavigationBar() {
        
        self.title = "Feedback".localized()
        
        self.navigationController?.isNavigationBarHidden = false
        
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSAttributedStringKey.font: UIFont.createPoppinsBoldFont(withSize: 18.0)
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        // set navigation bar color
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 30/255.0, green: 197/255.0, blue: 155/255.0, alpha: 1.0)
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "back_white"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
    }
    
    //MARK:- navigation bar back button action
    ///navigation bar back button action
    @objc func backButtonPressed() {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- setUpView
    ///SetupView
    func setUpView() {
        
        commentsTextView.textColor = UIColor.lightGray
        commentsTextView.text = commentPlaaceHolder
        
        givenRating = 1
        
        veryBadButon.setImage(#imageLiteral(resourceName: "verybadActive"), for: .normal)
        veryBadUILabel.textColor = UIColor.MyanCarePatient.colorForActiveReviewStatus
        
        commentsTextView.layer.cornerRadius = 5.0
        commentsTextView.layer.borderColor = UIColor.MyanCarePatient.colorForDeactiveReviewStatus.cgColor
        commentsTextView.layer.borderWidth = 1.0
        commentsTextView.layer.masksToBounds = true
        
        veryBadUILabel.text = "Very bad".localized()
        badUILabel.text = "Bad".localized()
        okayUILabel.text = "Okay".localized()
        goodUILabel.text = "Good".localized()
        greatUILabel.text = "Great".localized()
        feedbacklabel.text = "Feedback is annonymously shared".localized()
        ratedoctorlabel.text = "Rate Doctor!".localized()
        
        submitButon.setTitle("Submit".localized(), for: .normal)
    }
    
    ///Skip Button Clicked Action
    @IBAction func skipButtonAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- on very badd rating button action
    ///on very badd rating button action
    @IBAction func veryBadButtonAction(_ sender: Any) {
        
        givenRating = 1
        
        veryBadButon.setImage(#imageLiteral(resourceName: "verybadActive"), for: .normal)
        veryBadUILabel.textColor = UIColor.MyanCarePatient.colorForActiveReviewStatus
        
        badButon.setImage(#imageLiteral(resourceName: "bad"), for: .normal)
        badUILabel.textColor = UIColor.MyanCarePatient.colorForDeactiveReviewStatus
        
        okayButon.setImage(#imageLiteral(resourceName: "okay"), for: .normal)
        okayUILabel.textColor = UIColor.MyanCarePatient.colorForDeactiveReviewStatus
        
        goodButon.setImage(#imageLiteral(resourceName: "good"), for: .normal)
        goodUILabel.textColor = UIColor.MyanCarePatient.colorForDeactiveReviewStatus
        
        greatButon.setImage(#imageLiteral(resourceName: "great"), for: .normal)
        greatUILabel.textColor = UIColor.MyanCarePatient.colorForDeactiveReviewStatus
    }
    
    //MARK:- on bad rating button action
    ///on bad rating button action
    @IBAction func badButtonAction(_ sender: Any) {
        
        givenRating = 2
        
        veryBadButon.setImage(#imageLiteral(resourceName: "verybad"), for: .normal)
        veryBadUILabel.textColor = UIColor.MyanCarePatient.colorForDeactiveReviewStatus
        
        badButon.setImage(#imageLiteral(resourceName: "badActive"), for: .normal)
        badUILabel.textColor = UIColor.MyanCarePatient.colorForActiveReviewStatus
        
        okayButon.setImage(#imageLiteral(resourceName: "okay"), for: .normal)
        okayUILabel.textColor = UIColor.MyanCarePatient.colorForDeactiveReviewStatus
        
        goodButon.setImage(#imageLiteral(resourceName: "good"), for: .normal)
        goodUILabel.textColor = UIColor.MyanCarePatient.colorForDeactiveReviewStatus
        
        greatButon.setImage(#imageLiteral(resourceName: "great"), for: .normal)
        greatUILabel.textColor = UIColor.MyanCarePatient.colorForDeactiveReviewStatus
    }
    
    //MARK:- on okay rating button action
    ///on okay rating button action
    @IBAction func okayButtonAction(_ sender: Any) {
        
        givenRating = 3
        
        veryBadButon.setImage(#imageLiteral(resourceName: "verybad"), for: .normal)
        veryBadUILabel.textColor = UIColor.MyanCarePatient.colorForDeactiveReviewStatus
        
        badButon.setImage(#imageLiteral(resourceName: "bad"), for: .normal)
        badUILabel.textColor = UIColor.MyanCarePatient.colorForDeactiveReviewStatus
        
        okayButon.setImage(#imageLiteral(resourceName: "okayactive"), for: .normal)
        okayUILabel.textColor = UIColor.MyanCarePatient.colorForActiveReviewStatus
        
        goodButon.setImage(#imageLiteral(resourceName: "good"), for: .normal)
        goodUILabel.textColor = UIColor.MyanCarePatient.colorForDeactiveReviewStatus
        
        greatButon.setImage(#imageLiteral(resourceName: "great"), for: .normal)
        greatUILabel.textColor = UIColor.MyanCarePatient.colorForDeactiveReviewStatus
    }
    
    //MARK:- on good rating button action
    ///on good rating button action
    @IBAction func goodButtonAction(_ sender: Any) {
        
        givenRating = 4
        
        veryBadButon.setImage(#imageLiteral(resourceName: "verybad"), for: .normal)
        veryBadUILabel.textColor = UIColor.MyanCarePatient.colorForDeactiveReviewStatus
        
        badButon.setImage(#imageLiteral(resourceName: "bad"), for: .normal)
        badUILabel.textColor = UIColor.MyanCarePatient.colorForDeactiveReviewStatus
        
        okayButon.setImage(#imageLiteral(resourceName: "okay"), for: .normal)
        okayUILabel.textColor = UIColor.MyanCarePatient.colorForDeactiveReviewStatus
        
        goodButon.setImage(#imageLiteral(resourceName: "goodActive"), for: .normal)
        goodUILabel.textColor = UIColor.MyanCarePatient.colorForActiveReviewStatus
        
        greatButon.setImage(#imageLiteral(resourceName: "great"), for: .normal)
        greatUILabel.textColor = UIColor.MyanCarePatient.colorForDeactiveReviewStatus
    }
    
    //MARK:- on great rating button action
    ///on great rating button action
    @IBAction func greatButtonAction(_ sender: Any) {
        
        givenRating = 5
        
        veryBadButon.setImage(#imageLiteral(resourceName: "verybad"), for: .normal)
        veryBadUILabel.textColor = UIColor.MyanCarePatient.colorForDeactiveReviewStatus
        
        badButon.setImage(#imageLiteral(resourceName: "bad"), for: .normal)
        badUILabel.textColor = UIColor.MyanCarePatient.colorForDeactiveReviewStatus
        
        okayButon.setImage(#imageLiteral(resourceName: "okay"), for: .normal)
        okayUILabel.textColor = UIColor.MyanCarePatient.colorForDeactiveReviewStatus
        
        goodButon.setImage(#imageLiteral(resourceName: "good"), for: .normal)
        goodUILabel.textColor = UIColor.MyanCarePatient.colorForDeactiveReviewStatus
        
        greatButon.setImage(#imageLiteral(resourceName: "greatActive"), for: .normal)
        greatUILabel.textColor = UIColor.MyanCarePatient.colorForActiveReviewStatus
    }
    
    //MARK:- on submit button action
    ///on submit button action
    @IBAction func submitButtonAction(_ sender: Any) {
        
        if validateTheInputField() {
            callReviewCreateAPi()
        }
    }
    
    //MARK: - Call create review API
    ///Call create review API
    func callReviewCreateAPi()
    {
        self.view.endEditing(true)
        
        var doctor_id = ""
        if isFromCall
        {
            doctor_id = self.doctor_id
        }
        else
        {
            doctor_id = appointmentModelData.doctor_dict["id"] as! String
        }
        
        var appointment_id = ""
        if isFromCall
        {
            appointment_id = self.appointment_id
        }
        else
        {
            appointment_id = appointmentModelData.appointmentID 
        }
        
        let params = [
            "user" : doctor_id,
            "comment" : commentsTextView.text,
            "rating" : givenRating,
            "appointment" : appointment_id
            ] as [String : Any]
        
        print("parameters = ", params)
        
        HUD.show(.systemActivity, onView: self.view.window)
        
        let urlToHit =  EndPoints.userReview.path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .post, parameters: params, shouldDeserialize: true)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    let rejectionMsg = responseDictionary["msg"] as? String
                    
                    UtilityClass.showAlertWithTitle(title: "", message: rejectionMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                        
                        if self.isFromCall
                        {
                            self.dismiss(animated: true, completion: nil)
                        }
                        else
                        {
                            let controllers = self.navigationController?.viewControllers
                            for controller in controllers!
                            {
                                if controller .isKind(of: AppointmentSegmentViewController.self)
                                {
                                    self.navigationController?.popToViewController(controller, animated: true)
                                }
                            }
                        }
                    })
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: {(buttonIndex) in
                        
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    UtilityClass.showAlertWithTitle(title: "", message:replyMsg, onViewController: self, withButtonArray: nil, dismissHandler:{ (buttonIndex) in
                        
                        if self.isFromCall
                        {
                            self.dismiss(animated: true, completion: nil)
                        }
                    })
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }
    
    //MARK: - Validate InputField
    ///Validate InputField
    func validateTheInputField() -> Bool {
        
        if commentsTextView.text == nil || (commentsTextView.text?.isEmptyString())! {
            
            UtilityClass.showAlertWithTitle(title:App_Name, message: "Please enter comment.".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
            
            return false
            
        } else if commentsTextView.text == commentPlaaceHolder {
            
            UtilityClass.showAlertWithTitle(title:App_Name, message: "Please enter comment.".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
            
            return false
            
        } else if givenRating == 0 {
            
            UtilityClass.showAlertWithTitle(title:App_Name, message: "Please give rating.".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
            
            return false
        }
        
        return true
    }
}

//MARK: - extension -> UITextView -> UITextViewDelegate
///extension -> UITextView -> UITextViewDelegate
extension MyAppointmentsReviewViewController : UITextViewDelegate
{
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        if textView.text == commentPlaaceHolder {
            textView.text = ""
            textView.textColor = UIColor.darkGray
        }
        
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == commentPlaaceHolder {
            textView.textColor = UIColor.lightGray
        } else {
            textView.textColor = UIColor.darkGray
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if textView.text.count > 240 {
            if text == "" {
                return true
            } else {
                return false
            }
        }
        
        return true
    }
}
