//
//  EditRecordViewController.swift
//  MyancarePatient
//
//  Created by iOS on 05/03/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit
import PKHUD

//Edit Record View Controller
class EditRecordViewController: UIViewController {

    ///UITableView Outlet Refrence - AddRecord TableView
    @IBOutlet weak var addRecordTableView: UITableView!
    ///UIButton Outlet Refrence - Update Button
    @IBOutlet weak var updateButton: UIButton!
    ///UIButton Outlet Refrence - Record Button
    @IBOutlet weak var recordButton: UIButton!
    ///UIButton Outlet Refrence - Cancel Button
    @IBOutlet weak var cancelButton: UIButton!
    
    ///Medical Record Data Model Variable
    var medicalRecordData = MedicalRecord()
    
    /// View Conrollers Life Cycle Methods
    //MARK:- viewDidLoad

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setUpTableView()
        
        updateButton.setTitle("Update".localized(), for: .normal)
        recordButton.setTitle("Record Pages".localized(), for: .normal)
        cancelButton.setTitle("Cancel".localized(), for: .normal)
    }
    
    //MARK:- viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tabBarController?.tabBar.isHidden = true
        
        modelAddNewRecordProcess = AddNewRecordModel.init()
        
        setUpNavigationBar()
        setUpData()
    }
    
    //MARK:- View Will Disappear
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.tabBarController?.tabBar.isHidden = false
    }
    
    //MARK: - Deinit
    deinit {
        print("EditRecordViewController deinit")
    }
    
    //MARK: - setUpTableView
    /// Setup TableView Method
    func setUpTableView () {
        
        addRecordTableView.delegate = self
        addRecordTableView.dataSource = self
    }
    
    //MARK: - setUpData
    /// SetupData Method
    func setUpData () {
        
        modelAddNewRecordProcess.recordID = medicalRecordData.recordID
        modelAddNewRecordProcess.doctorName = medicalRecordData.doctorName
        modelAddNewRecordProcess.diseaseName = medicalRecordData.diseaseName
        modelAddNewRecordProcess.hospitalName = medicalRecordData.hospitalName
        modelAddNewRecordProcess.date = medicalRecordData.date
    }
    
    //MARK: - setUpNavigationBar
    ///Setup Naviagation Bar Method
    func setUpNavigationBar() {
        
        self.title = "Edit Medical Record".localized()
        
        self.navigationController?.isNavigationBarHidden = false
        
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSAttributedStringKey.font: UIFont.createPoppinsBoldFont(withSize: 18.0)
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        // set navigation bar color
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 30/255.0, green: 197/255.0, blue: 155/255.0, alpha: 1.0)
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "back_white"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
    }
    
    //MARK:- navigation bar back button action
    ///Back Button Clicked Action Event
    @objc func backButtonPressed() {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- on cancel button action
    ///Cancel Button Clicked Action Event
    @IBAction func cancelButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- on confirm button action
    ///Confirm Button Clicked Action Event
    @IBAction func confirmButtonAction(_ sender: Any) {
        
        if validateTheInputField() {
            
            callEditNewRecordAPi()
        }
    }
    
    ///Record Pages Button Clicked Action Event
    @IBAction func recordPagesButtonAction(_ sender: Any) {
        
        let photosVC = UIStoryboard.getMyRecordStoryBoard().instantiateViewController(withIdentifier: "MyRecordPhotosVC") as! MyRecordPhotosViewController
        
        photosVC.myRecordID = medicalRecordData.recordID!
        photosVC.isAddFiles = true
        
        self.navigationController?.pushViewController(photosVC, animated: true)
        
    }
    
    //MARK:- get edit record params
    ///Get Edit Records Parameters
    func getEditRecordParametersdata () -> [String : String]
    {
        let dictAddNewRecord = [
            "medicalrecord_id" : modelAddNewRecordProcess.recordID,
            "doctor_name" : modelAddNewRecordProcess.doctorName,
            "disease_name" : modelAddNewRecordProcess.diseaseName,
            "hospital_name" : modelAddNewRecordProcess.hospitalName,
            "recorded_date" : modelAddNewRecordProcess.date
        ]
        
        return dictAddNewRecord as! [String : String]
    }
    
    //MARK: - Call Edit record API
    ///Call Edit record API

    func callEditNewRecordAPi()
    {
        self.view.endEditing(true)
        
        let params = getEditRecordParametersdata()
        print("params = \(params)")
        
        HUD.show(.systemActivity, onView: self.view.window)
        
        let urlToHit =  EndPoints.createMedicalRecords.path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .put, parameters: params, shouldDeserialize: true)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    let successMsg = responseDictionary["msg"] as? String
                    
                    UtilityClass.showAlertWithTitle(title: "", message: successMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                    
                        //self.navigationController?.popViewController(animated: true)
                    })
                }
                    
                else
                {
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: {(buttonIndex) in
                        
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    UtilityClass.showAlertWithTitle(title: "", message: replyMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                        
                    })
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }
    
    //MARK : - Validation method
    ///Validation Input Field Values method
    func validateTheInputField() -> Bool {
        
        if modelAddNewRecordProcess.doctorName == nil || modelAddNewRecordProcess.doctorName!.isEmptyString() {
            
            UtilityClass.showAlertWithTitle(title:App_Name, message: "Please enter doctor name.".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
            return false
            
        } else if modelAddNewRecordProcess.diseaseName == nil || modelAddNewRecordProcess.diseaseName!.isEmptyString() {
            
            UtilityClass.showAlertWithTitle(title:App_Name, message: "Please enter disease name.".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
            return false
            
        } else if modelAddNewRecordProcess.hospitalName == nil || modelAddNewRecordProcess.hospitalName!.isEmptyString() {
            
            UtilityClass.showAlertWithTitle(title:App_Name, message: "Please enter hospital name.".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
            return false
            
        } else if modelAddNewRecordProcess.date == nil || modelAddNewRecordProcess.date!.isEmptyString() {
            
            UtilityClass.showAlertWithTitle(title:App_Name, message: "Please select date.".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
            return false
        }
        
        return true
    }
}

//MARK:- Extention -> UITableView -> UITableViewDelegate, UITableViewDataSource
/// Extention -> UITableView -> UITableViewDelegate, UITableViewDataSource
extension EditRecordViewController :UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell : MyRecordAddTableViewCell!
        
        let cellIdentifier = "MyRecordAddTableViewCell"
        
        var cellType = AddRecordType.none
        
        switch indexPath.row {
            
        case 0:
            cellType = .name
            
        case 1:
            cellType = .disease
            
        case 2:
            cellType = .hospital
            
        case 3:
            cellType = .date
            
        default:
            break
        }
        
        cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! MyRecordAddTableViewCell
        
        cell.tag = indexPath.row
        
        cell.cellType = cellType
        
        cell.updateCell()
        
        cell.delegate = self
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return (80 * scaleFactorX)
    }
}

//MARK: - Extention -> PersonalDataCellDelegate
/// Extention -> PersonalDataCellDelegate
extension EditRecordViewController : MyRecordAddTableViewCellDelegate
{
    
    /// Record Add Data Cell
    ///
    /// - Parameters:
    ///   - cell: MyRecordAddTableViewCell
    ///   - inputValue: Input Text Values 
    func recordAddDataCell(_ cell: MyRecordAddTableViewCell, updatedInputFieldText inputValue: String) {
        
        switch cell.cellType {
            
        case .name:
            modelAddNewRecordProcess.doctorName = inputValue
            
        case .disease:
            modelAddNewRecordProcess.diseaseName = inputValue
            
        case .hospital:
            modelAddNewRecordProcess.hospitalName = inputValue
            
        case .date:
            modelAddNewRecordProcess.date = inputValue
            
        default:
            break
        }
    }
}

