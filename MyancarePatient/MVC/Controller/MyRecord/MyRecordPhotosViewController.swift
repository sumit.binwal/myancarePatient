//
//  MyRecordPhotosViewController.swift
//  MyancarePatient
//
//  Created by iOS on 09/02/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit
import PKHUD
import AWSS3


/// MyRecordPhotosViewController
class MyRecordPhotosViewController: UIViewController
{
    
    /// UIButton Refrence - Save Button
    @IBOutlet weak var saveButton: UIButton!
    /// UIButton Refrence - Skip Button
    @IBOutlet weak var skipButton: UIButton!
    /// UIView Refrence - BottomView
    @IBOutlet weak var bottonView: UIView!
    
    /// UICollectionView Refrence - recordFiles Data
    @IBOutlet weak var recordFilesCollectionView: UICollectionView!
    
    /// UiImagePicker Variable
    let imagePicker = UIImagePickerController()
    
    
    /// Variable - myRecordID
    var myRecordID = ""
    
    ///MyRecordFilesAddModels Array
    var selectedRecordedFilesArr = [MyRecordFilesAddModel]()
    var deletingRecordedFilesArr = [MyRecordFilesAddModel]()
    
    ///Bool Variable isAddFiles
    var isAddFiles = false
    
    ///NSMutableArray Variable
    var myRecordAddFilesArr : NSMutableArray = []
    
    
    //MARK:- viewDidLoad
    
    ///ViewController's Life Cycle Methods
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.tabBarController?.tabBar.isHidden = true
        
        getRecordFilesDataFromAPI()
    }

    //MARK:- viewWillAppear
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        setUpNavigationBar()
        
        saveButton.setTitle("Save".localized(), for: .normal)
        skipButton.setTitle("Skip".localized(), for: .normal)
        
        self.saveButton.isHidden = true
        
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        
        self.tabBarController?.tabBar.isHidden = false
    }
    
    //MARK:- viewDidAppear
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
    }
    
    //MARK: - Deinit
    deinit
    {
        print("MyRecordPhotosViewController deinit")
    }
    
    ///MARK:- setUpNavigationBar
    ///setUpNavigationBar
    func setUpNavigationBar()
    {
        self.title = "Medical Records".localized()
        
        self.navigationController?.isNavigationBarHidden = false
        
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSAttributedStringKey.font: UIFont.createPoppinsBoldFont(withSize: 18.0)
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        // set navigation bar color
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 30/255.0, green: 197/255.0, blue: 155/255.0, alpha: 1.0)
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "back_white"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
        
        if isAddFiles
        {
            let deleteImage = #imageLiteral(resourceName: "delete-icon")
            let addImage = #imageLiteral(resourceName: "add-reminder")
            
            let deleteButton  = UIBarButtonItem(image: deleteImage,  style: .plain, target: self, action: #selector(deleteButtonPressed))
            
            let addButton = UIBarButtonItem(image: addImage,  style: .plain, target: self, action: #selector(addButtonPressed))
            
            navigationItem.rightBarButtonItems = [addButton, deleteButton]
        }
    }
    
    //MARK:- navigation bar back button action
    
    /// BackButton Clicked Event
    @objc func backButtonPressed()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- navigation bar delete button action
    /// Delete Button Clicked Event
    @objc func deleteButtonPressed()
    {
        if selectedRecordedFilesArr.count > 0
        {
            UtilityClass.showAlertWithTitle(title: "", message: "Do you really want to delete these files?".localized(), onViewController: self, withButtonArray: ["Confirm".localized()], dismissHandler: { (buttonIndex) in
                
                if buttonIndex == 0
                {
                    self.callDeleteRecordFileMethod()
                }
            })
        }
        else
        {
            UtilityClass.showAlertWithTitle(title: App_Name, message: "Please select atleast one record file.".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
        }
    }
    
    /// Save Button Clicked Event
    @IBAction func saveButtonAction(_ sender: Any)
    {
        if myRecordAddFilesArr.count > 0
        {
            callCreateNewRecordFileAPi()
        }
        else
        {
            UtilityClass.showAlertWithTitle(title: App_Name, message: "Please add atleast one record file.".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
        }
    }
    
    /// Skip Button Clicked Event
    @IBAction func skipButtonAction(_ sender: Any)
    {
        let controllers = self.navigationController?.viewControllers
    
        for controller in controllers!
        {
            if controller .isKind(of: MyRecordSegmentViewController.self)
            {
                self.navigationController?.popToViewController(controller, animated: true)
            }
        }
    }
    
    //MARK:- navigation bar add button action
    /// Add Button Clicked Event
    @objc func addButtonPressed()
    {
        let optionMenu = UIAlertController(title: nil, message: "Choose Image".localized(), preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction(title: "Camera".localized(), style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            
            if UIImagePickerController.isSourceTypeAvailable (UIImagePickerControllerSourceType.camera)
            {
                self.imagePicker.sourceType     = .camera
                self.imagePicker.allowsEditing  = false
                self.imagePicker.delegate       = self
                
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        })
        
        let galleryAction = UIAlertAction(title: "Photo Gallery".localized(), style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            
            if UIImagePickerController.isSourceTypeAvailable (UIImagePickerControllerSourceType.photoLibrary)
            {
                self.imagePicker.sourceType = .photoLibrary
                self.imagePicker.allowsEditing =  false
                self.imagePicker.delegate = self
                
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        })
        
        let cancelAction = UIAlertAction(title: "Cancel".localized(), style: .cancel, handler:
        {
            (alert: UIAlertAction!) -> Void in
        })
        
        optionMenu.addAction(cameraAction)
        optionMenu.addAction(galleryAction)
        optionMenu.addAction(cancelAction)
        
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    //Mark :- get medical record listing from api
    /// GetRecord Files Data From Server
    func getRecordFilesDataFromAPI()
    {
        let urlToHit = EndPoints.getRecordFiles(myRecordID).path
        print(urlToHit)
        
        HUD.show(.systemActivity, onView: self.view.window)
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod:.get, parameters: nil) {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    let medicalRecordArr1 = responseDictionary["data"] as? [[String : Any]]
                    
                    guard medicalRecordArr1 != nil else
                    {
                        return
                    }
                    
                    self.updateModelArray(usingArray: medicalRecordArr1!)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: {(buttonIndex) in
                        
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    UtilityClass.showAlertWithTitle(title: "", message: replyMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                        
                    })
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }
    
    //MARK:- Updating Model Array

    /// Update Model Array Data From Server
    ///
    /// - Parameter array: DataDictionary From Server
    func updateModelArray(usingArray array : [[String:Any]]) -> Void
    {
        myRecordAddFilesArr.removeAllObjects()
        
        let dataArray = array
        
        for dict in dataArray // Iterating dictionaries
        {
            let myrecordFiles = MyRecordFilesAddModel()
            
            myrecordFiles.recordID = dict["id"] as! String
            myrecordFiles.recordName = dict["name"] as! String
            myrecordFiles.recordPathName = dict["path"] as! String
            myrecordFiles.recordImageUrl = dict["file_url"] as! String
            myrecordFiles.recordImageFromServer = 1
            
            self.myRecordAddFilesArr.add(myrecordFiles)
        }
        
        recordFilesCollectionView.reloadData()
        
        if self.myRecordAddFilesArr.count > 0
        {
            self.saveButton.isHidden = false
        }
        else
        {
            self.saveButton.isHidden = true
        }
    }
    

    /// Upload Image Using Image Url
    ///
    /// - Parameter usingImage: usingImage ImageURL
    func uploadImage(usingImage : NSURL) {
        
        HUD.show(.systemActivity, onView: self.view.window)
        
        let ext = "jpg"
        
        let keyName = UtilityClass.getCurrentTimeStamp() + ".jpg"
        
        let uploadRequest = AWSS3TransferManagerUploadRequest()
        uploadRequest?.body = usingImage as URL
        uploadRequest?.key = keyName
        uploadRequest?.bucket = AmazonS3BucketName+UtilityClass.getUserIdData()!
        uploadRequest?.contentType = "image/" + ext
        uploadRequest?.acl = .publicReadWrite
        
        let transferManager = AWSS3TransferManager.default()
        transferManager.upload(uploadRequest!).continueWith { (task) -> AnyObject! in
            
            if let error = task.error {
                print("Upload failed  (\(error))")
                
                DispatchQueue.main.async(execute: {
                    HUD.hide()
                    return
                })
            }
        
            if task.result != nil
            {
                print("Uploaded to:fksjflksdajf askldjf")
                
                DispatchQueue.main.async(execute: {
                    
                    HUD.hide()
                    
                    let myrecordFiles = MyRecordFilesAddModel()
                    
                    // dev = prod
                    myrecordFiles.recordID = self.myRecordID
                    myrecordFiles.recordName = keyName
                    myrecordFiles.recordPathName = "my-records/"+UtilityClass.getUserIdData()!+"/"+keyName
                    myrecordFiles.recordImageUrl = "https://s3-ap-southeast-1.amazonaws.com/" + AWSBucket + "/" + "my-records/"+UtilityClass.getUserIdData()!+"/"+keyName
                    myrecordFiles.recordImageFromServer = 0
                    
                    self.myRecordAddFilesArr.add(myrecordFiles)
                    
                    self.recordFilesCollectionView.reloadData()
                    
                    if self.myRecordAddFilesArr.count > 0 {
                        self.saveButton.isHidden = false
                    }
                    
                    return
                })
            }
            else
            {
                print("Unexpected empty result.")
                DispatchQueue.main.async(execute: {
                    HUD.hide()
                    return
                })
            }
            
            return nil
        }
    }
    
    //MARK:- get params for add new record
    
    /// Add New Record File Parameters Date
    ///
    /// - Returns: Data Dictionary
    func getAddNewRecordFileParametersdata () -> [String : Any]
    {
        let arrToSend : NSMutableArray = []
        for arr in myRecordAddFilesArr {
            
            let myrecordfile = arr as! MyRecordFilesAddModel
            
            let dictAddNewRecordFile = [
                "medical_record" : myrecordfile.recordID,
                "name" : myrecordfile.recordName,
                "path" : myrecordfile.recordPathName
                ] as [String : Any]
            
            arrToSend.add(dictAddNewRecordFile)
        }
        
        let param = [
            "files" : arrToSend
        ]
        
        return param
    }
    
    //MARK: - Call add new record file API
    
    /// Create/Add New Record File APi Call Method
    func callCreateNewRecordFileAPi()
    {
        let params = getAddNewRecordFileParametersdata()
        print(params)
        
        let urlToHit =  EndPoints.uploadRecordFile.path
        
        HUD.show(.systemActivity, onView: self.view.window)
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .post, parameters: params, shouldDeserialize: true)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    let controllers = self.navigationController?.viewControllers
                    for controller in controllers!
                    {
                        if controller .isKind(of: MyRecordSegmentViewController.self)
                        {
                            self.navigationController?.popToViewController(controller, animated: true)
                        }
                    }
                }
                    
                else
                {
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: {(buttonIndex) in
                        
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    UtilityClass.showAlertWithTitle(title: "", message: replyMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                        
                    })
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }
    
    
    /// Delete Record File Api Call Method
    func callDeleteRecordFileMethod() {
        
        deletingRecordedFilesArr.removeAll()
        
        for arr in selectedRecordedFilesArr {
            if arr.recordImageFromServer == 1 {
                deletingRecordedFilesArr.append(arr)
            }
        }
        
        if deletingRecordedFilesArr.count > 0 {
            callDeleteRecordFileAPi()
        } else {
            
            for arr in self.myRecordAddFilesArr {
                
                let myFilesArr = arr as! MyRecordFilesAddModel
                
                for arr1 in self.selectedRecordedFilesArr {
                    if myFilesArr.recordImageUrl == arr1.recordImageUrl {
                        self.myRecordAddFilesArr.remove(myFilesArr)
                        break
                    }
                }
            }
            
            self.selectedRecordedFilesArr.removeAll()
            
            self.recordFilesCollectionView.reloadData()
        }
    }
    
    
    /// Get CommaSeprated String From Array
    ///
    /// - Parameter completeArray: Array of MyReordFileAddModel Data
    /// - Returns: a Comma Seprated String of RecordID
    func getCommaSepratedStringFromArray(completeArray:[MyRecordFilesAddModel]) -> String {
        
        var nameArr: [String] = []
        
        for name in completeArray
        {
            let nameStr = name.recordID
            nameArr.append(nameStr)
        }
        
        let commaSeparatedNameString = nameArr.joined(separator: ",")
        
        return commaSeparatedNameString
    }
    
    //MARK: - Call delete file API
    
    /// Delete Record File Calling API Method
    func callDeleteRecordFileAPi()
    {
        var params : [String : Any]?
        if deletingRecordedFilesArr.count > 1 {
            params = ["recordfiles_id" : getCommaSepratedStringFromArray(completeArray: deletingRecordedFilesArr)]
        } else {
            params = ["recordfiles_id" : deletingRecordedFilesArr[0].recordID ]
        }
        
        print(params ?? "")
        
        HUD.show(.systemActivity, onView: self.view.window)
        
        let urlToHit =  EndPoints.uploadRecordFile.path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .delete, parameters: params, shouldDeserialize: true)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    //let successMsg = responseDictionary["msg"] as? String
                    //
                    //UtilityClass.showAlertWithTitle(title: "", message: successMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                    //
                        for arr in self.myRecordAddFilesArr {
                            
                            let myFilesArr = arr as! MyRecordFilesAddModel
                            
                            for arr1 in self.selectedRecordedFilesArr {
                                if myFilesArr.recordImageUrl == arr1.recordImageUrl {
                                    self.myRecordAddFilesArr.remove(myFilesArr)
                                    break
                                }
                            }
                        }
                        
                        self.selectedRecordedFilesArr.removeAll()
                        self.deletingRecordedFilesArr.removeAll()
                        
                        self.recordFilesCollectionView.reloadData()
                    //})
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: {(buttonIndex) in
                        
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    UtilityClass.showAlertWithTitle(title: "", message: replyMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                        
                    })
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }
}

//MARK: - Extension -> UICollectionView -> UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
extension MyRecordPhotosViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.myRecordAddFilesArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: MyRecordPhotosCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyRecordPhotosCollectionViewCell", for: indexPath) as! MyRecordPhotosCollectionViewCell
        
        let myrecordfile = myRecordAddFilesArr[indexPath.row] as! MyRecordFilesAddModel
        
        cell.recordImageView.setShowActivityIndicator(true)
        cell.recordImageView.setIndicatorStyle(.gray)
        
        cell.recordImageView.sd_setImage(with: URL.init(string: myrecordfile.recordImageUrl), placeholderImage: #imageLiteral(resourceName: "no_image_article"))
        
        if isAddFiles {
            cell.checkButton.isHidden = true
        } else {
            cell.checkButton.isHidden = true
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    
        if isAddFiles
        {
            let cell = collectionView.cellForItem(at: indexPath) as! MyRecordPhotosCollectionViewCell
            
            if cell.checkButton.isHidden
            {
                cell.checkButton.isHidden = false
                selectedRecordedFilesArr.append(myRecordAddFilesArr[indexPath.row] as! MyRecordFilesAddModel)
            }
            else
            {
                cell.checkButton.isHidden = true
                
                for (index, dict) in selectedRecordedFilesArr.enumerated()
                {
                    let myrecord = myRecordAddFilesArr[indexPath.row] as! MyRecordFilesAddModel
                    if dict.recordImageUrl == myrecord.recordImageUrl
                    {
                        cell.checkButton.isHidden = true
                        selectedRecordedFilesArr.remove(at: index)
                    }
                }
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        
        let totalSpace = flowLayout.sectionInset.left + flowLayout.sectionInset.right + (flowLayout.minimumInteritemSpacing * CGFloat(4 - 1))
        
        let size = (collectionView.bounds.width - totalSpace) / CGFloat(4)
        
        return CGSize(width: size, height: 94*scaleFactorX)
    }
}

///UIImagePickerController Delegate Extension Method
extension MyRecordPhotosViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        picker.dismiss(animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        if (info[UIImagePickerControllerOriginalImage] as? UIImage) != nil
        {
            picker.dismiss(animated: false, completion: { () -> Void in
                
                _ = UtilityClass.addFileToFolder("saved", fileName: "save.jpg", fileData: UIImageJPEGRepresentation((info[UIImagePickerControllerOriginalImage] as? UIImage)!, 0.6)!)
                
                if let fileURL = UtilityClass.getFileURLFromFolder("saved", fileName: "save.jpg") {
                    self.uploadImage(usingImage: fileURL as NSURL)
                }
                
                //let imageUrl = info[UIImagePickerControllerImageURL] as? NSURL
                //self.uploadImage(usingImage: imageUrl!)
            })
        }
    }
}


