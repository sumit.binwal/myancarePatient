//
//  PhotosShowViewController.swift
//  MyancarePatient
//
//  Created by iOS on 05/03/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit
import PKHUD


/// PhotosShow View Controller
class PhotosShowViewController: UIViewController {

    
    /// UICollectionView Refrence
    @IBOutlet weak var recordFilesCollectionView: UICollectionView!
    
    /// Variable - My RecordID
    var myRecordID = ""
    
    ///Variable - NSMutableArray Refrence Type
    var myRecordAddFilesArr : NSMutableArray = []
    
    //MARK:- viewDidLoad
    ///View Controller Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    //MARK:- viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tabBarController?.tabBar.isHidden = true
        
        setUpNavigationBar()
        
        getRecordFilesDataFromAPI()
    }
    
    //MARK :- ViewWill Disappear
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.tabBarController?.tabBar.isHidden = false
    }
    
    //MARK:- viewDidAppear
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    //MARK: - Deinit
    deinit {
        print("MyRecordPhotosViewController deinit")
    }
    
    //MARK:- setUpNavigationBar
    
    /// Setup Navigation Bar Method
    func setUpNavigationBar() {
        
        self.title = "Medical Records".localized()
        
        self.navigationController?.isNavigationBarHidden = false
        
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSAttributedStringKey.font: UIFont.createPoppinsBoldFont(withSize: 18.0)
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        // set navigation bar color
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 30/255.0, green: 197/255.0, blue: 155/255.0, alpha: 1.0)
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "back_white"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
    }
    
    //MARK:- navigation bar back button action
    /// Back Button Clicked Event Method
    @objc func backButtonPressed() {
        self.navigationController?.popViewController(animated: true)
    }
    
    //Mark :- get medical record listing from api
    /// Fetch Record Files Data From Server
    func getRecordFilesDataFromAPI()
    {
        let urlToHit = EndPoints.getRecordFiles(myRecordID).path
        print(urlToHit)
        
        HUD.show(.systemActivity, onView: self.view.window)
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod:.get, parameters: nil) {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    let medicalRecordArr1 = responseDictionary["data"] as? [[String : Any]]
                    
                    guard medicalRecordArr1 != nil else
                    {
                        return
                    }
                    
                    self.updateModelArray(usingArray: medicalRecordArr1!)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: {(buttonIndex) in
                        
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    UtilityClass.showAlertWithTitle(title: "", message: replyMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                        
                    })
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }
    
    //MARK:- Updating Model Array
    /// Update Model Array Data Method
    func updateModelArray(usingArray array : [[String:Any]]) -> Void
    {
        myRecordAddFilesArr.removeAllObjects()
        
        let dataArray = array
        
        for dict in dataArray // Iterating dictionaries
        {
            let myrecordFiles = MyRecordFilesAddModel()
            
            myrecordFiles.recordID = self.myRecordID
            myrecordFiles.recordName = dict["name"] as! String
            myrecordFiles.recordPathName = dict["path"] as! String
            myrecordFiles.recordImageUrl = dict["file_url"] as! String
            
            self.myRecordAddFilesArr.add(myrecordFiles)
        }
        
        recordFilesCollectionView.reloadData()
    }
}

//MARK: - Extension -> UICollectionView -> UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
extension PhotosShowViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.myRecordAddFilesArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: MyRecordPhotosCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyRecordPhotosCollectionViewCell", for: indexPath) as! MyRecordPhotosCollectionViewCell
        
        let myrecordfile = myRecordAddFilesArr[indexPath.row] as! MyRecordFilesAddModel
        
        cell.recordImageView.setShowActivityIndicator(true)
        cell.recordImageView.setIndicatorStyle(.gray)
        
        cell.recordImageView.sd_setImage(with: URL.init(string: myrecordfile.recordImageUrl), placeholderImage: #imageLiteral(resourceName: "no_image_article"))
        
        cell.checkButton.isHidden = true
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        
        let totalSpace = flowLayout.sectionInset.left + flowLayout.sectionInset.right + (flowLayout.minimumInteritemSpacing * CGFloat(4 - 1))
        
        let size = (collectionView.bounds.width - totalSpace) / CGFloat(4)
        
        return CGSize(width: size, height: 94*scaleFactorX)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let zoomVC = UIStoryboard.getMyRecordStoryBoard().instantiateViewController(withIdentifier: "ShowPhotosAndZoomVC") as! ShowPhotosAndZoomViewController
        
        zoomVC.myRecordAddFilesArr = myRecordAddFilesArr
        zoomVC.selectedIndex = indexPath.row
        
        self.navigationController?.pushViewController(zoomVC, animated: true)
    }
}
