//
//  RecordShareViewController.swift
//  MyancarePatient
//
//  Created by iOS on 14/02/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit
import PKHUD
import DZNEmptyDataSet

///Record Share View Controller
class RecordShareViewController: UIViewController
{
    //Mark: - Properties
    
    /// UITableView Refrence
    @IBOutlet weak var tableViewRecentDoctorList: UITableView!
    
    
    /// Array Variable - Doctor Model Array
    var doctorArr = [DoctorModel]()
    
    /// Medical Record Model Variable Data
    var myRecordDataShare = MedicalRecord()
    
    /// Doctor Model Data Variable
    var selectedDoctorData = [DoctorModel]()
    
    
    /// Bool Variable - isPaging
    var isPaging = false
    
    
    //MARK: - viewDidLoad
    
    /// View Controller Life Cycle Methods
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.tabBarController?.tabBar.isHidden = true
        
        setUpTableView()
    }
    
    //MARK: - viewWillAppear
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        setUpNavigationBar()
        
        if LocationManager.sharedInstance().isLocationAccessAllowed()
        {
            doctorArr.removeAll()
            isPaging = true
            
            getRecentDoctorsApi()
        }
        else
        {
            UtilityClass.showAlertWithTitle(title: "", message: "To Get Doctor Listing, Please Enable Your Location First".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
        }
        
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        
        self.tabBarController?.tabBar.isHidden = false
    }
    
    //MARK: - Deinit
    deinit
    {
        print("RecordShareViewController deinit")
    }
    
    //MARK: - setUpTableView
    
    /// SetupTable View Method
    func setUpTableView()
    {
        tableViewRecentDoctorList.delegate = self
        tableViewRecentDoctorList.dataSource = self
        
        tableViewRecentDoctorList.emptyDataSetSource = self
        tableViewRecentDoctorList.emptyDataSetDelegate = self
    }
    
    //MARK: - setUpNavigationBar
    /// Setup Navigation Bar Method
    func setUpNavigationBar()
    {
        self.title = "Doctor List".localized()
        
        self.navigationController?.isNavigationBarHidden = false
        
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSAttributedStringKey.font: UIFont.createPoppinsBoldFont(withSize: 18.0)
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        // set navigation bar color
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 30/255.0, green: 197/255.0, blue: 155/255.0, alpha: 1.0)
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "back_white"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Done".localized(), style: .plain, target: self, action: #selector(doneButtonPressed))
    }
    
    //MARK:- navigation bar back button action
    
    /// Back Button Clicked Event
    @objc func backButtonPressed()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- navigation bar done button action
    /// Done Button Clicked Event
    @objc func doneButtonPressed()
    {
        if selectedDoctorData.count == 0
        {
            UtilityClass.showAlertWithTitle(title: App_Name, message: "Please select atleast one doctor.".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
        }
        else
        {
            callShareMedicalRecordFileAPi()
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    //MARK: - get all doctors list from API
    
    /// Fetch Recent Doctors From Server Api Call
    @objc func getRecentDoctorsApi()
    {
        let urlToHit = EndPoints.doctor(String(LocationManager.sharedInstance().newLongitude), String(LocationManager.sharedInstance().newLatitude), "5000000", "", "", "", "", "", "", "1", doctorArr.count).path
        
        print("recent doctor list url = \(urlToHit)")
        
        if doctorArr.count == 0 {
            HUD.show(.systemActivity, onView: self.view.window)
        }
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod:.get, parameters: nil) {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    let doctorData = responseDictionary["data"] as? [[String : Any]]
                    
                    guard doctorData != nil else
                    {
                        return
                    }
                    
                    self.updateDoctorModelArray(usingArray: doctorData!)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: {(buttonIndex) in
                        
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    UtilityClass.showAlertWithTitle(title: "", message: replyMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                        
                    })
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }
    
    //MARK:- Updating Comment Model Array
    
    /// Update Doctor Model Array
    ///
    /// - Parameter array: Dictionary Data Array
    func updateDoctorModelArray(usingArray array : [[String:Any]]) -> Void
    {
        let dataArray = array
        if dataArray.count == 0
        {
            isPaging = false
        }
        
        for dict in dataArray // Iterating dictionaries
        {
            let model = DoctorModel () // Model creation
            model.updateModel(usingDictionary: dict) // Updating model
            doctorArr.append(model) // Adding model to array
        }
        
        tableViewRecentDoctorList.reloadData()
    }
    
    /// get array of shared doctor id's
    func getCommaSepratedStringFromArray(completeArray:[DoctorModel]) -> [String]
    {
        var nameArr: [String] = []
        
        for name in completeArray
        {
            let nameStr = name.doctorID
            nameArr.append(nameStr!)
        }
        
        return nameArr
    }
    
    /// Call share medical record file API
    func callShareMedicalRecordFileAPi()
    {
        let params = [
            "medicalrecord_id" : myRecordDataShare.recordID!,
            "shareWith" : getCommaSepratedStringFromArray(completeArray: selectedDoctorData)] as [String : Any]
        
        print(params)
        
        HUD.show(.systemActivity, onView: self.view.window)
        
        let urlToHit =  EndPoints.medicalRecordShareWith.path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .put, parameters: params, shouldDeserialize: true)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    let successMsg = responseDictionary["msg"] as? String
                    
                    UtilityClass.showAlertWithTitle(title: "", message: successMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                        
                        self.navigationController?.popViewController(animated: true)
                    })
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: {(buttonIndex) in
                        
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    UtilityClass.showAlertWithTitle(title: "", message: replyMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                        
                    })
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }
}

//Mark:- Extension -> UITableView -> UITableViewDelegate, UITableViewDataSource
/// Extension -> UITableView -> UITableViewDelegate, UITableViewDataSource
extension RecordShareViewController : UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return doctorArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell: DoctorsCustomCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier_DoctorsCell) as! DoctorsCustomCell
        
        cell.labelDoctorName.text = doctorArr[indexPath.row].doctorName
        
        let arrSpec : NSArray = doctorArr[indexPath.row].specializations! as NSArray
        var specilaization = ""
        
        for i in 0 ..< arrSpec.count
        {
            let dict = arrSpec.object(at: i) as! NSDictionary
            specilaization = specilaization.appending(dict.object(forKey: "name") as! String)
            
            if i != arrSpec.count - 1
            {
                specilaization = specilaization.appending(", ")
            }
        }
        
        cell.labelDoctorType.text = specilaization
        
        cell.labelDoctorLocation.text = doctorArr[indexPath.row].districttown?["town"] as? String
        
        cell.imageViewProfilePic.setShowActivityIndicator(true)
        cell.imageViewProfilePic.setIndicatorStyle(.gray)
        
        cell.imageViewProfilePic.sd_setImage(with: URL.init(string: doctorArr[indexPath.row].imageUrl!), placeholderImage: #imageLiteral(resourceName: "no_image_user"))
        
        if doctorArr[indexPath.row].online_status == "Online"
        {
            cell.imageViewStatus.image = #imageLiteral(resourceName: "greenDot")
        }
        else if doctorArr[indexPath.row].online_status == "Busy"
        {
            cell.imageViewStatus.image = #imageLiteral(resourceName: "redDot")
        }
        else
        {
            cell.imageViewStatus.image = #imageLiteral(resourceName: "greyDot")
        }
        
        if !doctorArr[indexPath.row].isStatusUpdated!
        {
            cell.getDoctorStatusApi(doctor_id: doctorArr[indexPath.row].doctorID!, index: indexPath.row)
        }
        
        if isPaging && indexPath.row == doctorArr.count - 1
        {
            getRecentDoctorsApi()
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 106 * scaleFactorX
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let cell = tableView.cellForRow(at: indexPath) as! DoctorsCustomCell
        
        if cell.checkedImage.isHidden
        {
            cell.checkedImage.isHidden = false
            selectedDoctorData.append(doctorArr[indexPath.row])
        }
        else
        {
            cell.checkedImage.isHidden = true
            
            for (index, dict) in selectedDoctorData.enumerated()
            {
                if dict.doctorID == doctorArr[indexPath.row].doctorID
                {
                    cell.checkedImage.isHidden = true
                    selectedDoctorData.remove(at: index)
                }
            }
        }
    }
}

//MARK:- Extention -> EZEmptyDataSet Delegate Method -> UITableViewDelegate, UITableViewDataSource
/// Extention -> EZEmptyDataSet Delegate Method -> UITableViewDelegate, UITableViewDataSource
extension RecordShareViewController : DZNEmptyDataSetSource, DZNEmptyDataSetDelegate
{
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString!
    {
        let text = "No Record Found".localized()
        
        let attributes = [NSAttributedStringKey.font:UIFont .boldSystemFont(ofSize: 16 * scaleFactorX), NSAttributedStringKey.foregroundColor:UIColor.black]
        
        return NSAttributedString (string: text, attributes: attributes)
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool
    {
        return true
    }
}
