//
//  PaymentViewController.swift
//  MyancarePatient
//
//  Created by Ratina on 5/9/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit
import PKHUD

/// This is the class of UIViewController used to show add payment gateway in weview
class PaymentViewController: UIViewController
{
    /// UIwebview varable to show payment gateway
    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.webView.delegate = self
    }

    //MARK: - viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        // Do any additional setup after appearing the view.
        
        self.tabBarController?.tabBar.isHidden = true
        
        setUpNavigationBar()
        
        HUD.show(.systemActivity, onView: self.view.window)
        callPaymentUrl()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.isNavigationBarHidden = true
    }
    
    //MARK: - Deinit
    deinit {
        print("PaymentViewController deinit")
    }
    
    // MARK: - setUpNavigationBar
    ///setUpNavigationBar
    func setUpNavigationBar() {
        
        self.title = "Payment".localized()
        
        self.navigationController?.isNavigationBarHidden = false
        
        UIApplication.shared.isStatusBarHidden = false
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        
        navigationController?.navigationBar.titleTextAttributes =
            [
                NSAttributedStringKey.font: UIFont.createPoppinsBoldFont(withSize: 18.0)
        ]
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        
        //remove navigation bar bottom line
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        navigationBar?.shadowImage = UIImage()
        
        // set navigation bar color
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 30/255.0, green: 197/255.0, blue: 155/255.0, alpha: 1.0)
        
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "back_white"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(backButtonPressed))
    }
    
    // MARK: - Navigation Back Button Action
    ///Navigation Back Button Action
    @objc func backButtonPressed() {
        self.navigationController?.popViewController(animated: true)
    }
    
    ///call Payment URL Method
    func callPaymentUrl()
    {
        print("url = \(EndPoints.paymentUrl(UtilityClass.getUserIdData()!).path)")
        self.webView.loadRequest(URLRequest.init(url: EndPoints.paymentUrl(UtilityClass.getUserIdData()!).path))
    }
}

///UIWebView Delegate Extension Method
extension PaymentViewController : UIWebViewDelegate
{
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
        HUD.hide()
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool
    {
        print("request.url?.absoluteString ===> ", request.url?.absoluteString ?? "")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0)
        {
            // your code here
            if (request.url?.absoluteString.contains("mobile_response?result=rejected"))!
            {
                print("rejected")
                self.navigationController?.popViewController(animated: true)
            }
            else if (request.url?.absoluteString.contains("mobile_response?result=canceled_by_user"))!
            {
                print("canceled_by_user")
                self.navigationController?.popViewController(animated: true)
            }
            else if (request.url?.absoluteString.contains("mobile_response?result=failed"))!
            {
                print("failed")
                self.navigationController?.popViewController(animated: true)
            }
            else if (request.url?.absoluteString.contains("mobile_response?result=success"))!
            {
                print("success")
                self.navigationController?.popViewController(animated: true)
            }
            else if (request.url?.absoluteString.contains("mobile_response?result=pending"))!
            {
                print("pending")
                self.navigationController?.popViewController(animated: true)
            }
        }
        
        return true
    }
}
