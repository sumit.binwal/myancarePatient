//
//  PasscodeViewController.swift
//  MyancarePatient
//
//  Created by iOS on 18/01/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit
import LocalAuthentication

///Passcode View Controller
class PasscodeViewController: UIViewController {
    
    ///UIStatus Bar Style Variable Refrence
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    ///Bool Variable - is From Passcode Setting
    var isFromPasscodeSetting = false
    
    ///NSLayoutConstraint Variable -topCenterYConstraint
    @IBOutlet weak var topCenterYConstraint: NSLayoutConstraint!
    ///NSLayoutConstraint Variable -titleIndentYConstraint

    @IBOutlet weak var titleIndentYConstraint: NSLayoutConstraint!
    ///UILabel Refrence - Title label Text
    @IBOutlet weak var titleLabel: UILabel!
    ///NSLayoutConstraint Variable -pinsIndentYConstraint

    @IBOutlet weak var pinsIndentYConstraint: NSLayoutConstraint!
    ///NSLayoutConstraint Variable -pinDiameterConstraint
    @IBOutlet weak var pinDiameterConstraint: NSLayoutConstraint!
    ///NSLayoutConstraint Variable -distanceBetweenPinsXConstraint
    @IBOutlet weak var distanceBetweenPinsXConstraint: NSLayoutConstraint!
    ///NSLayoutConstraint Variable -keyDiameterConstraint
    @IBOutlet weak var keyDiameterConstraint: NSLayoutConstraint!
    ///NSLayoutConstraint Variable -distanceBetweenKeysXConstraint
    @IBOutlet weak var distanceBetweenKeysXConstraint: NSLayoutConstraint!
    ///NSLayoutConstraint Variable -distanceBetweenKeysYConstraint
    @IBOutlet weak var distanceBetweenKeysYConstraint: NSLayoutConstraint!
    ///NSLayoutConstraint Variable -bottomButtonsIndentCenterYConstraint
    @IBOutlet weak var bottomButtonsIndentCenterYConstraint: NSLayoutConstraint!
    ///NSLayoutConstraint Variable -pinsViewCenterXConstraint
    @IBOutlet weak var pinsViewCenterXConstraint: NSLayoutConstraint!
    ///UIView Refrence Variable - pin1View
    @IBOutlet weak var pin1View: UIView!
    ///UIView Refrence Variable - pin2View
    @IBOutlet weak var pin2View: UIView!
    ///UIView Refrence Variable - pin3View
    @IBOutlet weak var pin3View: UIView!
    ///UIView Refrence Variable - pin4View
    @IBOutlet weak var pin4View: UIView!
    ///UIView Refrence Variable - key1BackgroundView
    @IBOutlet weak var key1BackgroundView: UIView!
    ///UIView Refrence Variable - key2BackgroundView
    @IBOutlet weak var key2BackgroundView: UIView!
    ///UIView Refrence Variable - key3BackgroundView
    @IBOutlet weak var key3BackgroundView: UIView!
    ///UIView Refrence Variable - key4BackgroundView
    @IBOutlet weak var key4BackgroundView: UIView!
    ///UIView Refrence Variable - key5BackgroundView
    @IBOutlet weak var key5BackgroundView: UIView!
    ///UIView Refrence Variable - key6BackgroundView
    @IBOutlet weak var key6BackgroundView: UIView!
    ///UIView Refrence Variable - key7BackgroundView
    @IBOutlet weak var key7BackgroundView: UIView!
    ///UIView Refrence Variable - key8BackgroundView
    @IBOutlet weak var key8BackgroundView: UIView!
    ///UIView Refrence Variable - key9BackgroundView
    @IBOutlet weak var key9BackgroundView: UIView!
    ///UIView Refrence Variable - key0BackgroundView
    @IBOutlet weak var key0BackgroundView: UIView!
    ///UIButton Refrence Variable - Key 1 Button
    @IBOutlet weak var key1Button: UIButton!
    ///UIButton Refrence Variable - Key 2 Button
    @IBOutlet weak var key2Button: UIButton!
    ///UIButton Refrence Variable - Key 3 Button
    @IBOutlet weak var key3Button: UIButton!
    ///UIButton Refrence Variable - Key 4 Button
    @IBOutlet weak var key4Button: UIButton!
    ///UIButton Refrence Variable - Key 5 Button
    @IBOutlet weak var key5Button: UIButton!
    ///UIButton Refrence Variable - Key 6 Button
    @IBOutlet weak var key6Button: UIButton!
    ///UIButton Refrence Variable - Key 7 Button
    @IBOutlet weak var key7Button: UIButton!
    ///UIButton Refrence Variable - Key 8 Button
    @IBOutlet weak var key8Button: UIButton!
    ///UIButton Refrence Variable - Key 9 Button
    @IBOutlet weak var key9Button: UIButton!
    ///UIButton Refrence Variable - Key 0 Button
    @IBOutlet weak var key0Button: UIButton!
    ///UIButton Refrence Variable - Delete Button
    @IBOutlet weak var deleteButton: UIButton!
    ///UIColor Refrence Variable - Pin Active Color
    let pinActiveColor: UIColor = .white
    ///UIColor Refrence Variable - Pin InActive Color
    let pinInactiveColor: UIColor = .clear
    ///CGFloat Refrence Variable - keyTouchDownAlpha
    let keyTouchDownAlpha: CGFloat = 0.6
    ///CGFloat Refrence Variable - keyTouchUpAlpha
    let keyTouchUpAlpha: CGFloat = 0.2
    ///Array Refrence Variable - Collection of UIViews (PinViews)
    var pinViews: [UIView] = []
    ///Array Refrence Variable - Collection of UIViews (Key Background Views)
    var keyBackgroundViews: [UIView] = []
    ///Array Refrence Variable - Collection of UiButton (Key Buttons)
    var keyButtons: [UIButton] = []
    ///TimeInterval Refrence Variable - Key ANimate Duration
    let keyAnimateDuration: TimeInterval = 0.4
    ///TimeInterval Refrence Variable - timerStep Duration
    let timerStep: TimeInterval = 0.01
    ///CGFloat Refrence Variable - Key Alpha Step
    var keyAlphaStep: CGFloat = 0
    ///Array Refrence Variable - Collection of Timers (keyTimers)
    var keyTimers: [Timer?] = Array(repeating: nil, count: 10)
    ///String Variable - Store Pin Code
    var pinCode: String = ""
    
    ///UIViewController LIfe Cycle Methods
    
    //MARK :- UIViewController LIfe Cycle Methods
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        let keyboard = Keyboard()
        
        topCenterYConstraint.constant = keyboard.topCenterY
        
        titleIndentYConstraint.constant = keyboard.titleIndentY
        titleLabel.font = keyboard.titleFont
        
        pinsIndentYConstraint.constant = keyboard.pinsIndentY
        pinDiameterConstraint.constant = keyboard.pinDiameter
        distanceBetweenPinsXConstraint.constant = keyboard.distanceBetweenPinsX
        
        keyDiameterConstraint.constant = keyboard.keyDiameter
        distanceBetweenKeysXConstraint.constant = keyboard.distanceBetweenKeysX
        distanceBetweenKeysYConstraint.constant = keyboard.distanceBetweenKeysY
        
        bottomButtonsIndentCenterYConstraint.constant = keyboard.bottomButtonsIndentCenterY
        
        pinViews = [pin1View, pin2View, pin3View, pin4View]
        
        keyBackgroundViews = [key0BackgroundView, key1BackgroundView, key2BackgroundView, key3BackgroundView, key4BackgroundView, key5BackgroundView, key6BackgroundView, key7BackgroundView, key8BackgroundView, key9BackgroundView]
        
        keyButtons = [key0Button, key1Button, key2Button, key3Button, key4Button, key5Button, key6Button, key7Button, key8Button, key9Button]
        
        for item in pinViews {
            item.layer.cornerRadius = keyboard.pinDiameter / 2
            item.layer.borderWidth  = 1.2
            item.layer.borderColor = pinActiveColor.cgColor
            item.backgroundColor = .clear
        }
        
        for item in keyBackgroundViews {
            item.layer.cornerRadius = keyboard.keyDiameter / 2
            item.alpha = keyTouchUpAlpha
            item.backgroundColor = .white
        }
        
        keyAlphaStep = (keyTouchDownAlpha - keyTouchUpAlpha) / CGFloat(keyAnimateDuration / timerStep)
        
        self.navigationController?.isNavigationBarHidden = true
    }
    
    //MARK: - viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tabBarController?.tabBar.isHidden = true
        
        if isFromPasscodeSetting {
            if UtilityClass.getPasscode() == "" {
                titleLabel.text = "Enter Passcode".localized()
            } else {
                titleLabel.text = "Enter Old Passcode".localized()
            }
        } else {
            titleLabel.text = "Enter Passcode".localized()
        }
        
        deleteButton.setTitle("Cancel".localized(), for: .normal)
    }
    
    // MARK: - Action
    
    @IBAction func keyButtonTouchDown(_ sender: UIButton) { addKey(keyIndex(sender)) }
    @IBAction func keyButtonTouchUpInside(_ sender: UIButton) { startTimer(keyIndex(sender)) }
    @IBAction func keyButtonTouchUpOutside(_ sender: UIButton) { cancelKey(keyIndex(sender)) }
    @IBAction func clickDeleteButton(_ sender: UIButton) { cancelKey() }
    
    func keyIndex(_ button: UIButton) -> Int { return keyButtons.index(of: button) ?? 0 }
    
    func addKey(_ index: Int) {
        if pinCode.count < 4 {
            pinCode += String(index)
            drawPinCode()
        }
        
        stopTimer(index)
        
        if pinCode.count >= 4 {
            let alpha = keyBackgroundViews[index].alpha - keyAlphaStep
            
            if alpha > keyTouchUpAlpha {
                keyBackgroundViews[index].alpha = alpha
            } else {
                keyBackgroundViews[index].alpha = keyTouchUpAlpha
                stopTimer(index)
            }
        } else {
            keyBackgroundViews[index].alpha = keyTouchDownAlpha
        }
    }
    
    func cancelKey(_ index: Int? = nil) {
        if !pinCode.isEmpty {
            pinCode.removeLast()
            drawPinCode()
        }
        
        if let index = index { keyBackgroundViews[index].alpha = keyTouchUpAlpha }
    }
    
    func drawPinCode() {
        let count = pinCode.count
        
        var title = "Delete".localized()
        if count == 0 {
            title = "Cancel".localized()
        }
        
        if deleteButton.currentTitle != title {
            deleteButton.titleLabel?.text = title
            deleteButton.setTitle(title, for: .normal)
        }
        
        for i in 0...3 {
            if count >= i+1 {
                pinViews[i].backgroundColor = pinActiveColor
            }
            else {
                pinViews[i].backgroundColor = pinInactiveColor
            }
        }
        
        if count >= 4 {
            // pinCode verification
            
            if isFromPasscodeSetting {
                
                if titleLabel.text == "Enter Old Passcode".localized() {
                    if UtilityClass.getPasscode() == pinCode {
                        titleLabel.text = "Enter New Passscode".localized()
                        pinCode = ""
                        self.drawPinCode()
                        self.keyboardUserInteractionEnabled(true)
                    } else {
                        wrongPasscodeAlert()
                    }
                } else {
                    UtilityClass.setPasscode(passcodString: pinCode)
                    successfulAlert()
                }
            } else {
                if UtilityClass.getPasscode() == "" {
                    
                    UtilityClass.setPasscodeOnOff(passcodeOnOff: true)
                    UtilityClass.setPasscode(passcodString: pinCode)
                    
                    successfulAlert()
                } else {
                    if pinCode == UtilityClass.getPasscode() {
                        redirectToNextScreen()
                    } else {
                        // If the pinCode is incorrect, then
                        pinViewsAnimation()
                    }
                }
            }
        }
    }
    
    //MARK: - BackButton Action
    @IBAction func onBackButtonAction(_ sender: UIButton)
    {
        navigationController?.popViewController(animated: true)
    }
    
    func wrongPasscodeAlert() {

        UtilityClass.showAlertWithTitle(title: App_Name, message: "Please enter correct passcode.".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
    }
    
    func successfulAlert()
    {
        let alertController = UIAlertController.init(title: "Passcode set Successfully".localized(), message: "Do you really want to enable your TouchID?".localized(), preferredStyle: .alert)
        
        let yes_action = UIAlertAction.init(title: "Yes Passcode".localized(), style: .default) { (_) in
            
            self.authenticateUserForEnableTouchID()
        }
        
        let no_action = UIAlertAction.init(title: "No Passcode".localized(), style: .cancel) { (_) in
            
            self.redirectToNextScreen()
        }
        
        alertController.addAction(yes_action)
        alertController.addAction(no_action)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func redirectToNextScreen() {
        print("redirect to next screen")
        
        if isFromPasscodeSetting {
            self.navigationController?.popViewController(animated: true)
        } else {
            if (UtilityClass.getUserSidData()) != nil
            {
                let homeTabVC = UIStoryboard.getHomeStoryBoard().instantiateInitialViewController() as! HomeTabBarController
                UtilityClass.changeRootViewController(with: homeTabVC)
            }
            else
            {
                let languageScreen = UIStoryboard.getLanguageStoryBoard().instantiateViewController(withIdentifier: "languageVC") as! LanguageViewController
                self.navigationController?.pushViewController(languageScreen, animated: true)
            }
        }
    }
    
    func authenticateUserForEnableTouchID() {
        let context = LAContext()
        
        var error: NSError?
        
        let reasonString = "Authentication is needed to access \(ApplicationName)".localized()
        
        if context.canEvaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, error: &error) {
            
            context.evaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, localizedReason: reasonString, reply: { (success, evalPolicyError) in
                
                if success {
                    print("success")
                    UtilityClass.enableTouchIDOrNot(enable: true)
                    
                    DispatchQueue.main.async {
                        print("success")
                        UtilityClass.enableTouchIDOrNot(enable: true)
                        self.redirectToNextScreen()
                    }
                }
                else{
                    
                    print("evalPolicyError = \(String(describing: evalPolicyError?.localizedDescription))")
                    
                    switch evalPolicyError?._code {
                        
                    case LAError.systemCancel.rawValue?:
                        print("Authentication was cancelled by the system")
                        DispatchQueue.main.async {
                            self.redirectToNextScreen()
                        }
                        
                    case LAError.userCancel.rawValue?:
                        print("Authentication was cancelled by the user")
                        DispatchQueue.main.async {
                            self.redirectToNextScreen()
                        }
                        
                    case LAError.userFallback.rawValue?:
                        print("User selected to enter custom password")
                        DispatchQueue.main.async {
                            self.redirectToNextScreen()
                        }
                        
                    default:
                        print("Authentication failed")
                        self.redirectToNextScreen()
                    }
                }
            })
            
        } else {
            
            print("error = \(String(describing: error))")
            
            // If the security policy cannot be evaluated then show a short message depending on the error.
            if #available(iOS 11.0, *) {
                switch error!.code{
                    
                case LAError.passcodeNotSet.rawValue:
                    print("A passcode has not been set")
                    self.redirectToNextScreen()
                    
                case LAError.biometryLockout.rawValue:
                    print("biometryLockout")
                    self.redirectToNextScreen()
                    
                case LAError.biometryNotAvailable.rawValue:
                    print("biometryNotAvailable")
                    self.redirectToNextScreen()
                    
                case LAError.biometryNotEnrolled.rawValue:
                    print("biometryNotEnrolled")
                    self.redirectToNextScreen()
                    
                default:
                    // The LAError.TouchIDNotAvailable case.
                    print("TouchID not available")
                    self.redirectToNextScreen()
                }
            } else {
                // Fallback on earlier versions
                switch error!.code{
                    
                case LAError.Code.touchIDNotEnrolled.rawValue:
                    print("TouchID not enrolled")
                    self.redirectToNextScreen()
                    
                case LAError.Code.passcodeNotSet.rawValue:
                    print("A passcode has not been set")
                    self.redirectToNextScreen()
                    
                default:
                    print("Authentication failed")
                    self.redirectToNextScreen()
                }
            }
        }
    }
    
    func keyboardUserInteractionEnabled(_ isUserInteractionEnabled: Bool) {
        for item in keyButtons { item.isUserInteractionEnabled = isUserInteractionEnabled }
    }
    
    func pinViewsAnimation() {
        keyboardUserInteractionEnabled(false)
        
        let duration = 0.1
        let steps: [CGFloat] = [-50, 100, -80, 60, -40, 20, -10]
        
        UIView.animateKeyframes(withDuration: duration * Double(steps.count), delay: 0, options: [], animations: {
            var startTime = 0.0
            
            for step in steps {
                UIView.addKeyframe(withRelativeStartTime: startTime, relativeDuration: duration, animations: {
                    self.pinsViewCenterXConstraint.constant += step
                    self.view.layoutIfNeeded()
                })
                
                startTime += duration
            }
        }, completion: { (finished: Bool) in
            self.pinCode = ""
            self.drawPinCode()
            self.keyboardUserInteractionEnabled(true)
        })
    }
    
    // MARK: - Timer
    
    func startTimer(_ index: Int) {
        keyTimers[index] = Timer.scheduledTimer(timeInterval: timerStep, target: self, selector: #selector(keyAnimation), userInfo: index, repeats: true)
    }
    
    func stopTimer(_ index: Int) {
        keyTimers[index]?.invalidate()
        keyTimers[index] = nil
    }
    
    @objc func keyAnimation(timer: Timer) {
        guard let index = timer.userInfo as? Int else { return }
        
        let alpha = keyBackgroundViews[index].alpha - keyAlphaStep
        
        if alpha > keyTouchUpAlpha {
            keyBackgroundViews[index].alpha = alpha
        } else {
            keyBackgroundViews[index].alpha = keyTouchUpAlpha
            stopTimer(index)
        }
    }
    
}

class Keyboard {
    
    let topCenterY: CGFloat
    
    let titleIndentY: CGFloat
    let titleFont: UIFont
    
    let pinsIndentY: CGFloat
    let pinDiameter: CGFloat
    let distanceBetweenPinsX: CGFloat
    
    let keyDiameter: CGFloat
    let distanceBetweenKeysX: CGFloat
    let distanceBetweenKeysY: CGFloat
    
    let bottomButtonsIndentCenterY: CGFloat
    
    init() {
        switch UIScreen.maxSize {
            
        case UIScreen.maxSizeIPhone4:
            topCenterY = -132
            
            titleIndentY = 11
            titleFont = .systemFont(ofSize: 18)
            
            pinsIndentY = 18
            pinDiameter = 14
            distanceBetweenPinsX = 23
            
            keyDiameter = 76
            distanceBetweenKeysX = 19
            distanceBetweenKeysY = 12
            
            bottomButtonsIndentCenterY = 45
            
        case UIScreen.maxSizeIPhone5:
            topCenterY = -122
            
            titleIndentY = 11
            titleFont = .systemFont(ofSize: 18)
            
            pinsIndentY = 38
            pinDiameter = 14
            distanceBetweenPinsX = 23
            
            keyDiameter = 76
            distanceBetweenKeysX = 19
            distanceBetweenKeysY = 12
            
            bottomButtonsIndentCenterY = 73
            
        case UIScreen.maxSizeIPhone6:
            topCenterY = -128
            
            titleIndentY = 24
            titleFont = .systemFont(ofSize: 19)
            
            pinsIndentY = 52
            pinDiameter = 14
            distanceBetweenPinsX = 24
            
            keyDiameter = 76
            distanceBetweenKeysX = 27
            distanceBetweenKeysY = 14
            
            bottomButtonsIndentCenterY = 111
            
        case UIScreen.maxSizeIPhoneX:
            topCenterY = -111
            
            titleIndentY = 21
            titleFont = .systemFont(ofSize: 22)
            
            pinsIndentY = 52
            pinDiameter = 14
            distanceBetweenPinsX = 24
            
            keyDiameter = 76
            distanceBetweenKeysX = 27
            distanceBetweenKeysY = 14
            
            bottomButtonsIndentCenterY = 148
            
        case UIScreen.maxSizeIPhonePlus:
            topCenterY = -138
            
            titleIndentY = 28
            titleFont = .systemFont(ofSize: 22)
            
            pinsIndentY = 60
            pinDiameter = 14
            distanceBetweenPinsX = 28
            
            keyDiameter = 82
            distanceBetweenKeysX = 32
            distanceBetweenKeysY = 17
            
            bottomButtonsIndentCenterY = 123
            
        case UIScreen.maxSizeIPad_9_7:
            topCenterY = -142
            
            titleIndentY = 18
            titleFont = .systemFont(ofSize: 22)
            
            pinsIndentY = 61
            pinDiameter = 16
            distanceBetweenPinsX = 30
            
            keyDiameter = 82
            distanceBetweenKeysX = 32
            distanceBetweenKeysY = 19
            
            bottomButtonsIndentCenterY = 0
            
        case UIScreen.maxSizeIPad_10_5:
            topCenterY = -142
            
            titleIndentY = 18
            titleFont = .systemFont(ofSize: 22)
            
            pinsIndentY = 61
            pinDiameter = 16
            distanceBetweenPinsX = 30
            
            keyDiameter = 82
            distanceBetweenKeysX = 32
            distanceBetweenKeysY = 19
            
            bottomButtonsIndentCenterY = 0
            
        case UIScreen.maxSizeIPad_12_9:
            topCenterY = -142
            
            titleIndentY = 18
            titleFont = .systemFont(ofSize: 22)
            
            pinsIndentY = 61
            pinDiameter = 16
            distanceBetweenPinsX = 30
            
            keyDiameter = 82
            distanceBetweenKeysX = 32
            distanceBetweenKeysY = 19
            
            bottomButtonsIndentCenterY = 0
            
        default:
            topCenterY = -132
            
            titleIndentY = 11
            titleFont = .systemFont(ofSize: 18)
            
            pinsIndentY = 18
            pinDiameter = 14
            distanceBetweenPinsX = 23
            
            keyDiameter = 76
            distanceBetweenKeysX = 19
            distanceBetweenKeysY = 12
            
            bottomButtonsIndentCenterY = 45
        }
    }
    
}

extension UIScreen {
    
    static let maxSize: CGFloat = { return max(main.bounds.size.width, main.bounds.size.height) }()
    
    static let maxSizeIPhone4:    CGFloat = { return  480 }()
    static let maxSizeIPhone5:    CGFloat = { return  568 }()
    static let maxSizeIPhone6:    CGFloat = { return  667 }()
    static let maxSizeIPhonePlus: CGFloat = { return  736 }()
    static let maxSizeIPhoneX:    CGFloat = { return  812 }()
    static let maxSizeIPad_9_7:   CGFloat = { return 1024 }()
    static let maxSizeIPad_10_5:  CGFloat = { return 1112 }()
    static let maxSizeIPad_12_9:  CGFloat = { return 1366 }()
    
}
