//
//  ConfirmAppointmentViewController.swift
//  MyancarePatient
//
//  Created by iOS on 06/02/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit
import PKHUD
///ConfirmAppointment ViewController
class ConfirmAppointmentViewController: UIViewController {

    ///UILabel Refrence - Reason Label Text
    @IBOutlet weak var reasonLabel: UILabel!
    ///UIButton Refrence - Cancel Label Text
    @IBOutlet weak var cancelButton: UIButton!
    ///UIButton Refrence - Confirm Label Text
    @IBOutlet weak var confirmButton: UIButton!
    ///UITextView Refrence - visit reason Text View
    @IBOutlet weak var visitReasonTextView: UITextView!
    ///UIView Refrence - OuterView
    @IBOutlet weak var outerView: UIView!
    ///UiImageView Refrence - Background Image
    @IBOutlet weak var backgroundImage: UIImageView!
    
    ///Variable UIImage - Backgrond Image Screen
    var backgroungImageScreen : UIImage?
    
    ///Variable - Visit Reason PlaceHolder Text
    let visitReasonPlaaceHolder = "Type visit reason here".localized()
    
    //MARK: - viewDidLoad
    ///UIVIewController Life Cycle Method
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setUpView()
    }

    //MARK: - viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tabBarController?.tabBar.isHidden = true
        
        self.backgroundImage.image = backgroungImageScreen
        
        self.navigationController?.isNavigationBarHidden = true
    }
    
    //MARK: - viewWillDisappear
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationController?.isNavigationBarHidden = false
    }
    
    //MARK: - Deinit
    deinit {
        print("ConfirmAppointmentViewController deinit")
    }
    
    //MARK: - setUpView
    ///SetupView Method
    func setUpView() {
        
        visitReasonTextView.delegate = self
        
        visitReasonTextView.textColor = UIColor.lightGray
        visitReasonTextView.text = visitReasonPlaaceHolder
        
        cancelButton.layer.cornerRadius = 5.0
        cancelButton.layer.borderColor = UIColor.MyanCarePatient.appDefaultGreenColor.cgColor
        cancelButton.layer.borderWidth = 1.0
        cancelButton.layer.masksToBounds = true
        
        confirmButton.layer.cornerRadius = 5.0
        confirmButton.layer.borderColor = UIColor.MyanCarePatient.appDefaultGreenColor.cgColor
        confirmButton.layer.borderWidth = 1.0
        confirmButton.layer.masksToBounds = true
        
        outerView.layer.cornerRadius = 5.0
        outerView.layer.borderColor = UIColor.MyanCarePatient.appDefaultGreenColor.cgColor
        outerView.layer.borderWidth = 1.0
        outerView.layer.masksToBounds = true
        
        visitReasonTextView.layer.cornerRadius = 5.0
        visitReasonTextView.layer.borderColor = UIColor.lightGray.cgColor
        visitReasonTextView.layer.borderWidth = 1.0
        visitReasonTextView.layer.masksToBounds = true
        
        reasonLabel.text = "Reason for visit:".localized()
        cancelButton.setTitle("Cancel".localized(), for: .normal)
        confirmButton.setTitle("Confirm".localized(), for: .normal)
    }
    
    //MARK: - on confirm Button Action
    ///on confirm Button Action
    @IBAction func confirmButtonAction(_ sender: Any) {
        
        if visitReasonTextView.text == visitReasonPlaaceHolder {
            UtilityClass.showAlertWithTitle(title: "", message: "Please enter visiting reason for Appointment.".localized(), onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                
            })
        } else if visitReasonTextView.text == "" {
            UtilityClass.showAlertWithTitle(title: "", message: "Please enter visiting reason for Appointment.".localized(), onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                
            })
        } else {
            modelAppointmentData.appointmentReason = visitReasonTextView.text
            
            if modelAppointmentData.isReschedule {
                callRescheduleAppointmentAPi()
            } else {
                callBookAppointmentAPi()
            }
        }
    }
    
    //MARK: - on cancel Button Action
    ///on cancel Button Action
    @IBAction func cancelButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    //MARK: - Call Book Appointment API
    ///Call Book Appointment API
    func callBookAppointmentAPi()
    {
        self.view.endEditing(true)
        
        let params = modelAppointmentData.getAppointmentDictionary()
        
        print("parameters = ", params)
        
        let urlToHit =  EndPoints.bookAppointment.path
        
        HUD.show(.systemActivity, onView: self.view.window)
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .post, parameters: params, shouldDeserialize: true)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    let bookingMsg = responseDictionary["msg"] as? String
                    
                    UtilityClass.showAlertWithTitle(title: "", message: bookingMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                        
                        var wallet_balance = UtilityClass.getUserInfoData()["wallet_balance"] as! Double
                        
                        var appointmentPrice = modelAppointmentData.total_consultant_fees
                        
                        appointmentPrice = appointmentPrice.replacingOccurrences(of: " Kyat", with: "")
                        
                        let appointmentFees = Double(appointmentPrice)!
                        
                        wallet_balance = wallet_balance - appointmentFees
                       
                        UtilityClass.setWalletBalanceInUserInfo (walletBalance: wallet_balance)
                        
                        self.navigationController?.popToRootViewController (animated: false)
                    })
                }
                    
                else
                {
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: {(buttonIndex) in
                        
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    UtilityClass.showAlertWithTitle(title: "", message:replyMsg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }
    
    //MARK: - Call reschedulr Appointment API
    ///Call reschedulr Appointment API
    func callRescheduleAppointmentAPi()
    {
        self.view.endEditing(true)
        
        let params = modelAppointmentData.getRescheduleAppointmentDictionary()
        
        print("parameters = ", params)
        
        let urlToHit =  EndPoints.rescheduleAppointment.path
        
        HUD.show(.systemActivity, onView: self.view.window)
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .post, parameters: params, shouldDeserialize: true)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    let bookingMsg = responseDictionary["msg"] as? String
                    
                    UtilityClass.showAlertWithTitle(title: "", message: bookingMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                        
                        let controllers = self.navigationController?.viewControllers
                        for controller in controllers!
                        {
                            if controller .isKind(of: AppointmentSegmentViewController.self)
                            {
                                self.navigationController?.popToViewController(controller, animated: true)
                            }
                        }
                    })
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: {(buttonIndex) in
                        
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                
                if (responseDictionary["msg"] as? String) != nil
                {
                    
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
                
                let statusCode = responseDictionary["code"] as! String
                
                UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                    
                    if statusCode == "SLOT_NOT_AVAILABLE"
                    {
                        let controllers = self.navigationController?.viewControllers
                        for controller in controllers!
                        {
                            if controller .isKind(of: ScheduleAppointmentViewController.self)
                            {
                                self.navigationController?.popToViewController (controller, animated: true)
                            }
                        }
                    }
                })
            }
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
}

//MARK: - extension -> UITextView -> UITextViewDelegate
///extension -> UITextView -> UITextViewDelegate
extension ConfirmAppointmentViewController : UITextViewDelegate
{
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        if textView.text == visitReasonPlaaceHolder {
            textView.text = ""
            textView.textColor = UIColor.darkGray
        }
        
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == visitReasonPlaaceHolder {
            textView.textColor = UIColor.lightGray
        } else {
            textView.textColor = UIColor.darkGray
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if textView.text.count > 240 {
            if text == "" {
                return true
            } else {
                return false
            }
        }
        
        return true
    }
}
