//
//  ScheduleAppointmentViewController.swift
//  MyanCareDoctor
//
//  Created by Jyoti on 18/01/18.
//  Copyright © 2018 sumit. All rights reserved.
//

import UIKit
import PKHUD
import DZNEmptyDataSet
import Localize_Swift

class ScheduleAppointmentViewController: UIViewController {
    
    ///Color Struct Define Color Variable
    struct Color {
        static let selectedText = UIColor.white
        static let text = UIColor.MyanCarePatient.dateColor
        static let textDisabled = UIColor.gray
        static let selectionBackground = UIColor(red: 1.0, green: 0.0, blue: 0.0, alpha: 1.0)
        static let sundayText = UIColor(red: 1.0, green: 0.0, blue: 0.0, alpha: 1.0)
        static let sundayTextDisabled = UIColor.lightGray
        static let sundaySelectionBackground = sundayText
    }
    
    // MARK: - Variables
    ///Array Variable - Store Operational Hours Model Value
    var arrayOperationalHours = [ModelOperationalHours]()
    ///Array Variable - Store Filterd Operational Hours Model Value
    var arrayFilterOperationalHours = [ModelOperationalHours]()

    ///UILabel Refrence - label avaiblie Dates
    @IBOutlet var labelAvaibleDates: UILabel!
    
    ///Array Variable - store start time values
    var arrayStartTime = NSMutableArray()
    ///Array Variable - store End time values
    var arrayEndTime = NSMutableArray()
    
    ///IndexPath Variable - to store selected IndexPath
    var selectedPath = IndexPath()
    
    ///Custome Cell Variable
    var cellObj = ScheduleAppointmentTableCustomCell()
    
    ///Array Variable - Selected Cells
    var arraySelectedIndexPath = NSMutableArray()
    ///Array Variable - Operating Hours
    var arrayOpertaingHours = NSMutableArray()
    ///Array Variable - Avaiblible Dates
    var arrayAvailableDates:[String] = []
    ///String Variable - Contains selected Date
    var selectedDate = String()
    ///Array Variable - contains selected Dates
    var datesSelected = NSMutableArray()
    
    // MARK: - Properties
    ///UILabel Refrence - Static Info Text
    @IBOutlet var labelStaticInfo: UILabel!
    
    ///CVCalenderView Outlet Refrence - Calendar View
    @IBOutlet weak var calendarView: CVCalendarView!
    ///CVCalenderMenuView Outlet Refrence - Calendar Menu View
    @IBOutlet weak var menuView: CVCalendarMenuView!
    ///UILabel Refrence - Month Label Text
    @IBOutlet weak var monthLabel: UILabel!
    ///UITableView Refrence - Shows Times Slots Contianer View
    @IBOutlet var tableviewTimeInterval: UITableView!
    
    ///Int Variable - Contains Current Day
    var currentDay  = Int()
    
    ///Array Variable - Random Number of Dot Markerker for Day
    fileprivate var randomNumberOfDotMarkersForDay = [Int]()
    
    ///Bool Variable - Shows Days Out
    var shouldShowDaysOut = true
    ///Bool Variable - Animation Finished
    var animationFinished = true
    
    ///DayView Refrence - Slected Day
    var selectedDay:DayView!
    
    ///Calendar Refrence - current calendar
    var currentCalendar: Calendar?
    
    ///Awake From Nib Method
    override func awakeFromNib() {
        
        //let timeZoneBias = 480 // (UTC+08:00)
        currentCalendar = Calendar.init(identifier: .gregorian)
        
        if let timeZone = TimeZone.init(secondsFromGMT: 0)//-timeZoneBias * 60)
        {
            currentCalendar?.timeZone = timeZone
        }
    }
    
    // MARK: - Life cycle
    ///UIViewController Life Cycle Method
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpView()
        
        labelAvaibleDates.isHidden = true
        
        if let currentCalendar = currentCalendar {
            monthLabel.text = CVDate(date: Date(), calendar: currentCalendar).globalDescription
        }
        
        calendarView.isMultipleTouchEnabled = true
        
        randomizeDotMarkers()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        // Do any additional setup after appearing the view.
        
        self.tabBarController?.tabBar.isHidden = true
        
        setUpNvigationBar()
        
        getAllDoctorsOpratingHoursApi()
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        currentDay = 1
        calendarView.commitCalendarViewUpdate()
        menuView.commitMenuViewUpdate()
    }


    
    ///Setup Navigation Bar Method
    func setUpNvigationBar() {
        
        //Set Navigation Bar
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
        self.title = "Set Appointment".localized()
        
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
        
        navigationController?.navigationBar.tintColor = UIColor.white
        
        navigationController?.navigationBar.barTintColor = UIColor.init(RED: 30, GREEN: 197, BLUE: 155, ALPHA: 1)
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "back_white"), style: .plain, target: self, action: #selector(onLeftButtonClickAction))
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Next".localized(), style: .plain, target: self, action: #selector(onRightButtonClickAction))
    }
    
    ///Setup View Method
    func setUpView() {
        
        arraySelectedIndexPath = NSMutableArray()
        arrayOpertaingHours = NSMutableArray()
        datesSelected = NSMutableArray()
        
        if Localize.currentLanguage() == "en" {
        
        labelStaticInfo.attributedText = UILabel.createAttributedStringForText(strObj: "Select Date (to find availbale time slot)".localized(), size: 18, size2: 13, location: 0, location2: 11, length: 11, length2: 30, color: UIColor.MyanCarePatient.welcomeScreenIntroTextColor, color2: UIColor.MyanCarePatient.welcomeScreenIntroTextColor)
            
        } else {
            
            labelStaticInfo.text = "Select Date (to find availbale time slot)".localized()
        }
        
        modelSignUpProcess.availability = NSMutableArray()
        
        arrayOpertaingHours = NSMutableArray()

        tableviewTimeInterval.emptyDataSetSource = self
        tableviewTimeInterval.emptyDataSetDelegate = self
    }
    
    ///Left Button Clicked Action Event
    @objc func onLeftButtonClickAction()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    ///Right Button Clicked Action Event
    @objc func onRightButtonClickAction()
    {
        if modelAppointmentData.slotID == "" {
             UtilityClass.showAlertWithTitle(title: App_Name, message: "Please select date with your available time slots First".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
        } else {
            
            let serviceInvoiceVC = UIStoryboard.getScheduleAppointmentStoryBoard().instantiateViewController(withIdentifier: "InvioceVC") as! InvioceViewController
            self.navigationController?.pushViewController(serviceInvoiceVC, animated: true)
        }
    }
    
    ///Remove Circle And Dot Remove From Calender Method
    @IBAction func removeCircleAndDot(sender: AnyObject) {
        if let dayView = selectedDay {
            calendarView.contentController.removeCircleLabel(dayView)
            if dayView.date.day < randomNumberOfDotMarkersForDay.count {
                randomNumberOfDotMarkersForDay[dayView.date.day] = 0
            }
            calendarView.contentController.refreshPresentedMonth()
        }
    }
    
    ///Refresh Current Month Method
    @IBAction func refreshMonth(sender: AnyObject) {
        calendarView.contentController.refreshPresentedMonth()
        
        randomizeDotMarkers()
    }
    
    ///Randomize Dot Markers Method
    private func randomizeDotMarkers() {
        randomNumberOfDotMarkersForDay = [Int]()
        for _ in 0...31 {
            randomNumberOfDotMarkersForDay.append(Int(arc4random_uniform(3) + 1))
        }
    }

    //MARK: - Get Doctor's Operation Hours
    ///Get Doctor's Operation Hours From Server method
    func getAllDoctorsOpratingHoursApi() {
        
        self.view.endEditing(true)
        
        let urlToHit = EndPoints.operationHours(modelAppointmentData.doctorID).path
        print("Doctor Opration Hours url = \(urlToHit)")
        
        HUD.show(.systemActivity, onView: self.view.window)
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod:.get, parameters: nil) {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    let doctorData = responseDictionary["data"] as? [[String : Any]]
                    
                    guard doctorData != nil else
                    {
                        return
                    }
                    
                    self.updateModelArray(usingArray: doctorData!)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: {(buttonIndex) in
                        
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    UtilityClass.showAlertWithTitle(title: "", message:replyMsg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }
    
    //MARK:- Updating Model Array
    ///Updating Model Array
    func updateModelArray(usingArray array : [[String:Any]]) -> Void
    {
        let dataArray = array
        arrayOperationalHours.removeAll()
        
        for dict in dataArray // Iterating dictionaries
        {
            let model = ModelOperationalHours () // Model creation
            model.updateOprationalHourModel(usingDictionary: dict)
            
            
            arrayOperationalHours.append(model) // Adding model to array
        }
        
        //Get Doctor's Avaible Date
        self.arrayAvailableDates.removeAll()
        
        for dictVal in self.arrayOperationalHours
        {
            if !self.arrayAvailableDates.contains(dictVal.date!)
            {
                self.arrayAvailableDates.append(dictVal.date!)
            }
        }
        
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "MM/dd/yyyy"
        dateformatter.timeZone = NSTimeZone.system
        
        let selecteddate = dateformatter.string(from: Date())
        
        for dict in arrayOperationalHours
        {
            if dict.date == selecteddate
            {
                self.arrayStartTime.add(dict.startTime!)
                self.arrayEndTime.add(dict.endTime!)

                arrayFilterOperationalHours.append(dict)
            }
        }
        
        tableviewTimeInterval.reloadData()
        
        if let cc = calendarView.contentController as? CVCalendarMonthContentViewController {
            cc.refreshPresentedMonth()
        }
    }
}

//MARK: - Extension -> UITableView
///Extension -> UITableView
extension ScheduleAppointmentViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrayFilterOperationalHours.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //CellForListItems
        cellObj  = tableView.dequeueReusableCell(withIdentifier: cellIdentifier_ScheduleCell) as! ScheduleAppointmentTableCustomCell
        
        let stringTime = String(format:"%@  ~ %@", (arrayFilterOperationalHours[indexPath.row].startTime)!, (arrayFilterOperationalHours[indexPath.row].endTime)!)
        
        if arrayFilterOperationalHours[indexPath.row].booking_status == 1 || arrayFilterOperationalHours[indexPath.row].booking_status == 3 {
            cellObj.tfbg.alpha = 1.0
        } else {
            cellObj.tfbg.alpha = 0.5
        }
        
        cellObj.labelTimeInterval.text = stringTime
        cellObj.imageViewSelection.isHidden = true
        
        return cellObj
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let cell1 : ScheduleAppointmentTableCustomCell = (tableView.cellForRow(at: indexPath) as? ScheduleAppointmentTableCustomCell)!
        
        if cell1.tfbg.alpha == 0.5 {
            return
        }
        
        for i in 0 ..< arrayFilterOperationalHours.count
        {
            let indexpath = IndexPath(row: i, section: 0)
            
            if indexPath == indexpath
            {
                let cell : ScheduleAppointmentTableCustomCell = (tableView.cellForRow(at: indexPath) as? ScheduleAppointmentTableCustomCell)!
                
                cell.imageViewSelection?.isHidden = false
                        
                if selectedDay != nil
                {
                    modelAppointmentData.slotID =  arrayFilterOperationalHours[indexpath.row].id!
                        
                    let selecteddate = UtilityClass.convertDateFormater(selectedDay.date.commonDescription)
                            
                    let str1 = UtilityClass.getDateFromString(date: selecteddate, formate: "MM/dd/yyyy")
                    
                    var str2 = UtilityClass.getStringFromDateFormat1(date: str1, formate: "dd MMM yyyy")
                    
                    str2 = str2.appending(" | \(arrayFilterOperationalHours[indexpath.row].startTime ?? "")")
                            
                    modelAppointmentData.scheduleDate = str2
                }
                else
                {
                    let cell : ScheduleAppointmentTableCustomCell = (tableView.cellForRow(at: indexPath) as? ScheduleAppointmentTableCustomCell)!
                    
                    cell.imageViewSelection?.isHidden = true
                }
            }
            else
            {
                if let cell : ScheduleAppointmentTableCustomCell = tableView.cellForRow(at: indexpath) as? ScheduleAppointmentTableCustomCell
                {
                    cell.imageViewSelection?.isHidden = true
                }
                
                tableView.deselectRow(at: indexpath, animated: true)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
}

// MARK: - CVCalendarViewDelegate & CVCalendarMenuViewDelegate
///CVCalendarViewDelegate & CVCalendarMenuViewDelegate
extension ScheduleAppointmentViewController: CVCalendarViewDelegate, CVCalendarMenuViewDelegate {
    
    /// Required method to implement!
    func presentationMode() -> CalendarMode {
        return .monthView
    }
    
    /// Required method to implement!
    func firstWeekday() -> Weekday {
        return .sunday
    }
    
    // MARK: Optional methods
    
    func calendar() -> Calendar? {
        return currentCalendar
    }
    
    func dayOfWeekTextColor(by weekday: Weekday) -> UIColor {
        return UIColor.MyanCarePatient.appDefaultGreenColor
    }
    
    func shouldShowWeekdaysOut() -> Bool {
        return shouldShowDaysOut
    }
    
    func shouldAnimateResizing() -> Bool {
        return true // Default value is true
    }
    
    private func shouldSelectDayView(dayView: DayView) -> Bool {
        return arc4random_uniform(3) == 0 ? true : false
    }
    
    func shouldAutoSelectDayOnMonthChange() -> Bool {
        return false
    }
    
    func didSelectDayView(_ dayView: CVCalendarDayView, animationDidFinish: Bool) {
        
        arrayFilterOperationalHours.removeAll()
        
        modelAppointmentData.slotID = ""
        modelAppointmentData.scheduleDate = ""
        
        arrayStartTime = NSMutableArray()
        arrayEndTime = NSMutableArray()
        
        arraySelectedIndexPath = NSMutableArray()
        
        selectedDay = dayView
        
        let selecteddate = UtilityClass.convertDateFormater(dayView.date.commonDescription)
        
        print("dateSelect:- \(selecteddate)")
        
        for dict in arrayOperationalHours
        {
            if dict.date == selecteddate
            {
                self.arrayStartTime.add(dict.startTime!)
                self.arrayEndTime.add(dict.endTime!)
                
                arrayFilterOperationalHours.append(dict)
            }
        }
        
        tableviewTimeInterval.reloadData()
    }
    
    func shouldSelectRange() -> Bool {
        return false
    }
    
//    func didSelectRange(from startDayView: DayView, to endDayView: DayView) {
//
//        print("RANGE SELECTED: \(startDayView.date.commonDescription) to \(endDayView.date.commonDescription)")
//
//        arraySelectedIndexPath = NSMutableArray()
//
//        let calendar = NSCalendar.current
//        let normalizedStartDate = calendar.startOfDay(for: startDayView.date.convertedDate()!)
//        let normalizedEndDate = calendar.startOfDay(for: endDayView.date.convertedDate()!)
//        var currentDate = normalizedStartDate
//        let CVdate : CVDate = CVDate(date: currentDate)
//        datesSelected.add(CVdate.commonDescription)
//        var components = DateComponents()
//        components.day = +1
//
//        repeat {
//            currentDate = calendar.date(byAdding: components , to: currentDate, wrappingComponents: true)!
//            let CVdate : CVDate = CVDate(date: currentDate)
//            datesSelected.add(CVdate.commonDescription)
//        } while !calendar.isDate(currentDate, inSameDayAs: normalizedEndDate)
//
//        var dic = NSDictionary()
//
//        if datesSelected.count > 0
//        {
//            for i in 0...datesSelected.count-1
//            {
//                var strDate : String = datesSelected[i] as! String
//
//                strDate = UtilityClass.convertDateFormater(strDate)
//
//                dic = ["date":strDate, "slot_start_time":Float("10.00")!, "slot_end_time":Float("5.00")!]
//
//                arrayOpertaingHours.add(dic)
//
//                //print(arrayOpertaingHours)
//            }
//        }
//    }
    
    func presentedDateUpdated(_ date: CVDate) {
        
        if monthLabel.text != date.globalDescription && self.animationFinished {
            
            currentDay = 3
            let updatedMonthLabel = UILabel()
            updatedMonthLabel.textColor = monthLabel.textColor
            updatedMonthLabel.font = monthLabel.font
            updatedMonthLabel.textAlignment = .center
            updatedMonthLabel.text = date.globalDescription
            updatedMonthLabel.sizeToFit()
            updatedMonthLabel.alpha = 0
            updatedMonthLabel.center = self.monthLabel.center
            let offset = CGFloat(48)
            updatedMonthLabel.transform = CGAffineTransform(translationX: 0, y: offset)
            updatedMonthLabel.transform = CGAffineTransform(scaleX: 1, y: 0.1)
            self.animationFinished = true
            self.monthLabel.frame = updatedMonthLabel.frame
            self.monthLabel.text = updatedMonthLabel.text
            self.monthLabel.transform = CGAffineTransform.identity
            self.monthLabel.alpha = 1
            updatedMonthLabel.removeFromSuperview()
            
            //            UIView.animate(withDuration: 0.35, delay: 0, options: UIViewAnimationOptions.curveEaseIn, animations: {
            //                self.animationFinished = false
            //                self.monthLabel.transform = CGAffineTransform(translationX: 0, y: -offset)
            //                self.monthLabel.transform = CGAffineTransform(scaleX: 1, y: 0.1)
            //                self.monthLabel.alpha = 0
            //
            //                updatedMonthLabel.alpha = 1
            //                updatedMonthLabel.transform = CGAffineTransform.identity
            //
            //            }) { _ in
            //
            //                self.animationFinished = true
            //                self.monthLabel.frame = updatedMonthLabel.frame
            //                self.monthLabel.text = updatedMonthLabel.text
            //                self.monthLabel.transform = CGAffineTransform.identity
            //                self.monthLabel.alpha = 1
            //                updatedMonthLabel.removeFromSuperview()
            //            }
            
            self.view.insertSubview(updatedMonthLabel, aboveSubview: self.monthLabel)
        }
    }
    
    func topMarker(shouldDisplayOnDayView dayView: CVCalendarDayView) -> Bool {
        return true
    }
    
    func weekdaySymbolType() -> WeekdaySymbolType {
        return .veryShort
    }
    
    func selectionViewPath() -> ((CGRect) -> (UIBezierPath)) {
        return { UIBezierPath(rect: CGRect(x: 0, y: 0, width: $0.width, height: $0.height)) }
    }
    
    func shouldShowCustomSingleSelection() -> Bool {
        return false
    }
    
    func preliminaryView(viewOnDayView dayView: DayView) -> UIView {
        if (currentDay == 1) {
            let circleView = CVAuxiliaryView(dayView: dayView, rect: dayView.frame, shape: CVShape.rect)
            circleView.fillColor = UIColor.lightGray
            
            return circleView
        }
        else
        {
            let circleView = CVAuxiliaryView(dayView: dayView, rect: dayView.frame, shape: CVShape.rect)
            circleView.fillColor = UIColor(red:0.88, green : 0.88, blue: 0.88, alpha: 1.0)
            
            return circleView
        }
    }
    
    func preliminaryView(shouldDisplayOnDayView dayView: DayView) -> Bool
    {
        if (dayView.isCurrentDay)
        {
            dayView.dayLabel.textColor = UIColor.white
            selectedDay = dayView
            currentDay = 1
            
            //var dict = NSDictionary()
            //var strDate = String(selectedDay.date.commonDescription)
            //strDate = UtilityClass.convertDateFormater(strDate)
            //
            //dict = ["date":strDate, "slot_start_time":Float("10.00")!, "slot_end_time":Float("5.00")!]
            //
            //arrayOpertaingHours.add(dict)
            //
            //modelSignUpProcess.availability = arrayOpertaingHours.mutableCopy() as? NSMutableArray
            
            return true
        }
        else
        {
            currentDay = 2
            return true
        }
    }
    
    func supplementaryView(viewOnDayView dayView: DayView) -> UIView {
        
        dayView.setNeedsLayout()
        dayView.layoutIfNeeded()
        
        let π = Double.pi
        
        let ringLayer = CAShapeLayer()
        let ringLineWidth: CGFloat = 0
        let ringLineColour = UIColor.blue
        
        let newView = UIView(frame: dayView.frame)
        
        let diameter = (min(newView.bounds.width, newView.bounds.height))
        let radius = diameter / 2.0 - ringLineWidth
        
        newView.layer.addSublayer(ringLayer)
        
        ringLayer.fillColor = nil
        ringLayer.lineWidth = ringLineWidth
        ringLayer.strokeColor = ringLineColour.cgColor
        
        let centrePoint = CGPoint(x: newView.bounds.width/2.0, y: newView.bounds.height/2.0)
        let startAngle = CGFloat(-π/2.0)
        let endAngle = CGFloat(π * 2.0) + startAngle
       
        let ringPath = UIBezierPath(arcCenter: centrePoint, radius: radius, startAngle: startAngle, endAngle: endAngle, clockwise: true)
        
        ringLayer.path = ringPath.cgPath
        ringLayer.frame = newView.layer.bounds
        
        return newView
    }
    
    func supplementaryView(shouldDisplayOnDayView dayView: DayView) -> Bool
    {
        guard let currentCalendar = currentCalendar else {
            return false
        }
     
        var components = Manager.componentsForDate(Foundation.Date(), calendar: currentCalendar)
        
        /* For consistency, always show supplementaryView on the 3rd, 13th and 23rd of the current month/year.  This is to check that these expected calendar days are "circled". There was a bug that was circling the wrong dates. A fix was put in for #408 #411.
         
         Other month and years show random days being circled as was done previously in the Demo code.
         */
        
        if dayView.date.year == components.year &&
            dayView.date.month == components.month
        {
            if (dayView.date.day != 0)
            {
                //print("Circle should appear on " + dayView.date.commonDescription)
                return true
            }
            
            return false
        }
        else
        {
            if (Int(arc4random_uniform(3)) == 1)
            {
                return true
            }
            
            return false
        }
    }
    
    func dayOfWeekTextColor() -> UIColor {
        return UIColor.white
    }
    
    func dayOfWeekBackGroundColor() -> UIColor {
        return UIColor.white
    }
    
    func disableScrollingBeforeDate() -> Date {
        return Date()
    }
    
    func maxSelectableRange() -> Int {
        return 14
    }
    
    func earliestSelectableDate() -> Date {
        return Date()
    }
    
    func latestSelectableDate() -> Date {
        var dayComponents = DateComponents()
        dayComponents.day = 5000
        let calendar = Calendar(identifier: .gregorian)
        
        if let lastDate = calendar.date(byAdding: dayComponents, to: Date()) {
            return lastDate
        } else {
            return Date()
        }
    }
}


// MARK: - CVCalendarViewAppearanceDelegate
///CVCalendarViewAppearanceDelegate Extension
extension ScheduleAppointmentViewController: CVCalendarViewAppearanceDelegate {
    
    
    func dotMarker(colorOnDayView dayView: DayView) -> [UIColor] {
        return [UIColor.red]
    }
    
    
    func dotMarker(moveOffsetOnDayView dayView: DayView) -> CGFloat {
        return 15.0
    }
    
    func dotMarker(shouldShowOnDayView dayView: DayView) -> Bool {
        
        print("arrayAvailableDates = ", arrayAvailableDates);
        
        //print(UtilityClass.getStringFromDate(date: dayView.date.convertedDate()!, formate: "MM/dd/yyyy"))
        
        for dateStr in arrayAvailableDates {
            
            if dateStr == UtilityClass.getStringFromDateFormat(date: dayView.date.convertedDate()!, formate: "MM/dd/yyyy")
            {
                return true
            }
        }
        return false
    }
    
    
    func dayLabelWeekdayDisabledColor() -> UIColor {
        return UIColor.lightGray
    }
    
    func dayLabelPresentWeekdayInitallyBold() -> Bool {
        return false
    }
    
    func spaceBetweenDayViews() -> CGFloat {
        return 0
    }
    
    func dayLabelFont(by weekDay: Weekday, status: CVStatus, present: CVPresent) -> UIFont {
        return UIFont.createPoppinsRegularFont(withSize: 18)
    }
    
    func dayLabelColor(by weekDay: Weekday, status: CVStatus, present: CVPresent) -> UIColor? {
        
        switch (weekDay, status, present) {
            
        case (_, .selected, _), (_, .highlighted, _):
            return Color.selectedText
            
        case (.sunday, .in, _):
            return UIColor.black
            
        case (.sunday, _, _):
            return Color.sundayTextDisabled
            
        case (_, .in, _):
            return Color.text
            
        default:
            return Color.textDisabled
        }
    }
    
    func dayLabelBackgroundColor(by weekDay: Weekday, status: CVStatus, present: CVPresent) -> UIColor? {
        
        //print("weekDay = \(weekDay)")
        //print("status = \(status)")
        //print("present = \(present)")
        
        switch (weekDay, status, present) {
        case (.sunday, .selected, _), (.sunday, .highlighted, _):
            return Color.sundaySelectionBackground
            
        case (_, .selected, _), (_, .highlighted, _):
            return Color.selectionBackground
            
        default:
            return nil
        }
    }
    
    /// Switch to MonthView mode.
    @IBAction func toMonthView(sender: AnyObject) {
        calendarView.changeMode(.monthView)
    }
    
    @IBAction func loadPrevious(sender: AnyObject) {
        calendarView.loadPreviousView()
    }
}

// MARK: - IB Actions
/// Buttons Clicked Event Methods - Extension
extension ScheduleAppointmentViewController {
    
    @IBAction func switchChanged(sender: UISwitch) {
        calendarView.changeDaysOutShowingState(shouldShow: sender.isOn)
        shouldShowDaysOut = sender.isOn
    }
    
    @IBAction func todayMonthView() {
        calendarView.toggleCurrentDayView()
    }
    
    /// Switch to WeekView mode.
    @IBAction func toWeekView(sender: AnyObject) {
        calendarView.changeMode(.weekView)
    }
    
    @IBAction func loadNext(sender: AnyObject) {
        calendarView.loadNextView()
    }
}

// MARK: - Convenience API Demo

extension ScheduleAppointmentViewController {
    
    func toggleMonthViewWithMonthOffset(offset: Int) {
        guard let currentCalendar = currentCalendar else {
            return
        }
        
        var components = Manager.componentsForDate(Foundation.Date(), calendar: currentCalendar) // from today
        components.month! += offset
        let resultDate = currentCalendar.date(from: components)!
        self.calendarView.toggleViewWithDate(resultDate)
    }
    
    
    func didShowNextMonthView(_ date: Date) {
        guard let currentCalendar = currentCalendar else {
            return
        }
        
        let components = Manager.componentsForDate(date, calendar: currentCalendar) // from today
        print("Showing Month: \(components.month!)")
        
        if let cc = calendarView.contentController as? CVCalendarMonthContentViewController {
            cc.refreshPresentedMonth()
        }
    }
    
    
    func didShowPreviousMonthView(_ date: Date) {
        guard let currentCalendar = currentCalendar else {
            return
        }
        
        let components = Manager.componentsForDate(date, calendar: currentCalendar) // from today
        print("Showing Month: \(components.month!)")
        
        if let cc = calendarView.contentController as? CVCalendarMonthContentViewController {
            cc.refreshPresentedMonth()
        }
    }
    
}

//MARK: - DZNEmpty Data Sets Delegate Methods
///DZNEmpty Data Sets Delegate Methods
extension ScheduleAppointmentViewController:DZNEmptyDataSetSource, DZNEmptyDataSetDelegate
{
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString!
    {
        let text = "Doctor Not Available on This Date\nPlease Choose Another Date".localized()
        
        let attributes = [NSAttributedStringKey.font:UIFont .boldSystemFont(ofSize: 16*scaleFactorX), NSAttributedStringKey.foregroundColor:UIColor.black]
        
        return NSAttributedString (string: text, attributes: attributes)
        
    }

  
    
    func verticalOffset(forEmptyDataSet scrollView: UIScrollView!) -> CGFloat {
        return 150*scaleFactorX

    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
    
}
