//
//  ChangePhoneNumberViewController.swift
//  MyanCareDoctor
//
//  Created by iOS on 19/01/18.
//  Copyright © 2018 sumit. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import PKHUD

class ChangePhoneNumberViewController: UIViewController {
    
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var screenHeaderLabel: UILabel!
    @IBOutlet weak var mobileTextField: UITextField!
    @IBOutlet var containerView: UIView!
    
    //MARK: - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setupView()
    }

    //MARK: - viewWillAppear
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        if modelSignUpProcess.mobile!.count != 0 {
            mobileTextField.text = modelSignUpProcess.mobile
        }
    }
    
    //MARK: - viewWillDisappear
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        UtilityClass.enableIQKeyboard()
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
    }
    
    //MARK: - Deinit
    deinit {
        print("ChangePhoneNumberViewController deinit")
    }
    
    //MARK: - setupView
    func setupView()  {
        
        let headerString = "Please enter your \n mobile number".localized()
        
        let attributedString = NSMutableAttributedString(string: headerString, attributes: [
            .font: UIFont.createPoppinsRegularFont(withSize: 18.0),
            .foregroundColor: UIColor.MyanCarePatient.welcomeScreenIntroTextColor,
            .kern: 0.8
            ])
        screenHeaderLabel.attributedText = attributedString
        
        UtilityClass.disableIQKeyboard()
        
        modelSignUpProcess.changeMobile = ""
        
        //Set Border To Container View
        containerView.setBorderColor(UIColor.MyanCarePatient.lightGreyColor, cornorRadius: 5, borderWidth: 0.5)
        
        doneButton.setTitle("Done".localized(), for: .normal)
    }
    
    //MARK: - Validate InputField
    func validateTheInputField() -> Bool {
        
        if (mobileTextField.text?.count)! < 6 || (mobileTextField.text?.count)! > 11
        {
            UtilityClass.showAlertWithTitle(title:App_Name, message: "Please enter valid mobile number.".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
            return false
        }
        
        return true
    }
    
    //MARK: - BackButton Action
    @IBAction func onBackButtonAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Done Button Action
    @IBAction func doneAction(_ sender: Any) {
        
        if validateTheInputField() {
            modelSignUpProcess.changeMobile = mobileTextField.text
            
            if modelSignUpProcess.changeMobile == modelSignUpProcess.mobile {
                callReSentOtpAPI()
            } else {
                callChangePhoneNumberAPi()
            }
        }
    }
    
    //MARK: - Call Change PHone Number API
    func callChangePhoneNumberAPi()
    {
        HUD.show(.systemActivity, onView: self.view.window)
        
        self.view.endEditing(true)
        
        let params = ModelLogin().getChangePhoneNumberDataDictionary()
        
        print(params)
        
        let urlToHit =  EndPoints.changeMobileNumber.path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .post, parameters: params, shouldDeserialize: true)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    let dataDict = responseDictionary["data"] as? [String : Any]
                    guard dataDict != nil else
                    {
                        return
                    }
                    
                    let notificationStatus = dataDict!["notification_setting"] as! Bool
                    UtilityClass.saveNotificationStatus(userDict: notificationStatus)
                    
                    UtilityClass.saveUserInfoData(userDict: dataDict!)
                    modelSignUpProcess.tempToken = dataDict!["tmp_token"] as? String
                    
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: {[unowned self] (buttonIndex) in
                        
                        self.dismiss(animated: true, completion: nil)
                    })
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                        
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                let replyMsg = responseDictionary["msg"] as! String
                
                guard replyMsg.isEmptyString() else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
                
                UtilityClass.showAlertWithTitle(title: "", message: replyMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                    
                })
            }
        }
    }
    
    //MARK: - Call Resend OTP API
    func callReSentOtpAPI()
    {
        HUD.show(.systemActivity, onView: self.view.window)
        
        self.view.endEditing(true)
        
        let params = ["tmp_token":modelSignUpProcess.tempToken ?? ""] as [String : Any]
        
        let urlToHit =  EndPoints.resendOTP.path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .post, parameters: params, shouldDeserialize: true)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyType = responseDictionary["status"] as! String
                
                if replyType == "success"
                {
                    UtilityClass.saveUserInfoData(userDict: responseDictionary["data"] as! [String : Any])
                    
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: {[unowned self] (buttonIndex) in
                        self.dismiss(animated: true, completion: nil)
                        }
                    )
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                        
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                let replyMsg = responseDictionary["msg"] as! String
                
                guard replyMsg.isEmptyString() else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return
                }
                
                UtilityClass.showAlertWithTitle(title: "", message: replyMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                    
                })
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}
