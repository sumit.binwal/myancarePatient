//
//  CongratulationViewController.swift
//  MyanCareDoctor
//
//  Created by iOS on 19/01/18.
//  Copyright © 2018 sumit. All rights reserved.
//

import UIKit
import LocalAuthentication
import Localize_Swift

class CongratulationViewController: UIViewController {

    @IBOutlet weak var screenHeaderLabel: UILabel!
    @IBOutlet weak var screenHeaderLabel1: UILabel!
    
    @IBOutlet weak var setUpPinButton: UIButton!
    @IBOutlet weak var getStartedButton: UIButton!
    
    //MARK: - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setUpView()
    }

    //MARK: - Deinit
    deinit {
        print("CongratulationViewController deinit")
    }
    
    //MARK: - setUpView
    func setUpView() {
        
        let nickName = UtilityClass.getPersonNickName()
        var personName = ""
        if nickName == "" || nickName == "None".localized() {
            personName = UtilityClass.getPersonName()
        } else {
            personName = nickName
        }
        
        let headerString = "Congratulation!".localized() + " " + personName
        
        let attributedString = NSMutableAttributedString(string: headerString, attributes: [
            .font: UIFont.createPoppinsRegularFont(withSize: 18.0),
            .foregroundColor: UIColor.MyanCarePatient.welcomeScreenIntroTextColor,
            .kern: 0.8
            ])
        
        attributedString.addAttribute(.font, value: UIFont.createPoppinsSemiBoldFont(withSize: 18.0), range: NSRange(location: "Congratulation!".localized().count + 1, length: personName.count))
        
        screenHeaderLabel.attributedText = attributedString
        
        let headerString1 = "Now your account is \n successfully created. \n You can setup PIN Code to \n protect your account.".localized()
        
        let attributedString1 = NSMutableAttributedString(string: headerString1, attributes: [
            .font: UIFont.createPoppinsRegularFont(withSize: 18.0),
            .foregroundColor: UIColor.MyanCarePatient.welcomeScreenIntroTextColor,
            .kern: 0.8
            ])
        
        screenHeaderLabel1.attributedText = attributedString1
        
        setUpPinButton.setTitle("Setup PIN".localized(), for: .normal)
        getStartedButton.setTitle("Get Started".localized(), for: .normal)
    }

    //MARK: - BackButton Action
    @IBAction func onBackButtonAction(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - SetUP Pin Button Clicked Action
    @IBAction func setUpPinAction(_ sender: Any) {
        
        if UtilityClass.getPasscode() != "" {
            
            UtilityClass.showAlertWithTitle(title: "", message:"This Device Already Setup A Passcode. So You Cant Setup A New Passcode.".localized() , onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                
            })
        } else {
            let passcodeVC = UIStoryboard.getPhoneInfoStoryBoard().instantiateViewController(withIdentifier: "PasscodeViewController") as! PasscodeViewController
            self.navigationController?.pushViewController(passcodeVC, animated: true)
        }
    }
    
    //MARK: - Get Started Button Clicked Action
    @IBAction func onGetStartedButtonClick(_ sender: Any) {
       
        let homeTabVC = UIStoryboard.getHomeStoryBoard().instantiateInitialViewController() as! HomeTabBarController
        UtilityClass.changeRootViewController(with: homeTabVC)
    }
}
