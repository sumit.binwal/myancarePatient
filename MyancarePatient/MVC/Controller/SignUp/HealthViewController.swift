//
//  HealthViewController.swift
//  MyancarePatient
//
//  Created by iOS on 22/01/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit

class HealthViewController: UIViewController {
    
    @IBOutlet weak var buttonView: UIView!
    @IBOutlet var healthTableView: UITableView!
    
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var screenHeaderLabel: UILabel!
    
    //MARK: - View Lyf Cycle Methods
    // MARK: - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setUpView()
        setupTableView()
    }
    
    // MARK: - deinit
    deinit
    {
        print("HealthViewController Deinit")
        
        modelSignUpProcess.height = ""
        modelSignUpProcess.weight = ""
        modelSignUpProcess.bloodGroup = ""
    }
    
    //MARK: setupView
    func setUpView() {
        let headerString = "Please provide your some basic \n health status".localized()
        
        let attributedString = NSMutableAttributedString(string: headerString, attributes: [
            .font: UIFont.createPoppinsRegularFont(withSize: 18.0),
            .foregroundColor: UIColor.MyanCarePatient.welcomeScreenIntroTextColor,
            .kern: 0.8
            ])
        screenHeaderLabel.attributedText = attributedString
        
        nextButton.setTitle("Next".localized(), for: .normal)
    }
    
    //MARK: - setupTableView
    func setupTableView()  {
        
        healthTableView.dataSource = self
        healthTableView.delegate = self
        
        buttonView.frame = CGRect(x: buttonView.frame.origin.x, y: buttonView.frame.origin.y, width: buttonView.frame.size.width*scaleFactorX, height: buttonView.frame.size.height*scaleFactorX)
    }
    
    //MARK: - BackButton Action
    @IBAction func onBackButtonAction(_ sender: UIButton)
    {
        navigationController?.popViewController(animated: true)
    }
    
    //MARK: - Next Button Click Action
    @IBAction func onNextButtonAction(_ sender: UIButton)
    {
        if validateTheInputField()
        {
            let phoneNumberVC =  UIStoryboard.getPhoneInfoStoryBoard().instantiateInitialViewController() as! PhoneNumberViewController
            navigationController?.pushViewController(phoneNumberVC, animated: true)
        }
    }
    
    //MARK: - Validate Input TextField
    func validateTheInputField() -> Bool
    {
        if modelSignUpProcess.bloodGroup == nil || modelSignUpProcess.bloodGroup!.isEmptyString()
        {
            UtilityClass.showAlertWithTitle(title:App_Name, message: "Please select blood group.".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
            return false
        }
        else if modelSignUpProcess.height == nil || modelSignUpProcess.height!.isEmptyString()
        {
            UtilityClass.showAlertWithTitle(title:App_Name, message: "Please enter your height.".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
            return false
        }
        else if modelSignUpProcess.weight == nil || modelSignUpProcess.weight!.isEmptyString()
        {
            UtilityClass.showAlertWithTitle(title:App_Name, message: "Please enter your weight.".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
            return false
        }
        
        return true
    }
}

//MARK: -
//MARK: - Extension -> UITableView -> UITableViewDataSource, UITableViewDelegate
extension HealthViewController: UITableViewDataSource, UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 64 * scaleFactorX
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 64 * scaleFactorX
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell : HealthDataCell!
        
        var cellIdentifier = kCellIdentifier_HealthData_BloodType
        
        var cellType = HealthDataCellType.height
        
        switch indexPath.row {
            
        case 0:
            cellType = .bloodGroup
            cellIdentifier = kCellIdentifier_HealthData_BloodType
            
        case 1:
            cellType = .height
            cellIdentifier = kCellIdentifier_HealthData_Height
            
        case 2:
            cellType = .weight
            cellIdentifier = kCellIdentifier_HealthData_Height
            
        default:
            break
        }
        
        cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! HealthDataCell
        
        cell.tag = indexPath.row
        
        cell.cellType = cellType
        
        cell.delegate = self
        
        cell.updateCell()
        
        return cell
    }
}

//MARK: - Extension -> HealthDataCellDelegate
extension HealthViewController: HealthDataCellDelegate
{
    func healthDataCell(_ cell: HealthDataCell, updatedInputFieldText inputValue: String)
    {
        switch cell.cellType {
            
        case .bloodGroup:
            //print("bloodGroup : "+inputValue)
            modelSignUpProcess.bloodGroup = inputValue
            
        case .height:
            //print("height : "+inputValue)
            modelSignUpProcess.height = inputValue
            
        case .weight:
            //print("weight : "+inputValue)
            modelSignUpProcess.weight = inputValue
            
        default:
            break
        }
    }
}

