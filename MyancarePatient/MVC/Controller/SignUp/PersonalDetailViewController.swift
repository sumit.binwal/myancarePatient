//
//  PersonalDetailViewController.swift
//  MyanCareDoctor
//
//  Created by Sumit Sharma on 18/12/2017.
//  Copyright © 2017 sumit. All rights reserved.
//

import UIKit

//MARK: -
var locationGlobleArr:[[String:Any]] = []

//MARK: -
var modelSignUpProcess = ModelLogin ()

//MARK: -
class PersonalDetailViewController: UIViewController {

    @IBOutlet weak var buttonView: UIView!
    @IBOutlet var signupTableView: UITableView!
    @IBOutlet var maleButton: UIButton!
    @IBOutlet var femaleButton: UIButton!
    @IBOutlet var maleTextLabel: UILabel!
    @IBOutlet var femaleTextLabel: UILabel!
    @IBOutlet weak var screenHeaderLabel: UILabel!
    @IBOutlet weak var nextButton: UIButton!
    
    //MARK: - ViewDidLoad
    override func viewDidLoad() {
        
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setupTableView()
        setupView()
    }
    
    //MARK: - Deinit
    deinit {
        print("PersonalDetailViewController Deinit")
        
        modelSignUpProcess.name = ""
        modelSignUpProcess.dob = ""
    }

    //MARK: - SetupView
    func setupView() {
        
        let headerString = "May I know your personal \n details?".localized()
        
        let attributedString = NSMutableAttributedString(string: headerString, attributes: [
            .font: UIFont.createPoppinsRegularFont(withSize: 18.0),
            .foregroundColor: UIColor.MyanCarePatient.welcomeScreenIntroTextColor,
            .kern: 0.8
            ])
        screenHeaderLabel.attributedText = attributedString
        
        modelSignUpProcess.gender = "male"
        
        maleTextLabel.textColor = UIColor.MyanCarePatient.signupTextFocus
        femaleTextLabel.textColor = UIColor.MyanCarePatient.signupTextUnFocus
        
        maleTextLabel.text = "Male".localized()
        femaleTextLabel.text = "Female".localized()
        
        nextButton.setTitle("Next".localized(), for: .normal)
        
        getTownListFromServer()
    }
    
    //MARK: - Setup TableView
    func setupTableView()  {
        
        signupTableView.delegate = self
        signupTableView.dataSource = self
        
        buttonView.frame = CGRect(x: buttonView.frame.origin.x, y: buttonView.frame.origin.y, width: buttonView.frame.size.width*scaleFactorX, height: buttonView.frame.size.height*scaleFactorX)
    }
    
    //MARK: - OnBackButton Action
    @IBAction func onBackButtonAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    //MARK: - On Gender Choose Button Action
    @IBAction func onGendarButtonAction(_ sender: UIButton)
    {
        if sender.tag == 0
        {
            modelSignUpProcess.gender = "male"
            
            maleTextLabel.textColor = UIColor.MyanCarePatient.signupTextFocus
            maleButton.setImage(#imageLiteral(resourceName: "signup-male-focus"), for: .normal)
            
            femaleButton.setImage(#imageLiteral(resourceName: "signup-female-unfocus"), for: .normal)
            femaleTextLabel.textColor = UIColor.MyanCarePatient.signupTextUnFocus
        }
        else
        {
            modelSignUpProcess.gender = "female"
            
            maleButton.setImage(#imageLiteral(resourceName: "signup-male-unfocus"), for: .normal)
            femaleButton.setImage(#imageLiteral(resourceName: "signup-female-focus"), for: .normal)
            
            femaleTextLabel.textColor = UIColor.MyanCarePatient.signupTextFocus
            maleTextLabel.textColor = UIColor.MyanCarePatient.signupTextUnFocus
        }
    }
    
    //MARK : - On Next Button Action
    @IBAction func onNextButtonAction(_ sender: UIButton) {
        
        if validateTheInputField()
        {
            var ageOfPerson : Int = 0
            ageOfPerson = Int(modelSignUpProcess.age!)!
            
            if (ageOfPerson == 25) {
                
                let languageVC = UIStoryboard.getLocationStoryBoard().instantiateInitialViewController() as! ChooseLocationViewController
                navigationController?.pushViewController(languageVC, animated: true)
            } else {
                let healthVC = UIStoryboard.getSignUpStoryBoard().instantiateViewController(withIdentifier: "shouldCallVC") as! ShouldCallViewController
                navigationController?.pushViewController(healthVC, animated: true)
            }
        }
    }
    
    //MARK : - Validation method
    func validateTheInputField() -> Bool {
        
        if modelSignUpProcess.name == nil || modelSignUpProcess.name!.isEmptyString()
        {
            UtilityClass.showAlertWithTitle(title:App_Name, message: "Please enter your name.".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
            return false
        }
        else if modelSignUpProcess.dob == nil || modelSignUpProcess.dob!.isEmptyString()
        {
            UtilityClass.showAlertWithTitle(title:App_Name, message: "Please select your date of birth.".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
            return false
        }
        
        return true
    }
    
    //MARK: - get Town List From API
    func getTownListFromServer()
    {
        let urlToHit =  EndPoints.districtTown.path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .get, parameters: nil, completionHandler: {[weak self] (data, dictionary, statusCode, error) in
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyType = responseDictionary["status"] as! String
                let isEqual = (replyType == "success")
                
                if isEqual
                {
                    // login...
                    guard responseDictionary["data"] != nil else
                    {
                        UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                        return
                    }
                    let userDataDict = (responseDictionary["data"] as! [[String : Any]] )
                    
                    locationGlobleArr = userDataDict
                }
            }
        })
    }
}

//MARK: - Extention -> UITableView -> UITableViewDelegate, UITableViewDataSource
extension PersonalDetailViewController:UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55 * scaleFactorX
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var cell : SignupCustomeCell!
        
        let cellIdentifier = cellIdentifier_SignupCell
        
        var cellType = PersonalDataCellType.none
        
        switch indexPath.row {
        case 0:
            cellType = .name
            
        case 1:
            cellType = .dob
        default:
            break
        }
        
        cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! SignupCustomeCell
        
        cell.tag = indexPath.row
        
        cell.cellType = cellType
        
        cell.delegate = self
        
        return cell
    }
}

//MARK: - Extention -> PersonalDataCellDelegate
extension PersonalDetailViewController: PersonalDataCellDelegate
{
    func personalDataCell(_ cell: SignupCustomeCell, dateOfBirthValue: String) {
        
        let selectedDate = UtilityClass.getDateFromString(date: dateOfBirthValue, formate: "dd MMM,yyyy")
        
        modelSignUpProcess.dob = UtilityClass.getStringFromDate(date: selectedDate, formate: "dd/mm/yyyy")
        
        modelSignUpProcess.age = String(UtilityClass.getPersonYearOld(date: dateOfBirthValue))
       
        print(modelSignUpProcess.dob ?? "")
        print(modelSignUpProcess.age ?? "ZZZ")
    }
    
    func personalDataCell(_ cell: SignupCustomeCell, updatedInputFieldText inputValue: String) {
        modelSignUpProcess.name = inputValue
    }
}
