//
//  PhoneNumberViewController.swift
//  MyanCareDoctor
//
//  Created by Sumit Sharma on 04/01/2018.
//  Copyright © 2018 sumit. All rights reserved.
//

import UIKit
import PKHUD

class PhoneNumberViewController: UIViewController {
    
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var canSkipLAbel: UILabel!
    @IBOutlet weak var screenHeaderLabel: UILabel!
    @IBOutlet weak var buttonView: UIView!
    @IBOutlet var phoneNumberTableView: UITableView!
    
    // MARK: - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setUpView()
        setupTableView()
    }
    
    // MARK: - deinit
    deinit
    {
        print("PhoneNumberViewController Deinit")
    }
    
    // MARK: - setUpView
    func setUpView() {
        let headerString = "Please help me to provide your \n phone and email for further \n communication.".localized()
        
        let attributedString = NSMutableAttributedString(string: headerString, attributes: [
            .font: UIFont.createPoppinsRegularFont(withSize: 18.0),
            .foregroundColor: UIColor.MyanCarePatient.welcomeScreenIntroTextColor,
            .kern: 0.8
            ])
        screenHeaderLabel.attributedText = attributedString
        
        nextButton.setTitle("Next".localized(), for: .normal)
        canSkipLAbel.text = "You can skip if you do not have email address".localized()
    }
    
    //MARK: - setupTableView
    func setupTableView()  {
        
        phoneNumberTableView.dataSource = self
        phoneNumberTableView.delegate = self
        
        buttonView.frame = CGRect(x: buttonView.frame.origin.x, y: buttonView.frame.origin.y, width: buttonView.frame.size.width*scaleFactorX, height: buttonView.frame.size.height*scaleFactorX)
    }
    
    //MARK: - Validate InputField
    func validateTheInputField() -> Bool {
        
        if modelSignUpProcess.mobile == nil || modelSignUpProcess.mobile!.isEmptyString()
        {
            UtilityClass.showAlertWithTitle(title:App_Name, message: "Please enter mobile number.".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
            return false
        }
        else if modelSignUpProcess.mobile!.count < 6 || modelSignUpProcess.mobile!.count > 11
        {
            UtilityClass.showAlertWithTitle(title:App_Name, message: "Please enter valid mobile number.".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
            return false
            
        }
        else if !modelSignUpProcess.mobile!.isEmptyString()
        {
            if modelSignUpProcess.email.count > 4
            {
                if !modelSignUpProcess.email.isValidEmailAddress()
                {
                    UtilityClass.showAlertWithTitle(title:App_Name, message: "Please enter a valid email id.".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                    return false
                }
            }
        }
        
        return true
    }
    
    //MARK: - BackButton Action
    @IBAction func onBackButtonAction(_ sender: UIButton)
    {
        navigationController?.popViewController(animated: true)
    }
    
    //MARK: - Confirm Button Action
    @IBAction func onNextButtonAction(_ sender: UIButton)
    {
        if validateTheInputField()
        {
            callPhoneNumberAPi()
        }
    }
    
    //MARK: - Call Phone Number API
    func callPhoneNumberAPi()
    {
        HUD.show(.systemActivity, onView: self.view.window)
        
        self.view.endEditing(true)
        
        let params = ModelLogin().getPhoneNumberDataDictionary()
        
        print(params)
        
        let urlToHit =  EndPoints.checkUserExist.path
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .post, parameters: params, shouldDeserialize: true)
        {[weak self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
        
            guard let `self` = self else {return}
            
            if error != nil
            {
                if error?.localizedDescription == "The network connection was lost."
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
                
                return
            }
            if dictionary == nil
            {
                UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                
                let replyCode = responseDictionary["code"] as! String
                
                if replyCode == "NOT_EXISTS"
                {
                    let passwordVC = UIStoryboard.getPhoneInfoStoryBoard().instantiateViewController(withIdentifier: "passwordVC") as! PasswordViewController
                    self.navigationController?.pushViewController(passwordVC, animated: true)
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: "", message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: { (_) in
                        
                    })
                }
            }
            else
            {
                let responseDictionary = dictionary!
                
                if let replyMsg = responseDictionary["msg"] as? String
                {
                    UtilityClass.showAlertWithTitle(title: "", message: replyMsg, onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                        
                    })
                }
                else
                {
                    UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                }
            }
        }
    }
}

//MARK: -
//MARK: - Extension ->  UITableView -> UITableViewDataSource, UITableViewDelegate
extension PhoneNumberViewController: UITableViewDataSource, UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 64 * scaleFactorX
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 64 * scaleFactorX
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell : PhoneInfoCell!
        
        var cellIdentifier = kCellIdentifier_PhoneInfo_PhoneNo
        
        var cellType = PhoneInfoCellType.none
        
        switch indexPath.row {
            
        case 0:
            cellType = .phoneNumber
            cellIdentifier = kCellIdentifier_PhoneInfo_PhoneNo
            
        case 1:
            cellIdentifier = kCellIdentifier_PhoneInfo_Email
            cellType = .email
            
        default:
            break
        }
        
        cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! PhoneInfoCell
        
        cell.tag = indexPath.row
        
        cell.cellType = cellType
        
        cell.delegate = self
        
        return cell
    }
}

//MARK: - Extension -> PhoneDataCellDelegate
extension PhoneNumberViewController : PhoneDataCellDelegate
{
    func phoneDataCell(_ cell: PhoneInfoCell, updatedInputFieldText inputValue: String) {
        
        switch cell.cellType {
            
        case .phoneNumber:
            modelSignUpProcess.mobile = inputValue
            
        case .email:
            modelSignUpProcess.email = inputValue
            
        default:
            break
        }
    }
}
