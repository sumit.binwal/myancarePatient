//
//  ShouldCallViewController.swift
//  MyanCareDoctor
//
//  Created by Sumit Sharma on 20/12/2017.
//  Copyright © 2017 sumit. All rights reserved.
//

import UIKit

class ShouldCallViewController: UIViewController {

    @IBOutlet weak var screenHeaderLabel: UILabel!
    @IBOutlet weak var relationCollectionView: UICollectionView!
    let itemsPerRow = 3
    
    @IBOutlet weak var confirmButtonAction: UIButton!
    
    var relationArr = [String]()
    
    var selectedRelationTag = 0
    
    // MARK: - ViewController LyfCycle Method
    // MARK: - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setupView()
    }
    
    // MARK: - deinit
    deinit {
        print("ShouldCallViewController Deinit")
    }
    
    //MARK: - setupView
    func setupView()
    {
        var ageOfPerson : Int = 0
        ageOfPerson = Int(modelSignUpProcess.age!)!
        
        if modelSignUpProcess.gender == "male" {
            if (ageOfPerson > 25) {
                relationArr.append("Aba".localized())
                relationArr.append("Ko".localized())
                relationArr.append("U".localized())
                relationArr.append("Bro".localized())
                relationArr.append("Uncle".localized())
                relationArr.append("None".localized())
            } else {
                relationArr.append("Maung Lay".localized())
                relationArr.append("Bro".localized())
                relationArr.append("None".localized())
            }
        } else {
            if (ageOfPerson > 25) {
                relationArr.append("Amay".localized())
                relationArr.append("Ama".localized())
                relationArr.append("Daw".localized())
                relationArr.append("Sis".localized())
                relationArr.append("Aunty".localized())
                relationArr.append("None".localized())
            } else {
                relationArr.append("Nyima Lay".localized())
                relationArr.append("Nyima".localized())
                relationArr.append("Sis".localized())
                relationArr.append("None".localized())
            }
        }
        
        var relation = ""
        if ageOfPerson > 25 {
            relation = "elder"
        } else {
            relation = "younger"
        }
        
        let headerString = "Ohhh! You are \(relation) then me. \n How should I call you?".localized()
        
        let attributedString = NSMutableAttributedString(string: headerString, attributes: [
            .font: UIFont.createPoppinsRegularFont(withSize: 18.0),
            .foregroundColor: UIColor.MyanCarePatient.welcomeScreenIntroTextColor,
            .kern: 0.8
            ])
        screenHeaderLabel.attributedText = attributedString
        
        confirmButtonAction.setTitle("Confirm".localized(), for: .normal)
        
        relationCollectionView.delegate = self
        relationCollectionView.dataSource = self
        
        relationCollectionView.reloadData()
    }
    
    //MARK: - BackButton Action
    @IBAction func onBackButtonAction(_ sender: UIButton)
    {
        navigationController?.popViewController(animated: true)
    }

    // MARK: - on Confirm Button Action
    @IBAction func onNextButtonAction(_ sender: UIButton)
    {
        let chooseLocationVC = UIStoryboard.getLocationStoryBoard().instantiateInitialViewController() as! ChooseLocationViewController
        navigationController?.pushViewController(chooseLocationVC, animated: true)
    }
}

//MARK: - Extension -> UICollectionView -> UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
extension ShouldCallViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return relationArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: ShouldCallCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier_ShouldCallCollectionViewCell, for: indexPath) as! ShouldCallCollectionViewCell
        
        cell.relationBtn.setTitle(relationArr[indexPath.row], for: .normal)
        
        if selectedRelationTag == indexPath.row {
            cell.relationBtn.setBackgroundImage(#imageLiteral(resourceName: "call_selected"), for: .normal)
            cell.relationBtn.setTitleColor(UIColor.white, for: .normal)
            
            modelSignUpProcess.personNickName = relationArr[indexPath.row]
        } else {
            cell.relationBtn.setBackgroundImage(#imageLiteral(resourceName: "call_un_selected"), for: .normal)
            cell.relationBtn.setTitleColor(UIColor.init(red: 30.0/255.0, green: 197.0/255.0, blue: 155.0/255.0, alpha: 1), for: .normal)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        selectedRelationTag = indexPath.row
        
        for i in 0 ..< relationArr.count {
            
            let indexPath1 = IndexPath(row: i, section: 0)
            
            let cell = collectionView.cellForItem(at: indexPath1) as! ShouldCallCollectionViewCell
            
            if selectedRelationTag == i {
                cell.relationBtn.setBackgroundImage(#imageLiteral(resourceName: "call_selected"), for: .normal)
                cell.relationBtn.setTitleColor(UIColor.white, for: .normal)
                
                modelSignUpProcess.personNickName = relationArr[indexPath.row]
            } else {
                cell.relationBtn.setBackgroundImage(#imageLiteral(resourceName: "call_un_selected"), for: .normal)
                cell.relationBtn.setTitleColor(UIColor.init(red: 30.0/255.0, green: 197.0/255.0, blue: 155.0/255.0, alpha: 1), for: .normal)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        
        let totalSpace = flowLayout.sectionInset.left + flowLayout.sectionInset.right + (flowLayout.minimumInteritemSpacing * CGFloat(3 - 1))
            
        let size = (collectionView.bounds.width - totalSpace) / CGFloat(3)
            
        return CGSize(width: size, height: 50*scaleFactorX)
    }
}
