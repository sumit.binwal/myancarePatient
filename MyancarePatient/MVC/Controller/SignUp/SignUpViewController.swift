//
//  SignUpViewController.swift
//  MyanCareDoctor
//
//  Created by Sumit Sharma on 15/12/2017.
//  Copyright © 2017 sumit. All rights reserved.
//

import UIKit

//MARK: -
//MARK: -
class SignUpViewController: UIViewController {

    @IBOutlet weak var yesButton: UIButton!
    @IBOutlet weak var notNowButton: UIButton!
    
    @IBOutlet weak var whyLabel: UIButton!
    @IBOutlet weak var screenHeaderLabel: UILabel!
    
    //MARK: - View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setUpView()
    }
    
    //MARK: - Deinit
    deinit {
        print("SignUpViewController deinit")
    }
    
    //MARK: - setUpView
    func setUpView() {
        
        let headerString = "Would you like to sign up to use \n complete function of this \n application?".localized()
        
        let attributedString = NSMutableAttributedString(string: headerString, attributes: [
            .font: UIFont.createPoppinsRegularFont(withSize: 18.0),
            .foregroundColor: UIColor.MyanCarePatient.welcomeScreenIntroTextColor,
            .kern: 0.8
            ])
        screenHeaderLabel.attributedText = attributedString
        
        yesButton.setTitle("Yes".localized(), for: .normal)
        notNowButton.setTitle("Not Now".localized(), for: .normal)
        whyLabel.setTitle("Why need to Sign Up?".localized(), for: .normal)
    }
    
    //MARK: - On Back Button Action
    @IBAction func onBachButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - On Why SignUp Button Action
    @IBAction func onWhySignupButtonAction(_ sender: UIButton)
    {
        let staticPagesVC = UIStoryboard.getAboutUsStoryBoard().instantiateViewController(withIdentifier: "StaticPagesVC") as! StaticPagesViewController
        staticPagesVC.selectedIndex = 0
        self.navigationController?.pushViewController(staticPagesVC, animated: true)
        
        //UtilityClass.openSafariController(usingLink: LinksEnum.whySignUp, onViewController: self)
    }
    
    //MARK: - Yes Button Action
    @IBAction func onYesButtonAction(_ sender: UIButton) {
        
        let personalVC = UIStoryboard.getSignUpStoryBoard().instantiateViewController(withIdentifier: "personalDetailVC") as! PersonalDetailViewController
        navigationController?.pushViewController(personalVC, animated: true)
    }
    
    //MARK: - NO Button Action
    @IBAction func onNoButtonAction(_ sender: UIButton)
    {
        //navigationController?.popViewController(animated: true)
        
        let homeTabVC = UIStoryboard.getHomeStoryBoard().instantiateInitialViewController() as! HomeTabBarController
        UtilityClass.changeRootViewController(with: homeTabVC)
    }
}
