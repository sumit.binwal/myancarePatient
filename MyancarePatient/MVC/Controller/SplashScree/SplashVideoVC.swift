//
//  SplashVideoVC.swift
//  MyancarePatient
//
//  Created by Sumit Sharma on 30/05/2018.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit

class SplashVideoVC: UIViewController
{
    var player: AVPlayer?

    /// ViewController Life Cycle Methods
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        //loadVideo()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        loadVideo()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /// Splash Screen Video Initialize
    private func loadVideo()
    {
        //this line is important to prevent background music stop
        do
        {
            try AVAudioSession.sharedInstance().setCategory (AVAudioSessionCategoryAmbient)
        }
        catch
        {

        }
        
        let path = Bundle.main.path(forResource: "splashVideo", ofType:"mp4")
        
        player = AVPlayer.init(url: NSURL.init(fileURLWithPath: path!) as URL)
        let playerLayer = AVPlayerLayer(player: player)
        
        playerLayer.frame = self.view.frame
        playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        playerLayer.zPosition = -1
        
        self.view.layer.addSublayer(playerLayer)
        
        player?.seek(to: kCMTimeZero)
        player?.play()
        
        NotificationCenter.default.addObserver(self, selector: #selector(finishVideo), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
        
        //NotificationCenter.default.addObserver(self, selector: #selector(AVPlayerItemFailedToPlayToEndTime1), name: NSNotification.Name.AVPlayerItemPlaybackStalled, object: player)
        
        NotificationCenter.default.addObserver(self, selector: #selector(didnotStartToPlay11), name: NSNotification.Name.AVPlayerItemTimeJumped, object: nil)
    }
    
    //MARK: - Deinit
    deinit {
        print("SplashVideoVC deinit")
    }
    
    
    
    /// AVPlayer Event Tracking Method
    
    /// Failed To Play to End Time Method
    @objc func AVPlayerItemFailedToPlayToEndTime1()
    {
        UtilityClass.showAlertWithTitle(title: "", message: "AVPlayerItemFailedToPlayToEndTime", onViewController: self, withButtonArray: nil, dismissHandler: nil)
    }
    
    
    /// Video Not Start to Play
    @objc func didnotStartToPlay11()
    {

        Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(finishVideo), userInfo: nil, repeats: false)
        
    }
    
    /// Video Finish Playing
    @objc func finishVideo()
    {
        guard let appDeleg = appDelegate as? AppDelegate else
        {
            return
        }
        
        appDeleg.checkController()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}
