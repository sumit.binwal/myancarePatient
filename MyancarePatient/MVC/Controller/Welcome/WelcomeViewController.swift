//
//  WelcomeViewController.swift
//  MyancarePatient
//
//  Created by Jyoti on 04/01/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit
import Localize_Swift

class WelcomeViewController: UIViewController {
    
    //Mark:- Properties
    @IBOutlet var labelName: UILabel!
    @IBOutlet var labelMetBefore: UILabel!
    @IBOutlet var buttonYes: UIButton!
    @IBOutlet var buttonNo: UIButton!
    @IBOutlet var labelGuestUser: UILabel!
    @IBOutlet var labelContinueUser: UILabel!
    
    @IBOutlet weak var screenHeaderLabel: UILabel!
    
    //Mark:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setUpView()
    }
    
    //Mark:- viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = true
    }
    
    //MARK: - Deinit
    deinit {
        print("WelcomeViewController deinit")
    }
    
    //Mark:- SetupView
    func setUpView() {
        
        let headerString = "My name is Mi Care. I am going to \n be your medical assistant here.".localized()
        
        let attributedString = NSMutableAttributedString(string: headerString, attributes: [
            .font: UIFont.createPoppinsRegularFont(withSize: 18.0),
            .foregroundColor: UIColor.MyanCarePatient.welcomeScreenIntroTextColor,
            .kern: 0.8
            ])
        attributedString.addAttribute(.font, value: UIFont.createPoppinsSemiBoldFont(withSize: 18.0), range: NSRange(location: 11, length: 7))
        
        screenHeaderLabel.attributedText = attributedString
        
        labelName.text = "Mingalabar!".localized()
        
        labelMetBefore.text = "Have we met before?".localized()
        
        labelContinueUser.text = "Continue as a".localized()
        
        labelGuestUser.text = "Guest user".localized()
        
        buttonNo.setTitle("No, Not Yet".localized(), for: .normal)
        
        buttonYes.setTitle("Yes Welcome".localized(), for: .normal)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    //MARK:- On Yes Button Action
    @IBAction func onYesButtonAction(_ sender: UIButton)
    {
        let signinVC = UIStoryboard.getLoginStoryBoard().instantiateInitialViewController() as! LoginViewController
        navigationController?.pushViewController(signinVC, animated: true)
    }
    
    //MARK:- On No Button Action
    @IBAction func onNoButtonAction(_ sender: UIButton)
    {
        let signupVC = UIStoryboard.getSignUpStoryBoard().instantiateInitialViewController() as! SignUpViewController
        navigationController?.pushViewController(signupVC, animated: true)
    }
    
    //MARK:- On Guest User Button Action
    @IBAction func guestUserClicked(_ sender: Any) {
        
        let homeTabVC = UIStoryboard.getHomeStoryBoard().instantiateInitialViewController() as! HomeTabBarController
        UtilityClass.changeRootViewController(with: homeTabVC)
    }
}
