//
//  CommentModel.swift
//  MyancarePatient
//
//  Created by iOS on 24/01/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import Foundation

//MARK:-
class CommentModel
{
    var commentID : String?
    var commentDescription : String?
    var commentTime : String?
    
    var userProfile : [String : Any]?
    
    init() {
        commentID = ""
        commentTime = ""
        commentDescription = ""
        
        userProfile = [:]
    }
    
    deinit {
        print("Comment Model deinit")
    }
    
    func updateCommentModel(usingDictionary dictionary:[String:Any]) -> Void
    {
        if let cateID = dictionary["id"] as? String
        {
            commentID = cateID
        }
        
        if let comment = dictionary["comment"] as? String
        {
            commentDescription = comment
        }
        
        if let commentTime1 = dictionary["createdAt"] as? Int
        {
            commentTime = String(commentTime1)
        }
        
        if let userProfile1 = dictionary["userProfile"] as? [String : Any]
        {
            userProfile = userProfile1
        }
    }
}
