//
//  CustomButtonSelection.swift
//  MyancarePatient
//
//  Created by Jyoti on 15/01/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit

class CustomButtonSelection: UIButton
{
    override func draw(_ rect: CGRect)
    {
        
    }
    
    override func setBackgroundImage(_ image: UIImage?, for state: UIControlState)
    {
        if self.isEnabled
        {
            self.setImage(UIImage.init(named: "checked"), for: .normal)
        }
        else
        {
            self.setImage(UIImage.init(named: "uncheck"), for: .normal)
        }
    }
    
    static func setImage()
    {
       
    }
}
