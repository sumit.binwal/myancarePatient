//
//  AppointmentsTableCustomCell.swift
//  MyancarePatient
//
//  Created by Jyoti on 12/01/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit

let cellIdentifier_AppointmentsDetailTableCustomCell = "cellAppointmentTable"

class AppointmentsTableCustomCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descripionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
