//
//  ArticlesCollectionCustomCell.swift
//  MyancarePatient
//
//  Created by Jyoti on 09/01/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit
let cellIdentifier_ArticlesCollectionCell = "ArticlesCollectionCell"

class ArticlesCollectionCustomCell: UICollectionViewCell {
    
    @IBOutlet var imageViewArticleImage: UIImageView!
    @IBOutlet var labelArticleTitle: UILabel!
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        imageViewArticleImage.layer.cornerRadius = 5
        imageViewArticleImage.layer.borderColor = UIColor.lightGray.cgColor
        imageViewArticleImage.layer.borderWidth = 0.5
    }
}
