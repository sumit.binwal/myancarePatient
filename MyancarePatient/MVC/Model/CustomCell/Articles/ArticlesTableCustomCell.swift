//
//  ArticlesTableCustomCell.swift
//  MyancarePatient
//
//  Created by Jyoti on 09/01/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit
let cellIdentifier_ArticlesTableCell = "ArticlesTableCell"

class ArticlesTableCustomCell: UITableViewCell {
    
    @IBOutlet var imageViewArticleImage: UIImageView!
    @IBOutlet var labelArticleTitle: UILabel!
    @IBOutlet var labelArticleDescription: UILabel!
    @IBOutlet var labelArticleDate: UILabel!
    @IBOutlet var buttonTotalComments: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
