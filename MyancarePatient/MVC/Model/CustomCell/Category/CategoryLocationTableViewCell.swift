//
//  CategoryLocationTableViewCell.swift
//  MyancarePatient
//
//  Created by iOS on 13/02/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit

//MARK: -
let kCellIdentifier_CategoryLocationTableViewCell = "CategoryLocationTableViewCell"

//MARK: -
enum CategoryLocationDataCellType {
    case state
    case district
    case town
    case none
}

//MARK: -
protocol CategoryLocationDataCellDelegate: class
{
    func categoryLocataionDataCell(_ cell: CategoryLocationTableViewCell, updatedInputFieldText inputValue: String)
}

//MARK: -
//MARK: -

class CategoryLocationTableViewCell: UITableViewCell {

    @IBOutlet weak var heightBtn: UIButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var inputTextField: UITextField!
    
    var arrayState : [String] = []
    
    var cellType = CategoryLocationDataCellType.none
    
    var pickerBinder: PickerViewBinder?
    
    weak var delegate: CategoryLocationDataCellDelegate?
    
    //MARK: -
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        containerView.setBorderColor(UIColor.MyanCarePatient.lightGreyColor, cornorRadius: 5, borderWidth: 0.5)
        
        if inputTextField != nil {
            inputTextField.delegate = self
            inputTextField.text = ""
            
            inputTextField.setPlaceholderColor (UIColor.MyanCarePatient.textfieldPlaceholderColor)
        }
    }
    
    //MARK: -
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK: -
    func bindThePickerview(_arrValue : NSMutableArray)
    {
        let picker = UIPickerView ()
        
        pickerBinder = PickerViewBinder ()
        
        pickerBinder?.handlePickerView(_pickerView: picker, values: _arrValue as! [String], addDelegate: self)
        
        inputTextField.inputView = picker
    }
    
    //MARK: -
    override func layoutSubviews() {
        
        switch cellType {
            
        case .state:
            inputTextField.placeholder = "Select State".localized()
            inputTextField.tintColor = UIColor.clear
            
        case .district:
            inputTextField.placeholder = "Select District".localized()
            inputTextField.tintColor = UIColor.clear
            
        default:
            inputTextField.placeholder = "Select Township".localized()
            inputTextField.tintColor = UIColor.clear
            break
        }
        
        inputTextField.tag = self.tag
    }
    
    //MARK: -
    func updateCell()
    {
        switch cellType {
            
        case .state:
            guard modelDoctorFilterProcess.state != nil else
            {
                return
            }
            
            inputTextField.text = modelDoctorFilterProcess.state
            
        case .district:
            guard modelDoctorFilterProcess.district != nil else
            {
                return
            }
            
            inputTextField.text = modelDoctorFilterProcess.district
            
        case .town:
            guard modelDoctorFilterProcess.town != nil else
            {
                return
            }
            
            inputTextField.text = modelDoctorFilterProcess.town
            
        default:
            break
        }
    }
}

//MARK: -
//MARK: - Extension -> PickerViewBinderDelegate
extension CategoryLocationTableViewCell: PickerViewBinderDelegate
{
    func pickerBinderDatePicker(_ pickerBinder: PickerViewBinder, withUpdatedValue value: String) {
        
    }
    
    func pickerBinder(_ pickerBinder: PickerViewBinder, didSelectRow row: Int, withUpdatedValue value: String) {
        
        //inputTextField.text = value
        
        switch cellType {
            
        case .state,.district,.town:
            
            inputTextField.text = value
            
            guard let tempDelegate = delegate else {
                return
            }
            
            tempDelegate.categoryLocataionDataCell(self, updatedInputFieldText: value)
            
        default:
            break
        }
    }
}

//MARK: -
//MARK: - Extension -> UITextFieldDelegate
extension CategoryLocationTableViewCell: UITextFieldDelegate
{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        switch cellType {
            
        case .state:
            textField.clearsOnBeginEditing = true
            let stateArr = UtilityClass.getStateListing()
            bindThePickerview(_arrValue:stateArr)
            
        case .district:
            guard modelDoctorFilterProcess.state != nil, (modelDoctorFilterProcess.state?.count)! != 0  else
            {
                let  currentVC =  UIApplication.shared.keyWindow?.currentViewController()
                
                UtilityClass.showAlertWithTitle(title: App_Name, message: "Please select state first".localized(), onViewController: currentVC, withButtonArray: nil, dismissHandler: nil)
                
                return false
            }
            
            textField.clearsOnBeginEditing = true
            
            let districtArr = UtilityClass.getDistrictListing(_stateVal: modelDoctorFilterProcess.state!)
            bindThePickerview(_arrValue:districtArr)
            
        case .town:
            textField.clearsOnBeginEditing = true
            
            guard modelDoctorFilterProcess.district != nil, (modelDoctorFilterProcess.district?.count)! != 0 else
            {
                let  currentVC =  UIApplication.shared.keyWindow?.currentViewController()
                
                UtilityClass.showAlertWithTitle(title: App_Name, message: "Please select district first".localized(), onViewController: currentVC, withButtonArray: nil, dismissHandler: nil)
                
                return false
            }
            
            let townArr = UtilityClass.getTownArray(_districtVal: modelDoctorFilterProcess.district!)
            bindThePickerview(_arrValue:townArr)
            
        default:
            break
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        return false
    }
}
