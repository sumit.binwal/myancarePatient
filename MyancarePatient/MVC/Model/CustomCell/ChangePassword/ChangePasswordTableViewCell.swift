//
//  ChangePasswordTableViewCell.swift
//  MyancarePatient
//
//  Created by iOS on 13/02/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit

//MARK: -
enum ChangePasswordCellType {
    case oldPassword
    case newPassword
    case confirmPassword
    case none
}

//MARK: -
protocol ChangePasswordTableViewCellDelegate: class
{
    func changePasswordDataCell(_ cell: ChangePasswordTableViewCell, updatedInputFieldText inputValue: String)
}

class ChangePasswordTableViewCell: UITableViewCell {

    @IBOutlet weak var inputTextField: UITextField!
    @IBOutlet weak var labelName: UILabel!
    
    var cellType = ChangePasswordCellType.none
    
    var pickerBinder: PickerViewBinder?
    
    weak var delegate: ChangePasswordTableViewCellDelegate?
    
    //MARK: -
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        if inputTextField != nil {
            inputTextField.delegate = self
            inputTextField.text = ""
            inputTextField.setPlaceholderColor(UIColor.MyanCarePatient.textfieldPlaceholderColor)
        }
    }
    
    //MARK: -
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        switch cellType {
            
        case .oldPassword:
            labelName.text = "Old Password".localized()
            inputTextField.placeholder = "Type your old password here".localized()
            inputTextField.keyboardType = .default
            
        case .newPassword:
            labelName.text = "New Password".localized()
            inputTextField.placeholder = "Type your new password here".localized()
            inputTextField.keyboardType = .default
            
        case .confirmPassword:
            labelName.text = "Confirm Password".localized()
            inputTextField.placeholder = "Retype your new password".localized()
            inputTextField.keyboardType = .default
            
        default:
            inputTextField.placeholder = ""
            break
        }
        
        inputTextField.tag = self.tag
    }
}

extension ChangePasswordTableViewCell: UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        //var finalCount = 0
        var newString : String?
        
        if string.count == 0
        {
            if textField.text?.count == 0
            {
                //finalCount = 0
                newString = textField.text
            }
            else
            {
                //finalCount = textField.text!.count - 1
                var newStr = textField.text! as NSString
                newStr = newStr.replacingCharacters(in: range, with: string) as NSString
                newString = newStr as String
            }
        }
        else
        {
            //finalCount = textField.text!.count + 1
            var newStr = textField.text! as NSString
            
            newStr = newStr.replacingCharacters(in: range, with: string) as NSString
            
            newString = newStr as String
        }
        
        guard let phoneDelegate = delegate else {
            return true
        }
        
        phoneDelegate.changePasswordDataCell(self, updatedInputFieldText: newString!)
        
        return true
    }
}
