//
//  ChatViewCell.swift
//  OOTTBusinessApp
//
//  Created by Santosh on 13/07/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit
import SDWebImage

let kCellIdentifier_GroupCell_Other = "otherGroupCell"

let kCellIdentifier_GroupCell_OtherMedia = "otherGroupCellMedia"

let kCellIdentifier_GroupCell_Own = "ownCell"

let kCellIdentifier_GroupCell_OwnLike = "ownCellLike"

let kCellIdentifier_GroupCell_OwnLikeMedia = "ownCellLikeMedia"

let kCellIdentifier_GroupCell_Single = "otherSingleCell"

let kCellIdentifier_GroupCell_Business = "BusinessGroupCell"
let kCellIdentifier_GroupCell_BusinessMedia = "BusinessGroupCellMedia"

let kCellIdentifier_GroupCell_MediaSender = "senderCellMedia"
let kCellIdentifier_GroupCell_MediaOwner = "ownCellMedia"

enum ChatCellType {
    case groupOther
    case singleOther
    case user
    case userLike
    case none
}

protocol ChatViewCellDelegate : class
{
    func chatCell(_ cell : ChatViewCell, didTapOnLikeUnLikeButton likeUnLikeButton : UIButton)
    func chatCellDidDownloadTheMedia(_ cell: ChatViewCell)
    func chatCell(_ cell: ChatViewCell, didTapOnMedia mediaButton: UIButton)
    
    func ChatViewCellUserImageButtonButtonAction(_ cell: ChatViewCell)
    func ChatViewCellSenderImageButtonButtonAction(_ cell: ChatViewCell)
}

class ChatViewCell: UITableViewCell {

    @IBOutlet weak var imageviewUser: UIImageView!
    @IBOutlet weak var imageViewSender: UIImageView!
    
    @IBOutlet weak var userName: UILabel!
    
    @IBOutlet weak var imageViewSenderMedia: UIImageView!
    @IBOutlet weak var imageViewUserMedia: UIImageView!
    
    @IBOutlet weak var buttonSenderMedia: UIButton!
    @IBOutlet weak var buttonUserMedia: UIButton!
    
    @IBOutlet weak var userLocation: UILabel!
    
    @IBOutlet weak var conatinerTextView: UIView!
    
    @IBOutlet weak var messageLabel: UILabel!
    
    @IBOutlet weak var messageTime: UILabel!
    
    @IBOutlet weak var locationImage: UIImageView!
    
    @IBOutlet weak var buttonLikeUnLike: UIButton!
    
    @IBOutlet weak var employeePositionLabel: UILabel!
    
    @IBOutlet weak var buttonMediaView: UIButton!
    
    @IBOutlet weak var buttonDownloadMedia: UIButton!
    
    @IBOutlet weak var constraintLocationHeight: NSLayoutConstraint!
    
    @IBOutlet weak var loaderView: BRCircularProgressView!
    
    @IBOutlet weak var playIcon: UIImageView!
    
    var cellType = ChatCellType.none
    
    weak var delegate : ChatViewCellDelegate?
    
    var mediaString = ""
    
    var isDownloading = false
    
    //MARK: - deinit
    deinit {
        print("ChatViewCell Deinit")
    }
    
    //MARK: - awakeFromNib
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        initialCellSetup()
        
        self.conatinerTextView.layer.cornerRadius = 5 * scaleFactorX
    }

    //MARK: -
    func initialCellSetup()
    {
        self.backgroundColor = UIColor.clear
        
        if self.imageviewUser != nil
        {
            self.imageviewUser.layer.cornerRadius = self.imageviewUser.frame.size.width/2 * scaleFactorX
            self.imageviewUser.clipsToBounds = true
        }
        
        if self.imageViewSender != nil
        {
            self.imageViewSender.layer.cornerRadius = self.imageViewSender.frame.size.width/2 * scaleFactorX
            self.imageViewSender.clipsToBounds = true
        }
        
        if self.userName != nil
        {
            self.userName.textColor = UIColor.white
        }
        
        if buttonMediaView != nil
        {
            buttonMediaView.imageView?.contentMode = .scaleAspectFill
            buttonMediaView.imageView?.clipsToBounds = true
            buttonMediaView.layer.cornerRadius = 5 * scaleFactorX
        }
        
        if loaderView != nil
        {
            loaderView.isHidden = true
        
            loaderView.setCircleStrokeWidth(5)
            
            loaderView.setCircleStrokeColor(UIColor (RED: 0, GREEN: 0, BLUE: 0, ALPHA: 0.5), circleFillColor: UIColor (RED: 0, GREEN: 0, BLUE: 0, ALPHA: 0.5), progressCircleStrokeColor: color_pink, progressCircleFillColor: UIColor (RED: 0, GREEN: 0, BLUE: 0, ALPHA: 0.5))
        }
    }
    
    //MARK: - layoutSubviews
    override func layoutSubviews() {
        if messageLabel != nil {
            self.messageLabel.setNeedsDisplay()
        }
    }
    
    @IBAction func buttonSenderMediaAction(_ sender: UIButton) {
        
        guard let tempDelegate = self.delegate else {
            return
        }
        
        tempDelegate.ChatViewCellSenderImageButtonButtonAction(self)
    }
    
    @IBAction func buttonUserMediaAction(_ sender: UIButton) {
        
        guard let tempDelegate = self.delegate else {
            return
        }
        
        tempDelegate.ChatViewCellUserImageButtonButtonAction(self)
    }
    
    //MARK: - Update cell using Model
    func updateCell(usingModel model : ModelChatMessage, cellIndex : Int, folderPath: String)
    {
        if self.userName != nil
        {
            self.userName.text = model.senderName
        }
        
        if self.imageviewUser != nil
        {
            self.imageviewUser.setIndicatorStyle(.white)
            self.imageviewUser.setShowActivityIndicator(true)
            
            self.imageviewUser.sd_setImage(with: URL (string: model.senderPicture), placeholderImage: #imageLiteral(resourceName: "no_image_user"))
        }
        
        if self.imageViewSender != nil
        {
            self.imageViewSender.setIndicatorStyle(.white)
            self.imageViewSender.setShowActivityIndicator(true)
            
            self.imageViewSender.sd_setImage(with: URL (string: model.senderPicture), placeholderImage: #imageLiteral(resourceName: "no_image_user"))
        }

        self.messageTime.text = model.messageTime

        if messageLabel != nil
        {
            self.messageLabel.text = model.message.base64Decoded()
            self.messageLabel.sizeToFit()
        }

        if buttonLikeUnLike != nil
        {
            buttonLikeUnLike.tag = cellIndex
        }

        if self.imageViewUserMedia != nil
        {
            self.imageViewUserMedia.setIndicatorStyle(.white)
            self.imageViewUserMedia.setShowActivityIndicator(true)
            
            self.imageViewUserMedia.sd_setImage(with: URL (string: model.thumbString), placeholderImage: #imageLiteral(resourceName: "no_image_article"))
        }
        
        if self.imageViewSenderMedia != nil
        {
            self.imageViewSenderMedia.setIndicatorStyle(.white)
            self.imageViewSenderMedia.setShowActivityIndicator(true)
            
            self.imageViewSenderMedia.sd_setImage(with: URL (string: model.thumbString), placeholderImage: #imageLiteral(resourceName: "no_image_article"))
        }
    }
}

//MARK: -
//MARK: - extension -> Donwload Status Methods
extension ChatViewCell
{
    func handleDownloadState(usingData data: Data, isImage: Bool, thumbnail: UIImage?)
    {
        self.buttonDownloadMedia.isHidden = true
        self.loaderView.isHidden = true
        self.buttonMediaView.isUserInteractionEnabled = true
        
        var thumb: UIImage!
        
        if isImage {
            thumb = UIImage (data: data)
        } else {
            thumb = thumbnail!
        }
        
        if self.buttonMediaView.image(for: .normal) == nil {
            UIView.transition(with: self.buttonMediaView, duration: 0.25, options: .transitionCrossDissolve, animations: {[weak self] in
                
                guard let `self` = self else {return}
                
                self.buttonMediaView.setImage(thumb, for: .normal)
                self.buttonMediaView.setBackgroundImage(nil, for: .normal)
            },
            completion: nil)
        } else {
            self.buttonMediaView.setImage(thumb, for: .normal)
            self.buttonMediaView.setBackgroundImage(nil, for: .normal)
        }
    }
}
