//
//  CommentsTableCustomCell.swift
//  MyancarePatient
//
//  Created by Jyoti on 11/01/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit
let cellIdentifier_CommentsCell = "cellCommentTable"

class CommentsTableCustomCell: UITableViewCell {

    //Mark :- Properties
    @IBOutlet var labelComments: UILabel!
    @IBOutlet var labelDate: UILabel!
    @IBOutlet var labelName: UILabel!
    @IBOutlet var userImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        userImageView.layer.cornerRadius = userImageView.frame.size.width/2 * scaleFactorX
        userImageView.layer.borderColor = UIColor.lightGray.cgColor
        userImageView.layer.borderWidth = 1.0
        userImageView.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
