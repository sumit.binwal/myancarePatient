//
//  ConnectDirectCell.swift
//  OOTTBusinessApp
//
//  Created by Bharat Kumar Pathak on 10/07/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import UIKit

let kCellIdentifier_connect_direct_cell = "connectDirectCell"

class ConnectDirectCell: UITableViewCell {

    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userMessageLabel: UILabel!
    @IBOutlet weak var unreadCountBackView: UIView!
    @IBOutlet weak var unreadCountLabel: UILabel!
    @IBOutlet weak var labelTime: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //self.userImageView.roundedImageView()
        
        self.userImageView.layer.cornerRadius = self.userImageView.frame.size.width/2 * scaleFactorX
        
        self.userImageView.createBorder(withColor: color_cellImageBorder, andBorderWidth: 0.5)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func layoutSubviews() {
        self.unreadCountBackView.layer.cornerRadius = self.unreadCountBackView.frame.size.height/2 * scaleFactorX
    }
    
    func updateCell(usingModel model : ModelChatList)
    {
        self.userNameLabel.text = model.userName
        self.userMessageLabel.text = model.lastMessage.base64Decoded()

        self.userImageView.setIndicatorStyle(.white)
        self.userImageView.setShowActivityIndicator(true)

        self.userImageView.sd_setImage(with: URL (string: model.userPicture))
        {[unowned self] (image, error, cacheType, url) in
            self.userImageView.setShowActivityIndicator(false)
        }
        
        unreadCountBackView.isHidden = true
        if model.unreadCount > 0 {
            unreadCountBackView.isHidden = false
            unreadCountLabel.text = String(model.unreadCount)
        }
    }
}
