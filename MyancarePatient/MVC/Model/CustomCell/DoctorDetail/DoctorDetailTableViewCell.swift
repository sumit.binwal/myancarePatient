//
//  DoctorDetailTableViewCell.swift
//  MyancarePatient
//
//  Created by iOS on 30/01/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit

let kCellIdentifier_DoctorDetail_table_view_cell = "DoctorDetailTableViewCell"

class DoctorDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var address2Label: UILabel!
    @IBOutlet weak var address1label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
