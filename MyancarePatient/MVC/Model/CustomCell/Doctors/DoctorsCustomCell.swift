//
//  DoctorsCustomCell.swift
//  MyancarePatient
//
//  Created by Jyoti on 06/01/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit
let cellIdentifier_DoctorsCell = "cellDoctorsTable"

//MARK: -
protocol DoctorsCustomCellDelegate: class
{
    func DoctorsCustomCellImageViewClick(_ cell: DoctorsCustomCell)
    func DoctorsCustomCellDoctorStatus(_ cell: DoctorsCustomCell, dataDict : [String:Any])
}

class DoctorsCustomCell: UITableViewCell {
    
    //Mark:- Properties
    @IBOutlet weak var checkedImage: UIImageView!
    @IBOutlet weak var imageViewProfilePic: UIImageView!
    @IBOutlet var imageViewStatus: UIImageView!
    @IBOutlet weak var labelDoctorName: UILabel!
    @IBOutlet weak var labelDoctorType: UILabel!
    @IBOutlet weak var labelDoctorLocation: UILabel!
    @IBOutlet weak var labelDoctorRating: UILabel!

    private var pendingRequestWorkItem: DispatchWorkItem?
    
    weak var delegate: DoctorsCustomCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.disableSelection()
        
        //imageViewProfilePic.roundedImageView()
        
        imageViewProfilePic.layer.cornerRadius = imageViewProfilePic.frame.size.height/2 * (UIScreen.main.bounds.size.width/414)
        imageViewProfilePic.layer.borderWidth = 1.0
        imageViewProfilePic.layer.borderColor = UIColor.clear.cgColor
        imageViewProfilePic.layer.masksToBounds = true
        
        imageViewProfilePic.isUserInteractionEnabled = true
        let gesture = UITapGestureRecognizer.init(target: self, action: #selector(doctorImageViewClicked))
        imageViewProfilePic.addGestureRecognizer(gesture)
    }

    //MARK: - get doctor status from API
    @objc func getDoctorStatusApi(doctor_id : String, index : Int) {
        
        let urlToHit = EndPoints.get_status(doctor_id).path
        
        print("doctor status url = \(urlToHit)")
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod:.get, parameters: nil) {[weak self] (data, dictionary, statusCode, error) in
            
            guard let `self` = self else {return}
            
            if error != nil
            {
                //if error?.localizedDescription == "The network connection was lost."
                //{
                //    UtilityClass.showAlertWithTitle(title: App_Name, message: "The network connection was lost".localized(), onViewController: self, withButtonArray: nil, dismissHandler: nil)
                //}
                //else
                //{
                //    UtilityClass.showAlertWithTitle(title: App_Name, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                //}
                
                return
            }
            
            if dictionary == nil
            {
                //UtilityClass.showAlertWithTitle(title: App_Name, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary!
                let replyStatus = responseDictionary["status"] as! String
                
                if replyStatus == "success"
                {
                    // Cancel the currently pending item
                    self.pendingRequestWorkItem?.cancel()
                    
                    // Wrap our request in a work item
                    let requestWorkItem = DispatchWorkItem { [weak self] in
                        //self?.resultsLoader.loadResults(forQuery: searchText)
                        
                        guard let doctorDelegate = self?.delegate else {
                            return
                        }
                        
                        let doctorData = responseDictionary["data"] as? [String : Any]
                        
                        doctorDelegate.DoctorsCustomCellDoctorStatus(self!, dataDict: doctorData!)
                    }
                    
                    // Save the new work item and execute it after 250 ms
                    self.pendingRequestWorkItem = requestWorkItem
                    DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(250), execute: requestWorkItem)
                }
            }
        }
    }
    
    @objc func doctorImageViewClicked() {
        
        guard let doctorDelegate = delegate else {
            return
        }
        
        doctorDelegate.DoctorsCustomCellImageViewClick(self)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
