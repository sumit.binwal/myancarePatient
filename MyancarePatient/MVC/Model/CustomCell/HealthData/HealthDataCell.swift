//
//  HealthDataCell.swift
//  MyanCareDoctor
//
//  Created by Santosh on 18/12/17.
//  Copyright © 2017 sumit. All rights reserved.
//

import UIKit

//MARK: -
let kCellIdentifier_HealthData_BloodType = "healthCellBloodType"
let kCellIdentifier_HealthData_Height = "healthCellHeight"

//MARK: -
enum HealthDataCellType
{
    case bloodGroup
    case height
    case weight
    case state
    case district
    case town
    case none
}

//MARK: -
protocol HealthDataCellDelegate: class
{
    func healthDataCell(_ cell: HealthDataCell, updatedInputFieldText inputValue: String)
}

//MARK: -
//MARK: -
class HealthDataCell: UITableViewCell
{
    @IBOutlet weak var heightBtn: UIButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var labelBloodType: UILabel!
    @IBOutlet weak var inputTextField: UITextField!
    
    private var arrayBloodGroupTypes : NSMutableArray = []
    var arrayState : [String] = []
    
    var cellType = HealthDataCellType.none
    
    var pickerBinder: PickerViewBinder?
    
    weak var delegate: HealthDataCellDelegate?
    
    //MARK: -
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
        
        //arrayBloodGroupTypes.add("O-")
        //arrayBloodGroupTypes.add("O+")
        //arrayBloodGroupTypes.add("A-")
        //arrayBloodGroupTypes.add("A+")
        //arrayBloodGroupTypes.add("B-")
        //arrayBloodGroupTypes.add("B+")
        //arrayBloodGroupTypes.add("AB-")
        //arrayBloodGroupTypes.add("AB+")
        
        arrayBloodGroupTypes.add("O")
        arrayBloodGroupTypes.add("A")
        arrayBloodGroupTypes.add("B")
        arrayBloodGroupTypes.add("AB")
        
        containerView.setBorderColor(UIColor.MyanCarePatient.lightGreyColor, cornorRadius: 5, borderWidth: 0.5)
        
        if inputTextField != nil
        {
            inputTextField.delegate = self
            inputTextField.text = ""
            
            inputTextField.setPlaceholderColor (UIColor.MyanCarePatient.textfieldPlaceholderColor)
        }
    }
    
    //MARK: -
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    //MARK: -
    override func layoutSubviews()
    {
        switch cellType
        {
        
        case .height:
            inputTextField.placeholder = "Height".localized()
            inputTextField.keyboardType = .numberPad
            heightBtn.setTitle("in".localized(), for: .normal)
            
        case .weight:
            inputTextField.placeholder = "Weight".localized()
            inputTextField.keyboardType = .numberPad
            heightBtn.setTitle("lb".localized(), for: .normal)
            
        case .state:
            inputTextField.placeholder = "Select State".localized()
            inputTextField.tintColor = UIColor.clear
            
        case .district:
            inputTextField.placeholder = "Select District".localized()
            inputTextField.tintColor = UIColor.clear
            
        case .town:
            inputTextField.placeholder = "Select Township".localized()
            inputTextField.tintColor = UIColor.clear
            
        default:
            inputTextField.placeholder = "Select blood group".localized()
            bindThePickerview(_arrValue: arrayBloodGroupTypes)
            inputTextField.tintColor = UIColor.clear
            
            break
        }
        
        inputTextField.tag = self.tag
    }
    
    //MARK: -
    func bindThePickerview(_arrValue : NSMutableArray)
    {
        let picker = UIPickerView ()
        pickerBinder = PickerViewBinder ()
        pickerBinder?.handlePickerView(_pickerView: picker, values: _arrValue as! [String], addDelegate: self)
        
        inputTextField.inputView = picker
    }
    
    //MARK: -
    func updateCell()
    {
        switch cellType {
            
        case .state:
            guard modelSignUpProcess.stateName != nil else
            {
                return
            }
            
            inputTextField.text = modelSignUpProcess.stateName
            
        case .district:
            guard modelSignUpProcess.districtName != nil else
            {
                return
            }
            
            inputTextField.text = modelSignUpProcess.districtName
            
        case .town:
            guard modelSignUpProcess.townName != nil else
            {
                return
            }
            
            inputTextField.text = modelSignUpProcess.townName
            
        case .bloodGroup:
            guard modelSignUpProcess.bloodGroup != nil else
            {
                return
            }
            
            inputTextField.text = modelSignUpProcess.bloodGroup
            
        case .height:
            guard modelSignUpProcess.height != nil else
            {
                return
            }
            
            inputTextField.text = modelSignUpProcess.height
            
        case .weight:
            guard modelSignUpProcess.weight != nil else
            {
                return
            }
            
            inputTextField.text = modelSignUpProcess.weight
            
        default:
            break
        }
    }
    
    func updateHeightValue(usingValue value: String)
    {
        var numberString = UtilityClass.extractNumber(fromString: value)
        //print("number string = \(numberString)")
        
        if numberString.count == 1
        {
            numberString = numberString + "'"
        }
        else
        {
            numberString.insert("'", at: numberString.index(after: numberString.startIndex))
        }
        
        inputTextField.text = numberString
    }
}

//MARK: -
//MARK: - Extension -> PickerViewBinderDelegate
extension HealthDataCell: PickerViewBinderDelegate
{
    func pickerBinderDatePicker(_ pickerBinder: PickerViewBinder, withUpdatedValue value: String)
    {
        
    }
    
    func pickerBinder(_ pickerBinder: PickerViewBinder, didSelectRow row: Int, withUpdatedValue value: String)
    {
        inputTextField.text = value
        
        switch cellType
        {
            
        case .bloodGroup:
            inputTextField.text = value
            
            guard let tempDelegate = delegate else
            {
                return
            }
            
            tempDelegate.healthDataCell(self, updatedInputFieldText: value)
            
        case .state,.district,.town:
            inputTextField.text = value
            
            guard let tempDelegate = delegate else
            {
                return
            }
            
            tempDelegate.healthDataCell(self, updatedInputFieldText: value)
            
        default:
            break
        }
    }
}

//MARK: -
//MARK: - Extension -> UITextFieldDelegate
extension HealthDataCell: UITextFieldDelegate
{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        
        switch cellType
        {
        
        case .state:
            textField.clearsOnBeginEditing = true
            let stateArr = UtilityClass.getStateListing()
            bindThePickerview(_arrValue:stateArr)
            
        case .district:
            guard modelSignUpProcess.stateName != nil, (modelSignUpProcess.stateName?.count)! != 0  else
            {
                let  currentVC =  UIApplication.shared.keyWindow?.currentViewController()
                
                UtilityClass.showAlertWithTitle(title: App_Name, message: "Please select state first".localized(), onViewController: currentVC, withButtonArray: nil, dismissHandler: nil)
                return false
            }
            
            textField.clearsOnBeginEditing = true
            let districtArr = UtilityClass.getDistrictListing(_stateVal: modelSignUpProcess.stateName!)
            bindThePickerview(_arrValue:districtArr)
            
        case .town:
            textField.clearsOnBeginEditing = true
            guard modelSignUpProcess.districtName != nil, (modelSignUpProcess.districtName?.count)! != 0 else
            {
                let  currentVC =  UIApplication.shared.keyWindow?.currentViewController()
                
                UtilityClass.showAlertWithTitle(title: App_Name, message: "Please select district first".localized(), onViewController: currentVC, withButtonArray: nil, dismissHandler: nil)
                return false
            }
            
            let townArr = UtilityClass.getTownArray(_districtVal: modelSignUpProcess.districtName!)
            bindThePickerview(_arrValue:townArr)
            
        case .bloodGroup :
            textField.clearsOnBeginEditing = true
            
        default:
            break
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if cellType == .bloodGroup || cellType == .state || cellType == .district || cellType == .town
        {
            return false
        }
        
        var newString : String?
        
        if string.count == 0
        {
            if textField.text?.count == 0
            {
                newString = textField.text
            }
            else
            {
                if textField.text?.last == "'"
                {
                    newString = ""
                }
                else
                {
                    var newStr = textField.text! as NSString
                    newStr = newStr.replacingCharacters(in: range, with: string) as NSString
                    newString = newStr as String
                }
            }
        }
        else
        {
            let length = (textField.text?.count)!
            
            if cellType == .height
            {
                if length >= 4
                {
                    newString = textField.text
                }
                else
                {
                    var newStr = textField.text! as NSString
                    
                    newStr = newStr.replacingCharacters(in: range, with: string) as NSString
                    
                    newString = newStr as String
                }
            }
            else
            {
                if length >= 3
                {
                    newString = textField.text
                }
                else
                {
                    var newStr = textField.text! as NSString
                    
                    newStr = newStr.replacingCharacters(in: range, with: string) as NSString
                    
                    newString = newStr as String
                }
            }
        }
        
        if cellType == .height
        {
            if string.count > 0
            {
                updateHeightValue(usingValue: newString!)
            }
            else
            {
                inputTextField.text = newString
            }
        }
        else
        {
            inputTextField.text = newString
        }
        
        guard let tempDelegate = delegate else
        {
            return true
        }
        
        tempDelegate.healthDataCell(self, updatedInputFieldText: newString!)
        
        return false
    }
}
