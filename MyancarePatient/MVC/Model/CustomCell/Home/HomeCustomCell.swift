//
//  HomeCustomCell.swift
//  MyancarePatient
//
//  Created by Jyoti on 05/01/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit

let cellIdentifier_HomeCell = "cellHomeTable"

protocol HomeDataCellDelegate: class
{
    func homeDataCell(_ cell: HomeCustomCell, withButtonTag:Int )
    func homeDataCellComment(_ cell: HomeCustomCell, withButtonTag:Int )
}

class HomeCustomCell: UITableViewCell {
    
    //Mark:- Properties
    @IBOutlet var labelArcTitle: UILabel!
    @IBOutlet var labelArcDate: UILabel!
    @IBOutlet var ImageViewArcImage: UIImageView!
    @IBOutlet var labelArcDescription: UILabel!
    @IBOutlet var buttonLikes: UIButton!
    @IBOutlet var buttonViews: UIButton!
    @IBOutlet var buttonComments: UIButton!
    
    weak var delegate: HomeDataCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.disableSelection()
    }

    @IBAction func likeButtonClickedAction(_ sender: UIButton)
    {
        
        
        guard let homeDelegate = delegate else {
            return
        }
        
        homeDelegate.homeDataCell(self, withButtonTag:sender.tag)
    }
    
    @IBAction func commentButtonClickedAction(_ sender: UIButton) {
        
        guard let homeDelegate = delegate else {
            return
        }
        
        homeDelegate.homeDataCellComment(self, withButtonTag:sender.tag)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
