//
//  LoginCustomCell.swift
//  MyancarePatient
//
//  Created by Jyoti on 08/01/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit
import UIKit

let cellIdentifier_LoginCell = "loginCell"

enum LoginCellType {
    case phoneNumber
    case password
    case none
}

//MARK: -
protocol LoginCellDelegate: class
{
    func loginDataCell(_ cell: LoginCustomeCell, updatedInputFieldText inputValue: String)
}

class LoginCustomeCell: UITableViewCell {
   
    @IBOutlet var lblCountryCode: UILabel!
    @IBOutlet var inputTextFieldContainer: UIView!
    @IBOutlet weak var inputTextField: UITextField!
    @IBOutlet var cellTypeImage: UIImageView!
    weak var delegate: LoginCellDelegate?
    var cellType = LoginCellType.none
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.disableSelection()
        
        inputTextField.delegate = self
        
        inputTextFieldContainer.setBorderColor (UIColor.MyanCarePatient.lightGreyColor, cornorRadius: 3, borderWidth: 0.5)
    }
    
    override func layoutSubviews() {
        switch cellType {
        case .phoneNumber:
            inputTextField.placeholder = "9xxxxxxxx"
            inputTextField.keyboardType = .numberPad
            cellTypeImage.image = #imageLiteral(resourceName: "login-phone-icon")
            lblCountryCode.isHidden = false
            
            inputTextField.leftView = UIView()
            inputTextField.leftView?.frame = CGRect(x: 0, y: 5, width: 40 , height:20)
            inputTextField.leftViewMode = .always
        case .password:
            inputTextField.placeholder = "Type your password here".localized()
            lblCountryCode.isHidden = true
            inputTextField.keyboardType = .asciiCapable
            inputTextField.isSecureTextEntry = true
            cellTypeImage.image = #imageLiteral(resourceName: "login-password-icon")
            //cellTypeImage.image = UIImage.init(named: "login-password-icon")
            
        default: break
            
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}

extension LoginCustomeCell: UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        //var finalCount = 0
        var newString : String?
        
        if string.count == 0
        {
            if textField.text?.count==0
            {
                //finalCount = 0
                newString = textField.text
            }
            else
            {
                //finalCount = textField.text!.count - 1
                var newStr = textField.text! as NSString
                newStr = newStr.replacingCharacters(in: range, with: string) as NSString
                newString = newStr as String
            }
        }
        else
        {
            //finalCount = textField.text!.count + 1
            var newStr = textField.text! as NSString
            
            newStr = newStr.replacingCharacters(in: range, with: string) as NSString
            
            newString = newStr as String
        }
        switch cellType {
        case .phoneNumber:
            if newString!.count > 12
            {
                return false
            }
        default:
            break
        }
        
        guard let loginDelegate = delegate else {
            return true
        }
        
        loginDelegate.loginDataCell(self, updatedInputFieldText: newString!)
        
        return true
    }
}
