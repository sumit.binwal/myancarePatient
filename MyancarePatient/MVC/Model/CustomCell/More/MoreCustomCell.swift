//
//  MoreCustomTableCell.swift
//  MyancarePatient
//
//  Created by Jyoti on 06/01/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit

let cellIdentifier_MoreCell = "cellMoreTable"
let cellIdentifier_SettingCell = "cellSettingMoreTable"

//MARK: -
protocol MoreNotificationSwitchDelegate: class
{
    func moreNotificationStatusCell(_ cell: MoreCustomCell, notificationStatus: Bool)
}

class MoreCustomCell: UITableViewCell {
    
    //MARK: - Properties (cellMoreTable)
    @IBOutlet weak var buttonListIcons: UIButton!
    @IBOutlet weak var buttonListNames: UIButton!
    @IBOutlet weak var buttonNextScreen: UIButton!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!

    //cellSettingMoreTable
    @IBOutlet weak var buttonSettingIcon: UIButton!
    @IBOutlet weak var buttonSettingListName: UIButton!
    @IBOutlet weak var buttonSettingNextScreen: UIButton!
    
    @IBOutlet weak var settingLabel: UILabel!
    @IBOutlet weak var switchImage: UISwitch!
    
    weak var delegate: MoreNotificationSwitchDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.disableSelection()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func switchButtonChanged(_ sender: UISwitch)
    {
        UtilityClass.saveNotificationStatus(userDict: sender.isOn)
        
        print(UtilityClass.getNotificationStatus()!)
        
        guard let tempDelegate = delegate else {
            return
        }
        
        tempDelegate.moreNotificationStatusCell(self, notificationStatus: UtilityClass.getNotificationStatus()!)
    }
}
