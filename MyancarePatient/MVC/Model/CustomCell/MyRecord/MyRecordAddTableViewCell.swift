//
//  MyRecordAddTableViewCell.swift
//  MyancarePatient
//
//  Created by iOS on 09/02/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit

//MARK: -
enum AddRecordType {
    case name
    case disease
    case hospital
    case date
    case none
}

//MARK: -
protocol MyRecordAddTableViewCellDelegate: class
{
    func recordAddDataCell(_ cell: MyRecordAddTableViewCell, updatedInputFieldText inputValue: String)
}

class MyRecordAddTableViewCell: UITableViewCell {

    weak var delegate: MyRecordAddTableViewCellDelegate?
    
    var cellType = AddRecordType.none
    
    var pickerBinder: PickerViewBinder?
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var fieldImage: UIImageView!
    @IBOutlet weak var fieldText: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {
        
        switch cellType {
            
        case .name:
            fieldText.placeholder = "Type doctor name here".localized()
            fieldText.keyboardType = .asciiCapable
            fieldText.delegate = self
            fieldText.autocorrectionType = .no
            nameLabel.text = "Doctor Name".localized()
            fieldImage.image = #imageLiteral(resourceName: "add-record-doctor")
            
        case .disease:
            fieldText.placeholder = "Type disease name here".localized()
            fieldText.keyboardType = .asciiCapable
            fieldText.delegate = self
            fieldText.autocorrectionType = .no
            nameLabel.text = "Disease Name".localized()
            fieldImage.image = #imageLiteral(resourceName: "add-record-disease")
            
        case .hospital:
            fieldText.placeholder = "Type hospital name here".localized()
            fieldText.keyboardType = .asciiCapable
            fieldText.delegate = self
            fieldText.autocorrectionType = .no
            nameLabel.text = "Hospital Name".localized()
            fieldImage.image = #imageLiteral(resourceName: "add-record-hospotal")
            
        case .date:
            fieldText.placeholder = "Select Date".localized()
            nameLabel.text = "Date".localized()
            fieldText.delegate = self
            fieldImage.image = #imageLiteral(resourceName: "add-record-date")
            fieldText.tintColor = UIColor.clear
            break
            
        default:
            break
        }
        
        fieldText.tag = self.tag
    }
    
    //MARK: -
    func updateCell()
    {
        switch cellType {
            
        case .name:
            guard modelAddNewRecordProcess.doctorName != nil else
            {
                return
            }
            
            fieldText.text = modelAddNewRecordProcess.doctorName
            
        case .disease:
            guard modelAddNewRecordProcess.diseaseName != nil else
            {
                return
            }
            
            fieldText.text = modelAddNewRecordProcess.diseaseName
            
        case .hospital:
            guard modelAddNewRecordProcess.hospitalName != nil else
            {
                return
            }
            
            fieldText.text = modelAddNewRecordProcess.hospitalName
            
        case .date:
            guard modelAddNewRecordProcess.date != nil else
            {
                return
            }
            
            fieldText.text = modelAddNewRecordProcess.date
            
        default:
            break
        }
    }
    
    func bindThePickerview()
    {
        if cellType == .date, pickerBinder == nil
        {
            let pickerVw = UIDatePicker()
            
            pickerBinder = PickerViewBinder ()
            pickerBinder?.format = "dd-MMM-yyyy"
            
            pickerBinder?.handleDatePickerView(_pickerView: pickerVw, addDelegate: self)
            
            fieldText.inputView = pickerVw
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}

extension MyRecordAddTableViewCell: UITextFieldDelegate
{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        switch cellType {
            
        case .date:
            bindThePickerview()
            
        default:
            break
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if cellType == .date {
            return true
        }
        
        //var finalCount = 0
        var newString : String?
        
        if string.count == 0
        {
            if textField.text?.count==0
            {
                //finalCount = 0
                newString = textField.text
            }
            else
            {
                //finalCount = textField.text!.count - 1
                var newStr = textField.text! as NSString
                newStr = newStr.replacingCharacters(in: range, with: string) as NSString
                newString = newStr as String
            }
        }
        else
        {
            //finalCount = textField.text!.count + 1
            var newStr = textField.text! as NSString
            
            newStr = newStr.replacingCharacters(in: range, with: string) as NSString
            
            newString = newStr as String
        }
        
        guard let addRecordDelegate = delegate else {
            return true
        }
        
        addRecordDelegate.recordAddDataCell(self, updatedInputFieldText: newString!)
        
        return true
    }
}

extension MyRecordAddTableViewCell: DatePickerViewBinderDelegate
{
    func pickerBinderDatePicker(_ pickerBinder: PickerViewBinder, withUpdatedValue value: String) {
        
        fieldText.text = value
        
        guard let addRecordDelegate = delegate else {
            return
        }
        
        addRecordDelegate.recordAddDataCell(self, updatedInputFieldText: value)
    }
}
