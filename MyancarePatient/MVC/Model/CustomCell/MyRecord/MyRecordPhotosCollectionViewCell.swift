//
//  MyRecordPhotosCollectionViewCell.swift
//  MyancarePatient
//
//  Created by iOS on 12/02/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit

class MyRecordPhotosCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var checkButton: UIButton!
    @IBOutlet weak var recordImageView: UIImageView!
}
