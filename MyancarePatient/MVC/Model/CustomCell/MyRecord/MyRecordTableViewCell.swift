//
//  MyRecordTableViewCell.swift
//  MyancarePatient
//
//  Created by iOS on 09/02/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit

//MARK: -
protocol MyRecordTableViewCellDelegate: class
{
    func myRecordCellViewButtonAction(_ cell: MyRecordTableViewCell)
    func myRecordCellEditButtonAction(_ cell: MyRecordTableViewCell)
    func myRecordCellDeleteButtonAction(_ cell: MyRecordTableViewCell)
    func myRecordCellShareButtonAction(_ cell: MyRecordTableViewCell)
}

class MyRecordTableViewCell: UITableViewCell {

    weak var delegate: MyRecordTableViewCellDelegate?
    
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var viewButton: UIButton!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    
    @IBOutlet weak var doctorNameLabel: UILabel!
    @IBOutlet weak var diseaseNameLabel: UILabel!
    @IBOutlet weak var hospitalNameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        containerView.layer.cornerRadius = 5.0
        containerView.layer.borderWidth = 1.0
        containerView.layer.borderColor = UIColor.clear.cgColor
        containerView.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    //MARK:- on view button action
    @IBAction func viewRecordButtonClicked(_ sender: Any) {
        
        guard let myRecordDelegate = delegate else {
            return
        }
        
        myRecordDelegate.myRecordCellViewButtonAction(self)
    }
    
    //MARK:- on edit button action
    @IBAction func editRecordButtonClicked(_ sender: Any) {
        
        guard let myRecordDelegate = delegate else {
            return
        }
        
        myRecordDelegate.myRecordCellEditButtonAction(self)
    }
    
    //MARK:- on delete button action
    @IBAction func deleteRecordButtonClicked(_ sender: Any) {
        
        guard let myRecordDelegate = delegate else {
            return
        }
        
        myRecordDelegate.myRecordCellDeleteButtonAction(self)
    }
    
    //MARK:- on share button action
    @IBAction func shareRecordButtonClicked(_ sender: Any) {
        
        guard let myRecordDelegate = delegate else {
            return
        }
        
        myRecordDelegate.myRecordCellShareButtonAction(self)
    }
}
