//
//  ZoomCollectionViewCell.swift
//  MyancarePatient
//
//  Created by iOS on 06/03/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit

class ZoomCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageViewZommScrol: UIScrollView!
    @IBOutlet weak var zoomImageView: UIImageView!
}

extension ZoomCollectionViewCell : UIScrollViewDelegate
{
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        
        return zoomImageView
    }
}
