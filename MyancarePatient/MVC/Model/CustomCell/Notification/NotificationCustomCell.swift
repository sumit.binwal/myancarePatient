//
//  NotificationCustomCell.swift
//  MyancarePatient
//
//  Created by Jyoti on 08/01/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit
let cellIdentifier_NotificationCell = "cellNotificationTable"

class NotificationCustomCell: UITableViewCell {

    //Mark :- Properties
    @IBOutlet var imageViewProfile: UIImageView!
    @IBOutlet var labelNotificationText: UILabel!
    @IBOutlet var labelNotificationTime: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.disableSelection()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
