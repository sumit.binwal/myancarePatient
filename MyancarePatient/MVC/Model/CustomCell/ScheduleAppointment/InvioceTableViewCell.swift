//
//  InvioceTableViewCell.swift
//  MyancarePatient
//
//  Created by iOS on 06/02/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit

class InvioceTableViewCell: UITableViewCell {

    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
