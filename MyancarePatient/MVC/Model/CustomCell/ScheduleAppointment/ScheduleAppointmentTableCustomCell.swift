//
//  ScheduleAppointmentTableCustomCell.swift
//  CalendarViewDemo
//
//  Created by Jyoti on 16/01/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit

let cellIdentifier_ScheduleCell = "cellScheduleTable"
class ScheduleAppointmentTableCustomCell: UITableViewCell {

    @IBOutlet var labelTimeInterval: UILabel!
    @IBOutlet var imageViewSelection: UIImageView!
    @IBOutlet var tfbg: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        tfbg.frame.size.width = 335*scaleFactorY
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

