//
//  ServiceOfferCustomTableCell.swift
//  MyancarePatient
//
//  Created by Jyoti on 17/01/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import UIKit
let cellIdentifier_ServiceOfferCell = "cellServiceOfferTable"
class ServiceOfferCustomTableCell: UITableViewCell {

    //Mark :- Properties
    @IBOutlet var imageViewChatIcon: UIImageView!
    @IBOutlet var labelChatType: UILabel!
    @IBOutlet var tfServiceRate: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
