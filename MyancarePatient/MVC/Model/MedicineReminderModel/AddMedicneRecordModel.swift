//
//  AddMedicneRecordModel.swift
//  MyancarePatient
//
//  Created by iOS on 12/02/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import Foundation

class AddMedicneRecordModel
{
    var medicationID : String = ""
    var drugName : String = ""
    var medicationFor : String = ""
    var medicationType : String = ""
    var frequency : Int = 0
    var unit : String = ""
    var from : String = ""
    var to : String = ""
    var startTime : String = ""
    var repeatation : String = ""
    var from_date_long : Int = 0
    var to_date_long : Int = 0
    var start_time_long : Int = 0
    
    init()
    {
        
        
    }
}
