//
//  ModelDoctorFilter.swift
//  MyancarePatient
//
//  Created by iOS on 13/02/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import Foundation

class ModelDoctorFilter
{
    var state : String?
    var district : String?
    var town : String?
    var townID : String?
    var selectedCategoryId : String?
    
    var sr_pcode : String?
    var d_pcode : String?
    var town_pcode : String?
    
    var locationArry: NSMutableArray?
    
    var name : String?
    
    init() {
        
        state = ""
        district = ""
        townID = ""
        town = ""
        selectedCategoryId = ""
        
        sr_pcode = ""
        d_pcode = ""
        town_pcode = ""
        
        name = ""
        
        locationArry = []
    }
}
