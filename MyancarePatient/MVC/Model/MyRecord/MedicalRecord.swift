//
//  MedicalRecord.swift
//  MyancarePatient
//
//  Created by iOS on 09/02/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import Foundation

class MedicalRecord
{
    var recordID : String?
    var doctorName : String?
    var diseaseName : String?
    var hospitalName : String?
    var date : String?
    
    var recordFiles : [[String : Any]]?
    
    var user : [String : Any]?
    
    init()
    {
        recordID = ""
        doctorName = ""
        diseaseName = ""
        hospitalName = ""
        date = ""
        
        recordFiles = []
        
        user = [:]
    }
    
    deinit
    {
        print("MedicalRecord Model deinit")
    }
    
    func updateModel(usingDictionary dictionary:[String:Any]) -> Void
    {
        if let id = dictionary["id"] as? String
        {
            recordID = id
        }
        
        if let doctor_name1 = dictionary["doctor_name"] as? String
        {
            doctorName = doctor_name1
        }
        
        if let disease_name1 = dictionary["disease_name"] as? String
        {
            diseaseName = disease_name1
        }
        
        if let hospital_name1 = dictionary["hospital_name"] as? String
        {
            hospitalName = hospital_name1
        }
        
        if let recorded_date1 = dictionary["recorded_date"] as? String
        {
            date = recorded_date1
        }
        
        if let record_files1 = dictionary["record_files"] as? [[String : Any]]
        {
            recordFiles = record_files1
        }
        
        if let user1 = dictionary["user"] as? [String : Any]
        {
            user = user1
        }
    }
}
