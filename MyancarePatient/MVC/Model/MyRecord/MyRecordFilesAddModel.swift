//
//  MyRecordFilesAddModel.swift
//  MyancarePatient
//
//  Created by iOS on 05/03/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import Foundation

class MyRecordFilesAddModel
{
    var recordName : String = ""
    var recordID : String = ""
    var recordPathName : String = ""
    var recordImageUrl : String = ""
    var recordImageFromServer : Int = 0
    
    init()
    {
        
    }
    
    deinit
    {
        print("MyRecordFilesAdd Model deinit")
    }
}
