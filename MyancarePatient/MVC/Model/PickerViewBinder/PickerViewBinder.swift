//
//  PickerViewBinder.swift
//  MyanCareDoctor
//
//  Created by Santosh on 18/12/17.
//  Copyright © 2017 sumit. All rights reserved.
//

//MARK: -
enum pickerBinderType {
    case pickerView
    case datePicker
    case none
}

import Foundation
import UIKit

//MARK: -
//MARK: - PickerViewBinder (Binds picker view to view)
final class PickerViewBinder: NSObject, UIPickerViewDataSource, UIPickerViewDelegate
{
    private var pickerType = pickerBinderType.none
    private var picker: UIPickerView!
    private var datepicker: UIDatePicker!
    private weak var delegate: PickerViewBinderDelegate!
    private weak var datePickerdelegate: DatePickerViewBinderDelegate!
    private var keyValueForTitle: String = ""
    private var arrayValues = [String]()
    var format = "dd MMM,yyyy"
    
    //MARK: -
    override init() {
        
    }
    
    //MARK: - Handle PickerView
    func handlePickerView(_pickerView : UIPickerView, values:[String], addDelegate: PickerViewBinderDelegate)
    {
        picker = _pickerView
        arrayValues = values
        
        delegate = addDelegate
        
        picker.dataSource = self
        picker.delegate = self
        
        if arrayValues.count > 0 {
            
            guard let tempDelegate = delegate else {
                return
            }
            
            tempDelegate.pickerBinder(self, didSelectRow: 0, withUpdatedValue: arrayValues[0])
        }
    }
    
    func handlePickerViewWithDictArray(_pickerView : UIPickerView,values:[String], addDelegate: PickerViewBinderDelegate)
    {
        picker = _pickerView
        arrayValues = values
        
        delegate = addDelegate
        
        picker.dataSource = self
        picker.delegate = self
        
        if arrayValues.count > 0 {
            
            guard let tempDelegate = delegate else {
                return
            }
            
            tempDelegate.pickerBinder(self, didSelectRow: 0, withUpdatedValue: arrayValues[0])
        }
    }
    
    func handleDatePickerView(_pickerView : UIDatePicker, addDelegate: DatePickerViewBinderDelegate)
    {
        datepicker = _pickerView
        datepicker.timeZone = NSTimeZone.local
        datepicker.datePickerMode = .date
        datepicker.maximumDate = NSDate() as Date
        
        // Create date formatter
        let dateFormatter: DateFormatter = DateFormatter()
        
        dateFormatter.timeZone = NSTimeZone.system
        
        // Set date format
        dateFormatter.dateFormat = format
        
        datePickerdelegate = addDelegate
        
        // Apply date format
        let selectedDate: String = dateFormatter.string(from: Date())
        
        //print("Selected value \(selectedDate)")
        
        guard let tempDelegate = datePickerdelegate else {
            return
        }
        
        tempDelegate.pickerBinderDatePicker(self, withUpdatedValue: selectedDate)
        
        datepicker.addTarget(self, action: #selector(self.datePickerValueChanged(_:)), for: UIControlEvents.valueChanged)
    }
    
    func handleDatePickerViewMedicine(_pickerView : UIDatePicker, addDelegate: DatePickerViewBinderDelegate)
    {
        datepicker = _pickerView
        datepicker.timeZone = NSTimeZone.local
        datepicker.datePickerMode = .date
        //datepicker.maximumDate = NSDate() as Date
        
        let gregorian: NSCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
        let currentDate: Date = Date()
        let components: NSDateComponents = NSDateComponents()
        
        components.year = +1
        let maxDate: Date = gregorian.date(byAdding: components as DateComponents, to: currentDate, options: NSCalendar.Options(rawValue: 0))!
        
        datePickerdelegate = addDelegate
        
        self.datepicker.minimumDate = currentDate
        self.datepicker.maximumDate = maxDate
        
        // Create date formatter
        let dateFormatter: DateFormatter = DateFormatter()
        
        dateFormatter.timeZone = NSTimeZone.system
        
        // Set date format
        dateFormatter.dateFormat = format
        
        // Apply date format
        let selectedDate: String = dateFormatter.string(from: Date())
        
        //print("Selected value \(selectedDate)")
        
        guard let tempDelegate = datePickerdelegate else {
            return
        }
        
        tempDelegate.pickerBinderDatePicker(self, withUpdatedValue: selectedDate)
        
        datepicker.addTarget(self, action: #selector(self.datePickerValueChanged(_:)), for: UIControlEvents.valueChanged)
    }
    
    func handleTimePickerView(_pickerView : UIDatePicker, addDelegate: DatePickerViewBinderDelegate)
    {
        datepicker = _pickerView
        datepicker.timeZone = NSTimeZone.local
        datepicker.datePickerMode = .time
        //datepicker.maximumDate = NSDate() as Date
        
        // Create date formatter
        let dateFormatter: DateFormatter = DateFormatter()

        dateFormatter.timeZone = NSTimeZone.system
        
        // Set date format
        dateFormatter.dateFormat = format

        datePickerdelegate = addDelegate

        // Apply date format
        let selectedDate: String = dateFormatter.string(from: Date())

        //print("Selected value \(selectedDate)")

        guard let tempDelegate = datePickerdelegate else {
            return
        }

        datePickerdelegate = addDelegate

        tempDelegate.pickerBinderDatePicker(self, withUpdatedValue: selectedDate)
        
        datepicker.addTarget(self, action: #selector(self.datePickerValueChanged(_:)), for: UIControlEvents.valueChanged)
    }
    
    func handleTimePickerViewMedication(_pickerView : UIDatePicker, addDelegate: DatePickerViewBinderDelegate)
    {
        datepicker = _pickerView
        datepicker.timeZone = NSTimeZone.local
        datepicker.datePickerMode = .time
        
        // Create date formatter
        let dateFormatter: DateFormatter = DateFormatter()
        
        dateFormatter.timeZone = NSTimeZone.system
        
        // Set date format
        dateFormatter.dateFormat = format
        
        datePickerdelegate = addDelegate
        
        let selectedDate: String = dateFormatter.string(from: Date())
        
        if !modelAddMedicineProcess.from.isEmptyString() {
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd MMM, yyyy"
            
            dateFormatter.timeZone = NSTimeZone.system
            
            let fromDate = dateFormatter.date(from: modelAddMedicineProcess.from)
            print("fromDate = \(String(describing: fromDate))")
            
            let cal = Calendar (identifier: .gregorian)
            
            let comparisonResult = cal.compare(fromDate!, to: Date(), toGranularity: .day)
            var datesame = 0
            
            switch comparisonResult {
            case .orderedAscending:
                print("orderedAscending")
                datesame = 0
            case .orderedSame:
                print("orderedSame")
                datesame = 1
            case .orderedDescending:
                print("orderedDescending")
                datesame = 0
            }
            
            if datesame == 1 {
                datepicker.minimumDate = Date()
            }
        }
        
        guard let tempDelegate = datePickerdelegate else {
            return
        }
        
        datePickerdelegate = addDelegate
        
        tempDelegate.pickerBinderDatePicker(self, withUpdatedValue: selectedDate)
        
        datepicker.addTarget(self, action: #selector(self.datePickerValueChanged(_:)), for: UIControlEvents.valueChanged)
    }
    
    @objc func datePickerValueChanged(_ sender: UIDatePicker){
        
        // Create date formatter
        let dateFormatter: DateFormatter = DateFormatter()
        
        dateFormatter.timeZone = NSTimeZone.system
        
        // Set date format
        dateFormatter.dateFormat = format
        
        // Apply date format
        let selectedDate: String = dateFormatter.string(from: sender.date)
        
        //print("Selected value \(selectedDate)")
        
        guard let tempDelegate = datePickerdelegate else {
            return
        }
        
        tempDelegate.pickerBinderDatePicker(self, withUpdatedValue: selectedDate)
    }
    
    //MARK: -
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrayValues.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrayValues[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if arrayValues.count > 0 {
            
            guard let tempDelegate = delegate else {
                return
            }
            
            tempDelegate.pickerBinder(self, didSelectRow: row, withUpdatedValue: arrayValues[row])
        }
    }
}

//MARK: -
//MARK: - Protocol -> PickerViewBinderDelegate
protocol PickerViewBinderDelegate: class
{
    //MARK: -
    func pickerBinder(_ pickerBinder: PickerViewBinder, didSelectRow row: Int, withUpdatedValue value: String)
    
    //MARK: -
    func pickerBinderDatePicker(_ pickerBinder: PickerViewBinder, withUpdatedValue value: String)
}

protocol DatePickerViewBinderDelegate: class
{
    //MARK: -
    func pickerBinderDatePicker(_ pickerBinder: PickerViewBinder, withUpdatedValue value: String)
}
