//
//  WorkingAddressModel.swift
//  MyanCareDoctor
//
//  Created by iOS on 07/03/18.
//  Copyright © 2018 sumit. All rights reserved.
//

import Foundation

class WorkingAddressModel
{
    var id : String?
    var name: String?
    
    init() {
        id = ""
        name = ""
    }
    
    deinit {
        print("WorkingAddress Model deinit")
    }
    
    func updateModel(usingDictionary dictionary:[String:Any]) -> Void
    {
        if let cateID = dictionary["id"] as? String
        {
            id = cateID
        }
        
        if let name1 = dictionary["address"] as? String
        {
            name = name1
        }
    }
}
