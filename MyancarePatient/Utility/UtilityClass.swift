//
//  UtilityClass.swift
//  MyanCareDoctor
//
//  Created by Santosh on 19/12/17.
//  Copyright © 2017 sumit. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import SafariServices

private enum UserDefaultEnum : String
{
    case userInfoData = "userInfoData"
    case notificationStatus = "notificationStatus"
    case fcmDeviceToken = "FCM_Token"
    case chatUnReadCount = "chat_un_read_count"
    case apnsDeviceToken = "APNS_Token"
}

class UtilityClass: NSObject
{
    //MARK:- Disable IQKeyboard Manager
    class func disableIQKeyboard()
    {
        IQKeyboardManager.sharedManager().enable = false
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
    }
    
    //MARK:- Enable IQKeyboard Manager
    class func enableIQKeyboard()
    {
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().enableAutoToolbar = true
    }
    
    //MARK:- Extract Number
    class func extractNumber(fromString string:String) -> String
    {
        let characterSet = CharacterSet.decimalDigits.inverted
        let stringArray = string.components(separatedBy: characterSet)
        let newString = stringArray.joined(separator: "")
        return newString
    }
    
    //MARK:- Alert View
    class func showAlertWithTitle(title:String? = "MyanCare".localized(), message:String? = "app_global_message".localized(), onViewController:UIViewController?, withButtonArray buttonArray:[String]? = [], dismissHandler:((_ buttonIndex:Int)->())?) -> Void
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        var ignoreButtonArray = false
        
        if buttonArray == nil
        {
            ignoreButtonArray = true
        }
        
        if !ignoreButtonArray
        {
            for item in buttonArray!
            {
                let action = UIAlertAction(title: item, style: .default, handler: { (action) in
                    
                    alertController.dismiss(animated: true, completion: nil)
                    
                    guard (dismissHandler != nil) else
                    {
                        return
                    }
                    
                    dismissHandler!(buttonArray!.index(of: item)!)
                })
                
                alertController.addAction(action)
            }
        }
        
        let action = UIAlertAction(title: "Dismiss".localized(), style: .cancel, handler: { (action) in
            
            guard (dismissHandler != nil) else
            {
                return
            }
            
            dismissHandler!(LONG_MAX)
        })
        
        alertController.addAction(action)
        
        onViewController?.present(alertController, animated: true, completion: nil)
    }
    
    //MARK:- Get User Age From DOB
    
    class func getPersonYearOld (date : String) -> Int
    {
        //26/01/2018
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM,yyyy" //Your date format
        dateFormatter.timeZone = NSTimeZone.system //Current time zone
        let date1 = dateFormatter.date(from: date) //according to date format your date string
        
        let calendar = Calendar.current
        
        let components : DateComponents = (calendar as NSCalendar).components([NSCalendar.Unit.year], from: date1!, to: Date(), options: NSCalendar.Options())
        
        return components.year!
    }
    
    class func setPasscode (passcodString : String)
    {
        UserDefaults.standard.set(passcodString, forKey: "passcode_string")
        UserDefaults.standard.synchronize()
    }
    
    class func getPasscode () -> String
    {
        if UserDefaults.standard.object(forKey: "passcode_string") != nil
        {
            return UserDefaults.standard.object(forKey: "passcode_string") as! String
        }
        else
        {
            return ""
        }
    }
    
    class func setPasscodeOnOff (passcodeOnOff : Bool)
    {
        UserDefaults.standard.set(passcodeOnOff, forKey: "passcode_on_off")
        UserDefaults.standard.synchronize()
    }
    
    class func getPasscodeOnOff () -> Bool
    {
        if UserDefaults.standard.object(forKey: "passcode_on_off") != nil
        {
            return UserDefaults.standard.object(forKey: "passcode_on_off") as! Bool
        }
        else
        {
            return false
        }
    }
    
    class func enableTouchIDOrNot (enable : Bool)
    {
        UserDefaults.standard.set(enable, forKey: "set_touchID")
        UserDefaults.standard.synchronize()
    }
    
    class func getEnableTouchIDOrNot () -> Bool
    {
        if UserDefaults.standard.object(forKey: "set_touchID") != nil
        {
            return UserDefaults.standard.object(forKey: "set_touchID") as! Bool
        }
        else
        {
            return false
        }
    }
    
    class func getDateFromString (date : String, formate : String) -> Date
    {
        //26/01/2018
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formate //Your date format
        dateFormatter.timeZone = NSTimeZone.system
        let date1 = dateFormatter.date(from: date) //according to date format your date string
        
        return date1!
    }
    
    //MARK: - Take a Screenshot
    class func screenShotMethod() -> UIImage
    {
        let layer = UIApplication.shared.keyWindow!.layer
        let scale = UIScreen.main.scale
        
        UIGraphicsBeginImageContextWithOptions(layer.frame.size, false, scale);
        
        layer.render(in: UIGraphicsGetCurrentContext()!)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        return image!
    }
    
    class func getStringFromDate (date : Date, formate : String) -> String
    {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        formatter.timeZone = NSTimeZone.system
        
        let myString = formatter.string(from: date)
        let yourDate: Date? = formatter.date(from: myString)
        formatter.dateFormat = "MM-dd-yyyy"
        let updatedString = formatter.string(from: yourDate!)
        
        return updatedString
    }
    
    class func getStringFromDateFormat (date : Date, formate : String) -> String
    {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        formatter.timeZone = NSTimeZone.system
        
        let myString = formatter.string(from: date)
        let yourDate: Date? = formatter.date(from: myString)
        formatter.dateFormat = formate
        let updatedString = formatter.string(from: yourDate!)
        
        return updatedString
    }
    
    class func getStringFromDateFormat1 (date : Date, formate : String) -> String
    {
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        
        formatter.timeZone = NSTimeZone.system
        
        let myString = formatter.string(from: date)
        let yourDate: Date? = formatter.date(from: myString)
        formatter.dateFormat = formate
        let updatedString = formatter.string(from: yourDate!)
        
        return updatedString
    }
    
    //MARK:- Get State Listing
    class func getStateListing() -> NSMutableArray
    {
        let stateArr: NSMutableArray  = []
        
        for dict in locationGlobleArr
        {
            let dict1 = dict
            
            let mainstateName:String = dict1["state_region"] as! String
            
            if !stateArr.contains(mainstateName)
            {
                stateArr.add(mainstateName)
            }
        }
        
        return stateArr
    }
    
    //MARK:- Get District Listing
    class func getDistrictListing(_stateVal: String) -> NSMutableArray
    {
        let stateArr: NSMutableArray  = []
        
        for dict in locationGlobleArr
        {
            let dict1 = dict
            
            let mainstateName:String = dict1["state_region"] as! String
            
            let districtName:String = dict1["district"] as! String
            
            if mainstateName == _stateVal
            {
                if !stateArr.contains(districtName)
                {
                    stateArr.add(districtName)
                }
            }
        }
        
        return stateArr
    }
    
    //MARK:- Get Town Listing
    class func getTownArray(_districtVal: String) -> NSMutableArray
    {
        let stateArr: NSMutableArray  = []
        
        for dict in locationGlobleArr
        {
            let dict1 = dict
            
            let mainstateName:String = dict1["district"] as! String
            
            let districtName:String = dict1["town"] as! String
            
            if mainstateName == _districtVal
            {
                if !stateArr.contains(districtName)
                {
                    stateArr.add(districtName)
                }
            }
        }
        
        return stateArr
    }
    
    //MARK:- Get Town ID From Listing
    class func getTownID(_ townVal: String) -> String
    {
        var townID: String = ""
        
        for dict in locationGlobleArr
        {
            let dict1 = dict
            
            let mainstateName:String = dict1["town"] as! String
            
            if mainstateName == townVal
            {
                townID = dict1["id"] as! String
            }
        }
        
        return townID
    }
    
    //MARK:- Get state p code From Listing
    class func getStatePCode(_ townVal: String) -> String
    {
        var townID: String = ""
        
        for dict in locationGlobleArr
        {
            let dict1 = dict
            
            let mainstateName:String = dict1["state_region"] as! String
            
            if mainstateName == townVal
            {
                townID = dict1["sr_pcode"] as! String
            }
        }
        
        return townID
    }
    
    //MARK:- Get district p code From Listing
    class func getDistrictPCode(_ townVal: String) -> String
    {
        var townID: String = ""
        
        for dict in locationGlobleArr
        {
            let dict1 = dict
            
            let mainstateName:String = dict1["district"] as! String
            
            if mainstateName == townVal
            {
                townID = dict1["d_pcode"] as! String
            }
        }
        
        return townID
    }
    
    //MARK:- Get town p code From Listing
    class func getTownPCode(_ townVal: String) -> String
    {
        var townID: String = ""
        
        for dict in locationGlobleArr
        {
            let dict1 = dict
            
            let mainstateName:String = dict1["town"] as! String
            
            if mainstateName == townVal
            {
                townID = dict1["town_pcode"] as! String
            }
        }
        
        return townID
    }
    
    //MARK:- Get Town ID From Listing
    class func getLatitudeOfTown(_ townVal: String) -> Double
    {
        var townID: Double = 0
        
        for dict in locationGlobleArr
        {
            let dict1 = dict
            
            let mainstateName:String = dict1["town"] as! String
            
            if mainstateName == townVal
            {
                townID = dict1["latitude"] as! Double
            }
        }
        
        return townID
    }
    
    //MARK:- Get Town ID From Listing
    class func getLongitudeOfTown(_ townVal: String) -> Double
    {
        var townID: Double = 0
        
        for dict in locationGlobleArr
        {
            let dict1 = dict
            
            let mainstateName:String = dict1["town"] as! String
            
            if mainstateName == townVal
            {
                townID = dict1["longitude"] as! Double
            }
        }
        
        return townID
    }
    
    //MARK:- User Info ( SAVE )
    class func saveUserInfoData(userDict : [String:Any]) -> Void
    {
        let data = NSKeyedArchiver.archivedData(withRootObject: userDict)
        userDefaults.set(data, forKey: UserDefaultEnum.userInfoData.rawValue)
    }
    
    //MARK:- User Info ( GET )
    class func getUserInfoData() -> [String:Any]
    {
        let data : Data = userDefaults.object(forKey: UserDefaultEnum.userInfoData.rawValue) as! Data
        let userDict : [String:Any] = NSKeyedUnarchiver.unarchiveObject(with: data) as! [String : Any]
        
        return userDict
    }
    
    //MARK:- User Name ( GET )
    class func getPersonName() -> String
    {
        let data : Data = userDefaults.object(forKey: UserDefaultEnum.userInfoData.rawValue) as! Data
        let userDict : [String:Any] = NSKeyedUnarchiver.unarchiveObject(with: data) as! [String : Any]
        
        let name = userDict["name"] as! String
        
        return name
    }
    
    //MARK:- User Nick Name ( GET )
    class func getPersonNickName() -> String
    {
        let data : Data = userDefaults.object(forKey: UserDefaultEnum.userInfoData.rawValue) as! Data
        let userDict : [String:Any] = NSKeyedUnarchiver.unarchiveObject(with: data) as! [String : Any]
        
        let name = userDict["nick_name"] as! String
        
        return name
    }
    
    //MARK:-
    //MARK:- Open Safari ViewController as A WebView
    class func openSafariController(usingLink openLink:LinksEnum, onViewController: UIViewController?) -> Void
    {
        guard let visibleController = onViewController else
        {
            return
        }
        
        let safariController : SFSafariViewController = SFSafariViewController(url: URL(string: openLink.rawValue)!)
        
        safariController.delegate = visibleController as? SFSafariViewControllerDelegate
 
        visibleController.present(safariController, animated: true, completion: nil)
    }
    
    //MARK:- Open Safari browser
    class func openSafariBrowser (usingLink urlLink : LinksEnum) -> Void
    {
        let openUrl = URL(string: urlLink.rawValue)
        
        let application:UIApplication = UIApplication.shared
        
        if (application.canOpenURL(openUrl!))
        {
            if #available(iOS 10.0, *)
            {
                application.open(openUrl!, options: [:], completionHandler: nil)
            }
            else
            {
                // Fallback on earlier versions
                print("other versions")
                application.openURL(openUrl!)
            }
        }
        else
        {
            print("can not open url")
        }
    }
    
    //MARK:- get timeStamp from current data
    class func getCurrentTimeStamp () -> String
    {
        // Get the Unix timestamp
        let timestamp = Date().timeIntervalSince1970
        //print(timestamp)
        
        return String(Int(timestamp))
    }
    
    //MARK:- Change RootViewController
    class func changeRootViewController(with newRootViewController : UIViewController) -> Void
    {
        if let navigationCont = appDelegate?.window??.rootViewController as? UINavigationController
        {
            navigationCont.popToRootViewController(animated: false)
        }
        
        appDelegate?.window??.rootViewController = newRootViewController
    }
    
    //MARK: User Sid ( GET )
    class func getUserSidData() -> String?
    {
        guard let data = userDefaults.object(forKey: UserDefaultEnum.userInfoData.rawValue) as? Data else
        {
            return nil
        }
     
        let userDict : [String:Any] = NSKeyedUnarchiver.unarchiveObject(with: data) as! [String : Any]
        
        var strSid = userDict["jwtToken"] as? String
        
        guard strSid != nil else
        {
            strSid = ""
        
            return strSid
        }
        
        return strSid
    }
    
    //MARK: - set Status bar color change
    class func changeStatusBarColor ()
    {
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
    
        if statusBar.responds(to:#selector(setter: UIView.backgroundColor))
        {
            statusBar.backgroundColor = UIColor.MyanCarePatient.appDefaultGreenColor
        }
        
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    class func setStatusBarBGColor() -> Void
    {
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        
        if statusBar.responds(to:#selector(setter: UIView.backgroundColor))
        {
            statusBar.backgroundColor = UIColor.MyanCarePatient.appDefaultGreenColor
        }
        
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    //MARK: User ID ( GET )
    class func getUserIdData() -> String?
    {
        guard let data = userDefaults.object(forKey: UserDefaultEnum.userInfoData.rawValue) as? Data else
        {
            return ""
        }
        
        let userDict : [String:Any] = NSKeyedUnarchiver.unarchiveObject(with: data) as! [String : Any]
        let strSid = userDict["id"] as! String
        
        return strSid
    }
    
    //MARK: User Image ( GET )
    class func getUserImageData() -> String?
    {
        guard let data = userDefaults.object(forKey: UserDefaultEnum.userInfoData.rawValue) as? Data else
        {
            return ""
        }
        
        let userDict : [String:Any] = NSKeyedUnarchiver.unarchiveObject(with: data) as! [String : Any]
        let strSid = userDict["avatar_url"] as! String
        
        return strSid
    }
    
    class func getDateStringFromTimeStamp(timeStamp : String, dateFormat: String) -> NSString
    {
        var time = timeStamp
        
        if (time.count) > 10
        {
            let index2 = (time.index((time.startIndex), offsetBy: 10))
            let indexStart = index2
            
            let indexEnd = (time.endIndex)
            
            time.removeSubrange(indexStart ..< indexEnd)
        }
        
        let date = Date(timeIntervalSince1970: Double(time)! )
        
        let dateFormatter = DateFormatter()
        //dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want
        
        dateFormatter.timeZone = NSTimeZone.system
        
        dateFormatter.locale = NSLocale.current
        
        //12-Sep-2017 | 10:00 AM
        dateFormatter.dateFormat = dateFormat //Specify your format that you want
        let strDate = dateFormatter.string(from: date)
        
        return strDate as NSString
    }
    
    class func getDateStringFromTimeStamp(timeStamp : String) -> NSString
    {
        var time = timeStamp

        if (time.count) > 10
        {
            let index2 = (time.index((time.startIndex), offsetBy: 10))
            let indexStart = index2
            
            let indexEnd = (time.endIndex)
            
            time.removeSubrange(indexStart ..< indexEnd)
        }
        
        let date = Date(timeIntervalSince1970: Double(time)! )
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.timeZone = NSTimeZone.system
        
        dateFormatter.locale = NSLocale.current
        
        //12-Sep-2017 | 10:00 AM
        dateFormatter.dateFormat = "dd-MMM-yyyy | hh:mm a" //Specify your format that you want
        let strDate = dateFormatter.string(from: date)
        
        return strDate as NSString
    }
    
    class func getDateStringFromTimeStampDetail(timeStamp : String) -> NSString
    {
        var time = timeStamp
        
        if (time.count) > 10
        {
            let index2 = (time.index((time.startIndex), offsetBy: 10))
            let indexStart = index2
            
            let indexEnd = (time.endIndex)
            
            time.removeSubrange(indexStart ..< indexEnd)
        }
        
        let date = Date(timeIntervalSince1970: Double(time)! )
        let dateFormatter = DateFormatter()
        //dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        
        dateFormatter.timeZone = NSTimeZone.system
        
        //12-Sep-2017 | 10:00 AM
        dateFormatter.dateFormat = "dd MMM yyyy | hh:mm a" //Specify your format that you want
        let strDate = dateFormatter.string(from: date)
        
        return strDate as NSString
    }
    
    class func getDateStringFromTimeStamp1(timeStamp : String) -> NSString
    {
        var time = timeStamp
        
        if (time.count) > 10 {
            
            let index2 = (time.index((time.startIndex), offsetBy: 10))
            let indexStart = index2
            
            let indexEnd = (time.endIndex)
            
            time.removeSubrange(indexStart ..< indexEnd)
        }
        
        let date = Date(timeIntervalSince1970: Double(time)! )
        let dateFormatter = DateFormatter()
        //    dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        
        dateFormatter.timeZone = NSTimeZone.system
        
        //12-Sep-2017 | 10:00 AM
        dateFormatter.dateFormat = "dd-MMM-yyyy" //Specify your format that you want
        let strDate = dateFormatter.string(from: date)
        
        return strDate as NSString
    }
    
    class func getDateStringFromTimeStamp2(timeStamp : String) -> NSString
    {
        var time = timeStamp
        
        if (time.count) > 10 {
            
            let index2 = (time.index((time.startIndex), offsetBy: 10))
            let indexStart = index2
            
            let indexEnd = (time.endIndex)
            
            time.removeSubrange(indexStart ..< indexEnd)
        }
        
        let date = Date(timeIntervalSince1970: Double(time)!)
        let dateFormatter = DateFormatter()
        //    dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        
        dateFormatter.timeZone = NSTimeZone.system
        
        //12-Sep-2017 | 10:00 AM
        dateFormatter.dateFormat = "dd MMM, yyyy" //Specify your format that you want
        let strDate = dateFormatter.string(from: date)
        
        return strDate as NSString
    }
    
    class func getDateStringFromTimeStamp3(timeStamp : String) -> NSString
    {
        var time = timeStamp
        
        if (time.count) > 10
        {
            let index2 = (time.index((time.startIndex), offsetBy: 10))
            let indexStart = index2
            
            let indexEnd = (time.endIndex)
            
            time.removeSubrange(indexStart ..< indexEnd)
        }
        
        let date = Date(timeIntervalSince1970: Double(time)! )
        let dateFormatter = DateFormatter()
        //    dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        
        dateFormatter.timeZone = NSTimeZone.system
        
        //12-Sep-2017 | 10:00 AM
        dateFormatter.dateFormat = "dd-MM-yyyy hh:mm a" //Specify your format that you want
        let strDate = dateFormatter.string(from: date)
        
        return strDate as NSString
    }
    
    //MARK:- Action Sheet
    static func showActionSheetWithTitle(title:String? = App_Name, message:String? = App_Global_Msg, onViewController:UIViewController?, withButtonArray buttonArray:[String]? = [], dismissHandler:((_ buttonIndex:Int)->())?) -> Void
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        
        var ignoreButtonArray = false
        
        if buttonArray == nil
        {
            ignoreButtonArray = true
        }
        
        if !ignoreButtonArray
        {
            for item in buttonArray!
            {
                let action = UIAlertAction(title: item, style: .default, handler: { (action) in
                    
                    alertController.dismiss(animated: true, completion: nil)
                    
                    guard (dismissHandler != nil) else
                    {
                        return
                    }
                    
                    dismissHandler!(buttonArray!.index(of: item)!)
                })
  
                alertController.addAction(action)
            }
        }
        
        let action = UIAlertAction(title: "Cancel".localized(), style: .cancel, handler: nil)
        alertController.addAction(action)
        
        onViewController?.present(alertController, animated: true, completion: nil)
    }
    
    //MARK:- get timeStamp from date
    class func getTimeStampFromDate (dateString : String) -> Int
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM,yyyy"
        
        dateFormatter.timeZone = NSTimeZone.system
        
        let date = dateFormatter.date(from: dateString)
        
        // Get the Unix timestamp
        let timestamp = date?.timeIntervalSince1970
        //print(timestamp ?? 0)
        
        return Int(timestamp!*1000)
    }
    
    //MARK:- get timeStamp from time
    class func getTimeStampFromTime (dateString : String) -> Int
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM, yyyy hh:mm a"
        
        dateFormatter.timeZone = NSTimeZone.system
        
        let date = dateFormatter.date(from: dateString)
        
        // Get the Unix timestamp
        let timestamp = date?.timeIntervalSince1970
        //print(timestamp ?? 0)
        
        return Int(timestamp!*1000)
    }
    
    //MARK: User Info ( DELETE )
    class func deleteUserData() -> Void
    {
        guard userDefaults.object(forKey: UserDefaultEnum.userInfoData.rawValue) != nil else
        {
            return
        }
        
        userDefaults.removeObject(forKey: UserDefaultEnum.userInfoData.rawValue)
        
        modelChangePasswordProcess = ChangePasswordModel.init()
        modelEditProfileProcess = EditProfileModel.init()
        modelSignUpProcess = ModelLogin.init()
        modelAppointmentData = ModelAppointment.init()
        modelAddMedicineProcess = AddMedicneRecordModel.init()
        modelAddNewRecordProcess = AddNewRecordModel.init()
        modelDoctorFilterProcess = ModelDoctorFilter.init()
    }
    
    //MARK:- Get Comma Seprated String From Array
    
    class func getCommaSepratedStringFromArrayDict(completeArray:[[String:Any]], withKeyName key:String) -> String
    {
        var nameArr: [String] = []
        
        for name in completeArray
        {
            let nameStr = name[key] as! String
            nameArr.append(nameStr)
        }
       
        let commaSeparatedNameString = nameArr.joined(separator: ", ")
        
        return commaSeparatedNameString
    }
    
    class func getCommaSepratedStringFromArray(completeArray arr : NSMutableArray) -> String
    {
        var nameArr: [String] = []
        
        for i in 0 ..< arr.count
        {
            let nameStr = arr.object(at: i) as! String
            nameArr.append(nameStr)
        }
        
        let commaSeparatedNameString = nameArr.joined(separator: ", ")
        
        return commaSeparatedNameString
    }
    
    class func getCommaSepratedStringFromArrayDoctorFilter(completeArray arr : NSMutableArray) -> String
    {
        var nameArr: [String] = []
        
        for i in 0 ..< arr.count
        {
            let nameStr = arr.object(at: i) as! String
            nameArr.append(nameStr)
        }
        
        let commaSeparatedNameString = nameArr.joined(separator: ",")
        
        return commaSeparatedNameString
    }
    
    class func convertDateFormater(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/mm/yyyy"
        
        dateFormatter.timeZone = NSTimeZone.system
        
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "mm/dd/yyyy"
        
        return  dateFormatter.string(from: date!)
    }
    
    class func getTimeFromDateString(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy hh:mm a"
        
        dateFormatter.timeZone = NSTimeZone.system
        
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "hh:mm a"
        
        return  dateFormatter.string(from: date!)
    }
    
    class func getDateFromDateString(_ date: String) -> Date
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy hh:mm a"
        
        dateFormatter.timeZone = NSTimeZone.system
        
        let date = dateFormatter.date(from: date)
        
        return date!
    }
    
    //MARK:- set wallet balave into User Info ( SET )
    class func setWalletBalanceInUserInfo(walletBalance : Double)
    {
        let data : Data = userDefaults.object(forKey: UserDefaultEnum.userInfoData.rawValue) as! Data
        var userDict : [String:Any] = NSKeyedUnarchiver.unarchiveObject(with: data) as! [String : Any]
        
        userDict["wallet_balance"] = walletBalance
        
        let data1 = NSKeyedArchiver.archivedData(withRootObject: userDict)
        userDefaults.set(data1, forKey: UserDefaultEnum.userInfoData.rawValue)
    }
    
    class func timeAgoSinceDate(_ date:Date, currentDate:Date, numericDates:Bool) -> String
    {
        let calendar = Calendar.current
        let now = currentDate
        let earliest = (now as NSDate).earlierDate(date)
        let latest = (earliest == now) ? date : now
        let components:DateComponents = (calendar as NSCalendar).components([NSCalendar.Unit.minute , NSCalendar.Unit.hour , NSCalendar.Unit.day , NSCalendar.Unit.weekOfYear , NSCalendar.Unit.month , NSCalendar.Unit.year , NSCalendar.Unit.second], from: earliest, to: latest, options: NSCalendar.Options())
        
        if (components.year! >= 2)
        {
            return "\(components.year!) " + "years ago".localized()
        }
        else if (components.year! >= 1)
        {
            if (numericDates)
            {
                return "1 " + "years ago".localized()
            }
            else
            {
                return "Last year".localized()
            }
        }
        else if (components.month! >= 2)
        {
            return "\(components.month!) " + "months ago".localized()
        }
        else if (components.month! >= 1)
        {
            if (numericDates)
            {
                return "1 month ago".localized()
            }
            else
            {
                return "Last month".localized()
            }
        }
        else if (components.weekOfYear! >= 2)
        {
            return "\(components.weekOfYear!) " + "weeks ago".localized()
        }
        else if (components.weekOfYear! >= 1)
        {
            if (numericDates)
            {
                return "1 " + "weeks ago".localized()
            }
            else
            {
                return "Last week".localized()
            }
        }
        else if (components.day! >= 2)
        {
            return "\(components.day!) " + "days ago".localized()
        }
        else if (components.day! >= 1)
        {
            if (numericDates)
            {
                return "1 " + "days ago".localized()
            }
            else
            {
                return "Yesterday".localized()
            }
        }
        else if (components.hour! >= 2)
        {
            return "\(components.hour!) " + "hours ago".localized()
        }
        else if (components.hour! >= 1)
        {
            if (numericDates)
            {
                let str = "1 " + "hours ago".localized()
                return str
            }
            else
            {
                let str = "An hour ago".localized()
                return str
            }
        }
        else if (components.minute! >= 2)
        {
            let str = "\(components.minute!) " + "minutes ago".localized()
            return str
        }
        else if (components.minute! >= 1)
        {
            if (numericDates)
            {
                return "1 " + "minutes ago".localized()
            }
            else
            {
                return "A minute ago".localized()
            }
        }
        else if (components.second! >= 3)
        {
            return "\(components.second!)" + " " + "seconds ago".localized()
        }
        else
        {
            return "Just now".localized()
        }
    }
    
    class func timeLeftFromNow(_ date : Date, currentDate : Date, numericDates : Bool) -> String
    {
        let calendar = Calendar.current
        
        let now = currentDate
        
        let components : DateComponents = (calendar as NSCalendar).components([NSCalendar.Unit.minute , NSCalendar.Unit.hour , NSCalendar.Unit.day , NSCalendar.Unit.weekOfYear , NSCalendar.Unit.month , NSCalendar.Unit.year , NSCalendar.Unit.second], from: now, to: date, options: NSCalendar.Options())
        
        if (components.weekOfYear! > 2)
        {
            return "\(components.weekOfYear!) " + "weeks".localized() + " " + "left".localized()
        }
        else if (components.day! >= 1 && components.hour! >= 1)
        {
            return "\(components.day!) " + "days".localized() + " \(components.hour!) " + "hr".localized() + " " + "left".localized()
        }
        else if (components.hour! >= 2 && components.minute! >= 2)
        {
            return "\(components.hour!) " + "hr".localized() + " \(components.minute!) " + "min".localized() + " " + "left".localized()
        }
        else if (components.hour! >= 1 && components.minute! >= 2)
        {
            return "1 " + "hr".localized() + " \(components.minute!) " + "min".localized() + " " + "left".localized()
        }
        else if (components.minute! >= 2)
        {
            return "\(components.minute!) " + "min".localized() + " " + "left".localized()
        }
        else if (components.minute! >= 1)
        {
            return "1 min left".localized()
        }
        else if (components.second! >= 3)
        {
            return "\(components.second!) " + "sec left".localized()
        }
        else
        {
            return "Just now".localized()
        }
    }
    
    class func timeAgoSinceDateChat(_ date:Date, currentDate:Date, numericDates:Bool) -> String
    {
        let calendar = Calendar.current
        let now = currentDate
        let earliest = (now as NSDate).earlierDate(date)
        let latest = (earliest == now) ? date : now
        let components:DateComponents = (calendar as NSCalendar).components([NSCalendar.Unit.minute , NSCalendar.Unit.hour , NSCalendar.Unit.day , NSCalendar.Unit.weekOfYear , NSCalendar.Unit.month , NSCalendar.Unit.year , NSCalendar.Unit.second], from: earliest, to: latest, options: NSCalendar.Options())
        
        if (components.year! >= 2)
        {
            return "\(components.year!) " + "years ago".localized()
        }
        else if (components.year! >= 1)
        {
            if (numericDates)
            {
                return "1 " + "years ago".localized()
            }
            else
            {
                return "Last year".localized()
            }
        }
        else if (components.month! >= 2)
        {
            return "\(components.month!) " + "months ago".localized()
        }
        else if (components.month! >= 1)
        {
            if (numericDates)
            {
                return "1 month ago".localized()
            }
            else
            {
                return "Last month".localized()
            }
        }
        else if (components.weekOfYear! >= 2)
        {
            return "\(components.weekOfYear!) " + "weeks ago".localized()
        }
        else if (components.weekOfYear! >= 1)
        {
            if (numericDates)
            {
                return "1 " + "weeks ago".localized()
            }
            else
            {
                return "Last week".localized()
            }
        }
        else if (components.day! >= 2)
        {
            return "\(components.day!) " + "days ago".localized()
        }
        else if (components.day! >= 1)
        {
            if (numericDates)
            {
                return "1 " + "days ago".localized()
            }
            else
            {
                return "Yesterday".localized()
            }
        }
        else if (components.hour! >= 2)
        {
            return "\(components.hour!) " + "hours ago".localized()
        }
        else if (components.hour! >= 1)
        {
            if (numericDates)
            {
                let str = "1 " + "hours ago".localized()
                return str
            }
            else
            {
                let str = "An hour ago".localized()
                return str
            }
        }
        else if (components.minute! >= 2)
        {
            let str = "\(components.minute!) " + "minutes ago".localized()
            return str
        }
        else if (components.minute! >= 1)
        {
            if (numericDates)
            {
                return "1 " + "minutes ago".localized()
            }
            else
            {
                return "A minute ago".localized()
            }
        }
        else if (components.second! >= 3)
        {
            return "\(components.second!)" + " " + "seconds ago".localized()
        }
        else
        {
            return "Just now".localized()
        }
    }
    
    class func getDisplayDayFromTime (timeString : String) -> String
    {
        if timeString == ""
        {
            return ""
        }
        else
        {
            if timeString.contains("AM".localized()) || timeString.contains("am".localized())
            {
                return "Morning".localized()
            }
            else
            {
                var timeString1 = timeString
            
                timeString1 = timeString1.replacingOccurrences(of: " ", with: "")
            
                timeString1 = timeString1.replacingOccurrences(of: "PM".localized(), with: "")
            
                timeString1 = timeString1.replacingOccurrences(of: "pm".localized(), with: "")
            
                timeString1 = timeString1.replacingOccurrences(of: ":", with: ".")
            
                if Double(timeString1)! <= 4.0
                {
                    return "Afternoon".localized()
                }
                else if Double(timeString1)! > 4.0 && Double(timeString1)! <= 8.0
                {
                    return "Evening".localized()
                }
                else
                {
                    return "Night".localized()
                }
            }
        }
    }
    
    class func getDocumentsDirectoryPath() -> URL
    {
        return try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
    }
    
    class func getDocumentsFolder(withName folderName: String) -> URL?
    {
        let documentsPath = UtilityClass.getDocumentsDirectoryPath()
        print(documentsPath)
        
        let newDirectoryPath = documentsPath.appendingPathComponent(folderName) //documentsPath.appending("/"+folderName)
        
        let fileManager = FileManager.default
        
        if !fileManager.fileExists(atPath: newDirectoryPath.path)
        {
            do
            {
                try fileManager.createDirectory(atPath: newDirectoryPath.path, withIntermediateDirectories: true, attributes: nil)
            }
            catch
            {
                return nil
            }
        }
        
        return newDirectoryPath
    }
    
    class func addFileToFolder(_ folder: String, fileName: String, fileData: Data) -> Bool
    {
        guard let folderDirectory = UtilityClass.getDocumentsFolder(withName: folder) else
        {
            return false
        }
        
        let fileManager = FileManager.default
        
        let newFilePath = folderDirectory.appendingPathComponent(fileName) //folderDirectory.appending("/"+fileName)
        
        let isSaved = fileManager.createFile(atPath: newFilePath.path, contents: fileData, attributes: nil)
        
        return isSaved
    }
    
    class func getFileURLFromFolder(_ folder: String, fileName: String) -> URL?
    {
        guard let folderDirectory = UtilityClass.getDocumentsFolder(withName: folder) else
        {
            return nil
        }
        
        let fileManager = FileManager.default
        
        let newFilePath = folderDirectory.appendingPathComponent(fileName)
        
        if fileManager.fileExists(atPath: newFilePath.path)
        {
            return newFilePath
        }
        
        return nil
    }
    
    //MARK: Save Notification Status ( Save )
    class func saveNotificationStatus(userDict : Bool) -> Void
    {
        let data = NSKeyedArchiver.archivedData(withRootObject: userDict)
        userDefaults.set(data, forKey: UserDefaultEnum.notificationStatus.rawValue)
    }
    
    //MARK: Get Notification Status ( GET )
    class func getNotificationStatus() -> Bool?
    {
        guard let data = userDefaults.object(forKey: UserDefaultEnum.notificationStatus.rawValue) as? Data else
        {
            return nil
        }
        
        let userDict : Bool = NSKeyedUnarchiver.unarchiveObject(with: data) as! Bool
        
        return userDict
    }
    
    //MARK: Save APNS Device Token Status ( Save )
    class func saveDeviceAPNSToken(userDict : String) -> Void
    {
        let data = NSKeyedArchiver.archivedData(withRootObject: userDict)
        userDefaults.set(data, forKey: UserDefaultEnum.apnsDeviceToken.rawValue)
    }
    
    //MARK: Get APNS Device Token Status ( GET )
    class func getDeviceAPNSToken() -> String?
    {
        guard let data = userDefaults.object(forKey: UserDefaultEnum.apnsDeviceToken.rawValue) as? Data else
        {
            return ""
        }
        
        let userDict : String = NSKeyedUnarchiver.unarchiveObject(with: data) as! String
        
        return userDict
    }
    
    //MARK: Save Notification Status ( Save )
    class func saveDeviceFCMToken(userDict : String) -> Void
    {
        let data = NSKeyedArchiver.archivedData(withRootObject: userDict)
        userDefaults.set(data, forKey: UserDefaultEnum.fcmDeviceToken.rawValue)
    }
    
    //MARK: Get Notification Status ( GET )
    class func getDeviceFCMToken() -> String?
    {
        guard let data = userDefaults.object(forKey: UserDefaultEnum.fcmDeviceToken.rawValue) as? Data else
        {
            return ""
        }
        
        let userDict : String = NSKeyedUnarchiver.unarchiveObject(with: data) as! String
        
        return userDict
    }
    
    //MARK: Save chat unread count
    class func saveChatUnReadCount(count : Int) -> Void
    {
        userDefaults.set(count, forKey: UserDefaultEnum.chatUnReadCount.rawValue)
    }
    
    //MARK: Get chat unread count ( GET )
    class func getChatUnReadCount() -> Int?
    {
        guard let count = userDefaults.object(forKey: UserDefaultEnum.chatUnReadCount.rawValue) as? Int else
        {
            return 0
        }
        
        return count
    }
    
    class func stringEncode(_ s: String) -> String
    {
        //let utf8str = s.data(using: String.Encoding.utf8)
        //
        //if let base64Encoded = utf8str?.base64EncodedData(options: NSData.Base64EncodingOptions(rawValue: 0))
        //{
        //    print("Encoded:  \(base64Encoded)")
        //
        //    if let base64Decoded =  NSData(base64Encoded: base64Encoded, options: NSData.Base64DecodingOptions(rawValue: 0)).map({ (NSString(data: $0 as Data, encoding: String.Encoding.utf8.rawValue))})
        //    {
        //        // Convert back to a string
        //        print("Decoded:  \(base64Decoded ?? "")")
        //        return base64Decoded! as String
        //    }
        //}
        //
        //return ""
        
        let data = s.data(using: .nonLossyASCII, allowLossyConversion: true)!
        return String(data: data, encoding: .utf8)!
    }
    
    class func stringDecode(_ s: String) -> String?
    {
        //let decodedData = Data(base64Encoded: s)
        //let decodedString = String(data: decodedData!, encoding: .utf8)!
        //
        //print("decoded string = ", decodedString)
        //
        //return decodedString
        
        let data = s.data(using: .utf8)!
        return String(data: data, encoding: .nonLossyASCII)
    }
    
    class func callStatusCodeLogout()
    {
        // delete user data and model data
        UtilityClass.deleteUserData()
        SocketManagerHandler.sharedInstance().disconnectSocket()
        
        let loginVC = UIStoryboard.getWelcomeStoryBoard().instantiateInitialViewController()
        
        let navigationController = UINavigationController.init(rootViewController: loginVC!)
        
        self.changeRootViewController(with: navigationController)
    }
}
